macro(MMP_MSVC_AUTO_CONFIG)
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /source-charset:utf-8")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /source-charset:utf-8")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /EHsc")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /EHsc")

    set(USE_SDL ON)
    set(USE_OPENGL ON)
    set(USE_D3D ON)
    set(USE_VULKAN ON)

    # Hint : disable poco test
    set(ENABLE_TESTS OFF)

endmacro()