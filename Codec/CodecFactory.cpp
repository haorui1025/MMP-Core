#include "CodecFactory.h"

#include <Poco/SingletonHolder.h>

#include "PNG/PNGEncoder.h"
#include "PNG/PNGDecorder.h"

#if defined(USE_VAAPI)
    #include "VAAPI/VAH264Decoder.h"
#endif /* USE_VAAPI */

#if defined(USE_OPENH264)
    #include "openh264/OpenH264Decoder.h"
#endif /* USE_OPENH264 */

#if defined(USE_ROCKCHIP)
    #include "Rockchip/RKH264Decoder.h"
    #include "Rockchip/RKH265Decoder.h"
    #include "Rockchip/RKH264Encoder.h"
    #include "Rockchip/RKH265Encoder.h"
#endif /* USE_ROCKCHIP */

#if defined(USE_D3D)
    #include "D3D11/D3D11H264Decoder.h"
#endif /* USE_D3D */

#if defined(USE_D3D)
    #include "D3DVA/D3DVAH264Decoder.h"
    #include "D3DVA/D3DVAH264Encoder.h"
#endif /* USE_D3D */

#if defined(USE_VULKAN)
    #include "Common/VulkanDevice.h"
    #include "Vulkan/VulkanH264Decoder.h"
#endif /* USE_VULKAN */

namespace Mmp
{
namespace Codec
{

namespace
{
    static Poco::SingletonHolder<EncoderFactory> esh;
}

EncoderFactory::EncoderFactory()
{
    RegisterBuiltins();
}

EncoderFactory::~EncoderFactory()
{

}

void EncoderFactory::RegisterEncoderClass(const std::string& className, EncoderInstantiator* instantiator, const CodecDescription& description)
{
    std::lock_guard<std::mutex> lock(_mutex);
    _encoderFactory.registerClass(className, instantiator);
    _encoderDescriptions.push_back(description);
}

std::vector<CodecDescription> EncoderFactory::GetEncoderDescriptions()
{
    std::lock_guard<std::mutex> lock(_mutex);
    return _encoderDescriptions;
}

AbstractEncoder::ptr EncoderFactory::CreateEncoder(const std::string& className)
{
    std::lock_guard<std::mutex> lock(_mutex);
    return _encoderFactory.CreateInstance(className);
}

void EncoderFactory::RegisterBuiltins()
{
    std::lock_guard<std::mutex> lock(_mutex);
    {
        _encoderFactory.registerClass("PngEncoder", new Instantiator<PngEncoder, AbstractEncoder>);
        _encoderDescriptions.push_back(CodecDescription(CodecType::PNG, CodecProcessType::Software, CodecVendorType::LODEPNG, "PngEncoder"));
    }
    
#if defined(USE_ROCKCHIP)
    {
        _encoderFactory.registerClass("RKH264Encoder", new Instantiator<RKH264Encoder, AbstractEncoder>);
        _encoderDescriptions.push_back(CodecDescription(CodecType::H264, CodecProcessType::Hardware, CodecVendorType::ROCKCHIP, "RKH264Encoder"));
    }
    {
        _encoderFactory.registerClass("RKH265Encoder", new Instantiator<RKH265Encoder, AbstractEncoder>);
        _encoderDescriptions.push_back(CodecDescription(CodecType::H265, CodecProcessType::Hardware, CodecVendorType::ROCKCHIP, "RKH265Encoder"));
    }
#endif /* USE_ROCKCHIP */
#if defined(USE_D3D)
    {
        _encoderFactory.registerClass("D3DVAH264Encoder", new Instantiator<D3DVAH264Encoder, AbstractEncoder>);
        _encoderDescriptions.push_back(CodecDescription(CodecType::H264, CodecProcessType::Hardware, CodecVendorType::D3DVA, "D3DVAH264Encoder"));
    }
#endif 
}

EncoderFactory& EncoderFactory::DefaultFactory()
{
    return *esh.get();
}

} // namespace Codec
} // namespace Mmp

namespace Mmp
{
namespace Codec
{

namespace
{
    static Poco::SingletonHolder<DecoderFactory> dsh;
}

DecoderFactory::DecoderFactory()
{
    RegisterBuiltins();
}

DecoderFactory::~DecoderFactory()
{
    
}

void DecoderFactory::RegisterDecoderClass(const std::string& className, DecoderInstantiator* instantiator, const CodecDescription& description)
{
    std::lock_guard<std::mutex> lock(_mutex);
    _decoderDescriptions.push_back(description);
    _decoderFactory.registerClass(className, instantiator);
}

std::vector<CodecDescription> DecoderFactory::GetDecoderDescriptions()
{
    std::lock_guard<std::mutex> lock(_mutex);
    return _decoderDescriptions;
}

AbstractDecoder::ptr DecoderFactory::CreateDecoder(const std::string& className)
{
    std::lock_guard<std::mutex> lock(_mutex);
    return _decoderFactory.CreateInstance(className);
}

void DecoderFactory::RegisterBuiltins()
{
    std::lock_guard<std::mutex> lock(_mutex);
    {
        _decoderFactory.registerClass("PngDecoder", new Instantiator<PngDecoder, AbstractDecoder>);
        _decoderDescriptions.push_back(CodecDescription(CodecType::PNG, CodecProcessType::Software, CodecVendorType::LODEPNG, "PngDecoder"));
    }
#if defined(USE_VAAPI)
    if (VADevice::Instance()->Create())
    {
        std::set<VAProfile>  profiles = VADevice::Instance()->GetSupportProfiles();
        bool supportH264 = false;
        for (auto& profile : profiles)
        {
            if (profile == VAProfileH264ConstrainedBaseline || profile == VAProfileH264Main || profile == VAProfileH264High)
            {
                supportH264 = true;
            }
        }
        if (supportH264)
        {
            _decoderFactory.registerClass("VAH264Decoder", new Instantiator<VAH264Decoder, AbstractDecoder>);
            _decoderDescriptions.push_back(CodecDescription(CodecType::H264, CodecProcessType::Hardware, CodecVendorType::LIBVA, "VAH264Decoder"));
        }
        VADevice::Instance()->Destroy();
    }
#endif /* USE_VAAPI */
#if defined(USE_OPENH264)
    {
        _decoderFactory.registerClass("OpenH264Decoder", new Instantiator<OpenH264Decoder, AbstractDecoder>);
        _decoderDescriptions.push_back(CodecDescription(CodecType::H264, CodecProcessType::Software, CodecVendorType::OPENH264, "OpenH264Decoder"));
    }
#endif /* USE_OPENH264 */
#if defined(USE_ROCKCHIP)
    {
        _decoderFactory.registerClass("RKH264Decoder", new Instantiator<RKH264Decoder, AbstractDecoder>);
        _decoderDescriptions.push_back(CodecDescription(CodecType::H264, CodecProcessType::Hardware, CodecVendorType::ROCKCHIP, "RKH264Decoder"));
    }
    {
        _decoderFactory.registerClass("RKH265Decoder", new Instantiator<RKH265Decoder, AbstractDecoder>);
        _decoderDescriptions.push_back(CodecDescription(CodecType::H265, CodecProcessType::Hardware, CodecVendorType::ROCKCHIP, "RKH265Decoder"));
    }
#endif /* USE_ROCKCHIP */
#if defined(USE_D3D)
    {
        _decoderFactory.registerClass("D3D11H264Decoder", new Instantiator<D3D11H264Decoder, AbstractDecoder>);
        _decoderDescriptions.push_back(CodecDescription(CodecType::H264, CodecProcessType::Hardware, CodecVendorType::D3D11, "D3D11H264Decoder"));
    }
#endif 
#if defined(USE_D3D)
    {
        _decoderFactory.registerClass("D3DVAH264Decoder", new Instantiator<D3DVAH264Decoder, AbstractDecoder>);
        _decoderDescriptions.push_back(CodecDescription(CodecType::H264, CodecProcessType::Hardware, CodecVendorType::D3DVA, "D3DVAH264Decoder"));
    }
#endif 
#if defined(USE_VULKAN)
    if (VulkanDevice::Singleton()->GetVulkanExtensions().KHR_VIDEO_DECODE_QUEUE_EXTENSION_NAME)
    {
        if (VulkanDevice::Singleton()->GetVulkanExtensions().KHR_VIDEO_DECODE_H264_EXTENSION_NAME)
        {
            _decoderFactory.registerClass("VulkanH264Decoder", new Instantiator<VulkanH264Decoder, AbstractDecoder>);
            _decoderDescriptions.push_back(CodecDescription(CodecType::H264, CodecProcessType::Hardware, CodecVendorType::VULKAN, "VulkanH264Decoder"));
        }
    }
#endif
}

DecoderFactory& DecoderFactory::DefaultFactory()
{
    return *dsh.get();
}

} // namespace Codec
} // namespace Mmp