#include "CodecUtil.h"


namespace Mmp
{
namespace Codec
{

QuantRange H264VuiSyntaxToQuantRange(H264VuiSyntax::ptr vui)
{
    if (vui)
    {
        if (vui->video_full_range_flag)
        {
            return QuantRange::FULL;
        }
        else
        {
            return QuantRange::LIMIT;
        }
    }
    else
    {
        return QuantRange::UNSPECIFIED;
    }
}

ColorGamut H264VuiSyntaxToColorGamut(H264VuiSyntax::ptr vui)
{
    if (vui)
    {
        if (vui->colour_primaries == vui->transfer_characteristics && vui->transfer_characteristics == vui->matrix_coefficients)
        {
            switch (vui->colour_primaries) 
            {
                case 1: return ColorGamut_BT709;
                case 5: /* pass through */
                case 6: return ColorGamut_BT601;
                default:
                    return ColorGamut_UNSPECIFIED;
            }
        }
        else
        {
            return ColorGamut_UNSPECIFIED;
        }
    }
    else
    {
        return ColorGamut_UNSPECIFIED;
    }
}

} // namespace Codec
} // namespace Mmp
