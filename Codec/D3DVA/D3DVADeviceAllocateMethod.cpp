#include "D3DVADeviceAllocateMethod.h"

namespace Mmp
{
namespace Codec
{

D3DVADeviceAllocateMethod::D3DVADeviceAllocateMethod()
{

}

D3DVADeviceAllocateMethod::~D3DVADeviceAllocateMethod()
{
#if ENABLE_CODEC_D3DVA_TRACE_PROC
    if (srcFrame)
    {
        D3DVA_LOG_INFO << "D3DVADeviceAllocateMethod, frame track id is: " << srcFrame->trackId;
    }
#endif
}

void* D3DVADeviceAllocateMethod::Malloc(size_t size)
{
    // Hint : 延迟获取, 等到 GetAddress 再进行尝试
    return nullptr;
}

void* D3DVADeviceAllocateMethod::Resize(void* data, size_t size)
{
    assert(false);
    return nullptr;
}

void* D3DVADeviceAllocateMethod::GetAddress(uint64_t offset)
{
    if (!alloc)
    {
        alloc = decoderContext->TransferFrame(srcFrame);
    }
    if (alloc)
    {
        return alloc->GetAddress(offset);
    }
    else
    {
        return nullptr;
    }
}

const std::string& D3DVADeviceAllocateMethod::Tag()
{
    static std::string tag = "D3DVADeviceAllocateMethod";
    return tag;
}

} // namespace Codec
} // namespace Mmp