#include "D3DVADecoderContext.h"

#include "D3DVATranslator.h"

#include <atomic>

namespace Mmp
{
namespace Codec
{

static std::atomic<uint64_t> gCount(0);

#define MMP_D3DVA_TEXTURE_ALIGN(x)  ((x+D3D12_TEXTURE_DATA_PITCH_ALIGNMENT-1) & ~(D3D12_TEXTURE_DATA_PITCH_ALIGNMENT-1))

D3DVADecoderContext::D3DVADecoderContext()
{
    _index = gCount++;
    _tagPrefix = "VDC_" + std::to_string(_index) +  "_";

    _decoderHeap = nullptr;
    _commandQueue = nullptr;
    _decoder = nullptr;
    _commandList = nullptr;
    
    _stagingCommandList = nullptr;
    _stagingCommandQueue = nullptr;
    _stagingAllocator = nullptr;
    _stagingDownloadBuffer = nullptr;

    _format = DXGI_FORMAT::DXGI_FORMAT_UNKNOWN;
    _codecType = CodecType::H264;
    _width = 0;
    _height = 0;

    _bitStramSize = 0;
    _bufTrackId = 0;

    _bitRate = 0;
    _frameRateNumerator = 30; // to determine
    _frameRateDenominator = 1;

    _flags = D3D12_RESOURCE_FLAG_NONE;

    memset(&_config, 0, sizeof(D3D12_VIDEO_DECODE_CONFIGURATION));
}

D3DVADecoderContext::~D3DVADecoderContext()
{
    if (_fence && _commandQueue)
    {
        FenceSync(_commandQueue, _fence);
    }

    D3DVA_OBJECT_RELEASE(_decoderHeap);
    D3DVA_OBJECT_RELEASE(_commandList);
    D3DVA_OBJECT_RELEASE(_commandQueue);
    D3DVA_OBJECT_RELEASE(_decoder);

    D3DVA_OBJECT_RELEASE(_stagingCommandList);
    D3DVA_OBJECT_RELEASE(_stagingCommandQueue);
    D3DVA_OBJECT_RELEASE(_stagingDownloadBuffer);
    D3DVA_OBJECT_RELEASE(_stagingAllocator);
}

bool D3DVADecoderContext::Init(const D3DVADecoderParams& params)
{
    HRESULT hr = 0;
    D3DVACommandBundle::ptr bundle = nullptr;

    InitParams(params);
    _config.DecodeProfile = CodecTypeToD3D12VIDEODECODEPROFILE(_codecType);
    _maxDecodePictureBufferCount = (uint32_t)params.maxRefNum;
    _refResources.resize(_maxDecodePictureBufferCount);
    _refSubresources.resize(_maxDecodePictureBufferCount);
    // Check Feature
    {
        D3D12_FEATURE_DATA_VIDEO_DECODE_SUPPORT feature = {};
        {
            feature.NodeIndex = 0;
            feature.Configuration = _config;
            feature.Width = _width;
            feature.Height = _height;
            feature.DecodeFormat = _format;
            feature.FrameRate.Numerator = _frameRateNumerator;
            feature.FrameRate.Denominator = _frameRateDenominator;
            feature.BitRate = _bitRate;
        }
        hr = D3DDevice::Singleton()->CheckFeatureSupport(D3D12_FEATURE_VIDEO_DECODE_SUPPORT, &feature, sizeof(D3D12_FEATURE_DATA_VIDEO_DECODE_SUPPORT));
        if (D3DVA_FAILED(hr))
        {
            D3DVA_LOG_ERROR << "ID3D12VideoDevice::CheckFeatureSupport fail";
            assert(false);
            return false;
        }
        if (!(feature.SupportFlags & D3D12_VIDEO_DECODE_SUPPORT_FLAG_SUPPORTED))
        {
            D3DVA_LOG_ERROR << "Not support D3D12_VIDEO_DECODE_SUPPORT_FLAG_SUPPORTED";
            assert(false);
            return false;
        }
        if (!(feature.DecodeTier >= D3D12_VIDEO_DECODE_TIER_2))
        {
            D3DVA_LOG_ERROR << "Requires D3D12_VIDEO_DECODE_TIER_2 support";
            assert(false);
            return false;
        }
    }
    // ID3D12VideoDecoder
    {
        D3D12_VIDEO_DECODER_DESC desc = {};
        {
            desc.NodeMask = 0;
            desc.Configuration = _config;
        }
        hr = D3DDevice::Singleton()->CreateVideoDecoder(&desc, IID_ID3D12VideoDecoder, (void**)&_decoder);
        if (D3DVA_FAILED(hr))
        {
            D3DVA_LOG_ERROR << "ID3D12VideoDevice::CreateVideoDecoder fail";
            assert(false);
            return false;
        }
    }
    // D3DVAFenceContext
    {
        _fence = CreateD3DVAFenceContext();
        if (!_fence)
        {
            assert(false);
            return false;
        }
        _fence->tag = "VDC_Fence";
    }
    // ID3D12CommandQueue and ID3D12VideoDecodeCommandList
    {
        D3D12_COMMAND_QUEUE_DESC desc = {};
        {
            desc.Type     = D3D12_COMMAND_LIST_TYPE_VIDEO_DECODE;
            desc.Priority = 0;
            desc.Flags    = D3D12_COMMAND_QUEUE_FLAG_NONE;
            desc.NodeMask = 0;
        }
        hr = D3DDevice::Singleton()->CreateCommandQueue(&desc, IID_ID3D12CommandQueue, (void **)&_commandQueue);
        if (D3DVA_FAILED(hr))
        {
            D3DVA_LOG_ERROR << "ID3D12Device::CreateCommandQueue fail";
            assert(false);
            return false;
        }
        bundle = GetD3DVACommandBundle();
        if (!bundle)
        {
            D3DVA_LOG_ERROR << "GetD3DVACommandBundle fail";
            assert(false);
            return false;
        }
        hr = D3DDevice::Singleton()->CreateCommandList(0, desc.Type, bundle->allocator, nullptr, IID_ID3D12CommandList, (void **)&_commandList);
        if (D3DVA_FAILED(hr))
        {
            D3DVA_LOG_ERROR << "ID3D12Device::CreateCommandList fail";
            assert(false);
            return false;
        }
        hr = _commandList->Close();
        if (D3DVA_FAILED(hr))
        {
            D3DVA_LOG_ERROR << "ID3D12Device::Close fail";
            assert(false);
            return false;
        }
        _commandQueue->ExecuteCommandLists(1, (ID3D12CommandList**)&_commandList);
    }
    if (!FenceSync(_commandQueue, _fence))
    {
        D3DVA_LOG_ERROR << "FenceSync fail";
        assert(false);
        return false;
    }
    bundle->fenceValue = _fence->GetFenceValue();
    return true;
}

ID3D12VideoDecoderHeap* D3DVADecoderContext::GetDecoderHeap()
{
    if (!_decoderHeap)
    {
        D3D12_VIDEO_DECODER_HEAP_DESC desc = {};
        {
            desc.Configuration = _config;
            desc.DecodeWidth = _width;
            desc.DecodeHeight = _height;
            desc.FrameRate.Numerator = _frameRateNumerator;
            desc.FrameRate.Denominator = _frameRateDenominator;
            desc.MaxDecodePictureBufferCount = _maxDecodePictureBufferCount;
            desc.Format = _format;
            desc.BitRate = _bitRate;
        }
        HRESULT hr = D3DDevice::Singleton()->CreateVideoDecoderHeap(&desc, IID_ID3D12VideoDecoderHeap, (void **)&_decoderHeap);
        if (D3DVA_FAILED(hr))
        {
            D3DVA_LOG_ERROR << "ID3D12VideoDevice:: CreateVideoDecoderHeap fail";
            assert(false);
        }
    }
    return _decoderHeap;
}

bool D3DVADecoderContext::DecodeFrame(D3D12_VIDEO_DECODE_INPUT_STREAM_ARGUMENTS& inputStreamArguments, D3D12_VIDEO_DECODE_OUTPUT_STREAM_ARGUMENTS& outputStreamArguments, D3DVAVideoFrame::ptr frame, D3DVACommandBundle::ptr bundle)
{
    std::lock_guard<std::mutex> lock(_mtx);
    HRESULT hr = 0;
    D3D12_RESOURCE_BARRIER barriers[32] = { };
    uint32_t barrierNum = 0;
    {
        inputStreamArguments.ReferenceFrames.NumTexture2Ds = (UINT)_maxDecodePictureBufferCount;
        inputStreamArguments.ReferenceFrames.ppTexture2Ds = _refResources.data();
        inputStreamArguments.ReferenceFrames.pSubresources = _refSubresources.data();
        inputStreamArguments.pHeap = GetDecoderHeap();    
        if (!inputStreamArguments.pHeap)
        {
            D3DVA_LOG_ERROR << "GetDecoderHeap fail";
            assert(false);
            return false;
        }
    }
    // For barriers
    {
        {
            D3D12_RESOURCE_BARRIER& barrier = barriers[0];
            barrier.Type  = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
            barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
            {
                barrier.Transition.pResource   = outputStreamArguments.pOutputTexture2D;
                barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
                barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COMMON;
                barrier.Transition.StateAfter  = D3D12_RESOURCE_STATE_VIDEO_DECODE_WRITE;
            }
            barrierNum++;
        }
        ID3D12Resource** ppTexture2Ds = inputStreamArguments.ReferenceFrames.ppTexture2Ds;
        for (size_t i=0; i<inputStreamArguments.ReferenceFrames.NumTexture2Ds; i++)
        {
            if (inputStreamArguments.ReferenceFrames.ppTexture2Ds[i])
            {
                D3D12_RESOURCE_BARRIER& barrier = barriers[barrierNum];
                barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
                barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
                {
                    barrier.Transition.pResource = ppTexture2Ds[i];
                    barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
                    barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COMMON;
                    barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_VIDEO_DECODE_READ;
                }
                barrierNum++;
            }
        }
    }
    hr = bundle->allocator->Reset();
    if (D3DVA_FAILED(hr))
    {
        D3DVA_LOG_ERROR << "ID3D12CommandAllocator::Reset fail";
        assert(false);
        return false;
    }
    hr = _commandList->Reset(bundle->allocator);
    if (D3DVA_FAILED(hr))
    {
        D3DVA_LOG_ERROR << "ID3D12VideoDecodeCommandList::Reset fail";
        assert(false);
        return false;
    }
    _commandList->ResourceBarrier(barrierNum, barriers);
    _commandList->DecodeFrame(_decoder, &outputStreamArguments, &inputStreamArguments);
    // Swap
    for (uint32_t i=0; i<barrierNum; i++)
    {
        D3D12_RESOURCE_STATES StateBefore = barriers[i].Transition.StateBefore;
        barriers[i].Transition.StateBefore = barriers[i].Transition.StateAfter;
        barriers[i].Transition.StateAfter = StateBefore;
    }
    _commandList->ResourceBarrier(barrierNum, barriers);
    hr = _commandList->Close();
    if (D3DVA_FAILED(hr))
    {
        D3DVA_LOG_ERROR << "ID3D12VideoDecodeCommandList::Close fail";
        assert(false);
        return false;
    }
    _commandQueue->ExecuteCommandLists(1, (ID3D12CommandList**)&_commandList);
    hr = frame->sync->Signal(_commandQueue);
    if (D3DVA_FAILED(hr))
    {
        D3DVA_LOG_ERROR << "ID3D12CommandQueue::Signal fail";
        assert(false);
        return false;
    }
    hr = _fence->Signal(_commandQueue);
    if (D3DVA_FAILED(hr))
    {
        D3DVA_LOG_ERROR << "ID3D12CommandQueue::Signal fail";
        assert(false);
        return false;
    }
    bundle->fenceValue = _fence->fenceValue;
    return true;
}

NormalAllocateMethod::ptr D3DVADecoderContext::TransferFrame(D3DVAVideoFrame::ptr frame)
{

#if ENABLE_CODEC_D3DVA_TRACE_PROC
    D3DVA_LOG_INFO << "TransferFrame, trackId is: " << frame->trackId;
#endif

    D3D12_TEXTURE_COPY_LOCATION yTextureLocation = {}, uvTextureLocation = {};
    D3D12_TEXTURE_COPY_LOCATION yStaingLocation = {}, uvStaingLocation = {};
    D3D12_RESOURCE_BARRIER barrier = {};
    NormalAllocateMethod::ptr alloc;
    bool copySuccess = false;
    {
        // yTextureLocation and uvTextureLocation
        {
            yTextureLocation.pResource = frame->texture;
            yTextureLocation.Type = D3D12_TEXTURE_COPY_TYPE_SUBRESOURCE_INDEX;
            yTextureLocation.SubresourceIndex = 0;
            uvTextureLocation.pResource = frame->texture;
            uvTextureLocation.Type = D3D12_TEXTURE_COPY_TYPE_SUBRESOURCE_INDEX;
            uvTextureLocation.SubresourceIndex = 1;
        }
        // yStaingLocation and uvStaingLocation
        {
            yStaingLocation.Type = D3D12_TEXTURE_COPY_TYPE_PLACED_FOOTPRINT;
            yStaingLocation.PlacedFootprint.Offset = 0;
            yStaingLocation.PlacedFootprint.Footprint.Format = DXGI_FORMAT_R8_UNORM;
            yStaingLocation.PlacedFootprint.Footprint.Width = _width;
            yStaingLocation.PlacedFootprint.Footprint.Height = _height;
            yStaingLocation.PlacedFootprint.Footprint.Depth = 1;
            yStaingLocation.PlacedFootprint.Footprint.RowPitch = MMP_D3DVA_TEXTURE_ALIGN(_width);
            uvStaingLocation.Type = D3D12_TEXTURE_COPY_TYPE_PLACED_FOOTPRINT;
            uvStaingLocation.PlacedFootprint.Offset = MMP_D3DVA_TEXTURE_ALIGN(_width) * _height;
            uvStaingLocation.PlacedFootprint.Footprint.Format = DXGI_FORMAT_R8G8_UNORM;
            uvStaingLocation.PlacedFootprint.Footprint.Width = _width >> 1;
            uvStaingLocation.PlacedFootprint.Footprint.Height = _height >> 1;
            uvStaingLocation.PlacedFootprint.Footprint.Depth = 1;
            uvStaingLocation.PlacedFootprint.Footprint.RowPitch = MMP_D3DVA_TEXTURE_ALIGN(_width);
        }
        // barrier
        {
            barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
            barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
            barrier.Transition.pResource = frame->texture;
            barrier.Transition.StateBefore = D3D12_RESOURCE_STATE_COMMON;
            barrier.Transition.StateAfter = D3D12_RESOURCE_STATE_COPY_SOURCE;
            barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
        }
    }
    {
        std::lock_guard<std::mutex> lock(_stagingMtx);
        ID3D12Resource* stagingBuuffer = GetStagingDownloadBuffer();
        {
            HRESULT hr = 0;
            ID3D12CommandQueue* commandQueue = GetStagingCommandQueue();
            ID3D12CommandAllocator* allocater = GetStagingAllocator();
            ID3D12GraphicsCommandList* commandList = GetStagingCommandList();
            D3DVAFenceContext::ptr stagingFence = _stagingFence;
            if (!(commandList && stagingBuuffer && allocater && stagingFence))
            {
                assert(false);
                return alloc;
            }
            {
                yStaingLocation.pResource = stagingBuuffer;
                uvStaingLocation.pResource = stagingBuuffer;
            }
            hr = allocater->Reset();
            if (D3DVA_FAILED(hr))
            {
                D3DVA_LOG_ERROR << "ID3D12CommandAllocator::Reset fail";
                assert(false);
                return alloc;
            }
            hr = commandList->Reset(allocater, nullptr);
            if (D3DVA_FAILED(hr))
            {
                D3DVA_LOG_ERROR << "ID3D12GraphicsCommandList::Reset fail";
                assert(false);
                return alloc;
            }
            commandList->ResourceBarrier(1, &barrier);
            commandList->CopyTextureRegion(&yStaingLocation, 0, 0, 0, &yTextureLocation, nullptr);
            commandList->CopyTextureRegion(&uvStaingLocation, 0, 0, 0, &uvTextureLocation, nullptr);
            barrier.Transition.StateBefore = barrier.Transition.StateAfter;
            barrier.Transition.StateAfter  = D3D12_RESOURCE_STATE_COMMON;
            commandList->ResourceBarrier(1, &barrier);
            hr = commandList->Close();
            if (D3DVA_FAILED(hr))
            {
                D3DVA_LOG_ERROR << "ID3D12VideoDecodeCommandList::Close fail";
                assert(false);
                return alloc;
            }
            hr = frame->sync->Wait(commandQueue);
            if (D3DVA_FAILED(hr))
            {
                D3DVA_LOG_ERROR << "ID3D12CommandQueue::Wait fail";
                assert(false);
                return alloc;
            }
            commandQueue->ExecuteCommandLists(1, (ID3D12CommandList **)&commandList);
            if (!FenceSync(commandQueue, stagingFence))
            {
                return alloc;
            }
            copySuccess = true;
        }
        if (stagingBuuffer && copySuccess)
        {
            uint8_t* srcAddress = nullptr;
            uint8_t* dstAddress = nullptr;
            HRESULT hr = 0;
            hr =  stagingBuuffer->Map(0, nullptr, (void **)&srcAddress);
            if (D3DVA_FAILED(hr) || !srcAddress)
            {
                D3DVA_LOG_ERROR << "ID3D12Resource::Map fail";
                assert(false);
                return alloc;
            }
            size_t size = _width * _height * 3 / 2;
            alloc = std::make_shared<NormalAllocateMethod>();
            dstAddress = (uint8_t*)alloc->Malloc(size);
            if (!dstAddress)
            {
                return nullptr;
            }
            if (MMP_D3DVA_TEXTURE_ALIGN(_width) == _width)
            {
                memcpy(dstAddress, srcAddress, size);
            }
            else
            {
                for (size_t i=0; i<_height*3/2; i++)
                {
                    memcpy(dstAddress, srcAddress, _width);
                    srcAddress += MMP_D3DVA_TEXTURE_ALIGN(_width);
                    dstAddress += _width;
                }
            }
            stagingBuuffer->Unmap(0, nullptr);
        }
    }
    return alloc;
}

void D3DVADecoderContext::UpdateReferenceFrames(const std::vector<ID3D12Resource*>& refResources, std::vector<UINT> refSubresources)
{
    _refResources = refResources;
    _refSubresources = refSubresources;
    _refResources.resize(_maxDecodePictureBufferCount);
    _refSubresources.resize(_maxDecodePictureBufferCount);
}

bool D3DVADecoderContext::SyncFence(D3DVAFenceContext::ptr fence)
{
    return fence->Wait();
}

D3DVACommandBundle::ptr D3DVADecoderContext::GetD3DVACommandBundle()
{
    std::lock_guard<std::mutex> lock(_mtx);
    D3DVACommandBundle::ptr bundle;
    for (auto& __bundle : _bundles)
    {
        uint64_t completion = _fence->GetCompletedValue();
        if (completion > __bundle->fenceValue)
        {
            // D3DVA_LOG_INFO << "GetD3DVACommandBundle completion(" << completion << ") fenceValue(" << __bundle->fenceValue <<  ")";
            bundle = __bundle;
            break;
        }
    }
    if (!bundle)
    {
        bundle = CreateD3DVACommandBundle();
        _bundles.push_back(bundle);
    }
    while (_bundles.size() > 10)
    {
        // Hint : 一般 _bundles 大小应当为 2, 对应双缓冲
        assert(false);
        _bundles.pop_front();
    }
    return bundle;
}

D3DVACommandBundle::ptr D3DVADecoderContext::CreateD3DVACommandBundle()
{
    HRESULT hr = 0;
    D3DVACommandBundle::ptr bundle = std::make_shared<D3DVACommandBundle>();
    // ID3D12CommandAllocator
    {
        hr = D3DDevice::Singleton()->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_VIDEO_DECODE, IID_ID3D12CommandAllocator, (void **)&(bundle->allocator));
        if (D3DVA_FAILED(hr))
        {
            D3DVA_LOG_ERROR << "ID3D12Device::CreateCommandAllocator fail";
            return nullptr;
        }
    }
    // ID3D12Resource
    {
        D3D12_HEAP_PROPERTIES heapProps = {};
        D3D12_RESOURCE_DESC desc = {};
        {
            heapProps.Type           = D3D12_HEAP_TYPE_UPLOAD;
            desc.Dimension           = D3D12_RESOURCE_DIMENSION_BUFFER;
            desc.Alignment           = D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT;
            desc.Width               = _bitStramSize;
            desc.Height              = 1;
            desc.DepthOrArraySize    = 1;
            desc.MipLevels           = 1;
            desc.Format              = DXGI_FORMAT_UNKNOWN;
            desc.SampleDesc.Count    = 1;
            desc.SampleDesc.Quality  = 0;
            desc.Layout              = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
            desc.Flags               = D3D12_RESOURCE_FLAG_NONE;
        }
        hr = D3DDevice::Singleton()->CreateCommittedResource(&heapProps, D3D12_HEAP_FLAG_NONE, &desc, D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_ID3D12Resource, (void**)&(bundle->buffer));
        if (D3DVA_FAILED(hr))
        {
            D3DVA_LOG_ERROR << "ID3D12Device::CreateCommittedResource fail";
            return nullptr;
        }
    }
    return bundle;
}

ID3D12Resource* D3DVADecoderContext::GetStagingDownloadBuffer()
{
    if (!_stagingDownloadBuffer)
    {
        HRESULT hr = 0;
        D3D12_HEAP_PROPERTIES heapProps = {};
        {
            heapProps.Type = D3D12_HEAP_TYPE_READBACK;
        }
        D3D12_RESOURCE_DESC desc = {};
        {
            size_t size = 0;
            switch (_format)
            {
                case DXGI_FORMAT_NV12:
                {
                    size = MMP_D3DVA_TEXTURE_ALIGN(_width) * _height * 3 / 2;
                    break;
                }
                default:
                {
                    assert(false);
                    return nullptr;
                }
            }
            desc.Dimension = D3D12_RESOURCE_DIMENSION_BUFFER;
            desc.Alignment = 0;
            desc.Width = size;
            desc.Height = 1;
            desc.DepthOrArraySize = 1;
            desc.MipLevels = 1;
            desc.Format = DXGI_FORMAT_UNKNOWN;
            desc.SampleDesc.Count = 1;
            desc.SampleDesc.Quality = 0;
            desc.Layout = D3D12_TEXTURE_LAYOUT_ROW_MAJOR;
            desc.Flags = D3D12_RESOURCE_FLAG_NONE;
        }
        hr = D3DDevice::Singleton()->CreateCommittedResource(&heapProps, D3D12_HEAP_FLAG_NONE, &desc, D3D12_RESOURCE_STATE_COPY_DEST, nullptr, IID_ID3D12Resource, (void**)&_stagingDownloadBuffer);
        if (D3DVA_FAILED(hr))
        {
            D3DVA_LOG_ERROR << "ID3D12Device::CreateCommittedResource fail";
            assert(false);
            return nullptr;
        }
    }
    return _stagingDownloadBuffer;
}

ID3D12CommandQueue* D3DVADecoderContext::GetStagingCommandQueue()
{
    if (!_stagingCommandQueue)
    {
        HRESULT hr = 0;
        D3D12_COMMAND_QUEUE_DESC desc = {};
        {
            desc.Type = D3D12_COMMAND_LIST_TYPE_COPY;
            desc.Priority = 0;
            desc.NodeMask = 0;
        }
        hr = D3DDevice::Singleton()->CreateCommandQueue(&desc, IID_ID3D12CommandQueue, (void**)&_stagingCommandQueue);
        if (D3DVA_FAILED(hr))
        {
            D3DVA_LOG_ERROR << "ID3D12Device::CreateCommandQueue fail";
            _stagingCommandQueue = nullptr;
            assert(false);
        }
    }
    return _stagingCommandQueue;
}

ID3D12CommandAllocator* D3DVADecoderContext::GetStagingAllocator()
{
    if (!_stagingAllocator)
    {
        HRESULT hr = 0;
        hr = D3DDevice::Singleton()->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_COPY, IID_ID3D12CommandAllocator, (void**)&_stagingAllocator);
        if (D3DVA_FAILED(hr))
        {
            D3DVA_LOG_ERROR << "ID3D12Device::CreateCommandAllocator fail";
            _stagingAllocator = nullptr;
            assert(false);
        }
    }
    return _stagingAllocator;
}

ID3D12GraphicsCommandList* D3DVADecoderContext::GetStagingCommandList()
{
    if (!_stagingCommandList)
    {
        auto stagingCommandQueue = GetStagingCommandQueue();
        auto stagingAllocator = GetStagingAllocator();
        if (!_stagingFence)
        {
            _stagingFence = CreateD3DVAFenceContext();
            _stagingFence->tag = _tagPrefix + "Staging";
        }
        if (stagingAllocator && stagingCommandQueue && _stagingFence)
        {
            HRESULT hr = 0;
            hr = D3DDevice::Singleton()->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_COPY, stagingAllocator, nullptr, IID_ID3D12GraphicsCommandList, (void**)&_stagingCommandList);
            if (D3DVA_FAILED(hr))
            {
                D3DVA_LOG_ERROR << "ID3D12Device::CreateCommandList fail";
                _stagingCommandList = nullptr;
                assert(false);
            }
            if (_stagingCommandList)
            {
                _stagingCommandList->Close();
                stagingCommandQueue->ExecuteCommandLists(1, (ID3D12CommandList**)&_stagingCommandList);
                FenceSync(stagingCommandQueue, _stagingFence);
            }
        }
        else
        {
            assert(false);
        }
    }
    return _stagingCommandList;
}

void D3DVADecoderContext::InitParams(const D3DVADecoderParams& params)
{
    _codecType = params.codecType;
    _width = params.width;
    _height = params.height;
    _format = PixelFormatToD3DVADecoderFormat(params.format);
    _bitStramSize = (size_t)(BytePerPixel(params.format) * params.virWidth * params.virHeight);
}

D3DVAFenceContext::ptr D3DVADecoderContext::CreateD3DVAFenceContext()
{
    HRESULT hr = 0;
    D3DVAFenceContext::ptr fence = std::make_shared<D3DVAFenceContext>();
    fence = std::make_shared<D3DVAFenceContext>();
    hr = D3DDevice::Singleton()->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_ID3D12Fence, (void**)&(fence->fence));
    if (D3DVA_FAILED(hr))
    {
        D3DVA_LOG_ERROR << "ID3D12Device::CreateFence fail";
        assert(false);
        return nullptr;
    }
    fence->event = CreateEvent(nullptr, false, false, nullptr);
    if (!fence->event)
    {
        D3DVA_LOG_ERROR << "CreateEvent fail";
        assert(false);
        return nullptr;
    }
    return fence;
}

D3DVAVideoFrame::ptr D3DVADecoderContext::CreateD3DVAVideoFrame()
{
    D3DVAVideoFrame::ptr frame;
    // Hint : Try cache first
    {
        {
            std::lock_guard<std::mutex> lock(_bufferMtx);
            if (!_buffers.empty())
            {
                frame = _buffers.back();
                _buffers.pop_back();
            }
        }
        if (frame)
        {
            return frame;
        }
    }
    frame = std::make_shared<D3DVAVideoFrame>();
    frame->trackId = _bufTrackId++;
    frame->sync = CreateD3DVAFenceContext();
    frame->sync->tag = _tagPrefix + "F_" + std::to_string(frame->trackId);
    if (!frame->sync)
    {
        return nullptr;
    }
    {
        HRESULT hr = 0;
        D3D12_HEAP_PROPERTIES props = {};
        D3D12_RESOURCE_DESC desc = {};
        {
            props.Type = D3D12_HEAP_TYPE_DEFAULT;
        }
        {
            desc.Dimension          = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
            desc.Alignment          = 0;
            desc.Width              = _width;
            desc.Height             = _height;
            desc.DepthOrArraySize   = 1;
            desc.MipLevels          = 1;
            desc.Format             = _format;
            desc.SampleDesc.Count   = 1;
            desc.SampleDesc.Quality = 0;
            desc.Layout             = D3D12_TEXTURE_LAYOUT_UNKNOWN;
            desc.Flags              = _flags;
        }
        hr = D3DDevice::Singleton()->CreateCommittedResource(&props, D3D12_HEAP_FLAG_NONE, &desc, D3D12_RESOURCE_STATE_COMMON, nullptr, IID_ID3D12Resource, (void**)&(frame->texture));
        if (D3DVA_FAILED(hr))
        {
            D3DVA_LOG_ERROR << "ID3D12Device::CreateCommittedResource fail";
            assert(false);
            return nullptr;
        }
    }
    {
        std::weak_ptr<D3DVADecoderContext> weak = shared_from_this();
        frame->destoryProxy = [trackId = frame->trackId, weak, this](ID3D12Resource* texture, D3DVAFenceContext::ptr sync)
        {
            auto strong = weak.lock();
            if (!strong)
            {
                D3DVA_OBJECT_RELEASE(texture);
                return;
            }
            this->OnD3DVAVideoFrameFree(trackId, texture, sync);
        };
    }

    return frame;
}

bool D3DVADecoderContext::FenceSync(ID3D12CommandQueue* queue, D3DVAFenceContext::ptr fence)
{
    if (!queue || !fence)
    {
        assert(false);
        return false;
    }
    HRESULT hr = 0;
    hr = fence->Signal(queue);
    if (D3DVA_FAILED(hr))
    {
        D3DVA_LOG_ERROR << "ID3D12CommandQueue::Signal fail";
        assert(false);
        return false;
    }
    if (SyncFence(fence))
    {
        return true;
    }
    else
    {
        return false;
    }
}

void D3DVADecoderContext::OnD3DVAVideoFrameFree(uint64_t trackId, ID3D12Resource* texture, D3DVAFenceContext::ptr sync)
{

#if ENABLE_CODEC_D3DVA_TRACE_PROC
    D3DVA_LOG_INFO << "OnD3DVAVideoFrameFree, trackId is: " << trackId;
#endif

    std::weak_ptr<D3DVADecoderContext> weak = shared_from_this();
    D3DVAVideoFrame::ptr frame = std::make_shared<D3DVAVideoFrame>();
    frame->texture = texture;
    frame->sync = sync;
    frame->trackId = trackId;
    frame->destoryProxy = [trackId, weak, this](ID3D12Resource* texture, D3DVAFenceContext::ptr sync)
    {
        auto strong = weak.lock();
        if (!strong)
        {
            D3DVA_OBJECT_RELEASE(texture);
            return;
        }
        this->OnD3DVAVideoFrameFree(trackId, texture, sync);
    };
    {
        std::lock_guard<std::mutex> lock(_bufferMtx);
        _buffers.push_back(frame);
    }
}

#undef MMP_D3DVA_TEXTURE_ALIGN

} // namespace Codec
} // namespace Mmp