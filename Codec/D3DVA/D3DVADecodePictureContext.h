//
// D3DVADecodePictureContext.h
//
// Library: Common
// Package: Codec
// Module:  D3DVA
//

#pragma once

#include <mutex>

#include "D3DVACommon.h"

namespace Mmp
{
namespace Codec
{

class D3DVADecodePictureContext
{
public:
    using ptr = std::shared_ptr<D3DVADecodePictureContext>;
public:
    D3DVADecodePictureContext() = default;
    virtual ~D3DVADecodePictureContext() = default;
public:
    D3D12_VIDEO_DECODE_INPUT_STREAM_ARGUMENTS inputStreamArguments;
    D3D12_VIDEO_DECODE_OUTPUT_STREAM_ARGUMENTS outputStreamArgments;
};

} // namespace Codec
} // namespace Mmp