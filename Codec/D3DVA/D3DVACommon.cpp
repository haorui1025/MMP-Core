#include "D3DVACommon.h"

namespace Mmp
{
namespace Codec
{

D3DVADecoderParams::D3DVADecoderParams()
{
    format = PixelFormat::NV12;
    width = 0;
    height = 0;
    virWidth = 0;
    virHeight = 0;
    bitdepth = 8;
    codecType = CodecType::H264;
    maxRefNum = 15;
}

bool operator==(const D3DVADecoderParams& left, const D3DVADecoderParams& right)
{
    return left.format == right.format &&
           left.width  == right.width  &&
           left.height == right.height &&
           left.virWidth == right.virWidth &&
           left.virHeight == right.virHeight &&
           left.bitdepth == right.bitdepth &&
           left.codecType == right.codecType &&
           left.maxRefNum == right.maxRefNum;
}

bool operator!=(const D3DVADecoderParams& left, const D3DVADecoderParams& right)
{
    return !(left == right);
}

D3DVAFenceContext::D3DVAFenceContext()
{
    fence = nullptr;
    event = nullptr;
    fenceValue = 0;
}

D3DVAFenceContext::~D3DVAFenceContext()
{
    D3DVA_OBJECT_RELEASE(fence);
    if (event)
    {
        CloseHandle(event);
    }
}

uint64_t D3DVAFenceContext::GetCompletedValue()
{
    std::lock_guard<std::mutex> lock(mtx);
    if (fence)
    {
        return fence->GetCompletedValue();
    }
    else
    {
        assert(false);
        return 0;
    }
}

uint64_t D3DVAFenceContext::GetFenceValue()
{
    std::lock_guard<std::mutex> lock(mtx);
    return fenceValue;
}

void D3DVAFenceContext::UpdateFenceValue(uint64_t value)
{
    std::lock_guard<std::mutex> lock(mtx);
    fenceValue = value;
}

bool D3DVAFenceContext::Wait()
{
    uint64_t completion = fence->GetCompletedValue();
    HRESULT hr = 0;
    std::lock_guard<std::mutex> lock(mtx);

#if ENABLE_CODEC_D3DVA_TRACE_PROC
    D3DVA_LOG_INFO << "Wait, tag(" << tag << ") completion(" << completion << ") fenceValue(" << fenceValue << ") NeedWait(" << ((completion < fenceValue) ? "true" : "false") << ")";
#endif

    if (completion < fenceValue)
    {
        hr = fence->SetEventOnCompletion(fenceValue, event);
        if (D3DVA_FAILED(hr))
        {
            D3DVA_LOG_ERROR << "ID3D12Fence::SetEventOnCompletion fail";
            assert(false);
            return false;
        }
        WaitForSingleObjectEx(event, INFINITE, FALSE);
    }
    return true;
}

HRESULT D3DVAFenceContext::Wait(ID3D12CommandQueue* queue)
{
    std::lock_guard<std::mutex> lock(mtx);

#if ENABLE_CODEC_D3DVA_TRACE_PROC
    D3DVA_LOG_INFO << "CommandQueue Wait, tag(" << tag << ") fenceValue(" << fenceValue << ")";
#endif
    return queue->Wait(fence, fenceValue);
}

HRESULT D3DVAFenceContext::Signal(ID3D12CommandQueue* queue)
{
    std::lock_guard<std::mutex> lock(mtx);
    fenceValue++;

#if ENABLE_CODEC_D3DVA_TRACE_PROC
    D3DVA_LOG_INFO << "CommandQueue Signal, tag(" << tag << ") fenceValue(" << fenceValue << ")";
#endif

    return queue->Signal(fence, fenceValue);
}

D3DVACommandBundle::D3DVACommandBundle()
{
    allocator = nullptr;
    buffer = nullptr;
    fenceValue = 0;
}

D3DVACommandBundle::~D3DVACommandBundle()
{
    D3DVA_OBJECT_RELEASE(allocator);
    D3DVA_OBJECT_RELEASE(buffer);
}

D3DVAVideoFrame::D3DVAVideoFrame()
{
    trackId = 0;
    texture = nullptr;
    destoryProxy = nullptr;
}

D3DVAVideoFrame::~D3DVAVideoFrame()
{
    if (destoryProxy)
    {
        destoryProxy(texture, sync);
    }
    else
    {
        D3DVA_OBJECT_RELEASE(texture);
    }
    
}

} // namespace Codec
} // namespace Mmp