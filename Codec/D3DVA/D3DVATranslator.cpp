#include "D3DVATranslator.h"

#include <cassert>

namespace Mmp
{
namespace Codec
{

GUID CodecTypeToD3D12VIDEODECODEPROFILE(CodecType type)
{
    switch (type)
    {
        case CodecType::H264: return D3D12_VIDEO_DECODE_PROFILE_H264;
        case CodecType::H265: return D3D12_VIDEO_DECODE_PROFILE_HEVC_MAIN;
        case CodecType::VP8:  return D3D12_VIDEO_DECODE_PROFILE_VP8;
        case CodecType::VP9:  return D3D12_VIDEO_DECODE_PROFILE_VP9;
        case CodecType::AV1:  return D3D12_VIDEO_DECODE_PROFILE_AV1_PROFILE0;
        default:
        {
            assert(false);
            return GUID();
        }
    }
}

DXGI_FORMAT PixelFormatToD3DVADecoderFormat(const PixelFormat& format)
{
    switch (format)
    {
        case PixelFormat::NV12:
        {
            return DXGI_FORMAT_NV12;
        }
        default:
        {
            assert(false);
            return DXGI_FORMAT_P010;
        }
    }
}

} // namespace Codec
} // namespace Mmp