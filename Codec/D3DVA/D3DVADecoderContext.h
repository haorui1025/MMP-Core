//
// D3DVADecoderContext.h
//
// Library: Common
// Package: Codec
// Module:  D3DVA
//

#pragma once

#include <mutex>
#include <deque>
#include <vector>
#include <memory>

#include "D3DVACommon.h"

#include "Common/NormalAllocateMethod.h"

namespace Mmp
{
namespace Codec
{

class D3DVADecoderContext : public std::enable_shared_from_this<D3DVADecoderContext>
{
public:
    using ptr = std::shared_ptr<D3DVADecoderContext>;
public:
    D3DVADecoderContext();
    virtual ~D3DVADecoderContext();
public:
    bool Init(const D3DVADecoderParams& params);
public:
    bool DecodeFrame(D3D12_VIDEO_DECODE_INPUT_STREAM_ARGUMENTS& inputStreamArguments, D3D12_VIDEO_DECODE_OUTPUT_STREAM_ARGUMENTS& outputStreamArguments, D3DVAVideoFrame::ptr frame, D3DVACommandBundle::ptr bundle);
    D3DVACommandBundle::ptr GetD3DVACommandBundle();
    D3DVAVideoFrame::ptr CreateD3DVAVideoFrame();
    bool SyncFence(D3DVAFenceContext::ptr fence);
    NormalAllocateMethod::ptr TransferFrame(D3DVAVideoFrame::ptr frame);
    void UpdateReferenceFrames(const std::vector<ID3D12Resource*>& refResources, std::vector<UINT> refSubresources);
private:
    void InitParams(const D3DVADecoderParams& params);
    D3DVAFenceContext::ptr CreateD3DVAFenceContext();
    D3DVACommandBundle::ptr CreateD3DVACommandBundle();
    ID3D12VideoDecoderHeap* GetDecoderHeap();
    bool FenceSync(ID3D12CommandQueue* queue, D3DVAFenceContext::ptr fence);
private:
    ID3D12Resource* GetStagingDownloadBuffer();
    ID3D12CommandQueue* GetStagingCommandQueue();
    ID3D12CommandAllocator* GetStagingAllocator();
    ID3D12GraphicsCommandList* GetStagingCommandList();
private:
    void OnD3DVAVideoFrameFree(uint64_t trackId, ID3D12Resource* texture, D3DVAFenceContext::ptr sync);
private:
    uint64_t _index;
    std::string _tagPrefix;
    std::mutex _mtx;
    ID3D12VideoDecoder* _decoder;
    ID3D12CommandQueue* _commandQueue;
    ID3D12VideoDecodeCommandList* _commandList;
    ID3D12VideoDecoderHeap* _decoderHeap;
    D3DVAFenceContext::ptr _fence;
    D3D12_VIDEO_DECODE_CONFIGURATION _config;
    size_t _bitStramSize;
    D3D12_RESOURCE_FLAGS _flags;
    std::deque<D3DVACommandBundle::ptr> _bundles;
private:
    std::mutex _stagingMtx;
    ID3D12GraphicsCommandList* _stagingCommandList;
    ID3D12CommandAllocator* _stagingAllocator;
    ID3D12CommandQueue* _stagingCommandQueue;
    ID3D12Resource* _stagingDownloadBuffer;
    D3DVAFenceContext::ptr _stagingFence;
private:
    std::mutex _bufferMtx;
    std::deque<D3DVAVideoFrame::ptr> _buffers;
    uint64_t _bufTrackId;
private:
    uint32_t _width;
    uint32_t _height;
    DXGI_FORMAT _format; 
    uint32_t _bitRate;
    CodecType _codecType;
    uint32_t _frameRateNumerator;
    uint32_t _frameRateDenominator;
    uint32_t _maxDecodePictureBufferCount;
    std::vector<ID3D12Resource*> _refResources;
    std::vector<UINT> _refSubresources;
};

} // namespace Codec
} // namespace Mmp