#include "D3DVAEncoder.h"

#include <cassert>

namespace Mmp
{
namespace Codec
{

D3DVAEncoder::D3DVAEncoder()
{

}

bool D3DVAEncoder::Init()
{
    assert(false);
    return false;
}

void D3DVAEncoder::Uninit()
{
    assert(false);
}

bool D3DVAEncoder::Start()
{
    assert(false);
    return false;
}

void D3DVAEncoder::Stop()
{
    assert(false);
}

bool D3DVAEncoder::Push(AbstractFrame::ptr frame)
{
    assert(false);
    return false;
}

bool D3DVAEncoder::Pop(AbstractPack::ptr& pack)
{
    assert(false);
    return false;
}

bool D3DVAEncoder::CanPush()
{
    assert(false);
    return false;
}

bool D3DVAEncoder::CanPop()
{
    assert(false);
    return false;
}

const std::string& D3DVAEncoder::Description()
{
    static std::string description = "D3DVAEncoder";
    return description;
}

} // namespace Codec
} // namespace Mmp