//
// D3DVAH264Decoder.h
//
// Library: Common
// Package: Codec
// Module:  D3DVA
// 
//

#pragma once

#include "D3DVADecoder.h"

#include "H264/H264Deserialize.h"
#include "H264/H264SliceDecodingProcess.h"

namespace Mmp
{
namespace Codec
{

/**
 * @sa DXVA_H264 : https://download.microsoft.com/download/5/f/c/5fc4ec5c-bd8c-4624-8034-319c1bab7671/DXVA_H264.pdf
 */
class D3DVAH264Decoder : public D3DVADecoder
{
public:
    D3DVAH264Decoder();
    ~D3DVAH264Decoder();
public:
    bool Push(AbstractPack::ptr pack) override;
    const std::string& Description() override;
private:
    void FillDXVAPicParams(DXVA_PicParams_H264& pp, const std::vector<H264PictureContext::ptr>& refPictures, H264NalSyntax::ptr nal, H264SpsSyntax::ptr sps, H264PpsSyntax::ptr pps, H264SliceHeaderSyntax::ptr slice, H264SliceDecodingProcess::ptr sliceDecoding);
    void FillDXVAQmatrixH264(DXVA_Qmatrix_H264& qm, H264PpsSyntax::ptr pps);
    void FillReferenceFrames(std::vector<H264PictureContext::ptr>& refPictures, std::vector<ID3D12Resource*>& textures);
private:
    void StartFrame(const Any& context) override;
    void DecodedBitStream(const Any& context) override;
    void EndFrame(const Any& context) override;
private:
    H264Deserialize::ptr                                            _deserialize;
    H264ContextSyntax::ptr                                          _deserializeContext;
    H264SliceDecodingProcess::ptr                                   _sliceDecoding;
    uint32_t                                                        _reportId;
};

} // namespace Codec
} // namespace Mmp