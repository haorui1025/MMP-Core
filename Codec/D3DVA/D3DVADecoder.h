//
// D3DVADecoder.h
//
// Library: Common
// Package: Codec
// Module:  D3DVA
//

#pragma once

#include <mutex>

#include "D3DVACommon.h"
#include "D3DVADecoderContext.h"
#include "D3DVADecodePictureContext.h"

namespace Mmp
{
namespace Codec
{

class D3DVADecoder : public AbstractDecoder
{
public:
    using ptr = std::shared_ptr<D3DVADecoder>;
public:
    D3DVADecoder();
    virtual ~D3DVADecoder() = default;
public:
    void SetParameter(Any parameter, const std::string& property) override;
    Any GetParamter(const std::string& property) override;
    bool Init() override;
    void Uninit() override;
    bool Start() override;
    void Stop() override;
    bool Push(AbstractPack::ptr pack) override;
    bool Pop(AbstractFrame::ptr& frame) override;
    bool CanPush() override;
    bool CanPop() override;
    const std::string& Description() override;
protected:
    D3DVADecoderContext::ptr GetDecoderContext();
    D3DVADecoderParams GetDecoderParams();
    void SetDecoderParams(const D3DVADecoderParams& params);
    void PushFrame(D3DVAVideoFrame::ptr frame);
protected: /* Hook */
    virtual void StartFrame(const Any& context) = 0;
    virtual void DecodedBitStream(const Any& context) = 0;
    virtual void EndFrame(const Any& context) = 0;
protected: /* Event */
    virtual void OnDecoderParamsChange(const D3DVADecoderParams& oldValue, const D3DVADecoderParams& newValue);
private:
    std::mutex _bufMtx;
    std::deque<StreamFrame::ptr> _buffers;
private:
    D3DVADecoderParams _params;
    D3DVADecoderContext::ptr _decoderContext;
};

} // namespace Codec
} // namespace Mmp