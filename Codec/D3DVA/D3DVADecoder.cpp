#include "D3DVADecoder.h"

#include <cassert>

#include "D3DVADeviceAllocateMethod.h"

namespace Mmp
{
namespace Codec
{

D3DVADecoder::D3DVADecoder()
{
    _decoderContext = nullptr;
}

void D3DVADecoder::SetParameter(Any parameter, const std::string& property)
{
    AbstractDecoder::SetParameter(parameter, property);
}

Any D3DVADecoder::GetParamter(const std::string& property)
{
    return AbstractDecoder::GetParamter(property);
}

bool D3DVADecoder::Init()
{
    return true;
}

void D3DVADecoder::Uninit()
{
}

bool D3DVADecoder::Start()
{
    return true;
}

void D3DVADecoder::Stop()
{
}

bool D3DVADecoder::Push(AbstractPack::ptr pack)
{
    return false;
}

bool D3DVADecoder::Pop(AbstractFrame::ptr& frame)
{
    std::lock_guard<std::mutex> lock(_bufMtx);
    if (!_buffers.empty())
    {
        frame = _buffers.front();
        _buffers.pop_front();
        return true;
    }
    else
    {
        return false;
    }
}

bool D3DVADecoder::CanPush()
{
    return true;
}

bool D3DVADecoder::CanPop()
{
    std::lock_guard<std::mutex> lock(_bufMtx);
    return !_buffers.empty();
}

const std::string& D3DVADecoder::Description()
{
    static std::string description = "D3DVADecoder";
    return description;
}

D3DVADecoderContext::ptr D3DVADecoder::GetDecoderContext()
{
    return _decoderContext;
}

D3DVADecoderParams D3DVADecoder::GetDecoderParams()
{
    return _params;
}

void D3DVADecoder::SetDecoderParams(const D3DVADecoderParams& params)
{
    if (_params != params)
    {
        D3DVADecoderParams oldParams = _params;
        _params = params;
        _decoderContext = std::make_shared<D3DVADecoderContext>();
        if (!_decoderContext->Init(_params))
        {
            D3DVA_LOG_ERROR << "D3DVADecoderContext Init fail";
            assert(false);
        }
        OnDecoderParamsChange(oldParams, params);
    }
}

void D3DVADecoder::PushFrame(D3DVAVideoFrame::ptr frame)
{
    std::lock_guard<std::mutex> lock(_bufMtx);
    D3DVADeviceAllocateMethod::ptr alloc = std::make_shared<D3DVADeviceAllocateMethod>();
    PixelsInfo info;
    {
        alloc->srcFrame = frame;
        alloc->decoderContext = _decoderContext;
    }
    {
        info.width = _params.width;
        info.height = _params.height;
        info.format = _params.format;
        info.bitdepth = _params.bitdepth;
    }
    StreamFrame::ptr stramFrame = std::make_shared<StreamFrame>(info, alloc);
    _buffers.push_back(stramFrame);
}

void D3DVADecoder::OnDecoderParamsChange(const D3DVADecoderParams& oldValue, const D3DVADecoderParams& newValue)
{

}

} // namespace Codec
} // namespace Mmp