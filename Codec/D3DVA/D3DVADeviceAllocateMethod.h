//
// D3DVADeviceAllocateMethod.h
//
// Library: Common
// Package: Codec
// Module:  D3DVA
// 
//

#pragma once


#include "Common/AbstractAllocateMethod.h"
#include "Common/NormalAllocateMethod.h"

#include "D3DVACommon.h"
#include "D3DVADecoderContext.h"

namespace Mmp
{
namespace Codec
{

class D3DVADeviceAllocateMethod : public AbstractAllocateMethod
{
public:
    using ptr = std::shared_ptr<D3DVADeviceAllocateMethod>;
public:
    D3DVADeviceAllocateMethod();
    ~D3DVADeviceAllocateMethod();
public:
    void* Malloc(size_t size) override;
    void* Resize(void* data, size_t size) override;
    void* GetAddress(uint64_t offset) override;
    const std::string& Tag() override; 
public:
    D3DVAVideoFrame::ptr srcFrame;
    D3DVADecoderContext::ptr decoderContext;
    NormalAllocateMethod::ptr alloc;
};

} // namespace Codec
} // namespace Mmp