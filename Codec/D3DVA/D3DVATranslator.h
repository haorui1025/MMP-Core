//
// D3DVATranslator.h
//
// Library: Common
// Package: Codec
// Module:  D3DVA
// 

#pragma once


#include "D3DVACommon.h"

namespace Mmp
{
namespace Codec
{

GUID CodecTypeToD3D12VIDEODECODEPROFILE(CodecType type);

DXGI_FORMAT PixelFormatToD3DVADecoderFormat(const PixelFormat& format);

} // namespace Codec
} // namespace Mmp