//
// D3DVAEncoder.h
//
// Library: Common
// Package: Codec
// Module:  D3DVA
//

#pragma once

#include <mutex>

#include "D3DVACommon.h"

namespace Mmp
{
namespace Codec
{

class D3DVAEncoder : public AbstractEncoder
{
public:
    using ptr = std::shared_ptr<D3DVAEncoder>;
public:
    D3DVAEncoder();
    virtual ~D3DVAEncoder() = default;
public:
    bool Init() override;
    void Uninit() override;
    bool Start() override;
    void Stop() override;
    bool Push(AbstractFrame::ptr frame) override;
    bool Pop(AbstractPack::ptr& pack) override;
    bool CanPush() override;
    bool CanPop() override;
    const std::string& Description() override;
};

} // namespace Codec
} // namespace Mmp