//
// D3DVAH264Encoder.h
//
// Library: Common
// Package: Codec
// Module:  D3DVA
// 
//

#pragma once

#include "D3DVAEncoder.h"

namespace Mmp
{
namespace Codec
{

class D3DVAH264Encoder : public D3DVAEncoder
{
public:
    D3DVAH264Encoder();
    ~D3DVAH264Encoder();
};

} // namespace Codec
} // namespace Mmp