#include "D3DVAH264Decoder.h"

#include <algorithm>

#include "H264/H264Common.h"
#include "H264Deserialize.h"
#include "H264SliceDecodingProcess.h"

#define D3DVA_ENABLE_H264_LONG_SLICE_INFO 0
#define D3DVA_H264_FIELD_ORDER_CNT_OFFSET 65536


namespace Mmp
{
namespace Codec
{

class D3DVAH264ByteReader : public AbstractH26xByteReader
{
public:
    D3DVAH264ByteReader(AbstractPack::ptr pack)
    {
        _pack = pack;
        _end = (uint32_t)_pack->GetSize();
        _cur = 0;
    }
public:
    size_t Read(void* data, size_t bytes) override
    {
        uint32_t readSize = (uint32_t)bytes < (_end - _cur) ? (uint32_t)bytes : (_end - _cur);
        if (readSize != 0)
        {
            memcpy(data, (uint8_t*)_pack->GetData() + _cur, readSize);
        }
        _cur += readSize;
        return size_t(readSize);
    }
    bool Seek(size_t offset)  override
    {
        if (offset >= 0 && offset < _end)
        {
            _cur = (uint32_t)offset;
            return true;
        }
        else
        {
            return false;
        }
    }
    size_t Tell() override
    {
        return size_t(_cur);
    }
    bool Eof() override
    {
        return _cur == _end;
    }
private:
    uint32_t _cur;
    uint32_t _end;
    AbstractPack::ptr _pack;
};

} // namespace Codec
} // namespace Mmp


namespace Mmp
{
namespace Codec
{

class D3DVAH264PictureContext : public H264PictureContext
{
public:
    using ptr = std::shared_ptr<D3DVAH264PictureContext>;
public:
    D3DVAVideoFrame::ptr frame;
};

class D3DVAH264SliceDecodingProcess : public H264SliceDecodingProcess
{
protected:
    H264PictureContext::ptr CreatePictureContext() override;
};

H264PictureContext::ptr D3DVAH264SliceDecodingProcess::CreatePictureContext()
{
    return std::make_shared<D3DVAH264PictureContext>();
}

} // namespace Codec
} // namespace Mmp

namespace Mmp
{
namespace Codec
{

class D3DVAH264StartFrameContext : public D3DVADecodePictureContext
{
public:
    using ptr = std::shared_ptr<D3DVAH264StartFrameContext>;
public:
    H264SpsSyntax::ptr         sps;
    H264PpsSyntax::ptr         pps;
    H264SliceHeaderSyntax::ptr slice;
    H264NalSyntax::ptr         nal;
public:
    DXVA_PicParams_H264 pp = {};
    DXVA_Qmatrix_H264 qm = {};
    DXVA_Slice_H264_Short shortSliceInfo = {};
    DXVA_Slice_H264_Long longSliceInfo = {};
    D3DVAVideoFrame::ptr frame;
    AbstractPack::ptr pack;
public:
    std::vector<ID3D12Resource*> textures;
    std::vector<UINT> refSubresources;
    std::vector<H264PictureContext::ptr> refPictures;
};

D3DVAH264Decoder::D3DVAH264Decoder()
{
    _deserialize = std::make_shared<H264Deserialize>();
    _deserializeContext = std::make_shared<H264ContextSyntax>();
    _sliceDecoding = std::make_shared<D3DVAH264SliceDecodingProcess>();
    _reportId = 0;
}

D3DVAH264Decoder::~D3DVAH264Decoder()
{

}

bool D3DVAH264Decoder::Push(AbstractPack::ptr pack)
{
    H26xBinaryReader::ptr br = std::make_shared<H26xBinaryReader>(std::make_shared<D3DVAH264ByteReader>(pack));
    H264NalSyntax::ptr nal = std::make_shared<H264NalSyntax>();
    if (!_deserialize->DeserializeByteStreamNalUnit(br, nal))
    {
        return false;
    }
    switch (nal->nal_unit_type)
    {
        case H264NaluType::MMP_H264_NALU_TYPE_SPS:
        {
            _deserializeContext->spsSet[nal->sps->seq_parameter_set_id] = nal->sps;
            break;
        }
        case H264NaluType::MMP_H264_NALU_TYPE_PPS:
        {
            _deserializeContext->ppsSet[nal->pps->pic_parameter_set_id] = nal->pps;
            break;
        }
        default:
        break;
    }
    D3DVAH264StartFrameContext::ptr context = std::make_shared<D3DVAH264StartFrameContext>();
    if (nal->nal_unit_type != H264NaluType::MMP_H264_NALU_TYPE_IDR)
    {
        FillReferenceFrames(context->refPictures, context->textures);
    }
    _sliceDecoding->SliceDecodingProcess(nal);
    if (!(nal->nal_unit_type == H264NaluType::MMP_H264_NALU_TYPE_SLICE || nal->nal_unit_type == H264NaluType::MMP_H264_NALU_TYPE_IDR))
    {
        return true;
    }
    if (!CanPush())
    {
        return false;
    }
    {
        context->nal = nal;
        context->slice = nal->slice;
        if (_deserializeContext->ppsSet.count(nal->slice->pic_parameter_set_id) == 0)
        {
            return true;
        }
        context->pps = _deserializeContext->ppsSet[nal->slice->pic_parameter_set_id];
        if (_deserializeContext->spsSet.count(context->pps->seq_parameter_set_id) == 0)
        {
            return true;
        }
        context->sps = _deserializeContext->spsSet[context->pps->seq_parameter_set_id];
        context->pack = pack;
    }
    StartFrame(context);
    DecodedBitStream(context);
    EndFrame(context);
    return true;
}

const std::string& D3DVAH264Decoder::Description()
{
    static std::string description = "D3DVAH264Decoder";
    return description;
}

/**
 * @sa FFmpeg 6.x : ff_dxva2_h264_fill_picture_parameters
 */
void D3DVAH264Decoder::FillDXVAPicParams(DXVA_PicParams_H264& pp, const std::vector<H264PictureContext::ptr>& refPictures, H264NalSyntax::ptr nal, H264SpsSyntax::ptr sps, H264PpsSyntax::ptr pps, H264SliceHeaderSyntax::ptr slice, H264SliceDecodingProcess::ptr sliceDecoding)
{
    H264PictureContext::ptr curPicture = sliceDecoding->GetCurrentPictureContext();
    // fill RefFrameList
    {
        pp.UsedForReferenceFlags = 0;
        pp.NonExistingFrameFlags = 0;
        // Reset
        for (size_t i=0; i<16; i++)
        {
            pp.RefFrameList[i].bPicEntry = 0xFF;
            pp.FieldOrderCntList[i][0] = 0;
            pp.FieldOrderCntList[i][1] = 0;
            pp.FrameNumList[i]         = 0;
        }
        for (size_t i=0; i<refPictures.size() && i<16; i++)
        {
            H264PictureContext::ptr picture = refPictures[i];
            {
                uint8_t flag = 0;
                uint32_t index = picture->id & 0x7F;
                if (picture->referenceFlag & H264PictureContext::used_for_long_term_reference)
                {
                    flag = 1;
                }
                assert((index & 0x7F) == index);
                pp.RefFrameList[i].bPicEntry = index | (flag << 7);
            }
            pp.FrameNumList[i] = picture->referenceFlag & H264PictureContext::used_for_long_term_reference ? picture->long_term_frame_idx : picture->FrameNum;
            pp.FieldOrderCntList[i][0] = D3DVA_H264_FIELD_ORDER_CNT_OFFSET + picture->TopFieldOrderCnt;
            pp.FieldOrderCntList[i][1] = D3DVA_H264_FIELD_ORDER_CNT_OFFSET + picture->BottomFieldOrderCnt;
            if (picture->field_pic_flag == 1)
            {
                pp.UsedForReferenceFlags |= 1 << (2*i + 0);
                if (picture->bottom_field_flag == 1)
                {
                    pp.UsedForReferenceFlags |= 1 << (2*i + 1);
                }
            }
            else
            {
                pp.UsedForReferenceFlags |= (1 << (2*i + 0)) | (1 << (2*i + 1));
            }
        }
    }
    //
    // FIXME : MMP 暂时只支持 Frame 格式, 不支持 Top Field Picture 或者 Bottom Field Picture
    // See also : https://github.com/HR1025/MMP-H26X/commit/c55efc91cdafc4fe01ce0116dfd120ebdd72326e
    //
    pp.CurrPic.bPicEntry    =                  (uint8_t)(curPicture->id) | (curPicture->bottom_field_flag << 7);
    pp.wBitFields =                            (0                                 << 0)  |
                                               (sps->mb_adaptive_frame_field_flag << 1)  |
                                               (sps->separate_colour_plane_flag   << 2)  |
                                               (0                                 << 3)  |
                                               (sps->chroma_format_idc            << 4)  |
                                               ((nal->nal_ref_idc != 0)           << 6)  |
                                               (pps->constrained_intra_pred_flag  << 7)  |
                                               (pps->weighted_pred_flag           << 8)  |
                                               (pps->weighted_bipred_idc          << 9)  |
                                               (1                                 << 11) |
                                               (sps->frame_mbs_only_flag          << 12) |
                                               (pps->transform_8x8_mode_flag      << 13) |
                                               ((sps->level_idc >= 31)            << 14) |
                                               (1                                 << 15)
                                            ;
    pp.wFrameWidthInMbsMinus1       = sps->pic_width_in_mbs_minus1;
    pp.wFrameHeightInMbsMinus1       = sps->pic_height_in_map_units_minus1;
    pp.num_ref_frames                = sps->max_num_ref_frames;
    pp.bit_depth_luma_minus8         = sps->bit_depth_chroma_minus8;
    pp.bit_depth_chroma_minus8       = sps->bit_depth_chroma_minus8;
    pp.Reserved16Bits                = 3; /* ??? */
    pp.StatusReportFeedbackNumber    = ++_reportId;
    pp.CurrFieldOrderCnt[0]          = D3DVA_H264_FIELD_ORDER_CNT_OFFSET + curPicture->TopFieldOrderCnt;
    pp.CurrFieldOrderCnt[1]          = D3DVA_H264_FIELD_ORDER_CNT_OFFSET + curPicture->BottomFieldOrderCnt;
    pp.pic_init_qs_minus26           = pps->pic_init_qs_minus26;
    pp.chroma_qp_index_offset        = pps->chroma_qp_index_offset;
    pp.second_chroma_qp_index_offset = pps->second_chroma_qp_index_offset;
    pp.ContinuationFlag              = 1;
    pp.pic_init_qp_minus26           = pps->pic_init_qp_minus26;
    pp.num_ref_idx_l0_active_minus1  = slice->num_ref_idx_l0_active_minus1;
    pp.num_ref_idx_l1_active_minus1  = slice->num_ref_idx_l1_active_minus1;
    pp.Reserved8BitsA                = 0;
    pp.frame_num                     = curPicture->FrameNum;
    pp.log2_max_frame_num_minus4     = sps->log2_max_frame_num_minus4;
    pp.pic_order_cnt_type            = sps->pic_order_cnt_type;
    if (pp.pic_order_cnt_type == 0)
    {
        pp.log2_max_pic_order_cnt_lsb_minus4 = sps->log2_max_pic_order_cnt_lsb_minus4;
    }
    else if (pp.pic_order_cnt_type == 1)
    {
        pp.delta_pic_order_always_zero_flag = sps->delta_pic_order_always_zero_flag;
    }
    pp.direct_8x8_inference_flag                = sps->direct_8x8_inference_flag;
    pp.entropy_coding_mode_flag                 = pps->entropy_coding_mode_flag;
    pp.pic_order_present_flag                   = pps->bottom_field_pic_order_in_frame_present_flag;
    pp.num_slice_groups_minus1                  = pps->num_slice_groups_minus1;
    pp.slice_group_map_type                     = pps->slice_group_map_type;
    pp.deblocking_filter_control_present_flag   = pps->deblocking_filter_control_present_flag;
    pp.redundant_pic_cnt_present_flag           = pps->redundant_pic_cnt_present_flag;
    pp.Reserved8BitsB                           = 0;
    pp.slice_group_change_rate_minus1           = 0;
    if (slice->slice_type != MMP_H264_I_SLICE && slice->slice_type != MMP_H264_SI_SLICE)
    {
        pp.wBitFields &= ~(1 << 15);
    }
}

void D3DVAH264Decoder::FillDXVAQmatrixH264(DXVA_Qmatrix_H264& qm, H264PpsSyntax::ptr pps)
{
    for (size_t i=0; i<sizeof(qm.bScalingLists4x4)/sizeof(qm.bScalingLists4x4[0]) && i<pps->ScalingList4x4.size(); i++)
    {
        for (size_t j=0; j<sizeof(qm.bScalingLists4x4[0]) && j<pps->ScalingList4x4[0].size(); j++)
        {
            qm.bScalingLists4x4[i][j] = pps->ScalingList4x4[i][j];
        }
    }
    for (size_t i=0; i<sizeof(qm.bScalingLists8x8)/sizeof(qm.bScalingLists8x8[0]) && i<pps->ScalingList8x8.size(); i++)
    {
        for (size_t j=0; j<sizeof(qm.bScalingLists8x8[0]) && j<pps->ScalingList8x8[0].size(); j++)
        {
            qm.bScalingLists8x8[i][j] = pps->ScalingList8x8[i][j];
        }
    }
}

void D3DVAH264Decoder::FillReferenceFrames(std::vector<H264PictureContext::ptr>& refPictures, std::vector<ID3D12Resource*>& textures)
{
    H264PictureContext::cache pictures = _sliceDecoding->GetAllPictures();
    H264PictureContext::cache shortRefPictures;
    H264PictureContext::cache longRefPictures;
    // shortRefPictures
    {
        for (auto& picture : pictures)
        {
            if (picture->referenceFlag & H264PictureContext::used_for_short_term_reference)
            {
                shortRefPictures.push_back(picture);
            }
        }
        std::reverse(shortRefPictures.begin(), shortRefPictures.end());
    }
    // longRefPictures
    {
        for (auto& picture : pictures)
        {
            if (picture->referenceFlag & H264PictureContext::used_for_long_term_reference)
            {
                longRefPictures.push_back(picture);
            }
        }
    }
    for (auto& picture : shortRefPictures)
    {
        refPictures.push_back(picture);
        textures.push_back(std::dynamic_pointer_cast<D3DVAH264PictureContext>(picture)->frame->texture);
    }
    for (auto& picture : longRefPictures)
    {
        refPictures.push_back(picture);
        textures.push_back(std::dynamic_pointer_cast<D3DVAH264PictureContext>(picture)->frame->texture);
    }
}

void D3DVAH264Decoder::StartFrame(const Any& context)
{
    if (context.type() != typeid(D3DVAH264StartFrameContext::ptr))
    {
        assert(false);
        return;
    }
    
    const D3DVAH264StartFrameContext::ptr& _context = RefAnyCast<D3DVAH264StartFrameContext::ptr>(context);
    H264SpsSyntax::ptr sps = _context->sps;
    H264PpsSyntax::ptr pps = _context->pps;
    H264SliceHeaderSyntax::ptr slice = _context->slice;
    H264NalSyntax::ptr  nal = _context->nal;

    // Hint : Check param changed
    {
        D3DVADecoderParams param = GetDecoderParams();
        if (param.virWidth != (sps->pic_width_in_mbs_minus1 + 1) * 16 || 
            param.virHeight != (sps->pic_height_in_map_units_minus1 + 1) * 16
        )
        {
            param.codecType = CodecType::H264;
            param.virWidth = (sps->pic_width_in_mbs_minus1 + 1) * 16;
            param.virHeight = (sps->pic_height_in_map_units_minus1 + 1) * 16;
            param.width = param.virWidth - (sps->frame_crop_left_offset + sps->frame_crop_right_offset) * sps->context->CropUnitX;
            param.height = param.virHeight - (sps->frame_crop_top_offset + sps->frame_crop_bottom_offset) * sps->context->CropUnitY;
            param.maxRefNum = sizeof(_context->pp.RefFrameList) / sizeof(_context->pp.RefFrameList[0]) + 1;
            SetDecoderParams(param);
        }
    }
    _context->frame = GetDecoderContext()->CreateD3DVAVideoFrame();
    {
        D3DVAH264PictureContext::ptr picture = std::dynamic_pointer_cast<D3DVAH264PictureContext>(_sliceDecoding->GetCurrentPictureContext());
        picture->frame = _context->frame;
    }
    DXVA_PicParams_H264& pp = _context->pp;
    DXVA_Qmatrix_H264& qm = _context->qm;
    FillDXVAPicParams(pp, _context->refPictures, nal, sps, pps, slice, _sliceDecoding);
    for (auto& texture : _context->textures)
    {
        _context->refSubresources.push_back((UINT)0);
    }
    FillDXVAQmatrixH264(qm, pps);
}

void D3DVAH264Decoder::DecodedBitStream(const Any& context)
{

}

void D3DVAH264Decoder::EndFrame(const Any& context)
{
    if (context.type() != typeid(D3DVAH264StartFrameContext::ptr))
    {
        assert(false);
        return;
    }
    HRESULT hr = 0;
    const D3DVAH264StartFrameContext::ptr& _context = RefAnyCast<D3DVAH264StartFrameContext::ptr>(context);

    D3DVACommandBundle::ptr bundle = GetDecoderContext()->GetD3DVACommandBundle();
    GetDecoderContext()->SyncFence(_context->frame->sync);
    H264SliceHeaderSyntax::ptr slice = _context->slice;
    H264SpsSyntax::ptr sps = _context->sps;
    H264PpsSyntax::ptr pps = _context->pps;
    D3D12_VIDEO_DECODE_INPUT_STREAM_ARGUMENTS& inputStreamArguments = _context->inputStreamArguments;
    D3D12_VIDEO_DECODE_OUTPUT_STREAM_ARGUMENTS& outputStreamArguments = _context->outputStreamArgments;
    {
        memset(&inputStreamArguments, 0, sizeof(D3D12_VIDEO_DECODE_INPUT_STREAM_ARGUMENTS));
        inputStreamArguments.NumFrameArguments = 2;
        {
            inputStreamArguments.FrameArguments[0].Type = D3D12_VIDEO_DECODE_ARGUMENT_TYPE_PICTURE_PARAMETERS;
            inputStreamArguments.FrameArguments[0].Size = sizeof(DXVA_PicParams_H264);
            inputStreamArguments.FrameArguments[0].pData = (void*)(&(_context->pp));
        }
        {
            inputStreamArguments.FrameArguments[1].Type = D3D12_VIDEO_DECODE_ARGUMENT_TYPE_INVERSE_QUANTIZATION_MATRIX;
            inputStreamArguments.FrameArguments[1].Size = sizeof(DXVA_Qmatrix_H264);
            inputStreamArguments.FrameArguments[1].pData = (void*)(&(_context->qm));
        }
        GetDecoderContext()->UpdateReferenceFrames(_context->textures, _context->refSubresources);
    }
    //
    // FIX ME : 
    // (1)      理论上 DXVA_Slice_H264_Long 需在 bConfigBitstreamRaw == 0 || bConfigBitstreamRaw == 1 使用,
    //          DXVA_Slice_H264_Short 则可以在 bConfigBitstreamRaw == 2 使用
    //          但 D3D12 不像 D3D11 可以获取到 bConfigBitstreamRaw 所以不知道如何处理判断
    // (2)      DXVA_Slice_H264_Long 部分参数还不确认如何填充, 需参考文档描述
    // 
    // Hint : 目前发现码流中出现非默认字段(在DXVA_Slice_H264_Long中)时, 解码就会出现异常 (比如 cabac_init_idc)
    //        不确定是解码器能力限制, 还是由于用法错误导致
    //
    // See alos : DXVA2(https://download.microsoft.com/download/5/f/c/5fc4ec5c-bd8c-4624-8034-319c1bab7671/DXVA_H264.pdf)
    // 
#if D3DVA_ENABLE_H264_LONG_SLICE_INFO
    DXVA_Slice_H264_Long& sliceInfo = _context->longSliceInfo;
    {
        const std::vector<H264PictureContext::ptr>& refPictures = _context->refPictures;
        {
            uint8_t* address = (uint8_t*)_context->pack->GetData(0);
            sliceInfo.BSNALunitDataLocation = (UINT)0;
            sliceInfo.SliceBytesInBuffer = (UINT)_context->pack->GetSize();
            sliceInfo.wBadSliceChopping = 0;
            sliceInfo.first_mb_in_slice = slice->first_mb_in_slice;
            sliceInfo.NumMbsForSlice = 0; // TODO
            sliceInfo.BitOffsetToSliceData = slice->slice_data_bit_offset;
            sliceInfo.slice_type = slice->slice_type;
            if (slice->pwt)
            {
                sliceInfo.luma_log2_weight_denom = slice->pwt->luma_log2_weight_denom;
                sliceInfo.chroma_log2_weight_denom = slice->pwt->chroma_log2_weight_denom;
            }
            sliceInfo.num_ref_idx_l0_active_minus1 = slice->num_ref_idx_l0_active_minus1;
            sliceInfo.num_ref_idx_l1_active_minus1 = slice->num_ref_idx_l1_active_minus1;
            sliceInfo.slice_alpha_c0_offset_div2 = slice->slice_alpha_c0_offset_div2;
            sliceInfo.slice_beta_offset_div2 = slice->slice_beta_offset_div2;
            sliceInfo.Reserved8Bits = 0;
            // fill RefFrameList
            for (size_t list = 0; list<2; list++)
            {
                // Reset
                for (size_t i=0; i<32; i++)
                {
                    sliceInfo.RefPicList[list][i].bPicEntry = 0xFF;
                }
            }
            sliceInfo.slice_qs_delta = slice->slice_qs_delta;
            sliceInfo.slice_qp_delta = slice->slice_qp_delta;
            sliceInfo.redundant_pic_cnt = slice->redundant_pic_cnt;
            sliceInfo.direct_spatial_mv_pred_flag = slice->direct_spatial_mv_pred_flag;
            sliceInfo.cabac_init_idc = slice->cabac_init_idc;
            sliceInfo.disable_deblocking_filter_idc = slice->disable_deblocking_filter_idc;
            sliceInfo.slice_id = (USHORT)(_reportId);
        }
        {
            inputStreamArguments.FrameArguments[inputStreamArguments.NumFrameArguments].Type = D3D12_VIDEO_DECODE_ARGUMENT_TYPE_SLICE_CONTROL;
            inputStreamArguments.FrameArguments[inputStreamArguments.NumFrameArguments].Size = sizeof(DXVA_Slice_H264_Long);
            inputStreamArguments.FrameArguments[inputStreamArguments.NumFrameArguments].pData = (void*)(&sliceInfo);
            inputStreamArguments.NumFrameArguments++;
        }
    }
#else
    DXVA_Slice_H264_Short& sliceInfo = _context->shortSliceInfo;
    // DXVA_Slice_H264_Short
    {
        {
            uint8_t* address = (uint8_t*)_context->pack->GetData(0);
            sliceInfo.BSNALunitDataLocation = (UINT)0;
            sliceInfo.SliceBytesInBuffer = (UINT)_context->pack->GetSize();
            sliceInfo.wBadSliceChopping = 0;
        }
        {
            inputStreamArguments.FrameArguments[inputStreamArguments.NumFrameArguments].Type = D3D12_VIDEO_DECODE_ARGUMENT_TYPE_SLICE_CONTROL;
            inputStreamArguments.FrameArguments[inputStreamArguments.NumFrameArguments].Size = sizeof(DXVA_Slice_H264_Short);
            inputStreamArguments.FrameArguments[inputStreamArguments.NumFrameArguments].pData = (void*)(&sliceInfo);
            inputStreamArguments.NumFrameArguments++;
        }
    }
#endif
    {
        memset(&outputStreamArguments, 0, sizeof(D3D12_VIDEO_DECODE_OUTPUT_STREAM_ARGUMENTS));
        outputStreamArguments.ConversionArguments = {};
        outputStreamArguments.OutputSubresource = 0;
        outputStreamArguments.pOutputTexture2D = _context->frame->texture;
    }
    {
        ID3D12Resource* buffer = bundle->buffer;
        uint8_t* address = nullptr;
        buffer->Map(0, nullptr, (void**)&address);
        if (D3DVA_FAILED(hr) || !address)
        {
            D3DVA_LOG_ERROR << "ID3D12Resource::Map fail";
            assert(false);
            return;
        }
        memcpy(address, _context->pack->GetData(), _context->pack->GetSize());
        buffer->Unmap(0, nullptr);
        inputStreamArguments.CompressedBitstream.pBuffer = buffer;
        inputStreamArguments.CompressedBitstream.Offset = 0;
        inputStreamArguments.CompressedBitstream.Size = _context->pack->GetSize();
    }
    if (GetDecoderContext()->DecodeFrame(inputStreamArguments, outputStreamArguments, _context->frame, bundle))
    {
        PushFrame(_context->frame);
    }
}

} // namespace Codec
} // namespace Mmp