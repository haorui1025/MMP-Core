//
// D3DVADecoderContext.h
//
// Library: Common
// Package: Codec
// Module:  D3DVA
//

#pragma once

#include <mutex>
#include <deque>
#include <vector>
#include <memory>

#include "D3DVACommon.h"


namespace Mmp
{
namespace Codec
{

class D3DVAEncoderContext : public std::enable_shared_from_this<D3DVAEncoderContext>
{
public:
    using ptr = std::shared_ptr<D3DVAEncoderContext>;
public:
    D3DVAEncoderContext();
    virtual ~D3DVAEncoderContext();
};

} // namespace Codec
} // namespace Mmp