//
// D3DVACommon.h
//
// Library: Common
// Package: Codec
// Module:  D3DVA
// 
// See also : 
//     Work submission in Direct3D 12 : https://learn.microsoft.com/en-us/windows/win32/direct3d12/command-queues-and-command-lists
//

#pragma once

#include "Common/Common.h"
#include "Common/D3DDevice.h"
#include "Codec/CodecCommon.h"
#include "Codec/AbstractEncoder.h"
#include "Codec/AbstractDecorder.h"

#include <Unknwnbase.h>

#define D3DVA_LOG_TRACE   MMP_MLOG_TRACE("D3DVA")    
#define D3DVA_LOG_DEBUG   MMP_MLOG_DEBUG("D3DVA")    
#define D3DVA_LOG_INFO    MMP_MLOG_INFO("D3DVA")     
#define D3DVA_LOG_WARN    MMP_MLOG_WARN("D3DVA")     
#define D3DVA_LOG_ERROR   MMP_MLOG_ERROR("D3DVA")    
#define D3DVA_LOG_FATAL   MMP_MLOG_FATAL("D3DVA")   

#define ENABLE_CODEC_D3DVA_TRACE_PROC 0

namespace Mmp
{
namespace Codec
{

class D3DVADecoderParams
{
public:
    D3DVADecoderParams();
    ~D3DVADecoderParams() = default;
public:
    PixelFormat                        format;
    uint32_t                           width;
    uint32_t                           height;
    uint32_t                           virWidth;
    uint32_t                           virHeight;
    uint32_t                           bitdepth;
    CodecType                          codecType;
    size_t                             maxRefNum;
};
bool operator==(const D3DVADecoderParams& left, const D3DVADecoderParams& right);
bool operator!=(const D3DVADecoderParams& left, const D3DVADecoderParams& right);

class D3DVAEncoderParams
{
public:
    D3DVAEncoderParams();
    ~D3DVAEncoderParams() = default;
public:
    uint32_t                           width;
    uint32_t                           height;
    uint32_t                           virWidth;
    uint32_t                           virHeight;
    CodecType                          codecType;
};
bool operator==(const D3DVAEncoderParams& left, const D3DVAEncoderParams& right);
bool operator!=(const D3DVAEncoderParams& left, const D3DVAEncoderParams& right);

class D3DVAFenceContext
{
public:
    using ptr = std::shared_ptr<D3DVAFenceContext>;
public:
    D3DVAFenceContext();
    ~D3DVAFenceContext();
public:
    uint64_t GetCompletedValue();
    uint64_t GetFenceValue();
    void UpdateFenceValue(uint64_t value);
    bool Wait();
    HRESULT Wait(ID3D12CommandQueue* queue);
    HRESULT Signal(ID3D12CommandQueue* queue);
public:
    std::mutex mtx;
    std::string tag;
    ID3D12Fence* fence;
    HANDLE event;
    uint64_t fenceValue;
};

class D3DVACommandBundle
{
public:
    using ptr = std::shared_ptr<D3DVACommandBundle>;
public:
    D3DVACommandBundle();
    ~D3DVACommandBundle();
public:
    ID3D12CommandAllocator* allocator;
    ID3D12Resource*         buffer;
    uint64_t                fenceValue;
};

class D3DVAVideoFrame
{
public:
    using ptr = std::shared_ptr<D3DVAVideoFrame>;
public:
    using DestroyProxy = std::function<void(ID3D12Resource* texture, D3DVAFenceContext::ptr sync)>;
public:
    D3DVAVideoFrame();
    ~D3DVAVideoFrame();
public:
    uint64_t trackId;
    ID3D12Resource* texture;
    D3DVAFenceContext::ptr sync;
    DestroyProxy destoryProxy;
};

} // namespace Codec
} // namespace Mmp