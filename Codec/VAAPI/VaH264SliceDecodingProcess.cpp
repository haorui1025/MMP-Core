#include "VaH264SliceDecodingProcess.h"

namespace Mmp
{
namespace Codec
{

VAH264DecodePictureContext::VAH264DecodePictureContext()
{

}

VAH264DecodePictureContext::~VAH264DecodePictureContext()
{
    if (surface != VA_INVALID_ID)
    {
#if ENABLE_VA_BUFFER_TRACE
    VAAPI_LOG_INFO << "Destroy Surface id(" << id << ") FrameNum(" << FrameNum << ") referenceFlag(" << referenceFlag << ") surface is: " << surface;
#endif
    }
}

void VAH264DecodePictureContext::SetSurface(VASurfaceID surface)
{
    assert(surface != VA_INVALID_ID);
    this->surface = surface;
#if ENABLE_VA_BUFFER_TRACE
    VAAPI_LOG_INFO << "Create Surface id(" << id << ") FrameNum(" << FrameNum << ") referenceFlag(" << referenceFlag << ") surface is: " << surface;
#endif
}

H264PictureContext::ptr VaH264SliceDecodingProcess::CreatePictureContext()
{
    return std::make_shared<VAH264DecodePictureContext>();
}

void VaH264SliceDecodingProcess::OnBeforeDecodeReferencePictureMarkingProcess()
{
    if (onBeforeDecodeReferencePictureMarkingProcessFunc)
    {
        onBeforeDecodeReferencePictureMarkingProcessFunc();
    }
}

} // namespace Codec
} // namespace Mmp