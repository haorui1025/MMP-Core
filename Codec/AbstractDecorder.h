//
// AbstractDecorder.h
//
// Library: Codec
// Package: Codec
// Module:  Decoder
// 

#pragma once

#include <string>
#include <memory>

#include "CodecCommon.h"

namespace Mmp
{
namespace Codec
{
class AbstractDecoder
{
public:
    using ptr = std::shared_ptr<AbstractDecoder>;
public:
    virtual ~AbstractDecoder() = default;
public:
    /**
     * @brief      设置解码器参数
     * @param[in]  parameter : 根据编码器的类型决定
     * @param[in]  property  : 属性名称
     */
    virtual void SetParameter(Any parameter, const std::string& property = std::string()) {}
    /**
     * @brief      获取解码器参数
     * @param[in]  property : 属性名称
     */
    virtual Any GetParamter(const std::string& property = std::string()) { return Any(); }
    /**
     * @brief 初始化
     */
    virtual bool Init() { return true; };
    /**
     * @brief 重置
     */
    virtual void Uninit() {};
    /**
     * @brief 开始运行 
     */
    virtual bool Start() { return true; }
    /**
     * @brief 结束运行 
     */
    virtual void Stop() {};
    /**
     * @brief 提交压缩数据
     */
    virtual bool Push(AbstractPack::ptr pack) = 0;
    /**
     * @brief 获取解压数据
     */
    virtual bool Pop(AbstractFrame::ptr& frame) = 0;
    /**
     * @brief 是否可提交
     */
    virtual bool CanPush() = 0;
    /**
     * @brief 是否可获取
     */
    virtual bool CanPop() = 0;
    /**
     * @brief 获取解码器描述信息
     */
    virtual const std::string& Description() = 0;
};

static constexpr char kEnableDecoderAFBC[]     = "enableDecoderAFBC";   // bool

} // namespace Codec
} // namespace Mmp