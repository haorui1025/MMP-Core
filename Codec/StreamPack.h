//
// StreamPack.h
//
// Library: Codec
// Package: Codec
// Module:  Frame
// 

#pragma once

#include <chrono>

#include "Common/AbstractPack.h"
#include "Common/SharedDataDecorator.h"

#include "CodecCommon.h"

namespace Mmp
{
namespace Codec
{

class StreamPack : public SharedDataDecoratorTemplate<AbstractPack>
{
public:
    using ptr = std::shared_ptr<StreamPack>;
public:
    explicit StreamPack(CodecType type, size_t size, AbstractAllocateMethod::ptr allocateMethod = nullptr);
    explicit StreamPack(CodecType type, AbstractSharedData::ptr data);
public:
    CodecType                  type;
    std::chrono::milliseconds  pts;
    std::chrono::milliseconds  dts;
};

} // namespace Codec
} // namespace Mmp