//
// RKH264Encoder.h
//
// Library: Common
// Package: Codec
// Module:  Rockchip
// 

#include "RKEncoder.h"

namespace Mmp
{
namespace Codec
{

class RKH264Encoder : public RKEncoder
{
public:
    using ptr = std::shared_ptr<RKH264Encoder>;
public:
    RKH264Encoder();
    ~RKH264Encoder();
public:
    void SetParameter(Any parameter, const std::string& property) override;
    Any GetParamter(const std::string& property) override;
public:
    bool ReinitEncConfig() override;
    CodecType GetCodecType() override;
public:
    int8_t   _fixQp;
    int8_t   _initQp;
    int8_t   _maxQp;
    int8_t   _minQp;
    uint8_t  _porfile;
    uint8_t  _levelIdc;
};

} // namespace Codec
} // namespace Mmp
