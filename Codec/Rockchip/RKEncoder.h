//
// RKEncoder.h
//
// Library: Common
// Package: Codec
// Module:  Rockchip
// 
// Note : This file is modified from mpp/test/mpi_enc_test.c
//        Date : 2024-01-06
//        Hash : 5f1cd0f12d643f017aea61a196b3b766df511bf6
// Copyright 2015 Rockchip Electronics Co. LTD
//

#pragma once

#include <mutex>

#include "RKCommon.h"
#include "Common/TaskQueue.h"

#include "StreamPack.h"

namespace Mmp
{
namespace Codec
{

class RkBufferContext;

class RKEncoder : public AbstractEncoder
{
public:
    using ptr = std::shared_ptr<AbstractEncoder>;
public:
    RKEncoder();
    ~RKEncoder();
public:
    void SetParameter(Any parameter, const std::string& property) override;
    Any GetParamter(const std::string& property) override;
    bool Init() override;
    void Uninit() override;
    bool Start() override;
    void Stop() override;
    bool Push(AbstractFrame::ptr frame) override;
    bool Pop(AbstractPack::ptr& pack) override;
    bool CanPush() override;
    bool CanPop() override;
    const std::string& Description() override;
public:
    bool CreateMPI();
    void DestroyMPI();
    bool InitRkFrame();
    void UninitRkFrame();
    virtual bool ReinitEncConfig();
    virtual CodecType GetCodecType() = 0;
private:
    void OnFrameChanged(PixelsInfo info, const MppFrame& frame);
private:
    uint64_t _bps;
    uint64_t _maxBps;
    uint64_t _minBps;
    uint32_t _width;
    uint32_t _height;
    uint32_t _gop;
private:
    std::mutex _mtx;
    MppFrame   _frame;
    PixelsInfo _lastInfo;
private:
    std::atomic<bool>  _runing;
    std::mutex         _thMtx;
    TaskQueue::ptr     _thread;
private:
    std::mutex                     _bufMtx;
    std::deque<StreamPack::ptr>    _buffers;
protected:
    std::mutex      _mpiMtx;
    MppApi*         _mpi;
protected:
    MppCtx          _ctx;
    RateControlMode _rcMode;
private:
    std::shared_ptr<RkBufferContext> _frameBufferContext;
};

} // namespace Codec
} // namespace Mmp