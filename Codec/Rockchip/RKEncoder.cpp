#include "RKEncoder.h"

#include <cassert>
#include <cstdint>
#include <memory.h>

#include "Common/PixelFormat.h"
#include "Common/DmaHeapAllocateMethod.h"
#include "Common/AbstractAllocateMethod.h"

#include "StreamFrame.h"
#include "RKTranslator.h"
#include "RkStreamFrame.h"
#include "RKUtil.h"

namespace Mmp
{
namespace Codec
{

/**
 * @brief
 *      1 - 辅助将  DmaHeapAllocateMethod 转化为 MppFrame
 *      2 - 基于 DMA 创建 MppFrame, 用于拷贝普通内存
 */
class RkBufferContext
{
public:
    using ptr = std::shared_ptr<RkBufferContext>;
public:
    RkBufferContext();
    ~RkBufferContext();
public:
    void Init(DmaHeapAllocateMethod::ptr alloc, size_t size);
public:
    MppBufferGroup _frameGrp;
    DmaHeapAllocateMethod::ptr _alloc;
    MppBuffer _buffer;
    size_t _size;
};

RkBufferContext::RkBufferContext()
{
    {
        _buffer = nullptr;
        _size = 0;
    }
    MPP_RET rkRet = MPP_RET::MPP_OK;
    if (RK_OP_FAIL(mpp_buffer_group_get_external(&_frameGrp, MPP_BUFFER_TYPE_DMA_HEAP)))
    {
        RK_LOG_ERROR << "mpp_buffer_group_get_external fail, error is: " << RkMppRetToStr(rkRet);
        assert(false);
    }
}

RkBufferContext::~RkBufferContext()
{
    MPP_RET rkRet = MPP_RET::MPP_OK;
    if (_buffer)
    {
        if (RK_OP_FAIL(mpp_buffer_put(_buffer)))
        {
            RK_LOG_ERROR << "mpp_buffer_put fail, error is: " << RkMppRetToStr(rkRet);
            assert(false);
        }
    }
    if (_frameGrp)
    {
        if (RK_OP_FAIL(mpp_buffer_group_put(_frameGrp)))
        {
            RK_LOG_ERROR << "mpp_buffer_group_put fail, error is: " << RkMppRetToStr(rkRet);
            assert(false);
        }
        _frameGrp = nullptr;
    }
}

void RkBufferContext::Init(DmaHeapAllocateMethod::ptr alloc, size_t size)
{
    MPP_RET rkRet = MPP_RET::MPP_OK;
    if (!alloc)
    {
        alloc = std::make_shared<DmaHeapAllocateMethod>();
        alloc->Malloc(size);
    }
    _alloc = alloc;
    _size = size;
    MppBufferInfo commit = {};
    {
        commit.type = MppBufferType::MPP_BUFFER_TYPE_DMA_HEAP;
        commit.size = size;
        commit.fd = alloc->GetFd();
        commit.index = commit.fd;
    }
    if (RK_OP_FAIL(mpp_buffer_commit(_frameGrp, &commit)))
    {
        RK_LOG_ERROR << "mpp_buffer_commit fail, error is: " << RkMppRetToStr(rkRet);
        assert(false);
    }
    if (RK_OP_FAIL(mpp_buffer_get(_frameGrp, &_buffer, size)))
    {
        RK_LOG_ERROR << "mpp_buffer_get fail, error is: " << RkMppRetToStr(rkRet);
        assert(false); 
    }
}

} // namespace Codec
} // namespace Mmp

namespace Mmp
{
namespace Codec
{

class RkPackAllocateMethod : public AbstractAllocateMethod
{
public:
    using ptr = std::shared_ptr<RkPackAllocateMethod>;
public:
    RkPackAllocateMethod(MppPacket packet);
    ~RkPackAllocateMethod();
public:
    void* Malloc(size_t size) override;
    void* Resize(void* data, size_t size) override;
    void* GetAddress(uint64_t offset) override;
    const std::string& Tag() override;
private:
    MppPacket packet;
};

RkPackAllocateMethod::RkPackAllocateMethod(MppPacket packet)
{
    this->packet = packet;
}

RkPackAllocateMethod::~RkPackAllocateMethod()
{
    if (packet)
    {
        mpp_packet_deinit(&packet);
    }
}

void* RkPackAllocateMethod::Malloc(size_t size)
{
    if (packet)
    {
        return mpp_packet_get_pos(packet);
    }
    else
    {
        return nullptr;
    }
}

void* RkPackAllocateMethod::Resize(void* data, size_t size)
{
    assert(false);
    return Malloc(size);
}

void* RkPackAllocateMethod::GetAddress(uint64_t offset)
{
    if (packet)
    {
        return (uint8_t*)mpp_packet_get_pos(packet) + offset;
    }
    else
    {
        return nullptr;
    }
}

const std::string& RkPackAllocateMethod::Tag()
{
    static std::string tag = "RkPackAllocateMethod";
    return tag;
}

} // namespace Codec
} // namespace Mmp

namespace Mmp
{
namespace Codec
{

RKEncoder::RKEncoder()
{
    _bps = 4 * 1024 * 1024; // 4Mb/s
    _maxBps = 0;
    _minBps = 0;
    _gop = 60; // 30 fps 2 second, 60 fps 1 second
}

RKEncoder::~RKEncoder()
{

}

void RKEncoder::SetParameter(Any parameter, const std::string& property)
{
    if (property == kRateControlMode)
    {
        _rcMode = RefAnyCast<RateControlMode>(parameter);
    }
    else if (property == kGop)
    {
        _gop = RefAnyCast<uint32_t>(parameter);
    }
    else if (property == kBps)
    {
        _bps = RefAnyCast<uint64_t>(parameter);
    }
    else
    {
        AbstractEncoder::SetParameter(parameter, property);
    }
}

Any RKEncoder::GetParamter(const std::string& property)
{
    if (property == kRateControlMode)
    {
        return _rcMode;
    }
    else if (property == kGop)
    {
        return _gop;
    }
    else if (property == kBps)
    {
        return _bps;
    }
    else
    {
        return AbstractEncoder::GetParamter(property);
    }
}

bool RKEncoder::Init()
{
    std::lock_guard<std::mutex> lock(_mtx);
    RK_LOG_INFO << "Try to init RKEncoder";
    if (!CreateMPI())
    {
        RK_LOG_ERROR << "CreateMPI fail";
        assert(false);
        goto END;
    }
    if (!InitRkFrame())
    {
        RK_LOG_ERROR << "InitRkFrame fail";
        assert(false);
        goto END1;
    }
    return true;
END1:
    DestroyMPI();
END:
    return false;
}

void RKEncoder::Uninit()
{
    std::lock_guard<std::mutex> lock(_mtx);
    RK_LOG_INFO << "Uninit RKEncoder";
    UninitRkFrame();
    DestroyMPI();
    _lastInfo = PixelsInfo();
    return;
}

bool RKEncoder::Start()
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (_thread)
    {
        assert(false);
        return false;
    }
    _runing = true;
    _thread = std::make_shared<TaskQueue>();
    _thread->Start();
    _thread->Commit(std::make_shared<Promise<void>>([this]() -> void
    {
        RK_LOG_INFO << "RK_ENC_THD begin";
        do
        {
            MppPacket packet = nullptr;
            {
                std::lock_guard<std::mutex> lock(_mpiMtx);
                if (!_mpi)
                {
                    break;
                }
                MPP_RET rkRet = MPP_RET::MPP_OK;
                if (RK_OP_FAIL(_mpi->encode_get_packet(_ctx, &packet)))
                {
                    if (rkRet != MPP_RET::MPP_ERR_TIMEOUT)
                    {
                        RK_LOG_WARN << "MppApi::encode_get_packet fail, error is: " << RkMppRetToStr(rkRet);
                        assert(false);
                    }
                }
            }
            if (!packet)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(1));
                continue;
            }
            {
                std::lock_guard<std::mutex> lock(_bufMtx);
                RkPackAllocateMethod::ptr allocate = std::make_shared<RkPackAllocateMethod>(packet);
                StreamPack::ptr streamPack = std::make_shared<StreamPack>(GetCodecType(), mpp_packet_get_length(packet), allocate);
                _buffers.push_back(streamPack);
            }
        } while (_runing);
        RK_LOG_INFO << "RK_ENC_THD end";
    }));
    return true;

}

void RKEncoder::Stop()
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (!_thread)
    {
        assert(false);
    }
    _runing = false;
    _thread->Stop();
    _thread.reset();
}

bool RKEncoder::Push(AbstractFrame::ptr frame)
{
    std::lock_guard<std::mutex> lock(_mtx);
    StreamFrame::ptr streamFrame = std::dynamic_pointer_cast<StreamFrame>(frame);
    if (streamFrame)
    {
        bool res = true;
        MPP_RET rkRet = MPP_OK;
        MppFrame rkFrame;
        RkStreamFrame::ptr rkStreamFrame = std::dynamic_pointer_cast<RkStreamFrame>(frame);
        DmaHeapAllocateMethod::ptr dmaAlloc = std::dynamic_pointer_cast<DmaHeapAllocateMethod>(frame->GetAllocateMethod());
        RkBufferContext::ptr bufContext;
        // 1 - 原生 MppFrame , 零拷贝 
        if (rkStreamFrame)
        {
            rkFrame = rkStreamFrame->frame;
        }
        // 2 - DmaHeapAllocateMethod, 构造 MppFrame, 零拷贝
        else if (dmaAlloc)
        {
            bufContext = std::make_shared<RkBufferContext>();
            bufContext->Init(dmaAlloc, frame->GetSize());
            mpp_frame_set_width(_frame, streamFrame->info.width);
            mpp_frame_set_height(_frame, streamFrame->info.height);
            mpp_frame_set_ver_stride(_frame, streamFrame->info.width);
            mpp_frame_set_hor_stride(_frame, streamFrame->info.height);
            if (dmaAlloc->GetFlags() & DmaHeapAllocateMethod::kArmAFBC)
            {
                mpp_frame_set_buf_size(_frame, GetRkFrameSize(streamFrame->info, true));
            }
            else
            {
                mpp_frame_set_buf_size(_frame, streamFrame->info.width * streamFrame->info.height * 3 / 2);
            }
            mpp_frame_set_buffer(_frame, bufContext->_buffer);
            mpp_frame_set_colorspace(_frame, GenerateMppFrameColorSpace(streamFrame->info.colorGamut, streamFrame->info.quantRange));
            uint32_t format = (uint32_t)PixelFormatToMppFrameFormat(streamFrame->info.format);
            {
                if (dmaAlloc->GetFlags() & DmaHeapAllocateMethod::kArmAFBC)
                {
                    format |= MPP_FRAME_FBC_AFBC_V2;
                }
                if (MMP_DYNAMIC_RANGE_HDR(streamFrame->info.dynamicRange))
                {
                    format |= MPP_FRAME_HDR;
                }
            }
            mpp_frame_set_fmt(_frame, MppFrameFormat(format));
            mpp_frame_set_eos(_frame, 0);
            rkFrame = _frame;
        }
        // 3 - 普通内存, 拷贝
        else
        {
            mpp_frame_set_width(_frame, streamFrame->info.width);
            mpp_frame_set_height(_frame, streamFrame->info.height);
            mpp_frame_set_ver_stride(_frame, streamFrame->info.width);
            mpp_frame_set_hor_stride(_frame, streamFrame->info.height);
            mpp_frame_set_buf_size(_frame, streamFrame->info.width * streamFrame->info.height * 1.5);
            mpp_frame_set_colorspace(_frame, GenerateMppFrameColorSpace(streamFrame->info.colorGamut, streamFrame->info.quantRange));
            if (MMP_DYNAMIC_RANGE_HDR(streamFrame->info.dynamicRange))
            {
                mpp_frame_set_fmt(_frame, (MppFrameFormat)(PixelFormatToMppFrameFormat(streamFrame->info.format) | MPP_FRAME_HDR_MASK));
            }
            else 
            {
                mpp_frame_set_fmt(_frame, PixelFormatToMppFrameFormat(streamFrame->info.format));
            }
            mpp_frame_set_eos(_frame, 0);
            if (!_frameBufferContext || _frameBufferContext->_size != mpp_frame_get_buf_size(_frame))
            {
                _frameBufferContext = std::make_shared<RkBufferContext>();
                _frameBufferContext->Init(nullptr, mpp_frame_get_buf_size(_frame));
            }
            mpp_frame_set_buffer(_frame, _frameBufferContext->_buffer);
            // Hint : 高开销, 原则上尽量避免
            memcpy((uint8_t *)_frameBufferContext->_alloc->GetAddress(0), (uint8_t*)frame->GetAllocateMethod()->GetAddress(0), frame->GetSize());
            rkFrame = _frame;
        }
        if (streamFrame->info != _lastInfo)
        {
            _lastInfo = streamFrame->info;
            OnFrameChanged(_lastInfo, rkFrame);
        }
        if (RK_OP_FAIL(_mpi->encode_put_frame(_ctx, rkFrame)))
        {
            RK_LOG_ERROR << "MppApi::encode_put_frame fail, error is: " << RkMppRetToStr(rkRet);
            res = false;
            assert(false);
        }
        if (!rkStreamFrame)
        {
            //
            // Hint        : mpp_frame_set_buffer 会使 MppBuffer 的引用计数增加
            // Bad Solution: 所以在 MppApi::encode_put_frame 后手动将 mpp_frame_set_buffer 的 buf 设置为
            //               nullptr, 保证释放 GRP 时引用计数是正确的
            //
            mpp_frame_set_buffer(_frame, nullptr);
        }
        return res;
    }
    return false;
}

bool RKEncoder::Pop(AbstractPack::ptr& pack)
{
    std::lock_guard<std::mutex> lock(_bufMtx);
    if (_buffers.empty())
    {
        return false;
    }
    else
    {
        pack = _buffers.front();
        _buffers.pop_front();
        return true;
    }
}

bool RKEncoder::CanPush()
{
    return true;
}

bool RKEncoder::CanPop()
{
    std::lock_guard<std::mutex> lock(_bufMtx);
    return !_buffers.empty();
}

const std::string& RKEncoder::Description()
{
    static std::string description = "RKEncoder";
    return description;
}

bool RKEncoder::CreateMPI()
{
    std::lock_guard<std::mutex> lock(_mpiMtx);
    RK_LOG_INFO << "-- Create MPI";
    MppCodingType codecType = CodecTypeToRkType(GetCodecType());
    MPP_RET rkRet = MPP_OK;
    uint64_t param = 0;
    if (codecType == MppCodingType::MPP_VIDEO_CodingUnused)
    {
        RK_LOG_ERROR << "Unknown codec type, codec type is: " << GetCodecType();
        assert(false);
        goto END;
    }
    if (RK_OP_FAIL(mpp_create(&_ctx, &_mpi)))
    {
        RK_LOG_ERROR << "mpp_create fail, error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END1;
    }
    if (RK_OP_FAIL(mpp_init(_ctx, MppCtxType::MPP_CTX_ENC, codecType)))
    {
        RK_LOG_ERROR << "mpp_init fail, error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END2;
    }
    param = MPP_POLL_BLOCK;
    if (RK_OP_FAIL(_mpi->control(_ctx, MPP_SET_INPUT_TIMEOUT, &param)))
    {
        RK_LOG_ERROR << "mip control fail, cmd is: " << MpiCmdToStr(MPP_SET_INPUT_TIMEOUT) <<  " ,error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END1;
    }
    param = 1;
    if (RK_OP_FAIL(_mpi->control(_ctx, MPP_SET_OUTPUT_TIMEOUT, &param)))
    {
        RK_LOG_ERROR << "mip control fail, cmd is: " << MpiCmdToStr(MPP_SET_OUTPUT_TIMEOUT) <<  " ,error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END1;
    }
    if (!ReinitEncConfig())
    {
        assert(false);
    }
    _mpi->reset(_ctx);
    return true;
END2:
    mpp_destroy(_ctx);
END1:
    _ctx = nullptr;
    _mpi = nullptr;
END:
    return false;
}

void RKEncoder::DestroyMPI()
{
    std::lock_guard<std::mutex> lock(_mpiMtx);
    RK_LOG_INFO << "-- DestroyMPI";
    if (_ctx)
    {
        mpp_destroy(_ctx);
    }
    _ctx = nullptr;
    _mpi = nullptr;
}

bool RKEncoder::InitRkFrame()
{
    RK_LOG_INFO << "-- InitRkFrame";
    MPP_RET rkRet = MPP_OK;
    if (RK_OP_FAIL(mpp_frame_init(&_frame)))
    {
        RK_LOG_ERROR << "mpp_frame_init fail, error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END;
    }
    return true;
END:
    return false;
}

void RKEncoder::UninitRkFrame()
{
    RK_LOG_INFO << "-- UninitRkFrame";
    if (_frame)
    {
        mpp_frame_deinit(&_frame);
    }
    _frame = nullptr;
}

bool RKEncoder::ReinitEncConfig()
{
    MPP_RET rkRet = MPP_OK;
    MppEncCfg  cfg;
    if (RK_OP_FAIL(mpp_enc_cfg_init(&cfg)))
    {
        RK_LOG_ERROR << "mpp_enc_cfg_init fail, error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END;
    }
    if (RK_OP_FAIL(_mpi->control(_ctx, MPP_ENC_GET_CFG, cfg)))
    {
        RK_LOG_ERROR << "MppApi::control fail, cmd is:" << MpiCmdToStr(MPP_ENC_GET_CFG)  << " , error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END;
    }
    mpp_enc_cfg_set_s32(cfg, "rc:gop", _gop);
    mpp_enc_cfg_set_s32(cfg, "split:mode", MPP_ENC_SPLIT_NONE);
    mpp_enc_cfg_set_s32(cfg, "rc:bps_target", _bps);
    switch (_rcMode)
    {
        case RateControlMode::FIXQP:
        {
            // nothing to do
            break;
        }
        case RateControlMode::VBR: /* passthrough */
        case RateControlMode::AVBR:
        {
            mpp_enc_cfg_set_s32(cfg, "rc:bps_max", _maxBps ? _maxBps : _bps * 17/16);
            mpp_enc_cfg_set_s32(cfg, "rc:bps_min", _minBps ? _minBps : _bps * 1/16);
            break;
        }
        case RateControlMode::CBR: /* passthrough */
        default:
        {
            mpp_enc_cfg_set_s32(cfg, "rc:bps_max", _maxBps ? _maxBps : _bps * 17/16);
            mpp_enc_cfg_set_s32(cfg, "rc:bps_min", _minBps ? _minBps : _bps * 15/16);
            break;
        }
    }
    mpp_enc_cfg_set_u32(cfg, "rc:drop_mode", MPP_ENC_RC_DROP_FRM_DISABLED);
    mpp_enc_cfg_set_u32(cfg, "rc:drop_thd", 20);        /* 20% of max bps */
    mpp_enc_cfg_set_u32(cfg, "rc:drop_gap", 1);         /* Do not continuous drop frame */
    if (RK_OP_FAIL(_mpi->control(_ctx, MPP_ENC_SET_CFG, cfg)))
    {
        RK_LOG_ERROR << "MppApi::control fail, cmd is:" << MpiCmdToStr(MPP_ENC_SET_CFG)  << " , error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END;
    }
    mpp_enc_cfg_deinit(cfg);
    return true;
END:
    return false;
}

void RKEncoder::OnFrameChanged(PixelsInfo info, const MppFrame& frame)
{
    MPP_RET rkRet = MPP_OK;
    MppEncCfg  cfg;
    MppFrameFormat tmp;
    if (RK_OP_FAIL(mpp_enc_cfg_init(&cfg)))
    {
        RK_LOG_ERROR << "mpp_enc_cfg_init fail, error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END;
    }
    if (RK_OP_FAIL(_mpi->control(_ctx, MPP_ENC_GET_CFG, cfg)))
    {
        RK_LOG_ERROR << "MppApi::control fail, cmd is:" << MpiCmdToStr(MPP_ENC_GET_CFG)  << " , error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END;
    }
    mpp_enc_cfg_set_s32(cfg, "prep:width", info.width);
    mpp_enc_cfg_set_s32(cfg, "prep:height", info.height);
    if (info.horStride != -1)
    {
        mpp_enc_cfg_set_s32(cfg, "prep:hor_stride",  info.horStride);
    }
    else
    {
        mpp_enc_cfg_set_s32(cfg, "prep:hor_stride",  info.width);
    }
    if (info.virStride != -1)
    {
        mpp_enc_cfg_set_s32(cfg, "prep:ver_stride", info.virStride);
    }
    else
    {
        mpp_enc_cfg_set_s32(cfg, "prep:ver_stride", info.height);
    }
    if (info.colorGamut != ColorGamut::ColorGamut_UNSPECIFIED)
    {
        // Hint : 保持一致就可以了, 其他情况暂不考虑, 比较复杂
        mpp_enc_cfg_set_s32(cfg, "prep:colorspace", GenerateMppFrameColorSpace(info.colorGamut, info.quantRange));
        mpp_enc_cfg_set_s32(cfg, "prep:colorprim", GenerateMppFrameColorPrimaries(info.colorGamut, info.quantRange));
        mpp_enc_cfg_set_s32(cfg, "prep:colortrc", GenerateMppFrameColorTransferCharacteristic(info.colorGamut, info.bitdepth));
    }
    if (info.quantRange == QuantRange::FULL)
    {
        mpp_enc_cfg_set_s32(cfg, "prep:colorrange", 2);
    }
    else if (info.quantRange == QuantRange::LIMIT)
    {
        mpp_enc_cfg_set_s32(cfg, "prep:colorrange", 1);
    }
    mpp_enc_cfg_set_s32(cfg, "prep:format", mpp_frame_get_fmt(frame));
    if (RK_OP_FAIL(_mpi->control(_ctx, MPP_ENC_SET_CFG, cfg)))
    {
        RK_LOG_ERROR << "MppApi::control fail, cmd is:" << MpiCmdToStr(MPP_ENC_SET_CFG)  << " , error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END;
    }
    if (RK_OP_FAIL(_mpi->control(_ctx, MPP_ENC_SET_IDR_FRAME, nullptr)))
    {
        RK_LOG_ERROR << "MppApi::control fail, cmd is:" << MpiCmdToStr(MPP_ENC_SET_IDR_FRAME)  << " , error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END;
    }
    mpp_enc_cfg_deinit(cfg);
    return;
END:
    return; 
}

} // namespace Codec
} // namespace Mmp