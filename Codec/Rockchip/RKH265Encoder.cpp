#include "RKH265Encoder.h"

#include "RKUtil.h"

namespace Mmp
{
namespace Codec
{

RKH265Encoder::RKH265Encoder()
{
    _fixQp = 26;
    _initQp = -1;
    _maxQp = 51;
    _minQp = 10;
    _porfile = 0;
    _levelIdc = 0;
}

RKH265Encoder::~RKH265Encoder()
{

}

void RKH265Encoder::SetParameter(Any parameter, const std::string& property)
{
    if (property == kH26xLevelIdc)
    {
        _levelIdc = RefAnyCast<uint8_t>(parameter);
    }
    else if (property == kH26xFixQp)
    {
        _fixQp = RefAnyCast<int8_t>(parameter);
    }
    else if (property == kH26xInitQp)
    {
        _initQp = RefAnyCast<int8_t>(parameter);
    }
    else if (property == kH26xMaxQp)
    {
        _maxQp = RefAnyCast<int8_t>(parameter);
    }
    else if (property == kH26xMinQp)
    {
        _minQp = RefAnyCast<int8_t>(parameter);
    }
    else
    {
        RKEncoder::SetParameter(parameter, property);
    }
}

Any RKH265Encoder::GetParamter(const std::string& property)
{
    if (property == kH26xProfile)
    {
        return _porfile;
    }
    else if (property == kH26xLevelIdc)
    {
        return _levelIdc;
    }
    else if (property == kH26xFixQp)
    {
        return _fixQp;
    }
    else if (property == kH26xInitQp)
    {
        return _initQp;
    }
    else if (property == kH26xMaxQp)
    {
        return _maxQp;
    }
    else if (property == kH26xMinQp)
    {
        return _minQp;
    }
    else
    {
        return RKEncoder::GetParamter(property);
    }
}


bool RKH265Encoder::ReinitEncConfig()
{
    assert(_mpi);
    MPP_RET rkRet = MPP_OK;
    MppEncCfg  cfg;
    if (RK_OP_FAIL(mpp_enc_cfg_init(&cfg)))
    {
        RK_LOG_ERROR << "mpp_enc_cfg_init fail, error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END;
    }
    if (RK_OP_FAIL(_mpi->control(_ctx, MPP_ENC_GET_CFG, cfg)))
    {
        RK_LOG_ERROR << "MppApi::control fail, cmd is:" << MpiCmdToStr(MPP_ENC_GET_CFG)  << " , error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END;
    }
    // not support for now
    {
        // mpp_enc_cfg_set_s32(cfg, "h265:profile", _porfile);
        // mpp_enc_cfg_set_s32(cfg, "h265:level", _levelIdc);
    }
    switch (_rcMode)
    {
        case RateControlMode::FIXQP:
        {
            mpp_enc_cfg_set_s32(cfg, "rc:qp_init", _fixQp);
            mpp_enc_cfg_set_s32(cfg, "rc:qp_max", _fixQp);
            mpp_enc_cfg_set_s32(cfg, "rc:qp_min", _fixQp);
            mpp_enc_cfg_set_s32(cfg, "rc:qp_max_i", _fixQp);
            mpp_enc_cfg_set_s32(cfg, "rc:qp_min_i", _fixQp);
            mpp_enc_cfg_set_s32(cfg, "rc:qp_ip", 0);
            mpp_enc_cfg_set_s32(cfg, "rc:fqp_min_i", _fixQp);
            mpp_enc_cfg_set_s32(cfg, "rc:fqp_max_i", _fixQp);
            mpp_enc_cfg_set_s32(cfg, "rc:fqp_min_p", _fixQp);
            mpp_enc_cfg_set_s32(cfg, "rc:fqp_max_p", _fixQp);
            break;
        }
        case RateControlMode::VBR:
        case RateControlMode::AVBR:
        case RateControlMode::CBR:
        {
            mpp_enc_cfg_set_s32(cfg, "rc:qp_init", _initQp);
            mpp_enc_cfg_set_s32(cfg, "rc:qp_max", _maxQp);
            mpp_enc_cfg_set_s32(cfg, "rc:qp_min", _minQp);
            mpp_enc_cfg_set_s32(cfg, "rc:qp_max_i", _maxQp);
            mpp_enc_cfg_set_s32(cfg, "rc:qp_min_i", _minQp);
            mpp_enc_cfg_set_s32(cfg, "rc:qp_ip", 2);
            mpp_enc_cfg_set_s32(cfg, "rc:fqp_min_i", _minQp);
            mpp_enc_cfg_set_s32(cfg, "rc:fqp_max_i", _maxQp);
            mpp_enc_cfg_set_s32(cfg, "rc:fqp_min_p", _minQp);
            mpp_enc_cfg_set_s32(cfg, "rc:fqp_max_p", _maxQp);
            break;
        }
        default:
        {
            assert(false);
            break;
        }
    }
    if (RK_OP_FAIL(_mpi->control(_ctx, MPP_ENC_SET_CFG, cfg)))
    {
        RK_LOG_ERROR << "MppApi::control fail, cmd is:" << MpiCmdToStr(MPP_ENC_SET_CFG)  << " , error is: " << RkMppRetToStr(rkRet);
        assert(false);
        goto END;
    }
    mpp_enc_cfg_deinit(cfg);
    return RKEncoder::ReinitEncConfig();
END:
    return false;
}

CodecType RKH265Encoder::GetCodecType()
{
    return CodecType::H264;
}

} // namespace Codec
} // namespace Mmp