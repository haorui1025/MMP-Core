//
// RKTranslator.h
//
// Library: Common
// Package: Codec
// Module:  Rockchip
// 

#pragma once

#include "RKCommon.h"

namespace Mmp
{
namespace Codec
{

MppCodingType CodecTypeToRkType(CodecType type);

PixelFormat MppFrameFormatToPixformat(MppFrameFormat format);

MppFrameFormat PixelFormatToMppFrameFormat(PixelFormat format);

MppEncRcMode RateControlModeToMppEncRcMode(RateControlMode mode);

DynamicRange MppFrameFormatToDynamicRange(MppFrameFormat format);

ColorGamut MppFrameColorSpaceToColorGamut(MppFrameColorSpace colorPrimarie);

QuantRange MppFrameColorSpaceToQuantRange(MppFrameColorSpace colorPrimarie);

MppFrameColorSpace GenerateMppFrameColorSpace(ColorGamut colorGamut, QuantRange quantRange);

MppFrameColorTransferCharacteristic GenerateMppFrameColorTransferCharacteristic(ColorGamut colorGamut, size_t bits);

MppFrameColorPrimaries GenerateMppFrameColorPrimaries(ColorGamut colorGamut, QuantRange quantRange);

} // namespace Codec
} // namespace Mmp