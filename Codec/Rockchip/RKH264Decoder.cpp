#include "RKH264Decoder.h"

namespace Mmp
{
namespace Codec
{

CodecType RKH264Decoder::GetCodecType()
{
    return CodecType::H264;
}

} // namespace Codec
} // namespace Mmp