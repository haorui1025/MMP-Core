#include "RkStreamFrame.h"

namespace Mmp
{
namespace Codec
{

RkStreamFrame::RkStreamFrame(const PixelsInfo& info, AbstractAllocateMethod::ptr allocateMethod)
    : StreamFrame(info, allocateMethod)
{
    frame = nullptr;
}

RkStreamFrame::~RkStreamFrame()
{
    if (frame)
    {
        mpp_frame_deinit(&frame);
    }
}

} // namespace Codec
} // namespace Mmp