#include "RKTranslator.h"

#include "Common/PixelFormat.h"

#include <cassert>

namespace Mmp
{
namespace Codec
{

MppCodingType CodecTypeToRkType(CodecType type)
{
    switch (type) 
    {
        case CodecType::H264: return MppCodingType::MPP_VIDEO_CodingAVC;
        case CodecType::H265: return MppCodingType::MPP_VIDEO_CodingHEVC;
        case CodecType::VP8:  return MppCodingType::MPP_VIDEO_CodingVP8;
        case CodecType::VP9:  return MppCodingType::MPP_VIDEO_CodingVP9;
        case CodecType::AV1:  return MppCodingType::MPP_VIDEO_CodingAV1;
        default:
            assert(false);
            return MppCodingType::MPP_VIDEO_CodingUnused;
    }
}

PixelFormat MppFrameFormatToPixformat(MppFrameFormat format)
{
    switch (format)
    {
        case MppFrameFormat::MPP_FMT_YUV420SP:
            return PixelFormat::NV12;
        case MppFrameFormat::MPP_FMT_YUV420P:
            return PixelFormat::YUV420P;
        default:
            assert(false);
            return PixelFormat::NV12;
    }
}

MppFrameFormat PixelFormatToMppFrameFormat(PixelFormat format)
{
    switch (format)
    {
        case PixelFormat::NV12:
            return MppFrameFormat::MPP_FMT_YUV420SP;
        case PixelFormat::YUV420P:
            return MppFrameFormat::MPP_FMT_YUV420P;
        default:
            assert(false);
            return MppFrameFormat::MPP_FMT_YUV420SP;
    }
}

MppEncRcMode RateControlModeToMppEncRcMode(RateControlMode mode)
{
    switch (mode)
    {
        case RateControlMode::VBR: return MPP_ENC_RC_MODE_VBR;
        case RateControlMode::CBR: return MPP_ENC_RC_MODE_CBR;
        case RateControlMode::FIXQP: return MPP_ENC_RC_MODE_FIXQP;
        case RateControlMode::AVBR: return MPP_ENC_RC_MODE_AVBR;
        default:
            assert(false);
            return MPP_ENC_RC_MODE_VBR;
    }
}

DynamicRange MppFrameFormatToDynamicRange(MppFrameFormat format)
{
    if (MPP_FRAME_FMT_IS_HDR(format))
    {
        return DynamicRange::DynamicRange_HDR;
    }
    else
    {
        return DynamicRange::DynamicRange_SDR;
    }
}

ColorGamut MppFrameColorSpaceToColorGamut(MppFrameColorSpace colorPrimarie)
{
    switch (colorPrimarie) 
    {
        case MPP_FRAME_SPC_BT709:
        {
            return ColorGamut::ColorGamut_BT709;
        }
        case MPP_FRAME_SPC_BT470BG:
        case MPP_FRAME_SPC_SMPTE170M:
        case MPP_FRAME_SPC_SMPTE240M:
        {
            return ColorGamut::ColorGamut_BT601;
        }
        case MPP_FRAME_SPC_BT2020_NCL:
        case MPP_FRAME_SPC_BT2020_CL:
        {
            return ColorGamut::ColorGamut_BT2020;
        }
        default:
        {
            return ColorGamut::ColorGamut_UNSPECIFIED;
        }
    }
}

QuantRange MppFrameColorSpaceToQuantRange(MppFrameColorSpace colorPrimarie)
{
    switch (colorPrimarie) 
    {
        case MPP_FRAME_SPC_BT470BG:
        {
            return QuantRange::LIMIT;
        }
        default:
        {
            return QuantRange::FULL;
        }
    }
}

MppFrameColorSpace GenerateMppFrameColorSpace(ColorGamut colorGamut, QuantRange quantRange)
{
    switch (colorGamut) 
    {
        case ColorGamut::ColorGamut_BT709:
        {
            return MPP_FRAME_SPC_BT709;
        }
        case ColorGamut::ColorGamut_BT2020:
        {
            return MPP_FRAME_SPC_BT2020_NCL; // ??? or MPP_FRAME_SPC_BT2020_CL
        }
        case ColorGamut::ColorGamut_BT601:
        {
            if (quantRange == QuantRange::LIMIT)
            {
                return MPP_FRAME_SPC_BT470BG;
            }
            else
            {
                return MPP_FRAME_SPC_NB;
            }
        }
        default:
        {
            return MPP_FRAME_SPC_NB;
        }
    }
}

MppFrameColorTransferCharacteristic GenerateMppFrameColorTransferCharacteristic(ColorGamut colorGamut, size_t bits)
{
    switch (colorGamut) 
    {
        case ColorGamut::ColorGamut_BT709:
        {
            return MPP_FRAME_TRC_BT709;
        }
        case ColorGamut::ColorGamut_BT2020:
        {
            if (bits == 10)
            {
                return MPP_FRAME_TRC_BT2020_10;
            }
            else if (bits == 12)
            {
                return MPP_FRAME_TRC_BT2020_12;
            }
        }
        default:
        {
            return MPP_FRAME_TRC_UNSPECIFIED;
        }
    }
}

MppFrameColorPrimaries GenerateMppFrameColorPrimaries(ColorGamut colorGamut, QuantRange quantRange)
{
    switch (colorGamut)
    {
        case ColorGamut::ColorGamut_BT601:
        {
            if (quantRange == QuantRange::LIMIT)
            {
                return MPP_FRAME_PRI_BT470BG;
            }
            else
            {
                return MPP_FRAME_PRI_SMPTE170M;
            }
        }
        case ColorGamut::ColorGamut_BT709:
        {
            return MPP_FRAME_PRI_BT709;
        }
        case ColorGamut::ColorGamut_BT2020:
        {
            return MPP_FRAME_PRI_BT2020;
        }
        default:
            return MPP_FRAME_PRI_UNSPECIFIED;
    }
}

} // namespace Codec
} // namespace Mmp