//
// RkStreamFrame.h
//
// Library: Common
// Package: Codec
// Module:  Rockchip
//

#pragma once

#include <mutex>

#include "RKCommon.h"

namespace Mmp
{
namespace Codec
{

class RkStreamFrame : public StreamFrame
{
public:
    using ptr = std::shared_ptr<RkStreamFrame>;
public:
    RkStreamFrame(const PixelsInfo& info, AbstractAllocateMethod::ptr allocateMethod);
    ~RkStreamFrame();
public:
    MppFrame frame;
    Any sideData;
};

} // namespace Codec
} // namespace Mmp