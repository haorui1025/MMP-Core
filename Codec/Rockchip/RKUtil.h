//
// RKUtil.h
//
// Library: Common
// Package: Codec
// Module:  Rockchip
// 

#pragma once

#include "RKCommon.h"

#include "Common/PixelsInfo.h"

namespace Mmp
{
namespace Codec
{

std::string RkMppRetToStr(MPP_RET ret);

std::string MpiCmdToStr(MpiCmd cmd);

size_t GetRkFrameSize(const PixelsInfo& info, bool useAFBC);

} // namespace Codec
} // namespace Mmp