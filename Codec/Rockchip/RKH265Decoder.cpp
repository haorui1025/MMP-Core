#include "RKH265Decoder.h"
#include "CodecCommon.h"

namespace Mmp
{
namespace Codec
{

CodecType RKH265Decoder::GetCodecType()
{
    return CodecType::H265;
}

} // namespace Codec
} // namespace Mmp