//
// AbstractEncoder.h
//
// Library: Codec
// Package: Codec
// Module:  Encoder
// 

#pragma once

#include <string>
#include <memory>
#include <functional>

#include "CodecCommon.h"

namespace Mmp
{
namespace Codec
{
class AbstractEncoder
{
public:
    using ptr = std::shared_ptr<AbstractEncoder>;
public:
    virtual ~AbstractEncoder() = default;
public:
    /**
     * @brief      设置编码器参数
     * @param[in]  parameter : 根据编码器的类型决定
     * @param[in]  property  : 属性名称
     */
    virtual void SetParameter(Any parameter, const std::string& property = std::string()) {}
    /**
     * @brief      获取编码器参数
     * @param[in]  property : 属性名称
     */
    virtual Any GetParamter(const std::string& property = std::string()) { return Any(); }
    /**
     * @brief 初始化
     */
    virtual bool Init() { return true; }
    /**
     * @brief 重置
     */
    virtual void Uninit() {};
    /**
     * @brief 开始运行 
     */
    virtual bool Start() { return true; }
    /**
     * @brief 结束运行 
     */
    virtual void Stop() {};
    /**
     * @brief 提交压缩数据
     */
    virtual bool Push(AbstractFrame::ptr frame) = 0;
    /**
     * @brief 获取解压数据
     */
    virtual bool Pop(AbstractPack::ptr& pack) = 0;
    /**
     * @brief 是否可提交
     */
    virtual bool CanPush() = 0;
    /**
     * @brief 是否可获取
     */
    virtual bool CanPop() = 0;
    /**
     * @brief 获取编码器描述信息
     */
    virtual const std::string& Description() = 0;
};

/* common property */
static constexpr char kRateControlMode[] = "RateControlMode"; // RateControlMode
static constexpr char kGop[]             = "Gop";             // uint32_t
static constexpr char kBps[]             = "Bps";             // uint64_t
/* h264 and h265 common property */
static constexpr char kH26xProfile[]     = "H26xProfile";   // uint8_t
static constexpr char kH26xLevelIdc[]    = "H26xLevelIdc";  // uint8_t
static constexpr char kH26xFixQp[]       = "H26xFixQp";     // int8_t
static constexpr char kH26xInitQp[]      = "H26xInitQp";    // int8_t
static constexpr char kH26xMaxQp[]       = "H26xMaxQp";     // int8_t
static constexpr char kH26xMinQp[]       = "H26xMinQp";     // int8_t

} // namespace Codec
} // namespace Mmp