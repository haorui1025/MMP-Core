//
// CodecUtil.h
//
// Library: Codec
// Package: Codec
// Module:  Codec
// 

#pragma once

#include "Common/PixelsInfo.h"

#include "H264/H264Common.h"


namespace Mmp
{
namespace Codec
{

QuantRange H264VuiSyntaxToQuantRange(H264VuiSyntax::ptr vui);

ColorGamut H264VuiSyntaxToColorGamut(H264VuiSyntax::ptr vui);

} // namespace Codec
} // namespace Mmp
