#include "CodecCommon.h"

#include <cassert>

#include "PNG/PNGUtil.h"

#ifdef USE_VAAPI
#include "VAAPI/VADevice.h"
#endif /* USE_VAAPI */

namespace Mmp
{
namespace Codec
{

const std::string CodecTypeToStr(CodecType type)
{
    switch (type)
    {
        case CodecType::PNG:    return "PNG";
        case CodecType::H264:   return "H264";
        case CodecType::H265:   return "H265";
        case CodecType::VP8:    return "VP8";
        case CodecType::VP9:    return "VP9";
        case CodecType::AV1:    return "AV1";
        default: assert(false); return "UNKNOWN";
    }
}

std::ostream& operator<<(std::ostream& os, CodecType type)
{
    os << CodecTypeToStr(type);
    return os;
}

const std::string CodecProcessTypeToStr(CodecProcessType type)
{
    switch (type)
    {
        case CodecProcessType::Software:    return "Software";
        case CodecProcessType::Hardware:    return "Hardware";
        case CodecProcessType::Unknown:     return "Unknown";
        default: assert(false);             return "UNKNOWN";
    }
}

std::ostream& operator<<(std::ostream& os, CodecProcessType type)
{
    os << CodecProcessTypeToStr(type);
    return os;
}

const std::string CodecVendorTypeToStr(CodecVendorType type)
{
    switch (type)
    {
        case CodecVendorType::OPENH264:    return "OPENH264";
        case CodecVendorType::LIBVA:       return "LIBVA";
        case CodecVendorType::VULKAN:      return "VULKAN";
        case CodecVendorType::D3DVA:       return "D3DVA";
        case CodecVendorType::D3D11:       return "D3D11";
        case CodecVendorType::LODEPNG:     return "LODEPNG";
        case CodecVendorType::ROCKCHIP:    return "ROCKCHIP";
        case CodecVendorType::UNKNOWN:     return "UNKNOWN";
        default: assert(false);            return "UNKNOWN";
    }
}

std::ostream& operator<<(std::ostream& os, CodecVendorType type)
{
    os << CodecVendorTypeToStr(type);
    return os;
}

const std::string CodecTypeToStr(RateControlMode mode)
{
    switch (mode)
    {
        case RateControlMode::VBR: return "VBR";
        case RateControlMode::CBR: return "CBR";
        case RateControlMode::FIXQP: return "FIXQP";
        case RateControlMode::AVBR: return "AVBR";
        default: assert(false); return "UNKNOWN";
    }
}

extern std::ostream& operator<<(std::ostream& os, RateControlMode mode)
{
    os << CodecTypeToStr(mode);
    return os;
}

CodecDescription::CodecDescription()
{
    codecType = CodecType::H264;
    processType = CodecProcessType::Unknown;
    vendorType = CodecVendorType::UNKNOWN;
}

CodecDescription::CodecDescription(CodecType codecType, CodecProcessType processType, CodecVendorType vendorType, std::string name, std::string description)
{
    this->codecType = codecType;
    this->processType = processType;
    this->vendorType = vendorType;
    this->name = name;
    this->description = description;
}


} // namespace Codec
} // namespace Mmp
