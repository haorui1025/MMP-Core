//
// CodecCommon.h
//
// Library: Codec
// Package: Codec
// Module:  Codec
// 

#pragma once

#include <iostream>
#include <string>

#include "Common/Any.h"
#include "Common/MediaType.h"
#include "Common/PixelsInfo.h"
#include "Common/PixelFormat.h"
#include "Common/LogMessage.h"
#include "Common/AbstractSharedData.h"
#include "Common/AbstractPack.h"
#include "Common/AbstractFrame.h"
#include "Common/AbstractPicture.h"
#include "Common/NormalSharedData.h"
#include "Common/NormalPack.h"
#include "Common/NormalFrame.h"
#include "Common/NormalPicture.h"

#include "StreamFrame.h"

#define CODEC_LOG_TRACE   MMP_MLOG_TRACE("CODEC")    
#define CODEC_LOG_DEBUG   MMP_MLOG_DEBUG("CODEC")    
#define CODEC_LOG_INFO    MMP_MLOG_INFO("CODEC")     
#define CODEC_LOG_WARN    MMP_MLOG_WARN("CODEC")     
#define CODEC_LOG_ERROR   MMP_MLOG_ERROR("CODEC")    
#define CODEC_LOG_FATAL   MMP_MLOG_FATAL("CODEC")    

namespace Mmp
{
namespace Codec
{

/**
 * @brief 编解码器名称
 */
enum class CodecType
{
    PNG,
    H264,
    H265,
    VP8,
    VP9,
    AV1
};
const std::string CodecTypeToStr(CodecType type);
extern std::ostream& operator<<(std::ostream& os, CodecType type);

/**
 * @brief 编码器处理类型
 */
enum class CodecProcessType
{
    Software,
    Hardware,
    Unknown
};
const std::string CodecProcessTypeToStr(CodecProcessType type);
extern std::ostream& operator<<(std::ostream& os, CodecProcessType type);

/**
 * @brief 编码器提供商
 */
enum class CodecVendorType
{
    OPENH264,
    LIBVA,
    VULKAN,
    D3DVA,
    D3D11,
    ROCKCHIP,
    LODEPNG,
    UNKNOWN
};
const std::string CodecVendorTypeToStr(CodecVendorType type);
extern std::ostream& operator<<(std::ostream& os, CodecVendorType type);

/**
 * @brief 码率控制模式
 */
enum class RateControlMode
{
    VBR,
    CBR,
    FIXQP,
    AVBR
};
const std::string CodecTypeToStr(RateControlMode mode);
extern std::ostream& operator<<(std::ostream& os, RateControlMode mode);

/**
 * @brief 编码器类型
 */
class CodecDescription
{
public:
    CodecDescription();
    CodecDescription(CodecType codecType, CodecProcessType processType, CodecVendorType vendorType, std::string name, std::string description = std::string());
public:
    CodecType codecType;
    CodecProcessType processType;
    CodecVendorType vendorType;
    std::string name;
    std::string description;
    Any sideData;
};

} // namespace Codec
} // namespace Mmp

#include "PNG/PNGCommon.h"