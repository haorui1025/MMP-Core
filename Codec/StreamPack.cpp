#include "StreamPack.h"

namespace Mmp
{
namespace Codec
{

StreamPack::StreamPack(CodecType type, size_t size, AbstractAllocateMethod::ptr allocateMethod)
{
    this->_sharedData = std::make_shared<NormalSharedData>(size, allocateMethod);
    this->type = type;
}

StreamPack::StreamPack(CodecType type, AbstractSharedData::ptr data)
{
    this->_sharedData = data;
    this->type = type;
}

} // namespace Codec
} // namespace Mmp