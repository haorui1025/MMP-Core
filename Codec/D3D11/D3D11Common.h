//
// D3D11Common.h
//
// Library: Codec
// Package: D3D11
// Module:  D3D11
// 
//

#pragma once

#include <vector>

#include "Common/Common.h"
#include "Common/D3D11Device.h"
#include "Common/D3D11Resource.h"
#include "Codec/CodecCommon.h"
#include "Codec/AbstractEncoder.h"
#include "Codec/AbstractDecorder.h"

#include <Unknwnbase.h>

#define ENABLE_CODEC_D3D11_TRACE_PROC 0

namespace Mmp
{
namespace Codec
{

class D3D11DecoderParams
{
public:
    D3D11DecoderParams();
    ~D3D11DecoderParams() = default;
public:
    PixelFormat                        format;
    uint32_t                           width;
    uint32_t                           height;
    uint32_t                           virWidth;
    uint32_t                           virHeight;
    QuantRange                         quantRange;
    ColorGamut                         colorGamut;
    uint32_t                           bitdepth;
    CodecType                          codecType;
    size_t                             maxRefNum;
    std::vector<GUID>                  decoderProfiles;
};
bool operator==(const D3D11DecoderParams& left, const D3D11DecoderParams& right);
bool operator!=(const D3D11DecoderParams& left, const D3D11DecoderParams& right);

class D3D11VideoFrame : public ID3D11Texture
{
public:
    using ptr = std::shared_ptr<D3D11VideoFrame>;
public:
    using DestroyProxy = std::function<void(ID3D11Texture2D* texture, ID3D11VideoDecoderOutputView* view)>;
public:
    D3D11VideoFrame(const std::string& tag = std::string());
    ~D3D11VideoFrame();
public:
    ID3D11VideoDecoderOutputView* view;
public:
    std::string trackId;
    DestroyProxy destoryProxy;
public:
    std::chrono::milliseconds  pts;
    std::chrono::milliseconds  dts;
};

} // namespace Codec
} // namespace Mmp