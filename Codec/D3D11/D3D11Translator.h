//
// D3D11Translator.h
//
// Library: Codec
// Package: D3D11
// Module:  D3D11
// 

#pragma once


#include "D3D11Common.h"

namespace Mmp
{
namespace Codec
{

DXGI_FORMAT PixelFormatToD3D11DecoderFormat(const PixelFormat& format);

} // namespace Codec
} // namespace Mmp