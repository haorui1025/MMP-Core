#include "D3D11DecoderContext.h"

#include <cassert>
#include <algorithm>

namespace Mmp
{
namespace Codec
{

static std::atomic<uint64_t> gCount(0);

D3D11DecoderContext::D3D11DecoderContext()
{
    _index = gCount++;
    _tagPrefix = "VDC_" + std::to_string(_index) +  "_";
    _bufTrackId = 0;

    _bestDecoderConfig = {};
    _width = 0;
    _height = 0;
    _virWidth = 0;
    _virHeight = 0;
    _format = DXGI_FORMAT_UNKNOWN;
    _decoder = nullptr;
}

D3D11DecoderContext::~D3D11DecoderContext()
{

}

bool D3D11DecoderContext::Init(const D3D11DecoderParams& params)
{
    std::lock_guard<std::mutex> lock(_mtx);
    D3D11_VIDEO_DECODER_DESC desc = {};
    InitParams(params);
    if (!GetDecoderProfile(_guids))
    {
        D3D11_LOG_ERROR << "GetDecoderProfile fail";
        assert(false);
        return false;
    }
    if (!GetUsedDecoderProfile(_guids, _usedGuid))
    {
        D3D11_LOG_ERROR << "GetUsedDecoderProfile fail";
        assert(false);
        return false;
    }
    {
        desc.SampleWidth  = _virWidth;
        desc.SampleHeight = _virHeight;
        desc.OutputFormat = _format;
        desc.Guid         = _usedGuid;
    }
    if (!GetDecoderConfigs(desc, _decoderConfigs))
    {
        D3D11_LOG_ERROR << "GetDecoderConfigs fail";
        assert(false);
        return false;
    }
    if (!ChooseBestDecoderConfig(_decoderConfigs, _bestDecoderConfig))
    {
        D3D11_LOG_ERROR << "ChooseBestDecoderConfig fail";
        assert(false);
        return false;    
    }
    HRESULT hr = 0;
    hr = D3D11Device::Singleton()->CreateVideoDecoder(&desc, &_bestDecoderConfig, &_decoder);
    if (D3D11_FAILED(hr))
    {
        D3D11_LOG_ERROR << "ID3D11VideoDevice::CreateVideoDecoder fail, hr is: " << D3D11ReturnCodeToStr(hr);
        assert(false);
        return false;   
    }
    D3D11_LOG_INFO << "Init D3D11DecoderContext, " << _codecType << " "  << _width << "x" << _height;
    return true;
}

D3D11VideoFrame::ptr D3D11DecoderContext::CreateD3D11VideoFrame()
{
    D3D11VideoFrame::ptr frame;
    // Hint : Try cache first
    {
        {
            std::lock_guard<std::mutex> lock(_bufferMtx);
            if (!_buffers.empty())
            {
                frame = _buffers.back();
                _buffers.pop_back();
            }
        }
        if (frame)
        {
            return frame;
        }
    }
    frame = std::make_shared<D3D11VideoFrame>(_tagPrefix + "F_" + std::to_string(_bufTrackId++));
    {
        {
            HRESULT hr = 0;
            D3D11_TEXTURE2D_DESC desc = {};
            {
                desc.Width = _virWidth;
                desc.Height = _virHeight;
                desc.MipLevels = 1;
                desc.Format = _format;
                desc.SampleDesc.Count = 1;
                desc.ArraySize = 1;
                desc.Usage = D3D11_USAGE_DEFAULT;
                desc.BindFlags |= D3D11_BIND_DECODER;
            }
            hr = D3D11Device::Singleton()->CreateTexture2D(&desc, nullptr, &(frame->texture));
            if (D3D11_FAILED(hr))
            {
                D3D11_LOG_ERROR << "ID3D11Device::CreateTexture2D fail, hr is: " << D3D11ReturnCodeToStr(hr);
                assert(false);
                return nullptr;
            }
        }
        {
            HRESULT hr = 0;
            D3D11_VIDEO_DECODER_OUTPUT_VIEW_DESC desc = {};
            {
                desc.DecodeProfile = _usedGuid;
                desc.ViewDimension = D3D11_VDOV_DIMENSION_TEXTURE2D;
                desc.Texture2D.ArraySlice = 0;
            }
            hr = D3D11Device::Singleton()->CreateVideoDecoderOutputView(frame->texture, &desc, &(frame->view));
            if (D3D11_FAILED(hr))
            {
                D3D11_LOG_ERROR << "ID3D11VideoDevice::CreateVideoDecoderOutputView fail, hr is: " << D3D11ReturnCodeToStr(hr);
                assert(false);
                return nullptr;
            }
        }
        {
            std::weak_ptr<D3D11DecoderContext> weak = shared_from_this();
            frame->destoryProxy = [trackId = frame->trackId, weak, this](ID3D11Texture2D* texture, ID3D11VideoDecoderOutputView* view)
            {
                auto strong = weak.lock();
                if (!strong)
                {
                    D3D11_OBJECT_RELEASE(texture);
                    return;
                }
                this->OnD3D11VideoFrameFree(trackId, texture, view);
            };
        }
    }
    return frame;
}

bool D3D11DecoderContext::UpdateDecoderBuffer(D3D11_VIDEO_DECODER_BUFFER_TYPE type, uint8_t* data, size_t size)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (!_decoder)
    {
        return false;
    }
    HRESULT hr = 0;
    uint8_t* mapAddress = nullptr;
    size_t mapSize = 0;
    hr = D3D11Device::Singleton()->GetDecoderBuffer(_decoder, type, (UINT *)&mapSize, (void**)&mapAddress);
    if (D3D11_FAILED(hr))
    {
        D3D11_LOG_ERROR << "ID3D11VideoContext::GetDecoderBuffer fail, hr is: " << D3D11ReturnCodeToStr(hr);
        assert(false);
        return false;
    }
    memset(mapAddress, 0, mapSize);
    memcpy(mapAddress, data, std::min(mapSize, size));
    hr = D3D11Device::Singleton()->ReleaseDecoderBuffer(_decoder, type);
    if (D3D11_FAILED(hr))
    {
        D3D11_LOG_WARN << "ID3D11VideoContext::ReleaseDecoderBuffer fail, hr is: " << D3D11ReturnCodeToStr(hr);
    }
    return true;
}

bool D3D11DecoderContext::DecodeFrame(std::vector<D3D11_VIDEO_DECODER_BUFFER_DESC>& decoderBufferDesc, ID3D11VideoDecoderOutputView* view)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (!_decoder)
    {
        return false;
    }
    HRESULT hr = 0;
    constexpr size_t kMaxTryTime = 30;
    constexpr size_t kWaitTime = 10;
    size_t curTryTime = 0;
    do
    {
        hr = D3D11Device::Singleton()->DecoderBeginFrame(_decoder, view, 0, nullptr);
        if (hr == E_PENDING)
        {
            curTryTime++;
            std::this_thread::sleep_for(std::chrono::milliseconds(kWaitTime));
            if (curTryTime > kMaxTryTime)
            {
                continue;
            }
        }
        if (D3D11_FAILED(hr))
        {
            D3D11_LOG_ERROR << "ID3D11VideoContext::GetDecoderBuffer fail, hr is: " << D3D11ReturnCodeToStr(hr);
            assert(false);
            return false;
        }
        break;
    } while(1);
    if (curTryTime == kMaxTryTime)
    {
        D3D11_LOG_ERROR << "ID3D11VideoContext::GetDecoderBuffer fail, hr is: " << D3D11ReturnCodeToStr(hr);
        assert(false);
        return false;
    }
    hr = D3D11Device::Singleton()->SubmitDecoderBuffers(_decoder, (UINT)decoderBufferDesc.size(), decoderBufferDesc.data());
    if (D3D11_FAILED(hr))
    {
        D3D11_LOG_ERROR << "ID3D11VideoContext::SubmitDecoderBuffers fail, hr is: " << D3D11ReturnCodeToStr(hr);
        assert(false);
        return false;
    }
    hr = D3D11Device::Singleton()->DecoderEndFrame(_decoder);
    if (D3D11_FAILED(hr))
    {
        D3D11_LOG_ERROR << "ID3D11VideoContext::DecoderEndFrame fail, hr is: " << D3D11ReturnCodeToStr(hr);
        assert(false);
        return false;
    }
    return true;
}

NormalAllocateMethod::ptr D3D11DecoderContext::TransferFrame(D3D11VideoFrame::ptr frame)
{
    std::lock_guard<std::mutex> lock(_stagingMtx);
    D3D11VideoFrame::ptr stagingFrame = GetStagingDownloadBuffer();
    NormalAllocateMethod::ptr alloc;
    if (!stagingFrame)
    {
        return alloc;
    }
    HRESULT hr = 0;
    D3D11_MAPPED_SUBRESOURCE map = {};
    D3D11Device::Singleton()->CopySubresourceRegion(stagingFrame->texture, 0, 0, 0, 0, frame->texture, 0, nullptr);
    hr = D3D11Device::Singleton()->Map(stagingFrame->texture, 0, D3D11_MAP_READ, 0, &map);
    if (D3D11_FAILED(hr))
    {
        D3D11_LOG_ERROR << "ID3D11DeviceContext::Map fail, hr is: " << D3D11ReturnCodeToStr(hr);
        assert(false);
        return alloc;
    }
    {
        uint8_t* srcAddress = nullptr;
        uint8_t* dstAddress = nullptr;
        size_t size = _width * _height * 3 / 2;
        if (map.DepthPitch * map.RowPitch < size)
        {
            return alloc;
        }
        alloc = std::make_shared<NormalAllocateMethod>();
        srcAddress = (uint8_t*)map.pData;
        dstAddress = (uint8_t*)alloc->Malloc(size);
        if (!dstAddress)
        {
            return alloc;
        }
        if (map.RowPitch == _width)
        {
            memcpy(dstAddress, srcAddress, size);
        }
        else
        {
            for (size_t i=0; i<_height*3/2; i++)
            {
                memcpy(dstAddress, srcAddress, _width);
                srcAddress += map.RowPitch;
                dstAddress += _width;
            }
        }
    }
    D3D11Device::Singleton()->Unmap(stagingFrame->texture, 0);
    return alloc;
}

void D3D11DecoderContext::InitParams(const D3D11DecoderParams& params)
{
    _width = params.width;
    _height = params.height;
    _virWidth = params.virWidth;
    _virHeight = params.virHeight;
    _codecType = params.codecType;
    _availableDecoderProfiles = params.decoderProfiles;
    _format = DXGI_FORMAT_NV12;
}

bool D3D11DecoderContext::CheckOutputFormatSupport(GUID guid, DXGI_FORMAT format)
{
    HRESULT hr = 0;
    BOOL isSupported = FALSE;
    hr = D3D11Device::Singleton()->CheckVideoDecoderFormat(&guid, format, &isSupported);
    return SUCCEEDED(hr) && isSupported;
}

bool D3D11DecoderContext::ChooseBestDecoderConfig(const std::vector<D3D11_VIDEO_DECODER_CONFIG>& decoderDescs, D3D11_VIDEO_DECODER_CONFIG& bestDecoderConfig)
{
    bool res = false;
    uint32_t bestScore = 0;
    for (const auto& config : decoderDescs)
    {
        uint32_t score = 0;
        if (config.ConfigBitstreamRaw == 1)
        {
            score += 1;
        }
        else if (config.ConfigBitstreamRaw == 2)
        {
            score += 2;
        }
        else
        {
            continue;
        }
        if (score > bestScore)
        {
            bestScore = score;
            bestDecoderConfig = config;
            res = true;
        }
    }
    return res;
}

bool D3D11DecoderContext::GetDecoderConfigs(const D3D11_VIDEO_DECODER_DESC& desc, std::vector<D3D11_VIDEO_DECODER_CONFIG>& decoderDescs)
{
    HRESULT hr = 0;
    UINT cfgCount = 0;
    hr = D3D11Device::Singleton()->GetVideoDecoderConfigCount(&desc, &cfgCount);
    if (D3D11_FAILED(hr))
    {
        D3D11_LOG_ERROR << "ID3D11VideoDevice::GetVideoDecoderConfigCount fail, hr is: " << D3D11ReturnCodeToStr(hr);
        assert(false);
        return false;
    }
    if (cfgCount != 0)
    {
        decoderDescs.resize((size_t)cfgCount);
        for (UINT i=0; i<cfgCount; i++)
        {
            hr = D3D11Device::Singleton()->GetVideoDecoderConfig(&desc, i, (D3D11_VIDEO_DECODER_CONFIG*)(decoderDescs.data()) + i);
            if (D3D11_FAILED(hr))
            {
                D3D11_LOG_ERROR << "ID3D11VideoDevice::GetVideoDecoderConfig fail, hr is: " << D3D11ReturnCodeToStr(hr);
                decoderDescs.clear();
                assert(false);
                return false;
            }
        }
    }
    D3D11_LOG_DEBUG << "GetVideoDecoderConfig, config size is: " << decoderDescs.size();
    return true;
}

bool D3D11DecoderContext::GetDecoderProfile(std::vector<GUID>& guids)
{
    HRESULT hr = 0;
    size_t guidCount = 0;
    guidCount = D3D11Device::Singleton()->GetVideoDecoderProfileCount();
    if (guidCount == 0)
    {
        D3D11_LOG_WARN << "ID3D11VideoDevice::GetVideoDecoderProfileCount get zero";
        return false;
    }
    guids.resize(guidCount);
    for (size_t i=0; i<guidCount; i++)
    {
        hr = D3D11Device::Singleton()->GetVideoDecoderProfile((UINT)i, (GUID*)(guids.data()) + i);
        if (D3D11_FAILED(hr))
        {
            D3D11_LOG_ERROR << "ID3D11VideoDevice::GetVideoDecoderProfile fail, hr is: " << D3D11ReturnCodeToStr(hr);
            guids.clear();
            assert(false);
            return false;
        }
    }
    D3D11_LOG_DEBUG << "GetDecoderProfile, profile size is: " << guids.size();
    return true;
}

bool D3D11DecoderContext::GetUsedDecoderProfile(const std::vector<GUID>& guids, GUID& chooseGuid)
{
    bool findGuid = false;
    for (auto& guid : guids)
    {
        auto itr = std::find(_availableDecoderProfiles.begin(), _availableDecoderProfiles.end(), guid);
        if (itr !=  _availableDecoderProfiles.end())
        {
            chooseGuid = *itr;
            findGuid = true;
            break;
        }
    }
    return findGuid;
}

D3D11VideoFrame::ptr D3D11DecoderContext::GetStagingDownloadBuffer()
{
    if (!_stagingFrame)
    {
        HRESULT hr = 0;
        _stagingFrame = std::make_shared<D3D11VideoFrame>();
        D3D11_TEXTURE2D_DESC desc = {};
        {
            desc.Width = _width;
            desc.Height = _height;
            desc.MipLevels = 1;
            desc.Format = _format;
            desc.SampleDesc.Count = 1;
            desc.ArraySize = 1;
            desc.Usage = D3D11_USAGE_STAGING;
            desc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
        }
        D3D11Device::Singleton()->CreateTexture2D(&desc, nullptr, &(_stagingFrame->texture));
        if (D3D11_FAILED(hr))
        {
            D3D11_LOG_ERROR << "ID3D11Device::CreateTexture2D fail, hr is: " << D3D11ReturnCodeToStr(hr);
            _stagingFrame = nullptr;
            assert(false);
            return false;
        }
    }
    return _stagingFrame;
}

void D3D11DecoderContext::OnD3D11VideoFrameFree(const std::string& trackId, ID3D11Texture2D* texture, ID3D11VideoDecoderOutputView* view)
{
    std::weak_ptr<D3D11DecoderContext> weak = shared_from_this();
    D3D11VideoFrame::ptr frame = std::make_shared<D3D11VideoFrame>(trackId);
    frame->texture = texture;
    frame->view = view;
    frame->destoryProxy = [trackId, weak, this](ID3D11Texture2D* texture, ID3D11VideoDecoderOutputView* view)
    {
        auto strong = weak.lock();
        if (!strong)
        {
            D3D11_OBJECT_RELEASE(texture);
            return;
        }
        this->OnD3D11VideoFrameFree(trackId, texture, view);
    };
    {
        std::lock_guard<std::mutex> lock(_bufferMtx);
        _buffers.push_back(frame);
    }
}

} // namespace Codec
} // namespace Mmp