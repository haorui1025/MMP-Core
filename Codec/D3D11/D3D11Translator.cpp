#include "D3D11Translator.h"

#include <cassert>

namespace Mmp
{
namespace Codec
{

DXGI_FORMAT PixelFormatToD3D11DecoderFormat(const PixelFormat& format)
{
    switch (format)
    {
        case PixelFormat::NV12: return DXGI_FORMAT_NV12;
        case PixelFormat::YUV420P: return DXGI_FORMAT_420_OPAQUE;
        default:
            assert(false);
            return DXGI_FORMAT_UNKNOWN;
    }
}

} // namespace Codec
} // namespace Mmp