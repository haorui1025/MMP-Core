#include "D3D11CodecAllocatedMethod.h"

namespace Mmp
{
namespace Codec
{

void* D3D11CodecAllocatedMethod::GetAddress(uint64_t offset)
{
    if (!alloc)
    {
        D3D11VideoFrame::ptr frame = std::dynamic_pointer_cast<D3D11VideoFrame>(srcFrame);
        if (frame)
        {
            alloc = decoderContext->TransferFrame(frame);
        }   
    }
    if (alloc)
    {
        return alloc->GetAddress(offset);
    }
    else
    {
        return nullptr;
    }
}

const std::string& D3D11CodecAllocatedMethod::Tag()
{
    static std::string tag = "D3D11CodecAllocatedMethod";
    return tag;  
}

} // namespace Codec
} // namespace Mmp