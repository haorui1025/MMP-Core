#include "D3D11Decoder.h"

#include <cassert>

#include "D3D11CodecAllocatedMethod.h"


namespace Mmp
{
namespace Codec
{

D3D11Decoder::D3D11Decoder()
{
}

void D3D11Decoder::SetParameter(Any parameter, const std::string& property)
{
    AbstractDecoder::SetParameter(parameter, property);
}

Any D3D11Decoder::GetParamter(const std::string& property)
{
    return AbstractDecoder::GetParamter(property);
}

bool D3D11Decoder::Init()
{
    return true;
}

void D3D11Decoder::Uninit()
{
}

bool D3D11Decoder::Start()
{
    return true;
}

void D3D11Decoder::Stop()
{
}

bool D3D11Decoder::Push(AbstractPack::ptr pack)
{
    return false;
}

bool D3D11Decoder::Pop(AbstractFrame::ptr& frame)
{
    std::lock_guard<std::mutex> lock(_bufMtx);
    if (!_buffers.empty())
    {
        frame = _buffers.front();
        _buffers.pop_front();
        return true;
    }
    else
    {
        return false;
    }
}

bool D3D11Decoder::CanPush()
{
    return true;
}

bool D3D11Decoder::CanPop()
{
    std::lock_guard<std::mutex> lock(_bufMtx);
    return !_buffers.empty();
}

const std::string& D3D11Decoder::Description()
{
    static std::string description = "D3D11Decoder";
    return description;
}

D3D11DecoderContext::ptr D3D11Decoder::GetDecoderContext()
{
    return _decoderContext;
}

D3D11DecoderParams D3D11Decoder::GetDecoderParams()
{
    return _params;
}

void D3D11Decoder::SetDecoderParams(const D3D11DecoderParams& params)
{
    if (_params != params)
    {
        D3D11DecoderParams oldParams = _params;
        _params = params;
        _decoderContext = std::make_shared<D3D11DecoderContext>();
        if (!_decoderContext->Init(_params))
        {
            D3D11_LOG_ERROR << "D3D11DecoderContext Init fail";
            _decoderContext = nullptr;
            assert(false);
        }
        OnDecoderParamsChange(oldParams, params);
    }
}

void D3D11Decoder::PushFrame(D3D11VideoFrame::ptr frame)
{
    std::lock_guard<std::mutex> lock(_bufMtx);
    D3D11CodecAllocatedMethod::ptr alloc = std::make_shared<D3D11CodecAllocatedMethod>();
    PixelsInfo info;
    {
        alloc->srcFrame = frame;
        alloc->decoderContext = _decoderContext;
    }
    {
        info.width = _params.width;
        info.height = _params.height;
        info.horStride = _params.width;
        info.virStride = _params.height;
        info.format = _params.format;
        info.bitdepth = _params.bitdepth;
        info.colorGamut = _params.colorGamut;
        info.quantRange = _params.quantRange;
    }
    StreamFrame::ptr stramFrame = std::make_shared<StreamFrame>(info, alloc);
    {
        stramFrame->dts = frame->dts;
        stramFrame->pts = frame->pts;
    }
    _buffers.push_back(stramFrame);
}

void D3D11Decoder::OnDecoderParamsChange(const D3D11DecoderParams& oldValue, const D3D11DecoderParams& newValue)
{

}

} // namespace Codec
} // namespace Mmp