//
// D3D11DecoderContext.h
//
// Library: Codec
// Package: D3D11
// Module:  D3D11
//

#pragma once

#include <mutex>
#include <deque>
#include <vector>
#include <memory>

#include "D3D11Common.h"

#include "Common/NormalAllocateMethod.h"

namespace Mmp
{
namespace Codec
{

class D3D11DecoderContext : public std::enable_shared_from_this<D3D11DecoderContext>
{
public:
    using ptr = std::shared_ptr<D3D11DecoderContext>;
public:
    D3D11DecoderContext();
    virtual ~D3D11DecoderContext();
public:
    bool Init(const D3D11DecoderParams& params);
    D3D11VideoFrame::ptr CreateD3D11VideoFrame();
    bool UpdateDecoderBuffer(D3D11_VIDEO_DECODER_BUFFER_TYPE type, uint8_t* data, size_t size);
    bool DecodeFrame(std::vector<D3D11_VIDEO_DECODER_BUFFER_DESC>& decoderBufferDesc, ID3D11VideoDecoderOutputView* view);
    NormalAllocateMethod::ptr TransferFrame(D3D11VideoFrame::ptr frame);
private:
    void InitParams(const D3D11DecoderParams& params);
    bool CheckOutputFormatSupport(GUID guid, DXGI_FORMAT format);
    bool GetDecoderConfigs(const D3D11_VIDEO_DECODER_DESC& desc, std::vector<D3D11_VIDEO_DECODER_CONFIG>& decoderDescs);
    bool ChooseBestDecoderConfig(const std::vector<D3D11_VIDEO_DECODER_CONFIG>& decoderDescs, D3D11_VIDEO_DECODER_CONFIG& bestDecoderConfig);
    bool GetDecoderProfile(std::vector<GUID>& guids);
    bool GetUsedDecoderProfile(const std::vector<GUID>& guids, GUID& chooseGuid);
    D3D11VideoFrame::ptr GetStagingDownloadBuffer();
private:
    void OnD3D11VideoFrameFree(const std::string& trackId, ID3D11Texture2D* texture, ID3D11VideoDecoderOutputView* view);
private:
    uint64_t _index;
    std::string _tagPrefix;
    std::mutex _mtx;
    uint32_t _width;
    uint32_t _height;
    uint32_t _virWidth;
    uint32_t _virHeight;
    CodecType _codecType;
    DXGI_FORMAT _format;
    ID3D11VideoDecoder* _decoder;
private:
    GUID _usedGuid;
    std::vector<GUID> _guids;
    std::vector<GUID> _availableDecoderProfiles;
private:
    D3D11_VIDEO_DECODER_CONFIG _bestDecoderConfig;
    std::vector<D3D11_VIDEO_DECODER_CONFIG> _decoderConfigs;
private:
    std::mutex _stagingMtx;
    D3D11VideoFrame::ptr _stagingFrame;
private:
    std::mutex _bufferMtx;
    std::deque<D3D11VideoFrame::ptr> _buffers;
    uint64_t _bufTrackId;
};

} // namespace Codec
} // namespace Mmp