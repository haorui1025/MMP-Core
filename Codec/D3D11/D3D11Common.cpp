#include "D3D11Common.h"

namespace Mmp
{
namespace Codec
{

D3D11DecoderParams::D3D11DecoderParams()
{
    format = PixelFormat::NV12;
    width = 0;
    height = 0;
    virWidth = 0;
    virHeight = 0;
    bitdepth = 8;
    codecType = CodecType::H264;
    maxRefNum = 15;
    quantRange = QuantRange::UNSPECIFIED;
    colorGamut = ColorGamut_UNSPECIFIED;
}

bool operator==(const D3D11DecoderParams& left, const D3D11DecoderParams& right)
{
    return left.format == right.format &&
           left.width  == right.width  &&
           left.height == right.height &&
           left.virWidth == right.virWidth &&
           left.virHeight == right.virHeight &&
           left.quantRange == right.quantRange &&
           left.colorGamut == right.colorGamut &&
           left.bitdepth == right.bitdepth &&
           left.codecType == right.codecType &&
           left.maxRefNum == right.maxRefNum &&
           left.decoderProfiles == right.decoderProfiles;
}

bool operator!=(const D3D11DecoderParams& left, const D3D11DecoderParams& right)
{
    return !(left == right);
}

D3D11VideoFrame::D3D11VideoFrame(const std::string& tag)
{
    pts = std::chrono::milliseconds(0);
    dts = std::chrono::milliseconds(0);
    view = nullptr;
    this->trackId = tag;
#if ENABLE_CODEC_D3D11_TRACE_PROC
    if (!tag.empty())
    {
        D3D11_LOG_INFO << "D3D11VideoFrame " << tag;
    }
#endif
}

D3D11VideoFrame::~D3D11VideoFrame()
{
#if ENABLE_CODEC_D3D11_TRACE_PROC
    if (!trackId.empty())
    {
        D3D11_LOG_INFO << "~D3D11VideoFrame " << trackId;
    }
#endif
    if (destoryProxy)
    {
        destoryProxy(texture, view);
        texture = nullptr;
    }
    else
    {
        D3D11_OBJECT_RELEASE(view);
    }
}

} // namespace Codec
} // namespace Mmp