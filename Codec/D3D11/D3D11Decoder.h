//
// D3D11Decoder.h
//
// Library: Codec
// Package: D3D11
// Module:  D3D11
//

#pragma once

#include <set>
#include <mutex>
#include <deque>

#include "D3D11Common.h"
#include "D3D11DecoderContext.h"
#include "D3D11DecodePictureContext.h"

namespace Mmp
{
namespace Codec
{

class D3D11Decoder : public AbstractDecoder
{
public:
    using ptr = std::shared_ptr<D3D11Decoder>;
public:
    D3D11Decoder();
    virtual ~D3D11Decoder() = default;
public:
    void SetParameter(Any parameter, const std::string& property) override;
    Any GetParamter(const std::string& property) override;
    bool Init() override;
    void Uninit() override;
    bool Start() override;
    void Stop() override;
    bool Push(AbstractPack::ptr pack) override;
    bool Pop(AbstractFrame::ptr& frame) override;
    bool CanPush() override;
    bool CanPop() override;
    const std::string& Description() override;
protected:
    D3D11DecoderContext::ptr GetDecoderContext();
    D3D11DecoderParams GetDecoderParams();
    void SetDecoderParams(const D3D11DecoderParams& params);
    void PushFrame(D3D11VideoFrame::ptr frame);
protected: /* Hook */
    virtual void StartFrame(const Any& context) = 0;
    virtual void DecodedBitStream(const Any& context) = 0;
    virtual void EndFrame(const Any& context) = 0;
protected: /* Event */
    virtual void OnDecoderParamsChange(const D3D11DecoderParams& oldValue, const D3D11DecoderParams& newValue);
private:
    std::mutex _bufMtx;
    std::deque<StreamFrame::ptr> _buffers;
private:
    D3D11DecoderParams _params;
    D3D11DecoderContext::ptr _decoderContext;
};

} // namespace Codec
} // namespace Mmp