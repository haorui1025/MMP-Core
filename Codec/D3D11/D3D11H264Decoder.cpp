#include "D3D11H264Decoder.h"

#include <cassert>

#include "H264/H264Common.h"
#include "H264/H264Deserialize.h"
#include "H264/H264SliceDecodingProcess.h"

#include "Codec/CodecUtil.h"
#include "StreamPack.h"

#define D3D11_H264_FIELD_ORDER_CNT_OFFSET 65536

namespace Mmp
{
namespace Codec
{

class D3D11H264ByteReader : public AbstractH26xByteReader
{
public:
    D3D11H264ByteReader(AbstractPack::ptr pack)
    {
        _pack = pack;
        _end = (uint32_t)_pack->GetSize();
        _cur = 0;
    }
public:
    size_t Read(void* data, size_t bytes) override
    {
        uint32_t readSize = (uint32_t)bytes < (_end - _cur) ? (uint32_t)bytes : (_end - _cur);
        if (readSize != 0)
        {
            memcpy(data, (uint8_t*)_pack->GetData() + _cur, readSize);
        }
        _cur += readSize;
        return size_t(readSize);
    }
    bool Seek(size_t offset)  override
    {
        if (offset >= 0 && offset < _end)
        {
            _cur = (uint32_t)offset;
            return true;
        }
        else
        {
            return false;
        }
    }
    size_t Tell() override
    {
        return size_t(_cur);
    }
    bool Eof() override
    {
        return _cur == _end;
    }
private:
    uint32_t _cur;
    uint32_t _end;
    AbstractPack::ptr _pack;
};

} // namespace Codec
} // namespace Mmp

namespace Mmp
{
namespace Codec
{

class D3D11H264PictureContext : public H264PictureContext
{
public:
    using ptr = std::shared_ptr<D3D11H264PictureContext>;
public:
    D3D11VideoFrame::ptr frame;
};

class D3D11H264SliceDecodingProcess : public H264SliceDecodingProcess
{
protected:
    H264PictureContext::ptr CreatePictureContext() override;
};

H264PictureContext::ptr D3D11H264SliceDecodingProcess::CreatePictureContext()
{
    return std::make_shared<D3D11H264PictureContext>();
}

} // namespace Codec
} // namespace Mmp

namespace Mmp
{
namespace Codec
{

class D3D11H264FrameContext : public D3D11DecodePictureContext
{
public:
    using ptr = std::shared_ptr<D3D11H264FrameContext>;
public:
    H264SpsSyntax::ptr         sps;
    H264PpsSyntax::ptr         pps;
    H264SliceHeaderSyntax::ptr slice;
    H264NalSyntax::ptr         nal;
public:
    DXVA_PicParams_H264       pp;
    DXVA_Qmatrix_H264         qm;
    DXVA_Slice_H264_Short     sliceInfo;
    D3D11VideoFrame::ptr      frame;
    AbstractPack::ptr         pack;
public:
    std::vector<H264PictureContext::ptr> refPictures;
};

D3D11H264Decoder::D3D11H264Decoder()
{
    _deserialize = std::make_shared<H264Deserialize>();
    _deserializeContext = std::make_shared<H264ContextSyntax>();
    _sliceDecoding = std::make_shared<D3D11H264SliceDecodingProcess>();
    _reportId = 0;
}

D3D11H264Decoder::~D3D11H264Decoder()
{

}

bool D3D11H264Decoder::Push(AbstractPack::ptr pack)
{
    H26xBinaryReader::ptr br = std::make_shared<H26xBinaryReader>(std::make_shared<D3D11H264ByteReader>(pack));
    H264NalSyntax::ptr nal = std::make_shared<H264NalSyntax>();
    if (!_deserialize->DeserializeByteStreamNalUnit(br, nal))
    {
        return false;
    }
    switch (nal->nal_unit_type)
    {
        case H264NaluType::MMP_H264_NALU_TYPE_SPS:
        {
            _deserializeContext->spsSet[nal->sps->seq_parameter_set_id] = nal->sps;
            break;
        }
        case H264NaluType::MMP_H264_NALU_TYPE_PPS:
        {
            _deserializeContext->ppsSet[nal->pps->pic_parameter_set_id] = nal->pps;
            break;
        }
        default:
        break;
    }
    D3D11H264FrameContext::ptr context = std::make_shared<D3D11H264FrameContext>();
    if (nal->nal_unit_type != H264NaluType::MMP_H264_NALU_TYPE_IDR)
    {
        FillReferenceFrames(context->refPictures);
    }
    _sliceDecoding->SliceDecodingProcess(nal);
    if (!(nal->nal_unit_type == H264NaluType::MMP_H264_NALU_TYPE_SLICE || nal->nal_unit_type == H264NaluType::MMP_H264_NALU_TYPE_IDR))
    {
        return true;
    }
    if (!CanPush())
    {
        return false;
    }
    {
        context->nal = nal;
        context->slice = nal->slice;
        if (_deserializeContext->ppsSet.count(nal->slice->pic_parameter_set_id) == 0)
        {
            return true;
        }
        context->pps = _deserializeContext->ppsSet[nal->slice->pic_parameter_set_id];
        if (_deserializeContext->spsSet.count(context->pps->seq_parameter_set_id) == 0)
        {
            return true;
        }
        context->sps = _deserializeContext->spsSet[context->pps->seq_parameter_set_id];
        context->pack = pack;
    }
    StartFrame(context);
    DecodedBitStream(context);
    EndFrame(context);
    return true;
}

const std::string& D3D11H264Decoder::Description()
{
    static std::string description = "D3D11H264Decoder";
    return description;
}

void D3D11H264Decoder::FillDXVAPicParams(DXVA_PicParams_H264& pp, const std::vector<H264PictureContext::ptr>& refPictures, H264NalSyntax::ptr nal, H264SpsSyntax::ptr sps, H264PpsSyntax::ptr pps, H264SliceHeaderSyntax::ptr slice, H264SliceDecodingProcess::ptr sliceDecoding)
{
    memset(&pp, 0, sizeof(DXVA_PicParams_H264));
    H264PictureContext::ptr curPicture = sliceDecoding->GetCurrentPictureContext();
    // fill RefFrameList
    {
        pp.UsedForReferenceFlags = 0;
        pp.NonExistingFrameFlags = 0;
        // Reset
        for (size_t i=0; i<16; i++)
        {
            pp.RefFrameList[i].bPicEntry = 0xFF;
            pp.FieldOrderCntList[i][0] = 0;
            pp.FieldOrderCntList[i][1] = 0;
            pp.FrameNumList[i]         = 0;
        }
        for (size_t i=0; i<refPictures.size() && i<16; i++)
        {
            H264PictureContext::ptr picture = refPictures[i];
            {
                uint8_t flag = 0;
                uint32_t index = picture->id & 0x0F;
                if (picture->referenceFlag & H264PictureContext::used_for_long_term_reference)
                {
                    flag = 1;
                }
                assert((index & 0x0F) == index);
                pp.RefFrameList[i].bPicEntry = index | (flag << 7);
            }
            pp.FrameNumList[i] = picture->referenceFlag & H264PictureContext::used_for_long_term_reference ? picture->long_term_frame_idx : picture->FrameNum;
            pp.FieldOrderCntList[i][0] = D3D11_H264_FIELD_ORDER_CNT_OFFSET + picture->TopFieldOrderCnt;
            pp.FieldOrderCntList[i][1] = D3D11_H264_FIELD_ORDER_CNT_OFFSET + picture->BottomFieldOrderCnt;
            if (picture->field_pic_flag == 1)
            {
                pp.UsedForReferenceFlags |= 1 << (2*i + 0);
                if (picture->bottom_field_flag == 1)
                {
                    pp.UsedForReferenceFlags |= 1 << (2*i + 1);
                }
            }
            else
            {
                pp.UsedForReferenceFlags |= (1 << (2*i + 0)) | (1 << (2*i + 1));
            }
        }
    }
    //
    // FIXME : MMP 暂时只支持 Frame 格式, 不支持 Top Field Picture 或者 Bottom Field Picture
    // See also : https://github.com/HR1025/MMP-H26X/commit/c55efc91cdafc4fe01ce0116dfd120ebdd72326e
    //
    pp.CurrPic.bPicEntry    =                  (uint8_t)(curPicture->id & 0x0F) | (curPicture->bottom_field_flag << 7);
    pp.wBitFields =                            (0                                 << 0)  |
                                               (sps->mb_adaptive_frame_field_flag << 1)  |
                                               (sps->separate_colour_plane_flag   << 2)  |
                                               (0                                 << 3)  |
                                               (sps->chroma_format_idc            << 4)  |
                                               ((nal->nal_ref_idc != 0)           << 6)  |
                                               (pps->constrained_intra_pred_flag  << 7)  |
                                               (pps->weighted_pred_flag           << 8)  |
                                               (pps->weighted_bipred_idc          << 9)  |
                                               (1                                 << 11) |
                                               (sps->frame_mbs_only_flag          << 12) |
                                               (pps->transform_8x8_mode_flag      << 13) |
                                               ((sps->level_idc >= 31)            << 14) |
                                               (1                                 << 15)
                                            ;
    pp.wFrameWidthInMbsMinus1       = sps->pic_width_in_mbs_minus1;
    pp.wFrameHeightInMbsMinus1       = sps->pic_height_in_map_units_minus1;
    pp.num_ref_frames                = sps->max_num_ref_frames;
    pp.bit_depth_luma_minus8         = sps->bit_depth_chroma_minus8;
    pp.bit_depth_chroma_minus8       = sps->bit_depth_chroma_minus8;
    pp.Reserved16Bits                = 3;
    pp.StatusReportFeedbackNumber    = ++_reportId;
    pp.CurrFieldOrderCnt[0]          = D3D11_H264_FIELD_ORDER_CNT_OFFSET + curPicture->TopFieldOrderCnt;
    pp.CurrFieldOrderCnt[1]          = D3D11_H264_FIELD_ORDER_CNT_OFFSET + curPicture->BottomFieldOrderCnt;
    pp.pic_init_qs_minus26           = pps->pic_init_qs_minus26;
    pp.chroma_qp_index_offset        = pps->chroma_qp_index_offset;
    pp.second_chroma_qp_index_offset = pps->second_chroma_qp_index_offset;
    pp.ContinuationFlag              = 1;
    pp.pic_init_qp_minus26           = pps->pic_init_qp_minus26;
    pp.num_ref_idx_l0_active_minus1  = slice->num_ref_idx_l0_active_minus1;
    pp.num_ref_idx_l1_active_minus1  = slice->num_ref_idx_l1_active_minus1;
    pp.Reserved8BitsA                = 0;
    pp.frame_num                     = curPicture->FrameNum;
    pp.log2_max_frame_num_minus4     = sps->log2_max_frame_num_minus4;
    pp.pic_order_cnt_type            = sps->pic_order_cnt_type;
    if (pp.pic_order_cnt_type == 0)
    {
        pp.log2_max_pic_order_cnt_lsb_minus4 = sps->log2_max_pic_order_cnt_lsb_minus4;
    }
    else if (pp.pic_order_cnt_type == 1)
    {
        pp.delta_pic_order_always_zero_flag = sps->delta_pic_order_always_zero_flag;
    }
    pp.direct_8x8_inference_flag                = sps->direct_8x8_inference_flag;
    pp.entropy_coding_mode_flag                 = pps->entropy_coding_mode_flag;
    pp.pic_order_present_flag                   = pps->bottom_field_pic_order_in_frame_present_flag;
    pp.num_slice_groups_minus1                  = pps->num_slice_groups_minus1;
    pp.slice_group_map_type                     = pps->slice_group_map_type;
    pp.deblocking_filter_control_present_flag   = pps->deblocking_filter_control_present_flag;
    pp.redundant_pic_cnt_present_flag           = pps->redundant_pic_cnt_present_flag;
    pp.Reserved8BitsB                           = 0;
    pp.slice_group_change_rate_minus1           = 0;
    if (slice->slice_type != MMP_H264_I_SLICE && slice->slice_type != MMP_H264_SI_SLICE)
    {
        pp.wBitFields &= ~(1 << 15);
    }
}

void D3D11H264Decoder::FillDXVAQmatrixH264(DXVA_Qmatrix_H264& qm, H264SpsSyntax::ptr sps)
{
    memset(&qm, 0, sizeof(DXVA_Qmatrix_H264));
    for (size_t i=0; i<sizeof(qm.bScalingLists4x4)/sizeof(qm.bScalingLists4x4[0]) && i<sps->ScalingList4x4.size(); i++)
    {
        for (size_t j=0; j<sizeof(qm.bScalingLists4x4[0]) && j<sps->ScalingList4x4[0].size(); j++)
        {
            qm.bScalingLists4x4[i][j] = sps->ScalingList4x4[i][j];
        }
    }
    for (size_t i=0; i<sizeof(qm.bScalingLists8x8)/sizeof(qm.bScalingLists8x8[0]) && i<sps->ScalingList8x8.size(); i++)
    {
        for (size_t j=0; j<sizeof(qm.bScalingLists8x8[0]) && j<sps->ScalingList8x8[0].size(); j++)
        {
            qm.bScalingLists8x8[i][j] = sps->ScalingList8x8[i][j];
        }
    }
}

void D3D11H264Decoder::FillShortSlice(DXVA_Slice_H264_Short& sliceInfo, size_t position, size_t size)
{
    memset(&sliceInfo, 0, sizeof(DXVA_Slice_H264_Short));
    sliceInfo.BSNALunitDataLocation = (UINT)position;
    sliceInfo.SliceBytesInBuffer    = (UINT)size;
    sliceInfo.wBadSliceChopping     = 0;
}

void D3D11H264Decoder::FillLongSlice(DXVA_Slice_H264_Long& sliceInfo, H264SpsSyntax::ptr sps, H264PpsSyntax::ptr pps, H264SliceHeaderSyntax::ptr slice, size_t position, size_t size)
{
    memset(&sliceInfo, 0, sizeof(DXVA_Slice_H264_Long));
    sliceInfo.BSNALunitDataLocation = (UINT)position;
    sliceInfo.SliceBytesInBuffer = (UINT)size;
    sliceInfo.wBadSliceChopping = 0;

    sliceInfo.first_mb_in_slice = slice->first_mb_in_slice;
    sliceInfo.NumMbsForSlice = 0;
    sliceInfo.BitOffsetToSliceData = slice->slice_data_bit_offset;
    sliceInfo.slice_type = slice->slice_type;
    sliceInfo.luma_log2_weight_denom = slice->pwt->luma_log2_weight_denom;
    sliceInfo.chroma_log2_weight_denom = slice->pwt->chroma_log2_weight_denom;
    sliceInfo.num_ref_idx_l0_active_minus1 = slice->num_ref_idx_l0_active_minus1;
    sliceInfo.num_ref_idx_l1_active_minus1 = slice->num_ref_idx_l1_active_minus1;
    sliceInfo.slice_alpha_c0_offset_div2 = slice->slice_alpha_c0_offset_div2;
    sliceInfo.slice_beta_offset_div2 = slice->slice_beta_offset_div2;
    sliceInfo.Reserved8Bits = 0;

    // TODO : ...

    sliceInfo.slice_qs_delta = slice->slice_qs_delta;
    sliceInfo.slice_qp_delta = slice->slice_qp_delta;
    sliceInfo.redundant_pic_cnt = slice->redundant_pic_cnt;
    sliceInfo.direct_spatial_mv_pred_flag = slice->direct_spatial_mv_pred_flag;
    sliceInfo.cabac_init_idc = slice->cabac_init_idc;
    sliceInfo.disable_deblocking_filter_idc = slice->disable_deblocking_filter_idc;
    sliceInfo.slice_id = (USHORT)(_reportId);
}

void D3D11H264Decoder::StartFrame(const Any& context)
{
    if (context.type() != typeid(D3D11H264FrameContext::ptr))
    {
        assert(false);
        return;
    }
    
    const D3D11H264FrameContext::ptr& _context = RefAnyCast<D3D11H264FrameContext::ptr>(context);
    H264SpsSyntax::ptr sps = _context->sps;
    H264PpsSyntax::ptr pps = _context->pps;
    H264SliceHeaderSyntax::ptr slice = _context->slice;
    H264NalSyntax::ptr  nal = _context->nal;

    // Hint : Check param changed
    {
        D3D11DecoderParams param = GetDecoderParams();
        if (param.virWidth != (sps->pic_width_in_mbs_minus1 + 1) * 16 || 
            param.virHeight != (sps->pic_height_in_map_units_minus1 + 1) * 16
        )
        {
            param.codecType = CodecType::H264;
            param.virWidth = (sps->pic_width_in_mbs_minus1 + 1) * 16;
            param.virHeight = (sps->pic_height_in_map_units_minus1 + 1) * 16;
            param.width = param.virWidth - (sps->frame_crop_left_offset + sps->frame_crop_right_offset) * sps->context->CropUnitX;
            param.height = param.virHeight - (sps->frame_crop_top_offset + sps->frame_crop_bottom_offset) * sps->context->CropUnitY;
            param.quantRange = H264VuiSyntaxToQuantRange(sps->vui_seq_parameters);
            param.colorGamut = H264VuiSyntaxToColorGamut(sps->vui_seq_parameters);
            param.decoderProfiles = GetDecoderProfile();
            SetDecoderParams(param);
        }
    }
    _context->frame = GetDecoderContext()->CreateD3D11VideoFrame();
    {
        D3D11H264PictureContext::ptr picture = std::dynamic_pointer_cast<D3D11H264PictureContext>(_sliceDecoding->GetCurrentPictureContext());
        picture->frame = _context->frame;
    }
    DXVA_PicParams_H264& pp = _context->pp;
    DXVA_Qmatrix_H264& qm = _context->qm;
    FillDXVAPicParams(pp,  _context->refPictures, nal, sps, pps, slice, _sliceDecoding);
    FillDXVAQmatrixH264(qm, sps);
}

void D3D11H264Decoder::DecodedBitStream(const Any& context)
{

}

void D3D11H264Decoder::EndFrame(const Any& context)
{
    if (context.type() != typeid(D3D11H264FrameContext::ptr))
    {
        assert(false);
        return;
    }
    HRESULT hr = 0;
    const D3D11H264FrameContext::ptr& _context = RefAnyCast<D3D11H264FrameContext::ptr>(context);
    std::vector<D3D11_VIDEO_DECODER_BUFFER_DESC> decoderBufferDesc;
    {
        // D3D11_VIDEO_DECODER_BUFFER_PICTURE_PARAMETERS
        {
            D3D11_VIDEO_DECODER_BUFFER_DESC desc = {};
            GetDecoderContext()->UpdateDecoderBuffer(D3D11_VIDEO_DECODER_BUFFER_PICTURE_PARAMETERS, (uint8_t*)&(_context->pp), sizeof(_context->pp));
            desc.BufferType = D3D11_VIDEO_DECODER_BUFFER_PICTURE_PARAMETERS;
            desc.NumMBsInBuffer = 0;
            desc.DataSize = sizeof(_context->pp);
            decoderBufferDesc.push_back(desc);
        }
        // D3D11_VIDEO_DECODER_BUFFER_INVERSE_QUANTIZATION_MATRIX
        {
            D3D11_VIDEO_DECODER_BUFFER_DESC desc = {};
            GetDecoderContext()->UpdateDecoderBuffer(D3D11_VIDEO_DECODER_BUFFER_INVERSE_QUANTIZATION_MATRIX, (uint8_t*)&(_context->qm), sizeof(_context->qm));
            desc.BufferType = D3D11_VIDEO_DECODER_BUFFER_INVERSE_QUANTIZATION_MATRIX;
            desc.NumMBsInBuffer = 0;
            desc.DataSize = sizeof(_context->qm);
            decoderBufferDesc.push_back(desc);   
        }
        // D3D11_VIDEO_DECODER_BUFFER_BITSTREAM
        {
            D3D11_VIDEO_DECODER_BUFFER_DESC desc = {};
            GetDecoderContext()->UpdateDecoderBuffer(D3D11_VIDEO_DECODER_BUFFER_BITSTREAM, (uint8_t*)_context->pack->GetData(), _context->pack->GetSize());
            desc.BufferType = D3D11_VIDEO_DECODER_BUFFER_BITSTREAM;
            desc.NumMBsInBuffer = _context->sps->context->PicSizeInMapUnits;
            desc.DataSize = (UINT)(_context->pack->GetSize() + 127) & ~127;
            decoderBufferDesc.push_back(desc); 
        }
        // D3D11_VIDEO_DECODER_BUFFER_SLICE_CONTROL
        {
            D3D11_VIDEO_DECODER_BUFFER_DESC desc = {};
            DXVA_Slice_H264_Short& sliceInfo = _context->sliceInfo;
            memset(&sliceInfo, 0, sizeof(DXVA_Slice_H264_Short));
            {
                uint8_t* address = (uint8_t*)_context->pack->GetData(0);
                sliceInfo.BSNALunitDataLocation = (UINT)0;
                sliceInfo.SliceBytesInBuffer = (UINT)_context->pack->GetSize();
                sliceInfo.wBadSliceChopping = 0;
            }
            GetDecoderContext()->UpdateDecoderBuffer(D3D11_VIDEO_DECODER_BUFFER_SLICE_CONTROL, (uint8_t*)&sliceInfo, sizeof(sliceInfo));
            {
                desc.BufferType = D3D11_VIDEO_DECODER_BUFFER_SLICE_CONTROL;
                desc.NumMBsInBuffer = _context->sps->context->PicSizeInMapUnits;
                desc.DataSize = sizeof(sliceInfo);
            }
            decoderBufferDesc.push_back(desc); 
        }
    }
    if (GetDecoderContext()->DecodeFrame(decoderBufferDesc, _context->frame->view))
    {
        StreamPack::ptr stramPack = std::dynamic_pointer_cast<StreamPack>(_context->pack);
        if (stramPack)
        {
            _context->frame->dts = stramPack->dts;
            _context->frame->pts = stramPack->pts;
        }
        PushFrame(_context->frame);
    }
    else
    {
        D3D11_LOG_WARN << "DecodeFrame fail";
    }
}

void D3D11H264Decoder::FillReferenceFrames(std::vector<H264PictureContext::ptr>& refPictures)
{
    H264PictureContext::cache pictures = _sliceDecoding->GetAllPictures();
    H264PictureContext::cache shortRefPictures;
    H264PictureContext::cache longRefPictures;
    // shortRefPictures
    {
        for (auto& picture : pictures)
        {
            if (picture->referenceFlag & H264PictureContext::used_for_short_term_reference)
            {
                shortRefPictures.push_back(picture);
            }
        }
        std::reverse(shortRefPictures.begin(), shortRefPictures.end());
    }
    // longRefPictures
    {
        for (auto& picture : pictures)
        {
            if (picture->referenceFlag & H264PictureContext::used_for_long_term_reference)
            {
                longRefPictures.push_back(picture);
            }
        }
    }
    for (auto& picture : shortRefPictures)
    {
        refPictures.push_back(picture);
    }
    for (auto& picture : longRefPictures)
    {
        refPictures.push_back(picture);
    }
}

std::vector<GUID> D3D11H264Decoder::GetDecoderProfile()
{
    return {D3D11_DECODER_PROFILE_H264_VLD_FGT, D3D11_DECODER_PROFILE_H264_VLD_NOFGT};
}

} // namespace Codec
} // namespace Mmp