//
// D3D11DecodePictureContext.h
//
// Library: Codec
// Package: D3D11
// Module:  D3D11
//

#pragma once

#include <mutex>

#include "D3D11Common.h"

namespace Mmp
{
namespace Codec
{

class D3D11DecodePictureContext
{
public:
    using ptr = std::shared_ptr<D3D11DecodePictureContext>;
public:
    D3D11DecodePictureContext() = default;
    virtual ~D3D11DecodePictureContext() = default;
};

} // namespace Codec
} // namespace Mmp