//
// D3D11CodecAllocatedMethod.h
//
// Library: Codec
// Package: D3D11
// Module:  D3D11
// 

#pragma once

#include "Common/NormalAllocateMethod.h"
#include "Common/D3D11DeveiceAllocatedMethod.h"

#include "D3D11DecoderContext.h"

namespace Mmp
{
namespace Codec
{

class D3D11CodecAllocatedMethod : public D3D11DeviceAllocateMethod
{
public:
    using ptr = std::shared_ptr<D3D11CodecAllocatedMethod>;
public:
    void* GetAddress(uint64_t offset) override;
    const std::string& Tag() override;
public:
    D3D11DecoderContext::ptr decoderContext;
    NormalAllocateMethod::ptr alloc;
};

} // namespace Codec
} // namespace Mmp