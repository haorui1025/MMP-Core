//
// D3D11H264Decoder.h
//
// Library: Codec
// Package: D3D11
// Module:  D3D11
// 

#pragma once

#include "D3D11Decoder.h"

#include "H264/H264Deserialize.h"
#include "H264/H264SliceDecodingProcess.h"

namespace Mmp
{
namespace Codec
{

class D3D11H264Decoder : public D3D11Decoder
{
public:
    D3D11H264Decoder();
    ~D3D11H264Decoder();
public:
    bool Push(AbstractPack::ptr pack) override;
    const std::string& Description() override;
private:
    void StartFrame(const Any& context) override;
    void DecodedBitStream(const Any& context) override;
    void EndFrame(const Any& context) override;
private:
    void FillDXVAPicParams(DXVA_PicParams_H264& pp, const std::vector<H264PictureContext::ptr>& refPictures, H264NalSyntax::ptr nal, H264SpsSyntax::ptr sps, H264PpsSyntax::ptr pps, H264SliceHeaderSyntax::ptr slice, H264SliceDecodingProcess::ptr sliceDecoding);
    void FillDXVAQmatrixH264(DXVA_Qmatrix_H264& qm, H264SpsSyntax::ptr sps);
    void FillShortSlice(DXVA_Slice_H264_Short& sliceInfo, size_t position, size_t size);
    void FillLongSlice(DXVA_Slice_H264_Long& sliceInfo, H264SpsSyntax::ptr sps, H264PpsSyntax::ptr pps, H264SliceHeaderSyntax::ptr slice, size_t position, size_t size);
    void FillReferenceFrames(std::vector<H264PictureContext::ptr>& refPictures);
private:
    std::vector<GUID> GetDecoderProfile();
private:
    H264Deserialize::ptr                                            _deserialize;
    H264ContextSyntax::ptr                                          _deserializeContext;
    H264SliceDecodingProcess::ptr                                   _sliceDecoding;
    uint32_t                                                        _reportId;
};

} // namespace Codec
} // namespace Mmp