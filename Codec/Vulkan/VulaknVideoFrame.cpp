#include "VulaknVideoFrame.h"

#include <cassert>

#include "VulkanUtil.h"
#include "VulkanTranslator.h"

namespace Mmp
{
namespace Codec
{

VulaknVideoFrame::VulaknVideoFrame(const VulaknVideoFrameParams& params)
{
    this->params = params;
    image = VK_NULL_HANDLE;
    semaphore = VK_NULL_HANDLE;
    conversion = VK_NULL_HANDLE;
    semaphoreValue = 0;
}

VulaknVideoFrame::~VulaknVideoFrame()
{
    if (semaphore != VK_NULL_HANDLE)
    {
        VkSemaphoreWaitInfo semaphoreWaitInfo = {};
        {
            semaphoreWaitInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_WAIT_INFO;
            semaphoreWaitInfo.pSemaphores = &semaphore;
            semaphoreWaitInfo.pValues = &semaphoreValue;
            semaphoreWaitInfo.semaphoreCount = 1;
        }
        VulkanDevice::Singleton()->vkWaitSemaphores(&semaphoreWaitInfo, UINT64_MAX);
        VulkanDevice::Singleton()->vkDestroySemaphore(semaphore, nullptr);
    }
    if (image != VK_NULL_HANDLE)
    {
        VulkanDevice::Singleton()->vkDestroyImage(image, nullptr);
    }
    if (conversion != VK_NULL_HANDLE)
    {
        VulkanDevice::Singleton()->vkDestroySamplerYcbcrConversion(conversion, nullptr);
    }
}

bool VulaknVideoFrame::CreateImage()
{
    VkResult vkRes = VK_SUCCESS;
    VkExportSemaphoreCreateInfo exportSemaphoreCreateInfo = {};
    VkSemaphoreTypeCreateInfo semaphoreTypeCreateInfo = {};
    VkSemaphoreCreateInfo semaphoreCreateInfo = {};
    VkImageCreateInfo imageCreateInfo = {};
    // VkExportSemaphoreCreateInfo
#if MMP_PLATFORM(WINDOWS)
    {
        exportSemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO;
        exportSemaphoreCreateInfo.handleTypes = VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_BIT;
    }
#else
    {
        exportSemaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO;
        exportSemaphoreCreateInfo.handleTypes = VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT;
    }
#endif
    // VkSemaphoreTypeCreateInfo
    {
        semaphoreTypeCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_TYPE_CREATE_INFO;
        semaphoreTypeCreateInfo.semaphoreType = VK_SEMAPHORE_TYPE_TIMELINE;
        semaphoreTypeCreateInfo.initialValue = 0;
        semaphoreTypeCreateInfo.pNext = &exportSemaphoreCreateInfo;
    }
    // VkSemaphoreCreateInfo
    {
        semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
        semaphoreCreateInfo.pNext = &semaphoreCreateInfo;
    }
    // VkImageCreateInfo
    {
        imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        imageCreateInfo.pNext = nullptr; // TODO
        imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
        imageCreateInfo.format = params.format;
        {
            imageCreateInfo.extent.width = params.width;
            imageCreateInfo.extent.height = params.height;
            imageCreateInfo.extent.depth = 1;
        }
        imageCreateInfo.mipLevels = 1;
        imageCreateInfo.arrayLayers = params.arrayLayers;
        imageCreateInfo.flags = params.flags;
        imageCreateInfo.tiling = params.tiling;
        imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        imageCreateInfo.usage = params.usage;
        imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
        imageCreateInfo.pQueueFamilyIndices = params.pQueueFamilyIndices;
        imageCreateInfo.queueFamilyIndexCount = params.queueFamilyIndexCount;
        imageCreateInfo.sharingMode = params.queueFamilyIndexCount > 1 ? VK_SHARING_MODE_CONCURRENT : VK_SHARING_MODE_EXCLUSIVE;
    }
    vkRes = VulkanDevice::Singleton()->vkCreateImage(&imageCreateInfo, nullptr, &image);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vkCreateImage fail, error is: " << VkResultToStr(vkRes);
        assert(false);
        goto END;
    }
    vkRes = VulkanDevice::Singleton()->vkCreateSemaphore(&semaphoreCreateInfo, nullptr, &semaphore);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vkCreateSemaphore fail, error is: " << VkResultToStr(vkRes);
        assert(false);
        goto END;
    }
END:
    return VULKAN_SUCCEEDED(vkRes);
}

bool VulaknVideoFrame::CreateView()
{
    VkResult vkRes = VK_SUCCESS;
    VkSamplerYcbcrConversionCreateInfo conversionCreateInfo = {};
    VkSamplerYcbcrConversionInfo conversionInfo = {};
    VkImageViewCreateInfo viewCreateInfo = {};
    // VkSamplerYcbcrConversionCreateInfo
    {
        conversionCreateInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_CREATE_INFO;
        {
            conversionCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
            conversionCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            conversionCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            conversionCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        }
        conversionCreateInfo.ycbcrModel = VK_SAMPLER_YCBCR_MODEL_CONVERSION_RGB_IDENTITY;
        conversionCreateInfo.ycbcrRange = QuantRangeToVkSamplerYcbcrRange(params.quantRange);
        conversionCreateInfo.xChromaOffset = VK_CHROMA_LOCATION_COSITED_EVEN; // to check
        conversionCreateInfo.yChromaOffset = VK_CHROMA_LOCATION_MIDPOINT; // check
        conversionCreateInfo.format = params.format;
    }
    vkRes = VulkanDevice::Singleton()->vkCreateSamplerYcbcrConversion(&conversionCreateInfo, nullptr, &conversion);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vkCreateSamplerYcbcrConversion fail, error is: " << VkResultToStr(vkRes);
        assert(false);
        goto END;
    }
    // VkSamplerYcbcrConversionInfo
    {
        conversionInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_YCBCR_CONVERSION_INFO;
        conversionInfo.conversion = conversion;
    }
    // VkImageViewCreateInfo
    {
        viewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        viewCreateInfo.pNext = &conversionInfo;
        viewCreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        viewCreateInfo.format = params.format;
        viewCreateInfo.image = image;
        {
            viewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            viewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            viewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
            viewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        }
        {
            viewCreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            viewCreateInfo.subresourceRange.baseArrayLayer = 0;
            viewCreateInfo.subresourceRange.layerCount = 1;
            viewCreateInfo.subresourceRange.levelCount = 1;
        }
    }
    vkRes = VulkanDevice::Singleton()->vkCreateImageView(&viewCreateInfo, nullptr, &view);
END:
    return VULKAN_SUCCEEDED(vkRes);
}

} // namespace Codec
} // namespace Mmp