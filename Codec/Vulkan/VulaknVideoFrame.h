//
// VulaknVideoFrame.h
//
// Library: Codec
// Package: Vulkan
// Module:  Vulkan
// 
//

#pragma once

#include <memory>

#include "VulkanCommon.h"

namespace Mmp
{
namespace Codec
{

class VulaknVideoFrame
{
public:
    using ptr = std::shared_ptr<VulaknVideoFrame>;
public:
    VulaknVideoFrame(const VulaknVideoFrameParams& params);
    ~VulaknVideoFrame();
public:
    bool CreateImage();
    bool CreateView();
public:
    VulaknVideoFrameParams params;
    VkImage image;
    VkImageView view;
    VkSemaphore semaphore;
    uint64_t semaphoreValue;
    VkSamplerYcbcrConversion conversion;
};

} // namespace Codec
} // namespace Mmp