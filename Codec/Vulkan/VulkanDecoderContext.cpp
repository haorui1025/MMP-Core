#include "VulkanDecoderContext.h"

#include <memory.h>

#include "VulkanUtil.h"
#include "VulkanTranslator.h"

namespace Mmp
{
namespace Codec
{

VulkanDecoderContext::VulkanDecoderContext()
{
    _session = VK_NULL_HANDLE;
}

VulkanDecoderContext::~VulkanDecoderContext()
{
    if (_session != VK_NULL_HANDLE)
    {
        VulkanDevice::Singleton()->vkDestroyVideoSessionKHR(_session, nullptr);
    }
    for (auto& deviceMemorie : _deviceMemories)
    {
        if (deviceMemorie)
        {
            VulkanDevice::Singleton()->vkFreeMemory(deviceMemorie, nullptr);
        }
    }
}

bool VulkanDecoderContext::Init(const VulkanDecoderParams& decoderParams)
{
    _decoderParams = decoderParams;
    if (!InitProfile())
    {
        VULKAN_LOG_ERROR << "VulkanDecoderContext::InitProfile fail";
        assert(false);
        return false;
    }
    if (!CreateSession())
    {
        VULKAN_LOG_ERROR << "VulkanDecoderContext::CreateSession fail";
        assert(false);
        return false;
    }
    return true;
}

VkVideoSessionKHR VulkanDecoderContext::GetSession()
{
    return _session;
}

bool VulkanDecoderContext::DecodeFrame(VkVideoSessionParametersKHR sessionParameters, VkVideoDecodeInfoKHR* decodeInfo)
{
    std::lock_guard<std::mutex> lock(_mtx);
    VkResult vkRes = VK_SUCCESS;
    VkVideoBeginCodingInfoKHR videoBeginCodingInfo = {};
    VkVideoEndCodingInfoKHR videoEndCodingInfo = {};
    VkCommandBuffer commandBuffer;
    // VkVideoBeginCodingInfoKHR
    {
        videoBeginCodingInfo.sType = VK_STRUCTURE_TYPE_VIDEO_BEGIN_CODING_INFO_KHR;
        videoBeginCodingInfo.videoSession = GetSession();
        videoBeginCodingInfo.videoSessionParameters = sessionParameters;
        videoBeginCodingInfo.referenceSlotCount = decodeInfo->referenceSlotCount;
        videoBeginCodingInfo.pReferenceSlots = decodeInfo->pReferenceSlots;
    }
    // VkVideoEndCodingInfoKHR
    {
        videoEndCodingInfo.sType = VK_STRUCTURE_TYPE_VIDEO_END_CODING_INFO_KHR;
    }
    if (!_commonQueue->Start())
    {
        VULKAN_LOG_ERROR << "VulkanCommandQueue::Start fail";
        assert(false);
        return false;
    }
    VulkanDevice::Singleton()->vkCmdBeginVideoCodingKHR(commandBuffer, &videoBeginCodingInfo);
    VulkanDevice::Singleton()->vkCmdDecodeVideoKHR(commandBuffer, decodeInfo);
    VulkanDevice::Singleton()->vkCmdEndVideoCodingKHR(commandBuffer, &videoEndCodingInfo);
    if (!_commonQueue->Submit())
    {
        VULKAN_LOG_ERROR << "VulkanCommandQueue::Submit fail";
        assert(false);
        return false;
    }
    return VULKAN_SUCCEEDED(vkRes);
}

VulaknVideoFrame::ptr VulkanDecoderContext::CreateVulaknVideoFrame()
{
    VulaknVideoFrameParams frameParams = {};
    VulaknVideoFrame::ptr frame;
    {
        // TODO
        frameParams.arrayLayers = 1;
        frameParams.format = _vkFormat;
        frameParams.width = _decoderParams.info.width;
        frameParams.height = _decoderParams.info.height;
        frameParams.quantRange = _decoderParams.info.quantRange;
        frameParams.colorGamut = _decoderParams.info.colorGamut;
    }
    frame = std::make_shared<VulaknVideoFrame>(frameParams);
    if (!frame->CreateImage())
    {
        VULKAN_LOG_ERROR << "VulaknVideoFrame::CreateImage fail";
        assert(false);
        return nullptr;
    }
    if (!frame->CreateView())
    {
        VULKAN_LOG_ERROR << "VulaknVideoFrame::CreateView fail";
        assert(false);
        return nullptr;
    }
    return frame;
}

bool VulkanDecoderContext::InitProfile()
{
    VkResult vkRes = VK_SUCCESS;
    // VkVideoDecode...CapabilitiesKHR
    {
        switch (_decoderParams.codecType)
        {
            case CodecType::H264:
            {
                _h264Capabilities.sType = VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_CAPABILITIES_KHR;
                _decodeCapabilities.pNext = &_h264Capabilities;
                break;
            }
            default:
            {
                // TODO
                assert(false);
                return false;
            }
        }
    }
    // VkVideoDecode...ProfileInfoKHR
    {
        switch (_decoderParams.codecType)
        {
            case CodecType::H264:
            {
                _h264ProfileInfo.sType = VK_STRUCTURE_TYPE_VIDEO_DECODE_H264_PROFILE_INFO_KHR;
                _h264ProfileInfo.stdProfileIdc = ToStdVideoH264ProfileIdc(_decoderParams.profile);
                _h264ProfileInfo.pictureLayout = VK_VIDEO_DECODE_H264_PICTURE_LAYOUT_PROGRESSIVE_KHR;
                _decodeUsageInfo.pNext = &_h264ProfileInfo;
                break;
            }
            default:
            {
                // TODO
                assert(false);
                return false;
            }
        }
    }
    // VkVideoDecodeUsageInfoKHR
    {
        _decodeUsageInfo.sType = VK_STRUCTURE_TYPE_VIDEO_DECODE_USAGE_INFO_KHR;
        _decodeUsageInfo.videoUsageHints = VK_VIDEO_DECODE_USAGE_DEFAULT_KHR;
    }
    // VkVideoProfileInfoKHR
    {
        _videoProfileInfo.pNext = &_decodeUsageInfo;
        _videoProfileInfo.sType = VK_STRUCTURE_TYPE_VIDEO_PROFILE_INFO_KHR;
        _videoProfileInfo.videoCodecOperation = DecoderCodecTypeToVkVideoCodecOperationFlagBitsKHR(_decoderParams.codecType);
        _videoProfileInfo.lumaBitDepth = DepthToVkVideoComponentBitDepthFlagBitsKHR(_decoderParams.info.bitdepth);
        _videoProfileInfo.chromaBitDepth = _videoProfileInfo.lumaBitDepth;
        _videoProfileInfo.chromaSubsampling = PixelFormatToVkVideoChromaSubsamplingFlagBitsKHR(_decoderParams.info.format);
    }
    // VkVideoProfileListInfoKHR
    {
        _videoProfileListInfo.sType = VK_STRUCTURE_TYPE_VIDEO_PROFILE_LIST_INFO_KHR;
        _videoProfileListInfo.profileCount = 1;
        _videoProfileListInfo.pProfiles = &_videoProfileInfo;
    }
    // VkVideoDecodeCapabilitiesKHR
    {
        _decodeCapabilities.sType = VK_STRUCTURE_TYPE_VIDEO_DECODE_CAPABILITIES_KHR;
    }
    // VkVideoCapabilitiesKHR
    {
        _videoCapabilities.sType = VK_STRUCTURE_TYPE_VIDEO_CAPABILITIES_KHR;
        _videoCapabilities.pNext = &_decodeCapabilities;
    }
    vkRes = VulkanDevice::Singleton()->vkGetPhysicalDeviceVideoCapabilitiesKHR(&_videoProfileInfo, &_videoCapabilities);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vkGetPhysicalDeviceVideoCapabilitiesKHR fail, error is: " << VkResultToStr(vkRes);
        assert(false);
    }
    // VkPhysicalDeviceVideoFormatInfoKHR
    {
        _deviceVideoFormatInfo.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VIDEO_FORMAT_INFO_KHR;
        _deviceVideoFormatInfo.pNext = &_videoProfileListInfo;
        if (!(_decodeCapabilities.flags & VK_VIDEO_DECODE_CAPABILITY_DPB_AND_OUTPUT_COINCIDE_BIT_KHR))
        {
            _deviceVideoFormatInfo.imageUsage = VK_IMAGE_USAGE_VIDEO_DECODE_DPB_BIT_KHR;
        }
        else
        {
            _deviceVideoFormatInfo.imageUsage = VK_IMAGE_USAGE_VIDEO_DECODE_DPB_BIT_KHR | VK_IMAGE_USAGE_VIDEO_DECODE_DST_BIT_KHR | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_SAMPLED_BIT; 
        }
    }
    uint32_t videoFormatPropertyCount = 0;
    vkRes = VulkanDevice::Singleton()->vkGetPhysicalDeviceVideoFormatPropertiesKHR(&_deviceVideoFormatInfo, &videoFormatPropertyCount, nullptr);
    if (VULKAN_FAILED(vkRes))
    {
        assert(false);
        VULKAN_LOG_ERROR << "vkGetPhysicalDeviceVideoFormatPropertiesKHR fail, error is: " << VkResultToStr(vkRes);
    }
    _videoFormatProperties.resize(videoFormatPropertyCount);
    _vkFormat = VK_FORMAT_G8_B8R8_2PLANE_420_UNORM;
    vkRes = VulkanDevice::Singleton()->vkGetPhysicalDeviceVideoFormatPropertiesKHR(&_deviceVideoFormatInfo, &videoFormatPropertyCount, _videoFormatProperties.data());
    if (VULKAN_FAILED(vkRes))
    {
        assert(false);
        VULKAN_LOG_ERROR << "vkGetPhysicalDeviceVideoFormatPropertiesKHR fail, error is: " << VkResultToStr(vkRes);
    }
    {
        VULKAN_LOG_INFO << "Decoder Profile Info";
        VULKAN_LOG_INFO << "-- Header Name : " << _videoCapabilities.stdHeaderVersion.extensionName;
        VULKAN_LOG_INFO << "-- Header Version : " << _videoCapabilities.stdHeaderVersion.specVersion;
        VULKAN_LOG_INFO << "-- Width : " << _videoCapabilities.minCodedExtent.width << " -> " << _videoCapabilities.maxCodedExtent.width;
        VULKAN_LOG_INFO << "-- Height : " << _videoCapabilities.minCodedExtent.height << " -> " << _videoCapabilities.maxCodedExtent.height;
        VULKAN_LOG_INFO << "-- Width Alignment : " << _videoCapabilities.pictureAccessGranularity.width;
        VULKAN_LOG_INFO << "-- Height Alignment : " << _videoCapabilities.pictureAccessGranularity.height;
        VULKAN_LOG_INFO << "-- Bitstream Offset Aligment : " << _videoCapabilities.minBitstreamBufferOffsetAlignment;
        VULKAN_LOG_INFO << "-- Bitstream Size Aligment : " << _videoCapabilities.minBitstreamBufferSizeAlignment;
        VULKAN_LOG_INFO << "-- Max Dpb Slot : " << _videoCapabilities.maxDpbSlots;
        VULKAN_LOG_INFO << "-- Max ActiveReference Pictures : " << _videoCapabilities.maxActiveReferencePictures;
        VULKAN_LOG_INFO << "-- VkVideoDecodeCapabilityFlags : " << VkVideoDecodeCapabilityFlagsToStr(_decodeCapabilities.flags);
        VULKAN_LOG_INFO << "-- VkVideoCapabilityFlags : " << VkVideoCapabilityFlagsToStr(_videoCapabilities.flags);
    }
    return VULKAN_SUCCEEDED(vkRes);
}

bool VulkanDecoderContext::CreateSession()
{
    VkResult vkRes = VK_SUCCESS;
    VkPhysicalDeviceMemoryProperties deviceMemoryProperties = VulkanDevice::Singleton()->GetVkPhysicalDeviceMemoryProperties();
    uint32_t memoryRequirementsCount = 0;
    {
        VkVideoSessionCreateInfoKHR sessionCreateInfo = {};
        VkExtensionProperties extensionProperties = {};
        switch (_decoderParams.codecType)
        {
            case CodecType::H264:
            {
                memcpy(extensionProperties.extensionName, VK_STD_VULKAN_VIDEO_CODEC_H264_DECODE_EXTENSION_NAME, sizeof(VK_STD_VULKAN_VIDEO_CODEC_H264_DECODE_EXTENSION_NAME));
                extensionProperties.specVersion = VK_STD_VULKAN_VIDEO_CODEC_H264_DECODE_SPEC_VERSION;
                break;
            }
            default:
                assert(false);
                return false;
        }
        {
            sessionCreateInfo.sType                        = VK_STRUCTURE_TYPE_VIDEO_SESSION_CREATE_INFO_KHR;
            sessionCreateInfo.flags                        = 0;
            sessionCreateInfo.queueFamilyIndex             = VulkanDevice::Singleton()->GetQueueFamilyIndex(VK_QUEUE_VIDEO_DECODE_BIT_KHR);
            sessionCreateInfo.maxCodedExtent               = _videoCapabilities.maxCodedExtent;
            sessionCreateInfo.maxDpbSlots                  = _videoCapabilities.maxDpbSlots;
            sessionCreateInfo.maxActiveReferencePictures   = _videoCapabilities.maxActiveReferencePictures;
            sessionCreateInfo.pictureFormat                = _vkFormat;
            sessionCreateInfo.referencePictureFormat       = sessionCreateInfo.pictureFormat;
            sessionCreateInfo.pStdHeaderVersion            = &extensionProperties;
            sessionCreateInfo.pVideoProfile                = &_videoProfileInfo;
        }
        vkRes = VulkanDevice::Singleton()->vkCreateVideoSessionKHR(&sessionCreateInfo, nullptr, &_session);
        if (VULKAN_FAILED(vkRes))
        {
            VULKAN_LOG_ERROR << "vkCreateVideoSessionKHR fail, error is: " << VkResultToStr(vkRes);
            assert(false);
            goto END;
        }
    }
    {
        vkRes = VulkanDevice::Singleton()->vkGetVideoSessionMemoryRequirementsKHR(_session, &memoryRequirementsCount, nullptr);
        if (VULKAN_FAILED(vkRes))
        {
            VULKAN_LOG_ERROR << "vkGetVideoSessionMemoryRequirementsKHR fail, error is: " << VkResultToStr(vkRes);
            assert(false);
            goto END;
        }
        _memoryRequirements.resize(memoryRequirementsCount);
        _deviceMemories.resize(memoryRequirementsCount);
        for (auto& memoryRequirement : _memoryRequirements)
        {
            memoryRequirement.sType = VK_STRUCTURE_TYPE_VIDEO_SESSION_MEMORY_REQUIREMENTS_KHR;
        }
        vkRes = VulkanDevice::Singleton()->vkGetVideoSessionMemoryRequirementsKHR(_session, &memoryRequirementsCount, _memoryRequirements.data());
        if (VULKAN_FAILED(vkRes))
        {
            VULKAN_LOG_ERROR << "vkGetVideoSessionMemoryRequirementsKHR fail, error is: " << VkResultToStr(vkRes);
            assert(false);
            goto END;
        }
    }
    for (uint32_t i=0; i<memoryRequirementsCount; i++)
    {
        VkMemoryAllocateInfo memoryAllocateInfo = {};
        int32_t index = -1;
        for (size_t j=0; j<deviceMemoryProperties.memoryTypeCount; j++)
        {
            if (!(_memoryRequirements[i].memoryRequirements.memoryTypeBits) & (1 << i))
            {
                continue;
            }
            index = (int32_t)j;
            break;
        }
        if (index < 0)
        {
            assert(false);
            return false;
        }
        {
            memoryAllocateInfo.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
            memoryAllocateInfo.pNext = nullptr;
            memoryAllocateInfo.allocationSize = _memoryRequirements[i].memoryRequirements.size;
            memoryAllocateInfo.memoryTypeIndex = (uint32_t)index;
        }
        vkRes = VulkanDevice::Singleton()->vkAllocateMemory(&memoryAllocateInfo, nullptr, &_deviceMemories[i]);
        if (VULKAN_FAILED(vkRes))
        {
            VULKAN_LOG_ERROR << "vkAllocateMemory fail, error is: " << VkResultToStr(vkRes);
            assert(false);
            goto END;
        }
    }
    {
        std::vector<VkBindVideoSessionMemoryInfoKHR> bindVideoSessionMemoryInfos;
        bindVideoSessionMemoryInfos.resize(memoryRequirementsCount);
        for (uint32_t i=0; i<memoryRequirementsCount; i++)
        {
            bindVideoSessionMemoryInfos[i].sType = VK_STRUCTURE_TYPE_BIND_VIDEO_SESSION_MEMORY_INFO_KHR;
            bindVideoSessionMemoryInfos[i].memory = _deviceMemories[i];
            bindVideoSessionMemoryInfos[i].memoryBindIndex = _memoryRequirements[i].memoryBindIndex;
            bindVideoSessionMemoryInfos[i].memoryOffset = 0;
            bindVideoSessionMemoryInfos[i].memorySize = _memoryRequirements[i].memoryRequirements.size;
        }
        vkRes = VulkanDevice::Singleton()->vkBindVideoSessionMemoryKHR(_session, memoryRequirementsCount, bindVideoSessionMemoryInfos.data());
        if (VULKAN_FAILED(vkRes))
        {
            VULKAN_LOG_ERROR << "vkBindVideoSessionMemoryKHR fail, error is: " << VkResultToStr(vkRes);
            assert(false);
            goto END;
        }
    }
END:
    return VULKAN_SUCCEEDED(vkRes);
}

} // namespace Codec
} // namespace Mmp