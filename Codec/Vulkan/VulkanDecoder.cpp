#include "VulkanDecoder.h"

#include <cassert>


namespace Mmp
{
namespace Codec
{

VulkanDecoder::VulkanDecoder()
{
}

void VulkanDecoder::SetParameter(Any parameter, const std::string& property)
{
    AbstractDecoder::SetParameter(parameter, property);
}

Any VulkanDecoder::GetParamter(const std::string& property)
{
    return AbstractDecoder::GetParamter(property);
}

bool VulkanDecoder::Init()
{
    return true;
}

void VulkanDecoder::Uninit()
{
}

bool VulkanDecoder::Start()
{
    return true;
}

void VulkanDecoder::Stop()
{
}

bool VulkanDecoder::Push(AbstractPack::ptr pack)
{
    return true;
}

bool VulkanDecoder::Pop(AbstractFrame::ptr& frame)
{
    std::lock_guard<std::mutex> lock(_bufMtx);
    if (!_buffers.empty())
    {
        frame = _buffers.front();
        _buffers.pop_front();
        return true;
    }
    else
    {
        return false;
    }
}

bool VulkanDecoder::CanPush()
{
    return true;
}

bool VulkanDecoder::CanPop()
{
    std::lock_guard<std::mutex> lock(_bufMtx);
    return !_buffers.empty();
}

const std::string& VulkanDecoder::Description()
{
    static std::string description = "VulkanDecoder";
    return description;
}

VulkanDecoderContext::ptr VulkanDecoder::GetVulkanDecoderContext()
{
    return _decoderContext;
}

VulkanDecoderParams VulkanDecoder::GetDecoderParams()
{
    return _params;
}

void VulkanDecoder::SetDecoderParams(const VulkanDecoderParams& params)
{
    if (_params != params)
    {
        VulkanDecoderParams oldParams = _params;
        _params = params;
        _decoderContext = std::make_shared<VulkanDecoderContext>();
        if (!_decoderContext->Init(_params))
        {
            VULKAN_LOG_ERROR << "VulkanDecoderContext Init fail";
            assert(false);
        }
        OnDecoderParamsChange(oldParams, params);
    }
}

void VulkanDecoder::OnDecoderParamsChange(const VulkanDecoderParams& oldValue, const VulkanDecoderParams& newValue)
{

}

} // namespace Codec
} // namespace Mmp