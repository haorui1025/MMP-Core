//
// VulkanDecoder.h
//
// Library: Codec
// Package: Vulkan
// Module:  Vulkan
// 
//

#pragma once

#include <set>
#include <mutex>
#include <deque>

#include "VulkanCommon.h"
#include "VulkanDecoderContext.h"

namespace Mmp
{
namespace Codec
{

class VulkanDecoder : public AbstractDecoder
{
public:
    using ptr = std::shared_ptr<VulkanDecoder>;
public:
    VulkanDecoder();
    virtual ~VulkanDecoder() = default;
public:
    void SetParameter(Any parameter, const std::string& property) override;
    Any GetParamter(const std::string& property) override;
    bool Init() override;
    void Uninit() override;
    bool Start() override;
    void Stop() override;
    bool Push(AbstractPack::ptr pack) override;
    bool Pop(AbstractFrame::ptr& frame) override;
    bool CanPush() override;
    bool CanPop() override;
    const std::string& Description() override;
public: /* Hook */
    virtual void StartFrame(const Any& context) = 0;
    virtual void DecodedBitStream(const Any& context) = 0;
    virtual void EndFrame(const Any& context) = 0;
protected:
    VulkanDecoderContext::ptr GetVulkanDecoderContext();
    VulkanDecoderParams GetDecoderParams();
    void SetDecoderParams(const VulkanDecoderParams& params);
protected: /* Event */
    virtual void OnDecoderParamsChange(const VulkanDecoderParams& oldValue, const VulkanDecoderParams& newValue);
private:
    std::mutex _bufMtx;
    std::deque<StreamFrame::ptr> _buffers;
private:
    VulkanDecoderContext::ptr _decoderContext;
    VulkanDecoderParams _params;
};

} // namespace Codec
} // namespace Mmp