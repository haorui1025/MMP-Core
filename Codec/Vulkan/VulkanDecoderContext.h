//
// VulkanDecoderContext.h
//
// Library: Codec
// Package: Vulkan
// Module:  Vulkan
// 

#pragma once

#include <mutex>
#include <memory>

#include "VulkanCommon.h"
#include "VulaknVideoFrame.h"
#include "VulkanCommandQueue.h"

namespace Mmp
{
namespace Codec
{

class VulkanDecoderContext
{
public:
    using ptr = std::shared_ptr<VulkanDecoderContext>;
public:
    VulkanDecoderContext();
    virtual ~VulkanDecoderContext();
public:
    bool Init(const VulkanDecoderParams& decoderParams);
    VkVideoSessionKHR GetSession();
    bool DecodeFrame(VkVideoSessionParametersKHR sessionParameters, VkVideoDecodeInfoKHR* decodeInfo);
    VulaknVideoFrame::ptr CreateVulaknVideoFrame();
private:
    bool InitProfile();
    bool CreateSession();
private:
    std::mutex           _mtx;
    VulkanDecoderParams  _decoderParams;
    VkFormat             _vkFormat;
private:
    VkVideoSessionKHR    _session;
    VulkanCommandQueue::ptr _commonQueue;
    std::vector<VkVideoSessionMemoryRequirementsKHR> _memoryRequirements;
    std::vector<VkDeviceMemory> _deviceMemories;
private:
    VkVideoProfileListInfoKHR                _videoProfileListInfo = {};
    VkVideoDecodeUsageInfoKHR                _decodeUsageInfo = {};
    VkVideoProfileInfoKHR                    _videoProfileInfo = {};
    VkVideoCapabilitiesKHR                   _videoCapabilities = {};
    VkVideoDecodeCapabilitiesKHR             _decodeCapabilities = {};
    VkVideoDecodeH264CapabilitiesKHR         _h264Capabilities = {};
    VkVideoDecodeH264ProfileInfoKHR          _h264ProfileInfo = {};
    VkPhysicalDeviceVideoFormatInfoKHR       _deviceVideoFormatInfo = {};
    std::vector<VkVideoFormatPropertiesKHR>  _videoFormatProperties;
};

} // namespace Codec
} // namespace Mmp