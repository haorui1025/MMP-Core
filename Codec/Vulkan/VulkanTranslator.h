//
// VulkanTranslator.h
//
// Library: Codec
// Package: Vulkan
// Module:  Vulkan
// 
//

#pragma once

#include "Common/PixelFormat.h"

#include "VulkanCommon.h"

namespace Mmp
{
namespace Codec
{

VkSamplerYcbcrRange QuantRangeToVkSamplerYcbcrRange(const QuantRange& quantRange);

VkVideoChromaSubsamplingFlagBitsKHR PixelFormatToVkVideoChromaSubsamplingFlagBitsKHR(const PixelFormat& format);

VkVideoComponentBitDepthFlagBitsKHR DepthToVkVideoComponentBitDepthFlagBitsKHR(int32_t bitdepth);

StdVideoH264ProfileIdc ToStdVideoH264ProfileIdc(uint8_t profile_idc);

VkVideoCodecOperationFlagBitsKHR DecoderCodecTypeToVkVideoCodecOperationFlagBitsKHR(const CodecType& codecType);

VkSamplerYcbcrModelConversion ColorGamutToVkSamplerYcbcrModelConversion(ColorGamut colorGamut);

} // namespace Codec
} // namespace Mmp