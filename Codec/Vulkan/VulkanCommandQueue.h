//
// VulkanCommandQueue.h
//
// Library: Codec
// Package: Vulkan
// Module:  Vulkan

#pragma once

#include <set>
#include <mutex>
#include <deque>

#include "VulkanCommon.h"


namespace Mmp
{
namespace Codec
{

class VulkanCommandQueue
{
public:
    using ptr = std::shared_ptr<VulkanCommandQueue>;
public:
    VulkanCommandQueue();
    ~VulkanCommandQueue();
public:
    bool Init(const VulkanCommandQueueParams& params);
public:
    void Sync();
    bool Start();
    bool Submit();
private:
    std::mutex _mtx;
    VkCommandPool _commandPool;
    VkQueue _commandQueue;
    VkFence _commandBufferFence;
    VkCommandBuffer _commandBuffer;
    VkSemaphoreSubmitInfo _commandBufferSemWait;
    int32_t _commandBufferSemWaitCount;
    VkSemaphoreSubmitInfo _commandBufferSemSig;
    int32_t _commandBufferSemSigCount;
};

} // namespace Codec
} // namespace Mmp