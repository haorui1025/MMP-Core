#include "VulkanTranslator.h"

namespace Mmp
{
namespace Codec
{

VkSamplerYcbcrRange QuantRangeToVkSamplerYcbcrRange(const QuantRange& quantRange)
{
    switch (quantRange)
    {
        case QuantRange::LIMIT:  return VkSamplerYcbcrRange::VK_SAMPLER_YCBCR_RANGE_ITU_NARROW;
        default: return VkSamplerYcbcrRange::VK_SAMPLER_YCBCR_RANGE_ITU_FULL;
    }
}

VkVideoChromaSubsamplingFlagBitsKHR PixelFormatToVkVideoChromaSubsamplingFlagBitsKHR(const PixelFormat& format)
{
    switch (format)
    {
        case PixelFormat::NV12: return VkVideoChromaSubsamplingFlagBitsKHR::VK_VIDEO_CHROMA_SUBSAMPLING_420_BIT_KHR;
        default:
            // Hint : unsupport for now
            assert(false);
            return VkVideoChromaSubsamplingFlagBitsKHR::VK_VIDEO_CHROMA_SUBSAMPLING_420_BIT_KHR;
    }
}

VkVideoComponentBitDepthFlagBitsKHR DepthToVkVideoComponentBitDepthFlagBitsKHR(int32_t bitdepth)
{
    switch (bitdepth)
    {
        case 8: return VkVideoComponentBitDepthFlagBitsKHR::VK_VIDEO_COMPONENT_BIT_DEPTH_8_BIT_KHR;
        case 10: return VkVideoComponentBitDepthFlagBitsKHR::VK_VIDEO_COMPONENT_BIT_DEPTH_10_BIT_KHR;
        case 12: return VkVideoComponentBitDepthFlagBitsKHR::VK_VIDEO_COMPONENT_BIT_DEPTH_12_BIT_KHR;
        default:
            assert(false);
            return VkVideoComponentBitDepthFlagBitsKHR::VK_VIDEO_COMPONENT_BIT_DEPTH_8_BIT_KHR;
    }
}

StdVideoH264ProfileIdc ToStdVideoH264ProfileIdc(uint8_t profile_idc)
{
    switch (profile_idc)
    {
        case 66: return StdVideoH264ProfileIdc::STD_VIDEO_H264_PROFILE_IDC_BASELINE;
        case 77: return StdVideoH264ProfileIdc::STD_VIDEO_H264_PROFILE_IDC_MAIN;
        case 100: return StdVideoH264ProfileIdc::STD_VIDEO_H264_PROFILE_IDC_HIGH;
        case 244: return StdVideoH264ProfileIdc::STD_VIDEO_H264_PROFILE_IDC_HIGH_444_PREDICTIVE;
        default:
            assert(false);
            return STD_VIDEO_H264_PROFILE_IDC_INVALID;
    }
}

VkVideoCodecOperationFlagBitsKHR DecoderCodecTypeToVkVideoCodecOperationFlagBitsKHR(const CodecType& codecType)
{
    switch (codecType)
    {
        case CodecType::H264: return VkVideoCodecOperationFlagBitsKHR::VK_VIDEO_CODEC_OPERATION_DECODE_H264_BIT_KHR;
        case CodecType::H265: return VkVideoCodecOperationFlagBitsKHR::VK_VIDEO_CODEC_OPERATION_DECODE_H265_BIT_KHR;
        default:
            assert(false);
            return VkVideoCodecOperationFlagBitsKHR::VK_VIDEO_CODEC_OPERATION_NONE_KHR;
    }
}

VkSamplerYcbcrModelConversion ColorGamutToVkSamplerYcbcrModelConversion(ColorGamut colorGamut)
{
    if (MMP_COLOR_GAMUT_BT601(colorGamut))
    {
        return VkSamplerYcbcrModelConversion::VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_601;
    }
    else if (MMP_COLOR_GAMUT_BT709(colorGamut))
    {
        return VkSamplerYcbcrModelConversion::VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_709;
    }
    else if (MMP_COLOR_GAMUT_BT2020(colorGamut))
    {
        return VkSamplerYcbcrModelConversion::VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_2020;
    }
    else
    {
        return VkSamplerYcbcrModelConversion::VK_SAMPLER_YCBCR_MODEL_CONVERSION_YCBCR_IDENTITY;
    }
}

} // namespace Codec
} // namespace Mmp