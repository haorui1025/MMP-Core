#include "VulkanCommon.h"

namespace Mmp
{
namespace Codec
{

VulkanDecoderParams::VulkanDecoderParams()
{
    codecType = CodecType::H264;
    profile = 0;
}

bool operator==(const VulkanDecoderParams& left, const VulkanDecoderParams& right)
{
    if (left.info == right.info &&
        left.codecType == right.codecType &&
        left.profile == right.profile
    )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool operator!=(const VulkanDecoderParams& left, const VulkanDecoderParams& right)
{
    return !(left == right);
}

VulkanCommandQueueParams::VulkanCommandQueueParams()
{
    queueFamilyIndex = -1;
    queueIndex = -1;
}

VulkanCommandQueueParams::~VulkanCommandQueueParams()
{

}

VulaknVideoFrameParams::VulaknVideoFrameParams()
{
    quantRange = QuantRange::FULL;
    colorGamut = ColorGamut_UNSPECIFIED;
}

VulaknVideoFrameParams::~VulaknVideoFrameParams()
{
    
}

} // namespace Codec
} // namespace Mmp