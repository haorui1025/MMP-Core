//
// VulkanH264Decoder.h
//
// Library: Codec
// Package: Vulkan
// Module:  Vulkan
// 
//

#pragma once

#include "VulkanDecoder.h"

#include "H264/H264Deserialize.h"
#include "H264/H264SliceDecodingProcess.h"

namespace Mmp
{
namespace Codec
{

class VulkanH264Decoder : public VulkanDecoder
{
public:
    VulkanH264Decoder();
    ~VulkanH264Decoder();
public:
    bool Push(AbstractPack::ptr pack) override;
    const std::string& Description() override;
private:
    void StartFrame(const Any& context) override;
    void DecodedBitStream(const Any& context) override;
    void EndFrame(const Any& context) override;
private:
    bool CreateSessionParameters();
    void ResetSessionParameters();
private:
    H264Deserialize::ptr                                            _deserialize;
    H264ContextSyntax::ptr                                          _deserializeContext;
    H264SliceDecodingProcess::ptr                                   _sliceDecoding;
private:
    VkVideoSessionParametersKHR                                     _sessionParameters;
};

} // namespace Codec
} // namespace Mmp