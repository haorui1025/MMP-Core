#include "VulkanCommandQueue.h"

#include "VulkanUtil.h"

namespace Mmp
{
namespace Codec
{

VulkanCommandQueue::VulkanCommandQueue()
{
    _commandBuffer = VK_NULL_HANDLE;
    _commandBufferFence = VK_NULL_HANDLE;
    _commandPool = VK_NULL_HANDLE;
}

VulkanCommandQueue::~VulkanCommandQueue()
{
    if (_commandBufferFence)
    {
        VulkanDevice::Singleton()->vkWaitForFences(1, &_commandBufferFence, VK_TRUE, UINT64_MAX);
        VulkanDevice::Singleton()->vkDestroyFence(_commandBufferFence, nullptr);
    }
    if (_commandBuffer)
    {
        VulkanDevice::Singleton()->vkFreeCommandBuffers(_commandPool, 1, &_commandBuffer);
    }
    if (_commandPool)
    {
        VulkanDevice::Singleton()->vkDestroyCommandPool(_commandPool, nullptr);
    }
}

bool VulkanCommandQueue::Init(const VulkanCommandQueueParams& params)
{
    std::lock_guard<std::mutex> lock(_mtx);
    VkResult vkRes = VK_SUCCESS;
    VkCommandPoolCreateInfo commandPoolCreateInfo = {};
    VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
    VkFenceCreateInfo fenceCreateInfo = {};
    // VkCommandPoolCreateInfo
    {
        commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT | VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
        commandPoolCreateInfo.queueFamilyIndex = (uint32_t)params.queueFamilyIndex;
    }
    vkRes = VulkanDevice::Singleton()->vkCreateCommandPool(&commandPoolCreateInfo, nullptr, &_commandPool);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vkCreateCommandPool fail, error is: " << VkResultToStr(vkRes);
        assert(false);
        goto END;
    }
    // VkCommandBufferAllocateInfo
    {
        commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        commandBufferAllocateInfo.commandPool = _commandPool;
        commandBufferAllocateInfo.commandBufferCount = 1;
    }
    vkRes = VulkanDevice::Singleton()->vkAllocateCommandBuffers(&commandBufferAllocateInfo, &_commandBuffer);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vkAllocateCommandBuffers fail, error is: " << VkResultToStr(vkRes);
        assert(false);
        goto END;
    }
    // VkFenceCreateInfo
    {
        fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
        fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
    }
    vkRes = VulkanDevice::Singleton()->vkCreateFence(&fenceCreateInfo, nullptr, &_commandBufferFence);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vkCreateFence fail, error is: " << VkResultToStr(vkRes);
        assert(false);
        goto END;
    }
    VulkanDevice::Singleton()->vkGetDeviceQueue((uint32_t)params.queueFamilyIndex, (uint32_t)params.queueIndex, &_commandQueue);
END:
    return VULKAN_SUCCEEDED(vkRes);
}

void VulkanCommandQueue::Sync()
{
    std::lock_guard<std::mutex> lock(_mtx);
    VulkanDevice::Singleton()->vkWaitForFences(1, &_commandBufferFence, VK_TRUE, UINT64_MAX);
}

bool VulkanCommandQueue::Start()
{
    std::lock_guard<std::mutex> lock(_mtx);
    VkResult vkRes = VK_SUCCESS;
    VkCommandBufferBeginInfo commandBufferBeginInfo = {};
    {
        commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    }
    VulkanDevice::Singleton()->vkWaitForFences(1, &_commandBufferFence, VK_TRUE, UINT64_MAX);
    VulkanDevice::Singleton()->vkResetFences(1, &_commandBufferFence);
    vkRes = VulkanDevice::Singleton()->vkBeginCommandBuffer(_commandBuffer, &commandBufferBeginInfo);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vkBeginCommandBuffer fail, error is: " << VkResultToStr(vkRes);
        assert(false);
        goto END;
    }

END:
    return VULKAN_SUCCEEDED(vkRes);
}

bool VulkanCommandQueue::Submit()
{
    std::lock_guard<std::mutex> lock(_mtx);
    VkResult vkRes = VK_SUCCESS;
    VkCommandBufferSubmitInfo commandBufferSubmitInfo = {};
    VkSubmitInfo2 submitInfo = {};
    // VkCommandBufferSubmitInfo
    {
        commandBufferSubmitInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_SUBMIT_INFO;
        commandBufferSubmitInfo.commandBuffer = _commandBuffer;
    }
    // VkSubmitInfo2
    {
        submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO_2;
        submitInfo.pCommandBufferInfos = &commandBufferSubmitInfo;
        submitInfo.commandBufferInfoCount = 1;
        submitInfo.pWaitSemaphoreInfos = &_commandBufferSemWait;
        submitInfo.waitSemaphoreInfoCount = _commandBufferSemWaitCount;
        submitInfo.pSignalSemaphoreInfos = &_commandBufferSemSig;
        submitInfo.signalSemaphoreInfoCount = _commandBufferSemSigCount;
    }
    vkRes = VulkanDevice::Singleton()->vkEndCommandBuffer(_commandBuffer);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vkEndCommandBuffer fail, error is: " << VkResultToStr(vkRes);
        assert(false);
        goto END;
    }
    vkRes = VulkanDevice::Singleton()->vkQueueSubmit2(_commandQueue, 1, &submitInfo, _commandBufferFence);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vkQueueSubmit2 fail, error is: " << VkResultToStr(vkRes);
        assert(false);
        goto END;
    }
END:
    return VULKAN_SUCCEEDED(vkRes);
}

} // namespace Codec
} // namespace Mmp