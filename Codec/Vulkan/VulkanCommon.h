//
// VulkanCommon.h
//
// Library: Codec
// Package: Vulkan
// Module:  Vulkan
//
// See also : https://github.com/nvpro-samples/vk_video_samples/blob/main/vk_video_decoder/doc/VideoDecode.png
// 

#pragma once

#include <vector>

#include "Common/Common.h"
#include "Common/PixelFormat.h"
#include "Common/VulkanDevice.h"

#include "CodecCommon.h"

namespace Mmp
{
namespace Codec
{

class VulkanDecoderParams
{
public:
    VulkanDecoderParams();
    ~VulkanDecoderParams() = default;
public:
    PixelsInfo                         info;
    CodecType                          codecType;
    uint8_t                            profile;
};
bool operator==(const VulkanDecoderParams& left, const VulkanDecoderParams& right);
bool operator!=(const VulkanDecoderParams& left, const VulkanDecoderParams& right);

class VulkanCommandQueueParams
{
public:
    VulkanCommandQueueParams();
    ~VulkanCommandQueueParams();
public:
    int32_t queueFamilyIndex;
    int32_t queueIndex;
};

class VulaknVideoFrameParams
{
public:
    VulaknVideoFrameParams();
    ~VulaknVideoFrameParams();
public:
    uint32_t arrayLayers;
    VkImageCreateFlags flags;
    VkImageTiling tiling;
    VkImageUsageFlags usage;
    uint32_t* pQueueFamilyIndices;
    uint32_t  queueFamilyIndexCount;
    VkFormat format;
    uint32_t width;
    uint32_t height;
    QuantRange quantRange;
    ColorGamut colorGamut;
};

} // namespace Codec
} // namespace Mmp