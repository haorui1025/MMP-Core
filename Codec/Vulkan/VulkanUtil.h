//
// VulkanUtil.h
//
// Library: Codec
// Package: Vulkan
// Module:  Vulkan
// 
//

#pragma once

#include <vector>
#include <string>

#include "VulkanCommon.h"

namespace Mmp
{
namespace Codec
{

std::string VkVideoDecodeCapabilityFlagsToStr(VkVideoDecodeCapabilityFlagsKHR flag);

std::string VkVideoCapabilityFlagsToStr(VkVideoCapabilityFlagsKHR flag);

} // namespace Codec
} // namespace Mmp