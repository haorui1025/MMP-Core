#include "VulkanUtil.h"

namespace Mmp
{
namespace Codec
{

std::string VkVideoDecodeCapabilityFlagsToStr(VkVideoDecodeCapabilityFlagsKHR flag)
{
    if (flag & VK_VIDEO_DECODE_CAPABILITY_DPB_AND_OUTPUT_COINCIDE_BIT_KHR)
    {
        return "VK_VIDEO_DECODE_CAPABILITY_DPB_AND_OUTPUT_COINCIDE_BIT_KHR";
    }
    else if (flag & VK_VIDEO_DECODE_CAPABILITY_DPB_AND_OUTPUT_DISTINCT_BIT_KHR)
    {
        return "VK_VIDEO_DECODE_CAPABILITY_DPB_AND_OUTPUT_DISTINCT_BIT_KHR";
    }
    else
    {
        return "None";
    }
}

std::string VkVideoCapabilityFlagsToStr(VkVideoCapabilityFlagsKHR flag)
{
    if (flag & VK_VIDEO_CAPABILITY_PROTECTED_CONTENT_BIT_KHR)
    {
        return "VK_VIDEO_CAPABILITY_PROTECTED_CONTENT_BIT_KHR";
    }
    else if (flag & VK_VIDEO_CAPABILITY_SEPARATE_REFERENCE_IMAGES_BIT_KHR)
    {
        return "VK_VIDEO_CAPABILITY_SEPARATE_REFERENCE_IMAGES_BIT_KHR";
    }
    else
    {
        return "None";
    }
}

} // namespace Codec
} // namespace Mmp