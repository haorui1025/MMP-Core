#include "OpenH264Decoder.h"

#include <cassert>
#include <memory.h>

#include "Codec/CodecUtil.h"
#include "Codec/StreamPack.h"
#include "H264/H26xBinaryReader.h"

#include "OpenH264Util.h"

namespace Mmp
{
namespace Codec
{

class OpenH264H264ByteReader : public AbstractH26xByteReader
{
public:
    OpenH264H264ByteReader(AbstractPack::ptr pack)
    {
        _pack = pack;
        _end = (uint32_t)_pack->GetSize();
        _cur = 0;
    }
public:
    size_t Read(void* data, size_t bytes) override
    {
        uint32_t readSize = (uint32_t)bytes < (_end - _cur) ? (uint32_t)bytes : (_end - _cur);
        if (readSize != 0)
        {
            memcpy(data, (uint8_t*)_pack->GetData() + _cur, readSize);
        }
        _cur += readSize;
        return size_t(readSize);
    }
    bool Seek(size_t offset)  override
    {
        if (offset >= 0 && offset < _end)
        {
            _cur = (uint32_t)offset;
            return true;
        }
        else
        {
            return false;
        }
    }
    size_t Tell() override
    {
        return size_t(_cur);
    }
    bool Eof() override
    {
        return _cur == _end;
    }
private:
    uint32_t _cur;
    uint32_t _end;
    AbstractPack::ptr _pack;
};

} // namespace Codec
} // namespace Mmp


namespace Mmp
{
namespace Codec
{

OpenH264Decoder::OpenH264Decoder()
{
    _decoder = nullptr;
    _bufSize = 12;

    _deserialize = std::make_shared<H264Deserialize>();
    _deserialize->enableParseSEI = false;
    _deserialize->enableParseSLICE = false;
}

void OpenH264Decoder::SetParameter(Any parameter, const std::string& property)
{

}

Any OpenH264Decoder::GetParamter(const std::string& property)
{
    return Any();
}

bool OpenH264Decoder::Init()
{
    std::lock_guard<std::mutex> locK(_mtx);
    assert(!_decoder);
    if (WelsCreateDecoder(&_decoder) != 0)
    {
        OPENH264_LOG_ERROR << "WelsCreateDecoder fail";
        assert(false);
        goto END;
    }
    {
        int logLevel = WELS_LOG_WARNING;
        WelsTraceCallback logCallback = OpenH264LogCallBack;
        _decoder->SetOption(DECODER_OPTION_TRACE_LEVEL, &logLevel);
        _decoder->SetOption(DECODER_OPTION_TRACE_CALLBACK, (void *)&logCallback);
    }
    {
        SDecodingParam param = {};
        param.eEcActiveIdc = ERROR_CON_DISABLE;
        param.sVideoProperty.eVideoBsType = VIDEO_BITSTREAM_AVC;
        if (_decoder->Initialize(&param) != cmResultSuccess)
        {
            OPENH264_LOG_ERROR << "CWelsDecoder::Initialize fail";
            assert(false);
            goto END1;
        }
    }
    CODEC_LOG_INFO << "OpenH264Decoder::Init successfully";
    return true;
END1:
    WelsDestroyDecoder(_decoder);
    _decoder = nullptr;
END:
    return false;
}

void OpenH264Decoder::Uninit()
{
    std::lock_guard<std::mutex> lock(_mtx);
    assert(_decoder);
    WelsDestroyDecoder(_decoder);
    _decoder = nullptr;
    CODEC_LOG_INFO << "OpenH264Decoder::Uninit successfully";
}

bool OpenH264Decoder::Start()
{
    std::lock_guard<std::mutex> lock(_mtx);
    return true;
}

void OpenH264Decoder::Stop()
{
    std::lock_guard<std::mutex> lock(_mtx);
}

bool OpenH264Decoder::Push(AbstractPack::ptr pack)
{
    std::lock_guard<std::mutex> lock(_mtx);
    uint8_t *palnes[4] = {0};
    {
        H26xBinaryReader::ptr br = std::make_shared<H26xBinaryReader>(std::make_shared<OpenH264H264ByteReader>(pack));
        H264NalSyntax::ptr nal = std::make_shared<H264NalSyntax>();
        if (_deserialize->DeserializeByteStreamNalUnit(br, nal) && nal->nal_unit_type == H264NaluType::MMP_H264_NALU_TYPE_SPS)
        {
            this->_info.colorGamut = H264VuiSyntaxToColorGamut(nal->sps->vui_seq_parameters);
            this->_info.quantRange = H264VuiSyntaxToQuantRange(nal->sps->vui_seq_parameters);
        }
    }
    SBufferInfo info = {};
    StreamPack::ptr streamPack = std::dynamic_pointer_cast<StreamPack>(pack);
    {
        DECODING_STATE state = DECODING_STATE::dsErrorFree;
        SFrameBSInfo bsInfo = {};
        this->_decoder->DecodeFrameNoDelay(reinterpret_cast<const unsigned char*>(pack->GetData()), pack->GetSize(), palnes, &info);
        if (streamPack)
        {
            info.uiOutYuvTimeStamp = streamPack->pts.count();
            info.uiInBsTimeStamp = streamPack->dts.count();
        }
        if (state != DECODING_STATE::dsErrorFree)
        {
            OPENH264_LOG_WARN << "ISVCDecoder::DecodeFrameNoDelay fail";
            assert(false);
            return false;
        }
        if (info.iBufferStatus != 1)
        {
            return true;
        }
    }

    if (info.UsrData.sSystemBuffer.iWidth != this->_info.width 
        || info.UsrData.sSystemBuffer.iHeight != this->_info.height
        || OpenH264PixFormatToMppPixFormat((EVideoFormatType)info.UsrData.sSystemBuffer.iFormat) != this->_info.format)
    {
        OPENH264_LOG_INFO << "Pixels info change, width(" << info.UsrData.sSystemBuffer.iWidth 
                            << ") height(" << info.UsrData.sSystemBuffer.iHeight 
                            << ") Format(" << OpenH264PixFormatToMppPixFormat((EVideoFormatType)info.UsrData.sSystemBuffer.iFormat) << ")";
        this->_pool.reset();
        this->_info.width  = info.UsrData.sSystemBuffer.iWidth;
        this->_info.height = info.UsrData.sSystemBuffer.iHeight;
        this->_info.horStride = this->_info.width;
        this->_info.virStride = this->_info.height;
        this->_info.bitdepth = 8;
        this->_info.format = OpenH264PixFormatToMppPixFormat((EVideoFormatType)info.UsrData.sSystemBuffer.iFormat);
        this->_pool = std::make_shared<SharedDataPool>((size_t)(this->_info.width * this->_info.height * BytePerPixel(this->_info.format)), this->_bufSize);
    }
    {
        AbstractSharedData::ptr frame = this->_pool->Request();
        // Y
        for (uint32_t i=0; i<_info.height; i++)
        {
            uint8_t* src = palnes[0] + (info.UsrData.sSystemBuffer.iStride[0] * i);
            uint8_t* dst = (uint8_t*)frame->GetData(_info.width*i);
            uint32_t align = std::min(_info.width, info.UsrData.sSystemBuffer.iStride[0]);
            memcpy(dst, src, align);
        }
        // U
        for (uint32_t i=0; i<_info.height/2; i++)
        {
            uint8_t* src = palnes[1] + (info.UsrData.sSystemBuffer.iStride[1] * i);
            uint8_t* dst = (uint8_t*)frame->GetData(_info.width*_info.height + i * _info.width / 2);
            uint32_t align = std::min(_info.width/2, info.UsrData.sSystemBuffer.iStride[1]);
            memcpy(dst, src, align);
        }
        // V
        for (uint32_t i=0; i<_info.height/2; i++)
        {
            uint8_t* src = palnes[2] + (info.UsrData.sSystemBuffer.iStride[1] * i);
            uint8_t* dst = (uint8_t*)frame->GetData(_info.width*_info.height*5/4 + i * _info.width / 2);
            uint32_t align = std::min(_info.width/2, info.UsrData.sSystemBuffer.iStride[1]);
            memcpy(dst, src, align);   
        }
        std::lock_guard<std::mutex> bufLock(_bufMtx);
        StreamFrame::ptr streamFrame = std::make_shared<StreamFrame>(_info, frame);
        {
            streamFrame->pts = std::chrono::milliseconds(info.uiOutYuvTimeStamp);
            streamFrame->dts = std::chrono::milliseconds(info.uiInBsTimeStamp);
        }
        _buffers.push_back(streamFrame);
    }
    return true;
}

bool OpenH264Decoder::Pop(AbstractFrame::ptr& frame) 
{
    std::lock_guard<std::mutex> lock(_bufMtx);
    if (_buffers.empty())
    {
        return false;
    }
    else
    {
        frame = _buffers.front();
        _buffers.pop_front();
        return true;
    }
}

bool OpenH264Decoder::CanPush() 
{
    std::lock_guard<std::mutex> lock(_mtx);
    std::lock_guard<std::mutex> bufLock(_bufMtx);
    return _buffers.size() < _bufSize;
}

bool OpenH264Decoder::CanPop()
{
    std::lock_guard<std::mutex> lock(_mtx);
    return _buffers.empty();
}

const std::string& OpenH264Decoder::Description()
{
    static const std::string desc = "H264(openh264) decoder";
    return desc;
}

} // namespace Codec
} // namespace Mmp