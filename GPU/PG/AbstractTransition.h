//
// AbstractTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 

#pragma once

#include <memory>
#include <string>

#include "GPU/GL/GLDrawContex.h"

#include "TransitionCommon.h"


namespace Mmp
{
namespace Gpu 
{

class AbstractTransition
{
public:
    using ptr = std::shared_ptr<AbstractTransition>;
public:
    AbstractTransition() = default;
    virtual ~AbstractTransition() = default;
public:
    virtual void Transition(Texture::ptr from, Texture::ptr to, Texture::ptr transition, AbstractTransitionParams::ptr params) = 0;
};

} // namespace Gpu 
} // namespace Mmp