#include "TransitionFactory.h"

#include <Poco/SingletonHolder.h>

#include "Transition/BowTieHorizontalTransition.h"
#include "Transition/DirectionalTransition.h"
#include "Transition/BowTieVerticalTransition.h"
#include "Transition/GlitchMemoriesTransition.h"
#include "Transition/InvertedPageCurlTransition.h"
#include "Transition/LinearBlurTransition.h"
#include "Transition/PolkaDotsCurtainTransition.h"
#include "Transition/SimpleZoomTransition.h"
#include "Transition/StereoViewerTransition.h"
#include "Transition/WaterDropTransition.h"
#include "Transition/WindowsliceTransition.h"
#include "Transition/CircleCropTransition.h"
#include "Transition/ColourDistanceTransition.h"
#include "Transition/DirectionalwarpTransition.h"
#include "Transition/MorphTransition.h"
#include "Transition/PerlinTransition.h"
#include "Transition/SwirlTransition.h"
#include "Transition/CannabisleafTransition.h"
#include "Transition/ButterflyWaveScrawlerTransition.h"
#include "Transition/CrazyParametricFunTransition.h"
#include "Transition/CrosshatchTransition.h"
#include "Transition/CrossZoomTransition.h"
#include "Transition/DreamyTransition.h"
#include "Transition/KaleidoscopeTransition.h"
#include "Transition/GridFlipTransition.h"
#include "Transition/RadialTransition.h"
#include "Transition/ZoomInCirclesTransition.h"
#include "Transition/AngularTransition.h"
#include "Transition/BurnTransition.h"
#include "Transition/CircleopenTransition.h" 
#include "Transition/CircleTransition.h" 
#include "Transition/ColorphaseTransition.h" 
#include "Transition/DoomScreenTransitionTransition.h" 
#include "Transition/DreamyZoomTransition.h" 
#include "Transition/GlitchDisplaceTransition.h" 
#include "Transition/HexagonalizeTransition.h" 
#include "Transition/PinwheelTransition.h" 
#include "Transition/RippleTransition.h" 
#include "Transition/WindowblindsTransition.h" 
#include "Transition/CrosswarpTransition.h"
#include "Transition/CubeTransition.h"
#include "Transition/DirectionalwipeTransition.h"
#include "Transition/DoorwayTransition.h"
#include "Transition/FadecolorTransition.h"
#include "Transition/FadegrayscaleTransition.h"
#include "Transition/FadeTransition.h"
#include "Transition/FlyeyeTransition.h"
#include "Transition/HeartTransition.h"
#include "Transition/PixelizeTransition.h"
#include "Transition/PolarFunctionTransition.h"
#include "Transition/RandomsquaresTransition.h"
#include "Transition/SquareswireTransition.h"
#include "Transition/SqueezeTransition.h"
#include "Transition/SwapTransition.h"
#include "Transition/WindTransition.h"

namespace Mmp
{
namespace Gpu 
{

namespace
{
    static Poco::SingletonHolder<TransitionFactory> esh;
}

TransitionFactory::TransitionFactory()
{
    RegisterBuiltins();
}

TransitionFactory::~TransitionFactory()
{

}
void TransitionFactory::RegisterTransitionClass(const std::string& className, TransitionInstantiator* instantiator)
{
    std::lock_guard<std::mutex> lock(_mutex);
    _transitionFactory.registerClass(className, instantiator);
}

AbstractTransition::ptr TransitionFactory::CreateTransition(const std::string& className)
{
    std::lock_guard<std::mutex> lock(_mutex);
    return _transitionFactory.CreateInstance(className);
}

TransitionFactory& TransitionFactory::DefaultFactory()
{
    return *esh.get();
}

void TransitionFactory::RegisterBuiltins()
{
    std::lock_guard<std::mutex> lock(_mutex);
    _transitionFactory.registerClass("BowTieHorizontalTransition", new Instantiator<BowTieHorizontalTransition, AbstractTransition>);
    _transitionFactory.registerClass("BowTieVerticalTransition", new Instantiator<BowTieVerticalTransition, AbstractTransition>);
    _transitionFactory.registerClass("DirectionalTransition", new Instantiator<DirectionalTransition, AbstractTransition>);
    _transitionFactory.registerClass("GlitchMemoriesTransition", new Instantiator<GlitchMemoriesTransition, AbstractTransition>);
    _transitionFactory.registerClass("InvertedPageCurlTransition", new Instantiator<InvertedPageCurlTransition, AbstractTransition>);
    _transitionFactory.registerClass("LinearBlurTransition", new Instantiator<LinearBlurTransition, AbstractTransition>);
    _transitionFactory.registerClass("PolkaDotsCurtainTransition", new Instantiator<PolkaDotsCurtainTransition, AbstractTransition>);
    _transitionFactory.registerClass("SimpleZoomTransition", new Instantiator<SimpleZoomTransition, AbstractTransition>);
    _transitionFactory.registerClass("StereoViewerTransition", new Instantiator<StereoViewerTransition, AbstractTransition>);
    _transitionFactory.registerClass("WaterDropTransition", new Instantiator<WaterDropTransition, AbstractTransition>);
    _transitionFactory.registerClass("WindowsliceTransition", new Instantiator<WindowsliceTransition, AbstractTransition>);
    _transitionFactory.registerClass("CircleCropTransition", new Instantiator<CircleCropTransition, AbstractTransition>);
    _transitionFactory.registerClass("ColourDistanceTransition", new Instantiator<ColourDistanceTransition, AbstractTransition>);
    _transitionFactory.registerClass("DirectionalwarpTransition", new Instantiator<DirectionalwarpTransition, AbstractTransition>);
    _transitionFactory.registerClass("MorphTransition", new Instantiator<MorphTransition, AbstractTransition>);
    _transitionFactory.registerClass("PerlinTransition", new Instantiator<PerlinTransition, AbstractTransition>);
    _transitionFactory.registerClass("SwirlTransition", new Instantiator<SwirlTransition, AbstractTransition>);
    _transitionFactory.registerClass("CannabisleafTransition", new Instantiator<CannabisleafTransition, AbstractTransition>);
    _transitionFactory.registerClass("ButterflyWaveScrawlerTransition", new Instantiator<ButterflyWaveScrawlerTransition, AbstractTransition>);
    _transitionFactory.registerClass("CrazyParametricFunTransition", new Instantiator<CrazyParametricFunTransition, AbstractTransition>);
    _transitionFactory.registerClass("CrosshatchTransition", new Instantiator<CrosshatchTransition, AbstractTransition>);
    _transitionFactory.registerClass("CrossZoomTransition", new Instantiator<CrossZoomTransition, AbstractTransition>);
    _transitionFactory.registerClass("DreamyTransition", new Instantiator<DreamyTransition, AbstractTransition>);
    _transitionFactory.registerClass("KaleidoscopeTransition", new Instantiator<KaleidoscopeTransition, AbstractTransition>);
    _transitionFactory.registerClass("GridFlipTransition", new Instantiator<GridFlipTransition, AbstractTransition>);
    _transitionFactory.registerClass("RadialTransition", new Instantiator<RadialTransition, AbstractTransition>);
    _transitionFactory.registerClass("ZoomInCirclesTransition", new Instantiator<ZoomInCirclesTransition, AbstractTransition>);
    _transitionFactory.registerClass("AngularTransition", new Instantiator<AngularTransition, AbstractTransition>);
    _transitionFactory.registerClass("BurnTransition", new Instantiator<BurnTransition, AbstractTransition>);
    _transitionFactory.registerClass("CircleopenTransition", new Instantiator<CircleopenTransition, AbstractTransition>);
    _transitionFactory.registerClass("CircleTransition", new Instantiator<CircleTransition, AbstractTransition>);
    _transitionFactory.registerClass("ColorphaseTransition", new Instantiator<ColorphaseTransition, AbstractTransition>);
    _transitionFactory.registerClass("DoomScreenTransitionTransition", new Instantiator<DoomScreenTransitionTransition, AbstractTransition>);
    _transitionFactory.registerClass("DreamyZoomTransition", new Instantiator<DreamyZoomTransition, AbstractTransition>);
    _transitionFactory.registerClass("GlitchDisplaceTransition", new Instantiator<GlitchDisplaceTransition, AbstractTransition>);
    _transitionFactory.registerClass("HexagonalizeTransition", new Instantiator<HexagonalizeTransition, AbstractTransition>);
    _transitionFactory.registerClass("PinwheelTransition", new Instantiator<PinwheelTransition, AbstractTransition>);
    _transitionFactory.registerClass("RippleTransition", new Instantiator<RippleTransition, AbstractTransition>);
    _transitionFactory.registerClass("WindowblindsTransition", new Instantiator<WindowblindsTransition, AbstractTransition>);
    _transitionFactory.registerClass("CrosswarpTransition", new Instantiator<CrosswarpTransition, AbstractTransition>);
    _transitionFactory.registerClass("CubeTransition", new Instantiator<CubeTransition, AbstractTransition>);
    _transitionFactory.registerClass("DirectionalwipeTransition", new Instantiator<DirectionalwipeTransition, AbstractTransition>);
    _transitionFactory.registerClass("DoorwayTransition", new Instantiator<DoorwayTransition, AbstractTransition>);
    _transitionFactory.registerClass("FadecolorTransition", new Instantiator<FadecolorTransition, AbstractTransition>);
    _transitionFactory.registerClass("FadegrayscaleTransition", new Instantiator<FadegrayscaleTransition, AbstractTransition>);
    _transitionFactory.registerClass("FadeTransition", new Instantiator<FadeTransition, AbstractTransition>);
    _transitionFactory.registerClass("FlyeyeTransition", new Instantiator<FlyeyeTransition, AbstractTransition>);
    _transitionFactory.registerClass("HeartTransition", new Instantiator<HeartTransition, AbstractTransition>);
    _transitionFactory.registerClass("PixelizeTransition", new Instantiator<PixelizeTransition, AbstractTransition>);
    _transitionFactory.registerClass("PolarFunctionTransition", new Instantiator<PolarFunctionTransition, AbstractTransition>);
    _transitionFactory.registerClass("RandomsquaresTransition", new Instantiator<RandomsquaresTransition, AbstractTransition>);
    _transitionFactory.registerClass("SquareswireTransition", new Instantiator<SquareswireTransition, AbstractTransition>);
    _transitionFactory.registerClass("SqueezeTransition", new Instantiator<SqueezeTransition, AbstractTransition>);
    _transitionFactory.registerClass("SwapTransition", new Instantiator<SwapTransition, AbstractTransition>);
    _transitionFactory.registerClass("WindTransition", new Instantiator<WindTransition, AbstractTransition>);
}

} // namespace Gpu 
} // namespace Mmp