//
// TransitionFactory.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 

#pragma once

#include <mutex>
#include <vector>

#include "Common/DynamicFactory.h"
#include "Common/Instantiator.h"

#include "AbstractTransition.h"

namespace Mmp
{
namespace Gpu 
{

class TransitionFactory
{
public:
    using TransitionInstantiator = AbstractInstantiator<AbstractTransition>;
public:
    TransitionFactory();
    ~TransitionFactory();
public:
    void RegisterTransitionClass(const std::string& className, TransitionInstantiator* instantiator);
    AbstractTransition::ptr CreateTransition(const std::string& className);
    static TransitionFactory& DefaultFactory();
private:
    void RegisterBuiltins();
private:
    std::mutex _mutex;
    DynamicFactory<AbstractTransition> _transitionFactory;
};


} // namespace Gpu 
} // namespace Mmp