//
// TransitionCommon.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 

#pragma once

#include <memory>
#include <string>



namespace Mmp
{
namespace Gpu 
{

class AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<AbstractTransitionParams>;
public:
    AbstractTransitionParams();
    virtual ~AbstractTransitionParams() = default;
public:
    float progress;
};

} // namespace Gpu 
} // namespace Mmp