#include "SqueezeTransition.h"

namespace Mmp
{
namespace Gpu 
{

class SqueezeTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float colorSeparation = 0.04f;
};

SqueezeTransition::SqueezeTransition()
{
    _data = std::make_shared<RawData>(sizeof(SqueezeTransitionParamsPOD));
    SqueezeTransitionParamsPOD* uniforms = reinterpret_cast<SqueezeTransitionParamsPOD*>(_data->GetData());
    *uniforms = SqueezeTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> SqueezeTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "colorSeparation"}
    };
}

std::string SqueezeTransition::GetDescription()
{
    return "SqueezeTransition";
}

RawData::ptr SqueezeTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    SqueezeTransitionParamsPOD* uniforms = reinterpret_cast<SqueezeTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        SqueezeTransitionParams::ptr _params = std::dynamic_pointer_cast<SqueezeTransitionParams>(params);
        if (_params)
        {
            uniforms->colorSeparation = _params->colorSeparation;
        }
    }
    return _data;
}

std::string SqueezeTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition (vec2 uv) {
  float y = 0.5 + (uv.y-0.5) / (1.0-progress);
  if (y < 0.0 || y > 1.0) {
     return getToColor(uv);
  }
  else {
    vec2 fp = vec2(uv.x, y);
    vec2 off = progress * vec2(0.0, colorSeparation);
    vec4 c = getFromColor(fp);
    vec4 cn = getFromColor(fp - off);
    vec4 cp = getFromColor(fp + off);
    return vec4(cn.r, c.g, cp.b, c.a);
  }
}
)";
}

} // namespace Gpu 
} // namespace Mmp