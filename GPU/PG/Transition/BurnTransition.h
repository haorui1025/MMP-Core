//
//  BurnTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// author: Paweł Płóciennik
// license: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{
class BurnTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<BurnTransitionParams>;
public:
    float color[3] = {0.9f, 0.4f, 0.2f};
};

class BurnTransition : public TemplateTransition
{
public:
    BurnTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp