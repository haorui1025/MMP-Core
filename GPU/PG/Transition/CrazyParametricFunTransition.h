//
//  CrazyParametricFunTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: mandubian
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class CrazyParametricFunTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<CrazyParametricFunTransitionParams>;
public:
    float a = 4.0f;
    float b = 1.0f;
    float amplitude = 120.0f;
    float smoothness = 0.1f;
};

class CrazyParametricFunTransition : public TemplateTransition
{
public:
    CrazyParametricFunTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp