//
//  ButterflyWaveScrawlerTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: mandubian
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class ButterflyWaveScrawlerTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<ButterflyWaveScrawlerTransitionParams>;
public:
    float amplitude = 1.0f;
    float waves = 30.0f;
    float colorSeparation = 0.3f;
};

class ButterflyWaveScrawlerTransition : public TemplateTransition
{
public:
    ButterflyWaveScrawlerTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp