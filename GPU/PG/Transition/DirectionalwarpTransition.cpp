#include "DirectionalwarpTransition.h"

namespace Mmp
{
namespace Gpu 
{

class DirectionalwarpTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float direction[2] = {-1.0f, 1.0f};
    float smoothness = 0.5f;
    float center[2] = {-0.5f, -0.5f};
};

DirectionalwarpTransition::DirectionalwarpTransition()
{
    _data = std::make_shared<RawData>(sizeof(DirectionalwarpTransitionParamsPOD));
    DirectionalwarpTransitionParamsPOD* uniforms = reinterpret_cast<DirectionalwarpTransitionParamsPOD*>(_data->GetData());
    *uniforms = DirectionalwarpTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> DirectionalwarpTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT2, "direction"},
        {UniformType::FLOAT1, "smoothness"},
        {UniformType::FLOAT2, "center"}
    };
}

std::string DirectionalwarpTransition::GetDescription()
{
    return "DirectionalwarpTransition";
}

RawData::ptr DirectionalwarpTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    DirectionalwarpTransitionParamsPOD* uniforms = reinterpret_cast<DirectionalwarpTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        DirectionalwarpTransitionParams::ptr _params = std::dynamic_pointer_cast<DirectionalwarpTransitionParams>(params);
        if (_params)
        {
            uniforms->direction[0] = _params->direction[0];
            uniforms->direction[1] = _params->direction[1];
            uniforms->smoothness = _params->smoothness;
            uniforms->center[0] = uniforms->center[0];
            uniforms->center[1] = uniforms->center[1];
        }
    }
    return _data;
}

std::string DirectionalwarpTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition (vec2 uv) {
  vec2 v = normalize(direction);
  v /= abs(v.x) + abs(v.y);
  float d = v.x * center.x + v.y * center.y;
  float m = 1.0 - smoothstep(-smoothness, 0.0, v.x * uv.x + v.y * uv.y - (d - 0.5 + progress * (1.0 + smoothness)));
  return mix(getFromColor((uv - 0.5) * (1.0 - m) + 0.5), getToColor((uv - 0.5) * m + 0.5), m);
}
)";
}

} // namespace Gpu 
} // namespace Mmp