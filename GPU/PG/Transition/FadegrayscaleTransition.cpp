#include "FadegrayscaleTransition.h"

namespace Mmp
{
namespace Gpu 
{

class FadegrayscaleTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float intensity = 0.3f;
};

FadegrayscaleTransition::FadegrayscaleTransition()
{
    _data = std::make_shared<RawData>(sizeof(FadegrayscaleTransitionParamsPOD));
    FadegrayscaleTransitionParamsPOD* uniforms = reinterpret_cast<FadegrayscaleTransitionParamsPOD*>(_data->GetData());
    *uniforms = FadegrayscaleTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> FadegrayscaleTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "intensity"}
    };
}

std::string FadegrayscaleTransition::GetDescription()
{
    return "FadegrayscaleTransition";
}

RawData::ptr FadegrayscaleTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    FadegrayscaleTransitionParamsPOD* uniforms = reinterpret_cast<FadegrayscaleTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        FadegrayscaleTransitionParams::ptr _params = std::dynamic_pointer_cast<FadegrayscaleTransitionParams>(params);
        if (_params)
        {
            uniforms->intensity = _params->intensity;
        }
    }
    return _data;
}

std::string FadegrayscaleTransition::GetTransitionCode()
{
    return 
R"(
vec3 grayscale (vec3 color) {
  float _grayscale = 0.2126*color.r + 0.7152*color.g + 0.0722*color.b;
  return vec3(_grayscale, _grayscale, _grayscale);
}
 
vec4 transition (vec2 uv) {
  vec4 fc = getFromColor(uv);
  vec4 tc = getToColor(uv);
  return mix(
    mix(vec4(grayscale(fc.rgb), 1.0), fc, smoothstep(1.0-intensity, 0.0, progress)),
    mix(vec4(grayscale(tc.rgb), 1.0), tc, smoothstep(    intensity, 1.0, progress)),
    progress);
}
)";
}

} // namespace Gpu 
} // namespace Mmp