#include "CircleopenTransition.h"

namespace Mmp
{
namespace Gpu 
{

class CircleopenTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float smoothness = 0.3f;
};

CircleopenTransition::CircleopenTransition()
{
    _data = std::make_shared<RawData>(sizeof(CircleopenTransitionParamsPOD));
    CircleopenTransitionParamsPOD* uniforms = reinterpret_cast<CircleopenTransitionParamsPOD*>(_data->GetData());
    *uniforms = CircleopenTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> CircleopenTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "smoothness"}
    };
}

std::string CircleopenTransition::GetDescription()
{
    return "CircleopenTransition";
}

RawData::ptr CircleopenTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    CircleopenTransitionParamsPOD* uniforms = reinterpret_cast<CircleopenTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        CircleopenTransitionParams::ptr _params = std::dynamic_pointer_cast<CircleopenTransitionParams>(params);
        if (_params)
        {
            uniforms->smoothness = _params->smoothness;
        }
    }
    return _data;
}

std::string CircleopenTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition (vec2 uv) {
  vec2 center = vec2(0.5, 0.5);
  float SQRT_2 = 1.414213562373;
  float x = progress;
  float m = smoothstep(-smoothness, 0.0, SQRT_2*distance(center, uv) - x*(1.+smoothness));
  return mix(getFromColor(uv), getToColor(uv), 1.-m );
}
)";
}

} // namespace Gpu 
} // namespace Mmp