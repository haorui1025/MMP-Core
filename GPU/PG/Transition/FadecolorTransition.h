//
//  FadecolorTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// author: gre
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{
class FadecolorTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<FadecolorTransitionParams>;
public:
    float color[3] = {0.0f, 0.0f, 0.0f};
    float colorPhase = 0.4f;
};

class FadecolorTransition : public TemplateTransition
{
public:
    FadecolorTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp