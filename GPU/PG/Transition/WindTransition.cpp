#include "WindTransition.h"

namespace Mmp
{
namespace Gpu 
{

class WindTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float size = 0.2f;
};

WindTransition::WindTransition()
{
    _data = std::make_shared<RawData>(sizeof(WindTransitionParamsPOD));
    WindTransitionParamsPOD* uniforms = reinterpret_cast<WindTransitionParamsPOD*>(_data->GetData());
    *uniforms = WindTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> WindTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "size"}
    };
}

std::string WindTransition::GetDescription()
{
    return "WindTransition";
}

RawData::ptr WindTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    WindTransitionParamsPOD* uniforms = reinterpret_cast<WindTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        WindTransitionParams::ptr _params = std::dynamic_pointer_cast<WindTransitionParams>(params);
        if (_params)
        {
            uniforms->size = _params->size;
        }
    }
    return _data;
}

std::string WindTransition::GetTransitionCode()
{
    return 
R"(
float rand (vec2 co) {
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec4 transition (vec2 uv) {
  float r = rand(vec2(0, uv.y));
  float m = smoothstep(0.0, -size, uv.x*(1.0-size) + size*r - (progress * (1.0 + size)));
  return mix(
    getFromColor(uv),
    getToColor(uv),
    m
  );
}
)";
}

} // namespace Gpu 
} // namespace Mmp