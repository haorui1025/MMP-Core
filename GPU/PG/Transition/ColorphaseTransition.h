//
//  ColorphaseTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: gre
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class ColorphaseTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<ColorphaseTransitionParams>;
public:
    float fromStep[4] = {0.0f, 0.2f, 0.4f, 0.0f};
    float toStep[4] = {0.6f, 0.8f, 1.0f, 1.0f};
};

class ColorphaseTransition : public TemplateTransition
{
public:
    ColorphaseTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp