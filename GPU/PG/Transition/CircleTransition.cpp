#include "CircleTransition.h"

namespace Mmp
{
namespace Gpu 
{

class CircleTransitionParamsPOD
{
public:
    float backColor[4] = {0.1f, 0.1f, 0.1f, 1.0f}; // push first to make sure align
    float progress = 0.0f;
    float center[2] = {0.5f, 0.5f};
};

CircleTransition::CircleTransition()
{
    _data = std::make_shared<RawData>(sizeof(CircleTransitionParamsPOD));
    CircleTransitionParamsPOD* uniforms = reinterpret_cast<CircleTransitionParamsPOD*>(_data->GetData());
    *uniforms = CircleTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> CircleTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT4, "backColor"},
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT2, "center"}
    };
}

std::string CircleTransition::GetDescription()
{
    return "CircleTransition";
}

RawData::ptr CircleTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    CircleTransitionParamsPOD* uniforms = reinterpret_cast<CircleTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        CircleTransitionParams::ptr _params = std::dynamic_pointer_cast<CircleTransitionParams>(params);
        if (_params)
        {
            uniforms->backColor[0] = _params->backColor[0];
            uniforms->backColor[1] = _params->backColor[1];
            uniforms->backColor[2] = _params->backColor[2];
            uniforms->backColor[3] = _params->backColor[3];
            uniforms->center[0] = _params->center[0];
            uniforms->center[1] = _params->center[1];
        }
    }
    return _data;
}

std::string CircleTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition (vec2 uv) {
  
  float distance = length(uv - center);
  float radius = sqrt(8.0) * abs(progress - 0.5);
  
  if (distance > radius) {
    return backColor;
  }
  else {
    if (progress < 0.5) return getFromColor(uv);
    else return getToColor(uv);
  }
}
)";
}

} // namespace Gpu 
} // namespace Mmp