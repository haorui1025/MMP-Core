#include "DirectionalTransition.h"

#include "TransitionUtility.h"

namespace Mmp
{
namespace Gpu 
{

DirectionalTransition::DirectionalTransition()
{
    _data = std::make_shared<RawData>(sizeof(AbstractTransitionParamsPOD));
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> DirectionalTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"}
    };
}

std::string DirectionalTransition::GetDescription()
{
    return "DirectionalTransition";
}

RawData::ptr DirectionalTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    AbstractTransitionParamsPOD* uniforms = reinterpret_cast<AbstractTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    return _data;
}

std::string DirectionalTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition (vec2 uv) {
  vec2 direction = vec2(0.0f, 1.0f);
  vec2 p = uv + progress * sign(direction);
  vec2 f = fract(p);
  return mix(
    getToColor(f),
    getFromColor(f),
    step(0.0, p.y) * step(p.y, 1.0) * step(0.0, p.x) * step(p.x, 1.0)
  );
}
)";
}

} // namespace Gpu 
} // namespace Mmp