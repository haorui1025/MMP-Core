#include "DreamyTransition.h"

#include "TransitionUtility.h"

namespace Mmp
{
namespace Gpu 
{

DreamyTransition::DreamyTransition()
{
    _data = std::make_shared<RawData>(sizeof(AbstractTransitionParamsPOD));
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> DreamyTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"}
    };
}

std::string DreamyTransition::GetDescription()
{
    return "DreamyTransition";
}

RawData::ptr DreamyTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    AbstractTransitionParamsPOD* uniforms = reinterpret_cast<AbstractTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    return _data;
}

std::string DreamyTransition::GetTransitionCode()
{
    return 
R"(
vec2 offset(float progress, float x, float theta) {
  float phase = progress*progress + progress + theta;
  float shifty = 0.03*progress*cos(10.0*(progress+x));
  return vec2(0, shifty);
}
vec4 transition(vec2 p) {
  return mix(getFromColor(p + offset(progress, p.x, 0.0)), getToColor(p + offset(1.0-progress, p.x, 3.14)), progress);
}
)";
}

} // namespace Gpu 
} // namespace Mmp