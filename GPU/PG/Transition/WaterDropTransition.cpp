#include "WaterDropTransition.h"

namespace Mmp
{
namespace Gpu 
{

class WaterDropTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float amplitude = 30.0f;
    float speed = 30.0f;
};

WaterDropTransition::WaterDropTransition()
{
    _data = std::make_shared<RawData>(sizeof(WaterDropTransitionParamsPOD));
    WaterDropTransitionParamsPOD* uniforms = reinterpret_cast<WaterDropTransitionParamsPOD*>(_data->GetData());
    *uniforms = WaterDropTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> WaterDropTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "amplitude"},
        {UniformType::FLOAT1, "speed"}
    };
}

std::string WaterDropTransition::GetDescription()
{
    return "WaterDropTransition";
}

RawData::ptr WaterDropTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    WaterDropTransitionParamsPOD* uniforms = reinterpret_cast<WaterDropTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        WaterDropTransitionParams::ptr _params = std::dynamic_pointer_cast<WaterDropTransitionParams>(params);
        if (_params)
        {
            uniforms->amplitude = _params->amplitude;
            uniforms->speed = _params->speed;
        }
    }
    return _data;
}

std::string WaterDropTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition(vec2 p) {
  vec2 dir = p - vec2(0.5f, 0.5f);
  float dist = length(dir);

  if (dist > progress) {
    return mix(getFromColor( p), getToColor( p), progress);
  } else {
    vec2 offset = dir * sin(dist * amplitude - progress * speed);
    return mix(getFromColor( p + offset), getToColor( p), progress);
  }
}

)";
}

} // namespace Gpu 
} // namespace Mmp