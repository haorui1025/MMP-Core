//
//  DreamyZoomTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: Zeh Fernando
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class DreamyZoomTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<DreamyZoomTransitionParams>;
public:
    float rotation = 6.0f;
    float scale = 1.2f;
};

class DreamyZoomTransition : public TemplateTransition
{
public:
    DreamyZoomTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp