#include "TemplateTransition.h"

#include "Eigen/Dense"

#include "Common/ImmutableVectorAllocateMethod.h"
#include "TransitionUtility.h"

namespace Mmp
{
namespace Gpu 
{

TemplateTransition::TemplateTransition()
{
    _isInit = false;
}

void TemplateTransition::Transition(Texture::ptr from, Texture::ptr to, Texture::ptr transition, AbstractTransitionParams::ptr params)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (!_isInit)
    {
        _draw = GLDrawContex::Instance();
        {
            float worldViewProj[16] = {};
            Eigen::Matrix4d _worldViewProj;
            _draw->GetDefaultVorldViewProj(1, 1, worldViewProj, false);
            for (uint64_t i=0; i<4; i++)
            {
                for (uint64_t j=0; j<4; j++)
                {
                    _worldViewProj(j, i) = worldViewProj[j*4 + i];
                }
            }
            {
                std::vector<GLTransitionVertex> verts;
                // one rectangle
                {
                    Eigen::Vector4d postion1 = Eigen::Matrix<double, 1, 4>(0.0, 0.0, 1.0, 1.0) * _worldViewProj;
                    verts.push_back(GLTransitionVertex({(float)postion1[0], (float)postion1[1], 0.0f, 0.0f}));
                    Eigen::Vector4d postion2 = Eigen::Matrix<double, 1, 4>(1.0, 1.0, 1.0, 1.0) * _worldViewProj;
                    verts.push_back(GLTransitionVertex({(float)postion2(0), (float)postion2(1), 1.0f, 1.0f}));
                    Eigen::Vector4d postion3 = Eigen::Matrix<double, 1, 4>(0.0, 1.0, 1.0, 1.0) * _worldViewProj;
                    verts.push_back(GLTransitionVertex({(float)postion3(0), (float)postion3(1), 0.0f, 1.0f}));
                }
                {
                    // another rectangle
                    Eigen::Vector4d postion1 =  Eigen::Matrix<double, 1, 4>(0.0, 0.0, 1.0, 1.0) * _worldViewProj;
                    verts.push_back(GLTransitionVertex({(float)postion1(0), (float)postion1(1), 0.0f, 0.0f}));
                    Eigen::Vector4d postion2 = Eigen::Matrix<double, 1, 4>(1.0, 0.0, 1.0, 1.0) * _worldViewProj;
                    verts.push_back(GLTransitionVertex({(float)postion2(0), (float)postion2(1), 1.0f, 0.0f}));
                    Eigen::Vector4d postion3 =  Eigen::Matrix<double, 1, 4>(1.0, 1.0, 1.0, 1.0) * _worldViewProj;
                    verts.push_back(GLTransitionVertex({(float)postion3(0), (float)postion3(1),1.0f, 1.0f}));
                }
                std::shared_ptr<ImmutableVectorAllocateMethod<GLTransitionVertex>> allocate = std::make_shared<ImmutableVectorAllocateMethod<GLTransitionVertex>>();
                allocate->container.swap(verts);
                _vertexsRawData = std::make_shared<RawData>(allocate->container.size() * sizeof(GLTransitionVertex), allocate);
            }
        }
        _vbo = _draw->CreateBuffer(sizeof(GLTransitionVertex)*6, BufferUsageFlag::DYNAMIC | BufferUsageFlag::VERTEXDATA);
        _pipeLine = CreatePipeline();
        {
            SamplerStateDesc desc;
            desc.magFilter             = TextureFilter::LINEAR;
            desc.minFilter             = TextureFilter::LINEAR;
            desc.mipFilter             = TextureFilter::LINEAR;
            desc.maxAniso              = 0.0f;
            desc.wrapU                 = TextureAddressMode::CLAMP_TO_EDGE;
            desc.wrapV                 = TextureAddressMode::CLAMP_TO_EDGE;
            desc.wrapW                 = TextureAddressMode::CLAMP_TO_EDGE;
            desc.shadowCompareEnabled  = false;
            desc.shadowCompareFunc     = Comparison::ALWAYS;
            desc.borderColor           = BorderColor::DONT_CARE;
            SamplerState::ptr sample   = _draw->CreateSamplerState(desc);
            _samples.push_back(sample);
            _samples.push_back(sample);
        }
        _isInit = true;
    }
    FrameBuffer::ptr fbo;
    {
        FrameBufferDesc desc;
        desc.width  = transition->Width();
        desc.height = transition->Height();
        desc.depth  = 0;
        desc.multiSampleLevel = 0;
        desc.numLayers = 1;
        desc.zStencil = 0;
        desc.colorTexs.push_back(transition);
        desc.tag = GetDescription(); 
        fbo = _draw->CreateFrameBuffer(desc);
    }
    _draw->FenceBegin();
    {
        RenderPassInfo info;
        info.color      = RPAction::DONT_CARE;
        info.depth      = RPAction::DONT_CARE;
        info.stencil    = RPAction::DONT_CARE; 
        info.clearColor = 0xFF000000;
        _draw->BindFramebufferAsRenderTarget(fbo, info);
    }
    _draw->SetScissorRect(0, 0, transition->Width(), transition->Height());
    {
        Viewport vp;
        vp.TopLeftX  = 0;
        vp.TopLeftY  = 0;
        vp.Width     = (float)transition->Width();
        vp.Height    = (float)transition->Height();
        vp.MaxDepth  = 1.0;
        vp.MinDepth  = 0.0;
        _draw->SetViewport(vp);
    }
    _draw->BindSamplerStates(0, _samples);
    {
        std::vector<Texture::ptr> textures;
        textures.push_back(from);
        textures.push_back(to);
        _draw->BindTextures(0, textures);
    }
    Draw(params);
    _draw->FenceCommit();
    _draw->FenceEnd();
}

const std::string TemplateTransition::GetVertexShader()
{
    return GetTransitionVertexShader(_draw->GetShaderLanguage(), GetUniformsDescription());
}

const std::string TemplateTransition::GetFragmentShader()
{
    return GetTransitionFragmentShader(_draw->GetShaderLanguage(), GetUniformsDescription(), GetTransitionCode());
}

Pipeline::ptr TemplateTransition::CreatePipeline()
{
    ShaderModule::ptr vertexShader   = _draw->CreateShaderModule(ShaderStage::VERTEX, _draw->GetShaderLanguage(), GetVertexShader());
    ShaderModule::ptr fragmentShader = _draw->CreateShaderModule(ShaderStage::FRAGMENT, _draw->GetShaderLanguage(), GetFragmentShader());
    InputLayout::ptr  inputLayout;
    BlendState::ptr   blendState;
    DepthStencilState::ptr depthStencilState;
    RasterState::ptr rasterState;
    Pipeline::ptr pipeLine;
    {
        InputLayoutDesc desc;
        {
            BindingDesc binding;
            binding.stride       = sizeof(GLTransitionVertex);
            binding.instanceRate = false;
            desc.bindings.push_back(binding);
        }
        desc.attributes = GetTransitionAttributeDescs(_draw->GetShaderLanguage());
        inputLayout = _draw->CreateInputLayout(desc);
    }
    {
        BlendStateDesc desc;
        desc.enabled = true;
        desc.srcCol = BlendFactor::ONE;
        desc.dstCol = BlendFactor::ZERO;
        desc.colorMask = 0x0F;
        desc.srcAlpha  = BlendFactor::ONE;
        desc.dstAlpha  = BlendFactor::ZERO;
        blendState = _draw->CreateBlendState(desc);
    }
    {
        DepthStencilStateDesc desc;
        desc.depthTestEnabled = false;
        desc.depthWriteEnabled = false;
        desc.depthCompare = Comparison::LESS;
        desc.stencilEnabled = false;
        depthStencilState = _draw->CreateDepthStencilState(desc);
    }
    {
        RasterStateDesc desc;
        desc.cull = CullMode::NONE;
        rasterState = _draw->CreateRasterState(desc);
    }
    {
        PipelineDesc desc;
        desc.prim = Primitive::TRIANGLE_LIST;
        desc.shaders.push_back(vertexShader);
        desc.shaders.push_back(fragmentShader);
        desc.inputLayout = inputLayout;
        desc.depthStencil = depthStencilState;
        desc.blend = blendState;
        desc.raster = rasterState;
        {
            std::vector<std::pair<UniformType /* type */, std::string /* name */>> uniforms = GetUniformsDescription();
            size_t offset = 0;
            UniformBufferDesc uniform;
            for (auto& val : uniforms)
            {
                uniform.uniforms.push_back(UniformDesc(val.second, val.first, (int16_t)offset));
                uniform.uniformBufferSize++;
                switch (val.first)
                {
                    case UniformType::FLOAT1: offset += 4; break;
                    case UniformType::FLOAT2: offset += 8; break;
                    case UniformType::FLOAT3: offset += 12; break;
                    case UniformType::FLOAT4: offset += 16; break;
                    case UniformType::MATRIX4X4: offset += 64; break;
                    default: assert(false); break;
                }
            }
            desc.uniformBufferDesc = uniform;
        }
        {
            desc.samplers.push_back(SamplerDef("SceneA"));
            desc.samplers.push_back(SamplerDef("SceneB"));
        }
        pipeLine = _draw->CreateGraphicsPipeline(desc, GetDescription());
    }
    return pipeLine;
}

void TemplateTransition::Draw(AbstractTransitionParams::ptr params)
{
    _draw->BindPipeline(_pipeLine);
    _draw->UpdataUniformBuffer(GetUniformsData(params), GetUniformsDescription().size());
    // Update Vertex Buffer Object
    {
        _draw->UpdateBuffer(_vbo, _vertexsRawData, 0, _vertexsRawData->GetSize()); 
    }
    // Bind Vertex Buffer Object
    {
        std::vector<GLBuffer::ptr> vbos;
        vbos.push_back(_vbo);
        std::vector<int> offsets;
        offsets.push_back(0);
        _draw->BindVertexBuffers(0, vbos, offsets);
    }
    _draw->Draw(6, 0);
}

} // namespace Gpu 
} // namespace Mmp