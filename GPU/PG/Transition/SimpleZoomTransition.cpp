#include "SimpleZoomTransition.h"

namespace Mmp
{
namespace Gpu 
{

class SimpleZoomTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float zoom_quickness = 0.8f;
};

SimpleZoomTransition::SimpleZoomTransition()
{
    _data = std::make_shared<RawData>(sizeof(SimpleZoomTransitionParamsPOD));
    SimpleZoomTransitionParamsPOD* uniforms = reinterpret_cast<SimpleZoomTransitionParamsPOD*>(_data->GetData());
    *uniforms = SimpleZoomTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> SimpleZoomTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "zoom_quickness"}
    };
}

std::string SimpleZoomTransition::GetDescription()
{
    return "SimpleZoomTransition";
}

RawData::ptr SimpleZoomTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    SimpleZoomTransitionParamsPOD* uniforms = reinterpret_cast<SimpleZoomTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        SimpleZoomTransitionParams::ptr _params = std::dynamic_pointer_cast<SimpleZoomTransitionParams>(params);
        if (_params)
        {
            uniforms->zoom_quickness = _params->zoom_quickness;
        }
    }
    return _data;
}

std::string SimpleZoomTransition::GetTransitionCode()
{
    return 
R"(
#define nQuick clamp(zoom_quickness,0.2f,1.0f)

vec2 zoom(vec2 uv, float amount) {
  return 0.5 + ((uv - 0.5) * (1.0-amount));	
}

vec4 transition (vec2 uv) {
  return mix(
    getFromColor(zoom(uv, smoothstep(0.0, nQuick, progress))),
    getToColor(uv),
   smoothstep(nQuick-0.2, 1.0, progress)
  );
}
)";
}

} // namespace Gpu 
} // namespace Mmp