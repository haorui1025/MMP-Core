#include "RandomsquaresTransition.h"

namespace Mmp
{
namespace Gpu 
{

class RandomsquaresTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float size[2] = {10.0f, 10.0f};
    float smoothness = 0.5f;
};

RandomsquaresTransition::RandomsquaresTransition()
{
    _data = std::make_shared<RawData>(sizeof(RandomsquaresTransitionParamsPOD));
    RandomsquaresTransitionParamsPOD* uniforms = reinterpret_cast<RandomsquaresTransitionParamsPOD*>(_data->GetData());
    *uniforms = RandomsquaresTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> RandomsquaresTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT2, "size"},
        {UniformType::FLOAT1, "smoothness"}
    };
}

std::string RandomsquaresTransition::GetDescription()
{
    return "RandomsquaresTransition";
}

RawData::ptr RandomsquaresTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    RandomsquaresTransitionParamsPOD* uniforms = reinterpret_cast<RandomsquaresTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        RandomsquaresTransitionParams::ptr _params = std::dynamic_pointer_cast<RandomsquaresTransitionParams>(params);
        if (_params)
        {
            uniforms->size[0] = _params->size[0];
            uniforms->size[1] = _params->size[1];
            uniforms->smoothness = _params->smoothness;
        }
    }
    return _data;
}

std::string RandomsquaresTransition::GetTransitionCode()
{
    return 
R"(
float rand (vec2 co) {
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec4 transition(vec2 p) {
  float r = rand(floor(vec2(size) * p));
  float m = smoothstep(0.0, -smoothness, r - (progress * (1.0 + smoothness)));
  return mix(getFromColor(p), getToColor(p), m);
}
)";
}

} // namespace Gpu 
} // namespace Mmp