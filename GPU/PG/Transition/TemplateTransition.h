//
// AbstractTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "AbstractTransition.h"

namespace Mmp
{
namespace Gpu 
{

class TemplateTransition : public AbstractTransition
{
public:
    using ptr = std::shared_ptr<TemplateTransition>;
public:
    TemplateTransition();
public:
    void Transition(Texture::ptr from, Texture::ptr to, Texture::ptr transition, AbstractTransitionParams::ptr params) override;
protected:
    virtual std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription() = 0;
    virtual std::string GetTransitionCode() = 0;
    virtual std::string GetDescription() = 0;
    virtual RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params) = 0;
private:
    const std::string GetVertexShader();
    const std::string GetFragmentShader();
    Pipeline::ptr CreatePipeline();
    void Draw(AbstractTransitionParams::ptr params);
private:
    std::mutex                      _mtx;
    bool                            _isInit;
    GLBuffer::ptr                   _vbo;
    GLDrawContex::ptr               _draw;
    Pipeline::ptr                   _pipeLine;
    std::vector<SamplerState::ptr>  _samples;
    RawData::ptr                    _vertexsRawData;
protected:
    RawData::ptr                    _data;
};

} // namespace Gpu 
} // namespace Mmp