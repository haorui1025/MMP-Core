#include "FadeTransition.h"

#include "TransitionUtility.h"

namespace Mmp
{
namespace Gpu 
{

FadeTransition::FadeTransition()
{
    _data = std::make_shared<RawData>(sizeof(AbstractTransitionParamsPOD));
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> FadeTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"}
    };
}

std::string FadeTransition::GetDescription()
{
    return "FadeTransition";
}

RawData::ptr FadeTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    AbstractTransitionParamsPOD* uniforms = reinterpret_cast<AbstractTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    return _data;
}

std::string FadeTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition (vec2 uv) {
  return mix(
    getFromColor(uv),
    getToColor(uv),
    progress
  );
}
)";
}

} // namespace Gpu 
} // namespace Mmp