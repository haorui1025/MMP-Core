#include "CrazyParametricFunTransition.h"

namespace Mmp
{
namespace Gpu 
{

class CrazyParametricFunTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float a = 4.0f;
    float b = 1.0f;
    float amplitude = 120.0f;
    float smoothness = 0.1f;
};

CrazyParametricFunTransition::CrazyParametricFunTransition()
{
    _data = std::make_shared<RawData>(sizeof(CrazyParametricFunTransitionParamsPOD));
    CrazyParametricFunTransitionParamsPOD* uniforms = reinterpret_cast<CrazyParametricFunTransitionParamsPOD*>(_data->GetData());
    *uniforms = CrazyParametricFunTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> CrazyParametricFunTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "a"},
        {UniformType::FLOAT1, "b"},
        {UniformType::FLOAT1, "amplitude"},
        {UniformType::FLOAT1, "smoothness"}
    };
}

std::string CrazyParametricFunTransition::GetDescription()
{
    return "CrazyParametricFunTransition";
}

RawData::ptr CrazyParametricFunTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    CrazyParametricFunTransitionParamsPOD* uniforms = reinterpret_cast<CrazyParametricFunTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        CrazyParametricFunTransitionParams::ptr _params = std::dynamic_pointer_cast<CrazyParametricFunTransitionParams>(params);
        if (_params)
        {
            uniforms->a = _params->a;
            uniforms->b = _params->b;
            uniforms->amplitude = _params->amplitude;
            uniforms->smoothness = _params->smoothness;
        }
    }
    return _data;
}

std::string CrazyParametricFunTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition(vec2 uv) {
  vec2 p = uv.xy / vec2(1.0f, 1.0f).xy;
  vec2 dir = p - vec2(0.5f, 0.5f);
  float dist = length(dir);
  float x = (a - b) * cos(progress) + b * cos(progress * ((a / b) - 1.) );
  float y = (a - b) * sin(progress) - b * sin(progress * ((a / b) - 1.));
  vec2 offset = dir * vec2(sin(progress  * dist * amplitude * x), sin(progress * dist * amplitude * y)) / smoothness;
  return mix(getFromColor(p + offset), getToColor(p), smoothstep(0.2, 1.0, progress));
}
)";
}

} // namespace Gpu 
} // namespace Mmp