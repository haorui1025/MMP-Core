#include "DoorwayTransition.h"

namespace Mmp
{
namespace Gpu 
{

class DoorwayTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float reflection = 0.4f;
    float perspective = 0.4f;
    float depth = 3.0f;
};

DoorwayTransition::DoorwayTransition()
{
    _data = std::make_shared<RawData>(sizeof(DoorwayTransitionParamsPOD));
    DoorwayTransitionParamsPOD* uniforms = reinterpret_cast<DoorwayTransitionParamsPOD*>(_data->GetData());
    *uniforms = DoorwayTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> DoorwayTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "reflection"},
        {UniformType::FLOAT1, "perspective"},
        {UniformType::FLOAT1, "depth"}
    };
}

std::string DoorwayTransition::GetDescription()
{
    return "DoorwayTransition";
}

RawData::ptr DoorwayTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    DoorwayTransitionParamsPOD* uniforms = reinterpret_cast<DoorwayTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        DoorwayTransitionParams::ptr _params = std::dynamic_pointer_cast<DoorwayTransitionParams>(params);
        if (_params)
        {
            uniforms->reflection = _params->reflection;
            uniforms->perspective = _params->perspective;
            uniforms->depth = _params->depth;
        }
    }
    return _data;
}

std::string DoorwayTransition::GetTransitionCode()
{
    return 
R"(
bool inBounds (vec2 p) {
  vec2 boundMin = vec2(0.0, 0.0);
  vec2 boundMax = vec2(1.0, 1.0);
  return all(lessThan(boundMin, p)) && all(lessThan(p, boundMax));
}

vec2 project (vec2 p) {
  return p * vec2(1.0, -1.2) + vec2(0.0, -0.02);
}

vec4 bgColor (vec2 p, vec2 pto) {
  vec4 black = vec4(0.0, 0.0, 0.0, 1.0);
  vec4 c = black;
  pto = project(pto);
  if (inBounds(pto)) {
    c += mix(black, getToColor(pto), reflection * mix(1.0, 0.0, pto.y));
  }
  return c;
}


vec4 transition (vec2 p) {
  vec2 pfr = vec2(-1.0f, -1.0f), pto = vec2(-1.0f, -1.0f);
  float middleSlit = 2.0 * abs(p.x-0.5) - progress;
  if (middleSlit > 0.0) {
    pfr = p + (p.x > 0.5 ? -1.0 : 1.0) * vec2(0.5*progress, 0.0);
    float d = 1.0/(1.0+perspective*progress*(1.0-middleSlit));
    pfr.y -= d/2.;
    pfr.y *= d;
    pfr.y += d/2.;
  }
  float size = mix(1.0, depth, 1.-progress);
  pto = (p + vec2(-0.5, -0.5)) * vec2(size, size) + vec2(0.5, 0.5);
  if (inBounds(pfr)) {
    return getFromColor(pfr);
  }
  else if (inBounds(pto)) {
    return getToColor(pto);
  }
  else {
    return bgColor(p, pto);
  }
}
)";
}

} // namespace Gpu 
} // namespace Mmp