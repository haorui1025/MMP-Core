//
//  SwapTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: gre
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class SwapTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<SwapTransitionParams>;
public:
    float reflection = 0.4f;
    float perspective = 0.2f;
    float depth = 3.0f;
};

class SwapTransition : public TemplateTransition
{
public:
    SwapTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp