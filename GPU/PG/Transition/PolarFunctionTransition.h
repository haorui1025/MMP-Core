//
//  PolarFunctionTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: Fernando Kuteken
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class PolarFunctionTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<PolarFunctionTransitionParams>;
public:
    float segments = 5.0f;
};

class PolarFunctionTransition : public TemplateTransition
{
public:
    PolarFunctionTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp