#include "PixelizeTransition.h"

namespace Mmp
{
namespace Gpu 
{

class PixelizeTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float squaresMin[2] = {20.0f, 20.0f};
    float steps = 50.0f;
};

PixelizeTransition::PixelizeTransition()
{
    _data = std::make_shared<RawData>(sizeof(PixelizeTransitionParamsPOD));
    PixelizeTransitionParamsPOD* uniforms = reinterpret_cast<PixelizeTransitionParamsPOD*>(_data->GetData());
    *uniforms = PixelizeTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> PixelizeTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT2, "squaresMin"},
        {UniformType::FLOAT1, "steps"}
    };
}

std::string PixelizeTransition::GetDescription()
{
    return "PixelizeTransition";
}

RawData::ptr PixelizeTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    PixelizeTransitionParamsPOD* uniforms = reinterpret_cast<PixelizeTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        PixelizeTransitionParams::ptr _params = std::dynamic_pointer_cast<PixelizeTransitionParams>(params);
        if (_params)
        {
            uniforms->squaresMin[0] = _params->squaresMin[0];
            uniforms->squaresMin[1] = _params->squaresMin[1];
            uniforms->steps = _params->steps;
        }
    }
    return _data;
}

std::string PixelizeTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition(vec2 uv) {
  float d = min(progress, 1.0 - progress);
  float dist = steps>0 ? ceil(d * float(steps)) / float(steps) : d;
  vec2 squareSize = 2.0 * dist / vec2(squaresMin);
  vec2 p = dist>0.0 ? (floor(uv / squareSize) + 0.5) * squareSize : uv;
  return mix(getFromColor(p), getToColor(p), progress);
}
)";
}

} // namespace Gpu 
} // namespace Mmp