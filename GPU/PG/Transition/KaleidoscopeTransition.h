//
//  KaleidoscopeTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: nwoeanhinnogaehr
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class KaleidoscopeTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<KaleidoscopeTransitionParams>;
public:
    float speed = 1.0f;
    float angle = 1.0f;
    float power = 1.5f;
};

class KaleidoscopeTransition : public TemplateTransition
{
public:
    KaleidoscopeTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp