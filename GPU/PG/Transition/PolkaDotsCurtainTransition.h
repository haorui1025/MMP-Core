//
//  PolkaDotsCurtainTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// author: bobylito
// license: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class PolkaDotsCurtainTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<PolkaDotsCurtainTransitionParams>;
public:
    float dots = 20.0f;
    float center[2] = {0};
};

class PolkaDotsCurtainTransition : public TemplateTransition
{
public:
    PolkaDotsCurtainTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp