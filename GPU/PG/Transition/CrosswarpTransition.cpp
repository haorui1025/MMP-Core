#include "CrosswarpTransition.h"

#include "TransitionUtility.h"

namespace Mmp
{
namespace Gpu 
{

CrosswarpTransition::CrosswarpTransition()
{
    _data = std::make_shared<RawData>(sizeof(AbstractTransitionParamsPOD));
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> CrosswarpTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"}
    };
}

std::string CrosswarpTransition::GetDescription()
{
    return "CrosswarpTransition";
}

RawData::ptr CrosswarpTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    AbstractTransitionParamsPOD* uniforms = reinterpret_cast<AbstractTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    return _data;
}

std::string CrosswarpTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition(vec2 p) {
  float x = progress;
  x=smoothstep(.0,1.0,(x*2.0+p.x-1.0));
  return mix(getFromColor((p-.5)*(1.-x)+.5), getToColor((p-.5)*x+.5), x);
}
)";
}

} // namespace Gpu 
} // namespace Mmp