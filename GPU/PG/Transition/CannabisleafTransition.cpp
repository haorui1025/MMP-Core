#include "CannabisleafTransition.h"

#include "TransitionUtility.h"

namespace Mmp
{
namespace Gpu 
{

CannabisleafTransition::CannabisleafTransition()
{
    _data = std::make_shared<RawData>(sizeof(AbstractTransitionParamsPOD));
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> CannabisleafTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"}
    };
}

std::string CannabisleafTransition::GetDescription()
{
    return "CannabisleafTransition";
}

RawData::ptr CannabisleafTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    AbstractTransitionParamsPOD* uniforms = reinterpret_cast<AbstractTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    return _data;
}

std::string CannabisleafTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition (vec2 uv) {
  if(progress == 0.0){
    return getFromColor(uv);
  }
  vec2 leaf_uv = (uv - vec2(0.5f, 0.5f))/10./pow(progress,3.5);
	leaf_uv.y -= 0.35;
	float r = 0.18;
	float o = -_atan(leaf_uv.y, leaf_uv.x);
  return mix(getFromColor(uv), getToColor(uv), 1.-step(1. - length(leaf_uv)+r*(1.+sin(o))*(1.+0.9 * cos(8.*o))*(1.+0.1*cos(24.*o))*(0.9+0.05*cos(200.*o)), 1.));
}
)";
}

} // namespace Gpu 
} // namespace Mmp