//
//  RandomsquaresTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: gre
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class RandomsquaresTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<RandomsquaresTransitionParams>;
public:
    float size[2] = {10.0f, 10.0f};
    float smoothness = 0.5f;
};

class RandomsquaresTransition : public TemplateTransition
{
public:
    RandomsquaresTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp