#include "MorphTransition.h"

namespace Mmp
{
namespace Gpu 
{

class MorphTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float strength = 0.1f;
};

MorphTransition::MorphTransition()
{
    _data = std::make_shared<RawData>(sizeof(MorphTransitionParamsPOD));
    MorphTransitionParamsPOD* uniforms = reinterpret_cast<MorphTransitionParamsPOD*>(_data->GetData());
    *uniforms = MorphTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> MorphTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "strength"}
    };
}

std::string MorphTransition::GetDescription()
{
    return "MorphTransition";
}

RawData::ptr MorphTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    MorphTransitionParamsPOD* uniforms = reinterpret_cast<MorphTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        MorphTransitionParams::ptr _params = std::dynamic_pointer_cast<MorphTransitionParams>(params);
        if (_params)
        {
            uniforms->strength = _params->strength;
        }
    }
    return _data;
}

std::string MorphTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition(vec2 p) {
  vec4 ca = getFromColor(p);
  vec4 cb = getToColor(p);
  
  vec2 oa = (((ca.rg+ca.b)*0.5)*2.0-1.0);
  vec2 ob = (((cb.rg+cb.b)*0.5)*2.0-1.0);
  vec2 oc = mix(oa,ob,0.5)*strength;
  
  float w0 = progress;
  float w1 = 1.0-w0;
  return mix(getFromColor(p+oc*w0), getToColor(p-oc*w1), progress);
}
)";
}

} // namespace Gpu 
} // namespace Mmp