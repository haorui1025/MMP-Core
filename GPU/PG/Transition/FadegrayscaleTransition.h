//
//  FadegrayscaleTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: gre
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class FadegrayscaleTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<FadegrayscaleTransitionParams>;
public:
    float intensity = 0.3f;
};

class FadegrayscaleTransition : public TemplateTransition
{
public:
    FadegrayscaleTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp