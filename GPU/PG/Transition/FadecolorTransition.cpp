#include "FadecolorTransition.h"

namespace Mmp
{
namespace Gpu 
{

class FadecolorTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float color[3] = {0.0f, 0.0f, 0.0f};
    float colorPhase = 0.4f;
};

FadecolorTransition::FadecolorTransition()
{
    _data = std::make_shared<RawData>(sizeof(FadecolorTransitionParamsPOD));
    FadecolorTransitionParamsPOD* uniforms = reinterpret_cast<FadecolorTransitionParamsPOD*>(_data->GetData());
    *uniforms = FadecolorTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> FadecolorTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT3, "color"},
        {UniformType::FLOAT1, "colorPhase"}
    };
}

std::string FadecolorTransition::GetDescription()
{
    return "FadecolorTransition";
}

RawData::ptr FadecolorTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    FadecolorTransitionParamsPOD* uniforms = reinterpret_cast<FadecolorTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        FadecolorTransitionParams::ptr _params = std::dynamic_pointer_cast<FadecolorTransitionParams>(params);
        if (_params)
        {
            uniforms->color[0] = _params->color[0];
            uniforms->color[1] = _params->color[1];
            uniforms->color[2] = _params->color[2];
            uniforms->colorPhase = _params->colorPhase;
        }
    }
    return _data;
}

std::string FadecolorTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition (vec2 uv) {
  return mix(
    mix(vec4(color, 1.0), getFromColor(uv), smoothstep(1.0-colorPhase, 0.0, progress)),
    mix(vec4(color, 1.0), getToColor(uv), smoothstep(    colorPhase, 1.0, progress)),
    progress);
}
)";
}

} // namespace Gpu 
} // namespace Mmp