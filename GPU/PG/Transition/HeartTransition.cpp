#include "HeartTransition.h"

#include "TransitionUtility.h"

namespace Mmp
{
namespace Gpu 
{

HeartTransition::HeartTransition()
{
    _data = std::make_shared<RawData>(sizeof(AbstractTransitionParamsPOD));
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> HeartTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"}
    };
}

std::string HeartTransition::GetDescription()
{
    return "HeartTransition";
}

RawData::ptr HeartTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    AbstractTransitionParamsPOD* uniforms = reinterpret_cast<AbstractTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    return _data;
}

std::string HeartTransition::GetTransitionCode()
{
    return 
R"(
float inHeart (vec2 p, vec2 center, float size) {
  if (size==0.0) return 0.0;
  vec2 o = (p-center)/(1.6*size);
  o.y = - o.y;
  float a = o.x*o.x+o.y*o.y-0.3;
  return step(a*a*a, o.x*o.x*o.y*o.y*o.y);
}
vec4 transition (vec2 uv) {
  return mix(
    getFromColor(uv),
    getToColor(uv),
    inHeart(uv, vec2(0.5, 0.6), progress)
  );
}
)";
}

} // namespace Gpu 
} // namespace Mmp