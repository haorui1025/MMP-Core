#include "KaleidoscopeTransition.h"

namespace Mmp
{
namespace Gpu 
{

class KaleidoscopeTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float speed = 1.0f;
    float angle = 1.0f;
    float power = 1.5f;
};

KaleidoscopeTransition::KaleidoscopeTransition()
{
    _data = std::make_shared<RawData>(sizeof(KaleidoscopeTransitionParamsPOD));
    KaleidoscopeTransitionParamsPOD* uniforms = reinterpret_cast<KaleidoscopeTransitionParamsPOD*>(_data->GetData());
    *uniforms = KaleidoscopeTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> KaleidoscopeTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "speed"},
        {UniformType::FLOAT1, "angle"},
        {UniformType::FLOAT1, "power"}
    };
}

std::string KaleidoscopeTransition::GetDescription()
{
    return "KaleidoscopeTransition";
}

RawData::ptr KaleidoscopeTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    KaleidoscopeTransitionParamsPOD* uniforms = reinterpret_cast<KaleidoscopeTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        KaleidoscopeTransitionParams::ptr _params = std::dynamic_pointer_cast<KaleidoscopeTransitionParams>(params);
        if (_params)
        {
            uniforms->speed = _params->speed;
            uniforms->angle = _params->angle;
            uniforms->power = _params->power;
        }
    }
    return _data;
}

std::string KaleidoscopeTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition(vec2 uv) {
  vec2 p = uv.xy / vec2(1.0f, 1.0f).xy;
  vec2 q = p;
  float t = pow(progress, power)*speed;
  p = p -0.5;
  for (int i = 0; i < 7; i++) {
    p = vec2(sin(t)*p.x + cos(t)*p.y, sin(t)*p.y - cos(t)*p.x);
    t += angle;
    p = abs(mod(p, 2.0) - 1.0);
  }
  abs(mod(p, 1.0));
  return mix(
    mix(getFromColor(q), getToColor(q), progress),
    mix(getFromColor(p), getToColor(p), progress), 1.0 - 2.0*abs(progress - 0.5));
}
)";
}

} // namespace Gpu 
} // namespace Mmp