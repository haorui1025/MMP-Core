//
//  CircleTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: Fernando Kuteken
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class CircleTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<CircleTransitionParams>;
public:
    float center[2] = {0.5f, 0.5f};
    float backColor[4] = {0.1f, 0.1f, 0.1f, 1.0f};
};

class CircleTransition : public TemplateTransition
{
public:
    CircleTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp