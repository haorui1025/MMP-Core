//
//  WaterDropTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// author: Paweł Płóciennik
// license: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class WaterDropTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<WaterDropTransitionParams>;
public:
    float amplitude = 30.0f;
    float speed = 30.0f;
};

class WaterDropTransition : public TemplateTransition
{
public:
    WaterDropTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp