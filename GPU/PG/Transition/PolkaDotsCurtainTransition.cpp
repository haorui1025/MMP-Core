#include "PolkaDotsCurtainTransition.h"

namespace Mmp
{
namespace Gpu 
{

class PolkaDotsCurtainTransitionParamsPod
{
public:
    float progress = 0.0f;
    float dots = 20.0f;
    float center[2] = {0};
};

PolkaDotsCurtainTransition::PolkaDotsCurtainTransition()
{
    _data = std::make_shared<RawData>(sizeof(PolkaDotsCurtainTransitionParamsPod));
    PolkaDotsCurtainTransitionParamsPod* uniforms = reinterpret_cast<PolkaDotsCurtainTransitionParamsPod*>(_data->GetData());
    *uniforms = PolkaDotsCurtainTransitionParamsPod();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> PolkaDotsCurtainTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "dots"},
        {UniformType::FLOAT2, "center"}
    };
}

std::string PolkaDotsCurtainTransition::GetDescription()
{
    return "PolkaDotsCurtainTransition";
}

RawData::ptr PolkaDotsCurtainTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    PolkaDotsCurtainTransitionParamsPod* uniforms = reinterpret_cast<PolkaDotsCurtainTransitionParamsPod*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        PolkaDotsCurtainTransitionParams::ptr _params = std::dynamic_pointer_cast<PolkaDotsCurtainTransitionParams>(params);
        if (_params)
        {
            uniforms->center[0] = _params->center[0];
            uniforms->center[1] = _params->center[1];
            uniforms->dots = uniforms->dots;
        }
    }
    return _data;
}

std::string PolkaDotsCurtainTransition::GetTransitionCode()
{
    return 
R"(
const float SQRT_2 = 1.414213562373;

vec4 transition(vec2 uv) {
  bool nextImage = distance(fract(uv * dots), vec2(0.5, 0.5)) < ( progress / distance(uv, center));
  return nextImage ? getToColor(uv) : getFromColor(uv);
}
)";
}

} // namespace Gpu 
} // namespace Mmp