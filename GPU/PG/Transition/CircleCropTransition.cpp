#include "CircleCropTransition.h"

namespace Mmp
{
namespace Gpu 
{

class CircleCropTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float bgcolor[4] = {0.0f, 0.0f, 0.0f, 1.0f};
};

CircleCropTransition::CircleCropTransition()
{
    _data = std::make_shared<RawData>(sizeof(CircleCropTransitionParamsPOD));
    CircleCropTransitionParamsPOD* uniforms = reinterpret_cast<CircleCropTransitionParamsPOD*>(_data->GetData());
    *uniforms = CircleCropTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> CircleCropTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT4, "bgcolor"}
    };
}

std::string CircleCropTransition::GetDescription()
{
    return "CircleCropTransition";
}

RawData::ptr CircleCropTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    CircleCropTransitionParamsPOD* uniforms = reinterpret_cast<CircleCropTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        CircleCropTransitionParams::ptr _params = std::dynamic_pointer_cast<CircleCropTransitionParams>(params);
        if (_params)
        {
            uniforms->bgcolor[0] = _params->bgcolor[0];
            uniforms->bgcolor[1] = _params->bgcolor[1];
            uniforms->bgcolor[1] = _params->bgcolor[2];
            uniforms->bgcolor[1] = _params->bgcolor[3];
        }
    }
    return _data;
}

std::string CircleCropTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition(vec2 p) {
  float ratio = 1.7777; // A that corresponds to width/height (16:9)
  vec2 ratio2 = vec2(1.0, 1.0 / ratio);
  float s = pow(2.0 * abs(progress - 0.5), 3.0);
  float dist = length((vec2(p) - 0.5) * ratio2);
  return mix(
    progress < 0.5 ? getFromColor(p) : getToColor(p), // branching is ok here as we statically depend on progress uniform (branching won't change over pixels)
    bgcolor,
    step(s, dist)
  );
}
)";
}

} // namespace Gpu 
} // namespace Mmp