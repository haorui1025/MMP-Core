//
//  PixelizeTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: gre
// License: MIT
// forked from https://gist.github.com/benraziel/c528607361d90a072e98
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class PixelizeTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<PixelizeTransitionParams>;
public:
    float squaresMin[2] = {20.0f, 20.0f};
    float steps = 50.0f;
};

class PixelizeTransition : public TemplateTransition
{
public:
    PixelizeTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp