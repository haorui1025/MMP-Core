#include "DreamyZoomTransition.h"

namespace Mmp
{
namespace Gpu 
{

class DreamyZoomTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float rotation = 6.0f;
    float scale = 1.2f;
};

DreamyZoomTransition::DreamyZoomTransition()
{
    _data = std::make_shared<RawData>(sizeof(DreamyZoomTransitionParamsPOD));
    DreamyZoomTransitionParamsPOD* uniforms = reinterpret_cast<DreamyZoomTransitionParamsPOD*>(_data->GetData());
    *uniforms = DreamyZoomTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> DreamyZoomTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "rotation"},
        {UniformType::FLOAT1, "scale"}
    };
}

std::string DreamyZoomTransition::GetDescription()
{
    return "DreamyZoomTransition";
}

RawData::ptr DreamyZoomTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    DreamyZoomTransitionParamsPOD* uniforms = reinterpret_cast<DreamyZoomTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        DreamyZoomTransitionParams::ptr _params = std::dynamic_pointer_cast<DreamyZoomTransitionParams>(params);
        if (_params)
        {
            uniforms->rotation = _params->rotation;
            uniforms->scale = _params->scale;
        }
    }
    return _data;
}

std::string DreamyZoomTransition::GetTransitionCode()
{
    return 
R"(
// Definitions --------
#define DEG2RAD 0.03926990816987241548078304229099 // 1/180*PI

// The code proper --------

vec4 transition(vec2 uv) {
  float ratio = 1.7777; // A that corresponds to width/height (16:9)
  // Massage parameters
  float phase = progress < 0.5 ? progress * 2.0 : (progress - 0.5) * 2.0;
  float angleOffset = progress < 0.5 ? mix(0.0, rotation * DEG2RAD, phase) : mix(-rotation * DEG2RAD, 0.0, phase);
  float newScale = progress < 0.5 ? mix(1.0, scale, phase) : mix(scale, 1.0, phase);
  
  vec2 center = vec2(0, 0);

  // Calculate the source point
  vec2 assumedCenter = vec2(0.5, 0.5);
  vec2 p = (uv.xy - vec2(0.5, 0.5)) / newScale * vec2(ratio, 1.0);

  // This can probably be optimized (with distance())
  float angle = _atan(p.y, p.x) + angleOffset;
  float dist = distance(center, p);
  p.x = cos(angle) * dist / ratio + 0.5;
  p.y = sin(angle) * dist + 0.5;
  vec4 c = progress < 0.5 ? getFromColor(p) : getToColor(p);

  // Finally, apply the color
  return c + (progress < 0.5 ? mix(0.0, 1.0, phase) : mix(1.0, 0.0, phase));
}
)";
}

} // namespace Gpu 
} // namespace Mmp