#include "RippleTransition.h"

namespace Mmp
{
namespace Gpu 
{

class RippleTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float amplitude = 30.0f;
    float speed = 30.0f;
};

RippleTransition::RippleTransition()
{
    _data = std::make_shared<RawData>(sizeof(RippleTransitionParamsPOD));
    RippleTransitionParamsPOD* uniforms = reinterpret_cast<RippleTransitionParamsPOD*>(_data->GetData());
    *uniforms = RippleTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> RippleTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "amplitude"},
        {UniformType::FLOAT1, "speed"}
    };
}

std::string RippleTransition::GetDescription()
{
    return "RippleTransition";
}

RawData::ptr RippleTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    RippleTransitionParamsPOD* uniforms = reinterpret_cast<RippleTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        RippleTransitionParams::ptr _params = std::dynamic_pointer_cast<RippleTransitionParams>(params);
        if (_params)
        {
            uniforms->amplitude = _params->amplitude;
            uniforms->speed = _params->speed;
        }
    }
    return _data;
}

std::string RippleTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition (vec2 uv) {
  vec2 dir = uv - vec2(0.5f, 0.5f);
  float dist = length(dir);
  vec2 offset = dir * (sin(progress * dist * amplitude - progress * speed) + .5) / 30.;
  return mix(
    getFromColor(uv + offset),
    getToColor(uv),
    smoothstep(0.2, 1.0, progress)
  );
}
)";
}

} // namespace Gpu 
} // namespace Mmp