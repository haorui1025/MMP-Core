#include "ColourDistanceTransition.h"

namespace Mmp
{
namespace Gpu 
{

class ColourDistanceTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float power = 5.0f;
};

ColourDistanceTransition::ColourDistanceTransition()
{
    _data = std::make_shared<RawData>(sizeof(ColourDistanceTransitionParamsPOD));
    ColourDistanceTransitionParamsPOD* uniforms = reinterpret_cast<ColourDistanceTransitionParamsPOD*>(_data->GetData());
    *uniforms = ColourDistanceTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> ColourDistanceTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "power"}
    };
}

std::string ColourDistanceTransition::GetDescription()
{
    return "ColourDistanceTransition";
}

RawData::ptr ColourDistanceTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    ColourDistanceTransitionParamsPOD* uniforms = reinterpret_cast<ColourDistanceTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        ColourDistanceTransitionParams::ptr _params = std::dynamic_pointer_cast<ColourDistanceTransitionParams>(params);
        if (_params)
        {
            uniforms->power = _params->power;
        }
    }
    return _data;
}

std::string ColourDistanceTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition(vec2 p) {
  vec4 fTex = getFromColor(p);
  vec4 tTex = getToColor(p);
  float m = step(distance(fTex, tTex), progress);
  return mix(
    mix(fTex, tTex, m),
    tTex,
    pow(progress, power)
  );
}
)";
}

} // namespace Gpu 
} // namespace Mmp