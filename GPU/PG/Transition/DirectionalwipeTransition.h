//
//  DirectionalwipeTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: gre
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class DirectionalwipeTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<DirectionalwipeTransitionParams>;
public:
    float direction[2] = {1.0f, -1.0f};
    float smoothness = 0.5f;
};

class DirectionalwipeTransition : public TemplateTransition
{
public:
    DirectionalwipeTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp