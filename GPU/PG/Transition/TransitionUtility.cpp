#include "TransitionUtility.h"

namespace Mmp
{
namespace Gpu
{

GLTransitionVertex::GLTransitionVertex(float x, float y, float u, float v)
{
    this->x = x;
    this->y = y;
    this->u = u;
    this->v = v;
}

std::vector<AttributeDesc> GetTransitionAttributeDescs(ShaderLanguage langauge)
{
    std::vector<AttributeDesc> attributes;
    switch (langauge)
    {
        case ShaderLanguage::GLSL_VULKAN:
        case ShaderLanguage::ELSL_3xx:
        case ShaderLanguage::GLSL_4xx:
        {
            attributes.push_back(AttributeDesc("Position", 0, 0, DataFormat::R32G32_FLOAT, 0 /* ~ 7 */));
            attributes.push_back(AttributeDesc("iUV", 0, 1, DataFormat::R32G32_FLOAT, 8 /* ~ 15 */));
            break;
        }
        case ShaderLanguage::HLSL_D3D11:
        {
            attributes.push_back(AttributeDesc("POSITION", 0, 0, DataFormat::R32G32_FLOAT, 0 /* ~ 7 */));
            attributes.push_back(AttributeDesc("TEXCOORD", 0, 1, DataFormat::R32G32_FLOAT, 8 /* ~ 15 */));
            break;
        }
        default:
            assert(false);
            break;
    }

    return attributes;
}


std::string GetTransitionVertexShader(ShaderLanguage langauge, const std::vector<std::pair<UniformType /* type */, std::string /* name */>>& uniforms)
{
    std::string source;

    switch (GLDrawContex::Instance()->GetShaderLanguage())
    {
        case ShaderLanguage::GLSL_VULKAN:
        {
            source += 
R"(
layout (std140, set = 0, binding = 0) uniform bufferVals 
{)";
            for (const auto& val : uniforms)
            {
                const UniformType& type = val.first;
                const std::string& name = val.second;
                source += "\t";
                switch (type)
                {
                    case UniformType::FLOAT1: source += "float"; break;
                    case UniformType::FLOAT2: source += "vec2"; break;
                    case UniformType::FLOAT3: source += "vec3"; break;
                    case UniformType::FLOAT4: source += "vec4"; break;
                    case UniformType::MATRIX4X4: source += "mat4"; break;
                    default:
                        assert(false);
                        return std::string();
                }
                source += " " + name + ";\n";
            }
            source +=
R"(
} myBufferVals;
layout (location = 0) in  vec2 Position;
layout (location = 1) in  vec2 iUV;
layout (location = 0) out vec2 oUV;
out gl_PerVertex { vec4 gl_Position; };

void main(void)
{
    oUV         =  iUV;
    gl_Position =  vec4(Position, 1.0, 1.0);
}
)";
            break;
        }
        case ShaderLanguage::ELSL_3xx:
        case ShaderLanguage::GLSL_4xx:
        {
            source = 
R"(
attribute vec2 Position;
attribute vec2 iUV;
varying   vec2 oUV;

void main(void)
{
    oUV         =  iUV;
    gl_Position =  vec4(Position, 1.0, 1.0);
}
)";
            break;
        }
        case ShaderLanguage::HLSL_D3D11:
        {
            source += 
R"(
struct VS_INPUT 
{
    vec2 Position : POSITION;
    vec2 iUV      : TEXCOORD0;
};

struct VS_OUTPUT
{
    vec2 oUV      : TEXCOORD0;
    vec4 Position : SV_Position;
};

cbuffer ConstantBuffer : register(b0) 
{
)";
    for (const auto& val : uniforms)
    {
        const UniformType& type = val.first;
        const std::string& name = val.second;
        source += "     ";
        switch (type)
        {
            case UniformType::FLOAT1: source += "float"; break;
            case UniformType::FLOAT2: source += "vec2"; break;
            case UniformType::FLOAT3: source += "vec3"; break;
            case UniformType::FLOAT4: source += "vec4"; break;
            case UniformType::MATRIX4X4: source += "matrix"; break;
            default:
                assert(false);
                return std::string();
        }
        source += " " + name + ";\n";
    }
            source +=
R"(
};

VS_OUTPUT main(VS_INPUT input)
{
    VS_OUTPUT output;
    output.Position = vec4(input.Position, 1.0, 1.0);
    output.oUV      = input.iUV;
    return output;
}
)";
            break;
        }
        default:
            assert(false);
            break;
    }

    return source;
}

std::string GetTransitionFragmentShader(ShaderLanguage langauge, const std::vector<std::pair<UniformType /* type */, std::string /* name */>>& uniforms, const std::string& transitonCode)
{
    std::string source;
    switch (langauge)
    {
        case ShaderLanguage::GLSL_VULKAN:
        {
            source += 
R"(
layout (std140, set = 0, binding = 0) uniform bufferVals 
{)";
            for (const auto& val : uniforms)
            {
                const UniformType& type = val.first;
                const std::string& name = val.second;
                source += "\t";
                switch (type)
                {
                    case UniformType::FLOAT1: source += "float"; break;
                    case UniformType::FLOAT2: source += "vec2"; break;
                    case UniformType::FLOAT3: source += "vec3"; break;
                    case UniformType::FLOAT4: source += "vec4"; break;
                    case UniformType::MATRIX4X4: source += "mat4"; break;
                    default:
                        assert(false);
                        return std::string();
                }
                source += " " + name + ";\n";
            }
            source +=
R"(
} myBufferVals;

layout (location = 0) in  vec2 oUV;
layout (location = 0) out vec4 fragColor0;

layout(set = 0, binding = 1) uniform sampler2D SceneA;
layout(set = 0, binding = 2) uniform sampler2D SceneB;

vec4 getFromColor(vec2 uv)
{
    return texture(SceneA, uv);
}

vec4 getToColor(vec2 uv)
{
    return texture(SceneB, uv);
}
)";
            source += transitonCode;
            source +=
R"(
void main(void)
{
	fragColor0 = transition(oUV);
}
)";
            break;
        }
        case ShaderLanguage::ELSL_3xx:
        case ShaderLanguage::GLSL_4xx:
        {
            source += 
R"(
varying vec2 oUV;

uniform sampler2D SceneA;
uniform sampler2D SceneB;
)";
    for (const auto& val : uniforms)
    {
        const UniformType& type = val.first;
        const std::string& name = val.second;
        source += "uniform ";
        switch (type)
        {
            case UniformType::FLOAT1: source += "float"; break;
            case UniformType::FLOAT2: source += "vec2"; break;
            case UniformType::FLOAT3: source += "vec3"; break;
            case UniformType::FLOAT4: source += "vec4"; break;
            case UniformType::MATRIX4X4: source += "mat4"; break;
            default:
                assert(false);
                return std::string();
        }
        source += " " + name + ";\n";
    }
            source +=
R"(
vec4 getFromColor(vec2 uv)
{
    return texture(SceneA, uv);
}

vec4 getToColor(vec2 uv)
{
    return texture(SceneB, uv);
}
)";
    source += transitonCode;
    source +=
R"(
void main(void)
{
	gl_FragColor = transition(oUV);
}
)";
            break;
        }
        case ShaderLanguage::HLSL_D3D11:
        {
            source += 
R"(
struct PS_INPUT 
{ 
    vec2 oUV : TEXCOORD0;
    vec4 Position : SV_Position;
};

SamplerState SampA : register(s0);
Texture2D<float4> SceneA : register(t0);
SamplerState SampB : register(s1);
Texture2D<float4> SceneB : register(t1);

cbuffer ConstantBuffer : register(b0) 
{
)";
    for (const auto& val : uniforms)
    {
        const UniformType& type = val.first;
        const std::string& name = val.second;
        source += "    ";
        switch (type)
        {
            case UniformType::FLOAT1: source += "float"; break;
            case UniformType::FLOAT2: source += "vec2"; break;
            case UniformType::FLOAT3: source += "vec3"; break;
            case UniformType::FLOAT4: source += "vec4"; break;
            case UniformType::MATRIX4X4: source += "matrix"; break;
            default:
                assert(false);
                return std::string();
        }
        source += " " + name + ";\n";
    }
            source +=
R"(
};

vec4 getFromColor(vec2 uv)
{
    return SceneA.Sample(SampA, uv);
}

vec4 getToColor(vec2 uv)
{
    return SceneB.Sample(SampB, uv);
}

)";
    source += transitonCode;
    source +=
R"(

vec4 main(PS_INPUT input) : SV_Target 
{
    return transition(input.oUV);
}
)";
            break;
        }
        default:
            assert(false);
    }
    return source;
};

} // namespace Gpu
} // namespace Mmp