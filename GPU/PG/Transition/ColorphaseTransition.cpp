#include "ColorphaseTransition.h"

namespace Mmp
{
namespace Gpu 
{

class ColorphaseTransitionParamsPOD
{
public:
    float fromStep[4] = {0.0f, 0.2f, 0.4f, 0.0f};
    float toStep[4] = {0.6f, 0.8f, 1.0f, 1.0f};
    float progress = 0.0f;
};

ColorphaseTransition::ColorphaseTransition()
{
    _data = std::make_shared<RawData>(sizeof(ColorphaseTransitionParamsPOD));
    ColorphaseTransitionParamsPOD* uniforms = reinterpret_cast<ColorphaseTransitionParamsPOD*>(_data->GetData());
    *uniforms = ColorphaseTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> ColorphaseTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT4, "fromStep"},
        {UniformType::FLOAT4, "toStep"},
        {UniformType::FLOAT1, "progress"}
    };
}

std::string ColorphaseTransition::GetDescription()
{
    return "ColorphaseTransition";
}

RawData::ptr ColorphaseTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    ColorphaseTransitionParamsPOD* uniforms = reinterpret_cast<ColorphaseTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        ColorphaseTransitionParams::ptr _params = std::dynamic_pointer_cast<ColorphaseTransitionParams>(params);
        if (_params)
        {
            uniforms->fromStep[0] = _params->fromStep[0];
            uniforms->fromStep[1] = _params->fromStep[1];
            uniforms->fromStep[2] = _params->fromStep[2];
            uniforms->fromStep[3] = _params->fromStep[3];
            uniforms->toStep[0] = _params->toStep[0];
            uniforms->toStep[1] = _params->toStep[1];
            uniforms->toStep[2] = _params->toStep[2];
            uniforms->toStep[3] = _params->toStep[3];
        }
    }
    return _data;
}

std::string ColorphaseTransition::GetTransitionCode()
{
    return 
R"(
// Usage: fromStep and toStep must be in [0.0, 1.0] range 
// and all(fromStep) must be < all(toStep)

vec4 transition (vec2 uv) {
  vec4 a = getFromColor(uv);
  vec4 b = getToColor(uv);
  return mix(a, b, smoothstep(fromStep, toStep, vec4(progress, progress, progress, progress)));
}
)";
}

} // namespace Gpu 
} // namespace Mmp