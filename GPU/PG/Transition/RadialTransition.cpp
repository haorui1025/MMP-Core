#include "RadialTransition.h"

namespace Mmp
{
namespace Gpu 
{

class RadialTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float smoothness = 1.0f;
};

RadialTransition::RadialTransition()
{
    _data = std::make_shared<RawData>(sizeof(RadialTransitionParamsPOD));
    RadialTransitionParamsPOD* uniforms = reinterpret_cast<RadialTransitionParamsPOD*>(_data->GetData());
    *uniforms = RadialTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> RadialTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "smoothness"}
    };
}

std::string RadialTransition::GetDescription()
{
    return "RadialTransition";
}

RawData::ptr RadialTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    RadialTransitionParamsPOD* uniforms = reinterpret_cast<RadialTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        RadialTransitionParams::ptr _params = std::dynamic_pointer_cast<RadialTransitionParams>(params);
        if (_params)
        {
            uniforms->smoothness = _params->smoothness;
        }
    }
    return _data;
}

std::string RadialTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition(vec2 p) {
float PI = 3.141592653589;
  vec2 rp = p*2.-1.;
  return mix(
    getToColor(p),
    getFromColor(p),
    smoothstep(0., smoothness, _atan(rp.y,rp.x) - (progress-.5) * PI * 2.5)
  );
}
)";
}

} // namespace Gpu 
} // namespace Mmp