//
//  DoomScreenTransitionTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: Zeh Fernando
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class DoomScreenTransitionTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<DoomScreenTransitionTransitionParams>;
public:
    float bars = 30.0f;
    float amplitude = 2.0f;
    float noise = 0.1f;
    float frequency = 0.5f;
    float dripScale = 0.5f;
};

class DoomScreenTransitionTransition : public TemplateTransition
{
public:
    DoomScreenTransitionTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp