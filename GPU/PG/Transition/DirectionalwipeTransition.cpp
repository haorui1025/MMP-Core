#include "DirectionalwipeTransition.h"

namespace Mmp
{
namespace Gpu 
{

class DirectionalwipeTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float direction[2] = {1.0f, -1.0f};
    float smoothness = 0.5f;
};

DirectionalwipeTransition::DirectionalwipeTransition()
{
    _data = std::make_shared<RawData>(sizeof(DirectionalwipeTransitionParamsPOD));
    DirectionalwipeTransitionParamsPOD* uniforms = reinterpret_cast<DirectionalwipeTransitionParamsPOD*>(_data->GetData());
    *uniforms = DirectionalwipeTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> DirectionalwipeTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT2, "direction"},
        {UniformType::FLOAT1, "smoothness"}
    };
}

std::string DirectionalwipeTransition::GetDescription()
{
    return "DirectionalwipeTransition";
}

RawData::ptr DirectionalwipeTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    DirectionalwipeTransitionParamsPOD* uniforms = reinterpret_cast<DirectionalwipeTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        DirectionalwipeTransitionParams::ptr _params = std::dynamic_pointer_cast<DirectionalwipeTransitionParams>(params);
        if (_params)
        {
            uniforms->direction[0] = _params->direction[0];
            uniforms->direction[1] = _params->direction[1];
            uniforms->smoothness = _params->smoothness;
        }
    }
    return _data;
}

std::string DirectionalwipeTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition (vec2 uv) {
  vec2 center = vec2(0.5, 0.5);
  vec2 v = normalize(direction);
  v /= abs(v.x)+abs(v.y);
  float d = v.x * center.x + v.y * center.y;
  float m =
    (1.0-step(progress, 0.0)) * // there is something wrong with our formula that makes m not equals 0.0 with progress is 0.0
    (1.0 - smoothstep(-smoothness, 0.0, v.x * uv.x + v.y * uv.y - (d-0.5+progress*(1.+smoothness))));
  return mix(getFromColor(uv), getToColor(uv), m);
}
)";
}

} // namespace Gpu 
} // namespace Mmp