//
//  PerlinTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: Rich Harris
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class PerlinTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<PerlinTransitionParams>;
public:
    float scale = 4.0f;
    float smoothness = 0.01f;
    float seed = 12.9898f;
};

class PerlinTransition : public TemplateTransition
{
public:
    PerlinTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp