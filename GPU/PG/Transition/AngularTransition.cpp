#include "AngularTransition.h"

namespace Mmp
{
namespace Gpu 
{

class AngularTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float startingAngle = 90.0f;
};

AngularTransition::AngularTransition()
{
    _data = std::make_shared<RawData>(sizeof(AngularTransitionParamsPOD));
    AngularTransitionParamsPOD* uniforms = reinterpret_cast<AngularTransitionParamsPOD*>(_data->GetData());
    *uniforms = AngularTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> AngularTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "startingAngle"}
    };
}

std::string AngularTransition::GetDescription()
{
    return "AngularTransition";
}

RawData::ptr AngularTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    AngularTransitionParamsPOD* uniforms = reinterpret_cast<AngularTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        AngularTransitionParams::ptr _params = std::dynamic_pointer_cast<AngularTransitionParams>(params);
        if (_params)
        {
            uniforms->startingAngle = _params->startingAngle;
        }
    }
    return _data;
}

std::string AngularTransition::GetTransitionCode()
{
    return 
R"(
#define PI 3.141592653589

vec4 transition (vec2 uv) {
  
  float offset = startingAngle * PI / 180.0;
  float angle = _atan(uv.y - 0.5, uv.x - 0.5) + offset;
  float normalizedAngle = (angle + PI) / (2.0 * PI);
  
  normalizedAngle = normalizedAngle - floor(normalizedAngle);

  return mix(
    getFromColor(uv),
    getToColor(uv),
    step(normalizedAngle, progress)
    );
}
)";
}

} // namespace Gpu 
} // namespace Mmp