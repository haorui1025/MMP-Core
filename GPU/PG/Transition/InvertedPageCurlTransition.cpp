#include "InvertedPageCurlTransition.h"

#include "TransitionUtility.h"

namespace Mmp
{
namespace Gpu 
{

InvertedPageCurlTransition::InvertedPageCurlTransition()
{
    _data = std::make_shared<RawData>(sizeof(AbstractTransitionParamsPOD));
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> InvertedPageCurlTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"}
    };
}

std::string InvertedPageCurlTransition::GetDescription()
{
    return "InvertedPageCurlTransition";
}

RawData::ptr InvertedPageCurlTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    AbstractTransitionParamsPOD* uniforms = reinterpret_cast<AbstractTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    return _data;
}

std::string InvertedPageCurlTransition::GetTransitionCode()
{
    return 
R"(
#define MIN_AMOUNT -0.16
#define MAX_AMOUNT  1.5
#define amount (progress * (MAX_AMOUNT - MIN_AMOUNT) + MIN_AMOUNT)

#define PI  3.141592653589793

#define scale 512.0
#define sharpness  3.0

#define cylinderCenter  amount
// 360 degrees * amount
#define cylinderAngle (2.0 * PI * amount)

#define cylinderRadius  (1.0 / PI / 2.0)

vec3 hitPoint(float hitAngle, float yc, vec3 _point, mat3 rrotation)
{
        float hitPoint = hitAngle / (2.0 * PI);
        _point.y = hitPoint;
        return mul(rrotation, _point);
}

vec4 antiAlias(vec4 color1, vec4 color2, float distanc)
{
        distanc *= scale;
        if (distanc < 0.0) return color2;
        if (distanc > 2.0) return color1;
        float dd = pow(1.0 - distanc / 2.0, sharpness);
        return ((color2 - color1) * dd) + color1;
}

float distanceToEdge(vec3 _point)
{
        float dx = abs(_point.x > 0.5 ? 1.0 - _point.x : _point.x);
        float dy = abs(_point.y > 0.5 ? 1.0 - _point.y : _point.y);
        if (_point.x < 0.0) dx = -_point.x;
        if (_point.x > 1.0) dx = _point.x - 1.0;
        if (_point.y < 0.0) dy = -_point.y;
        if (_point.y > 1.0) dy = _point.y - 1.0;
        if ((_point.x < 0.0 || _point.x > 1.0) && (_point.y < 0.0 || _point.y > 1.0)) return sqrt(dx * dx + dy * dy);
        return min(dx, dy);
}

vec4 seeThrough(float yc, vec2 p, mat3 rotation, mat3 rrotation)
{
        float hitAngle = PI - (acos(yc / cylinderRadius) - cylinderAngle);
        vec3 _point = hitPoint(hitAngle, yc, mul(rotation, vec3(p, 1.0)), rrotation);
        if (yc <= 0.0 && (_point.x < 0.0 || _point.y < 0.0 || _point.x > 1.0 || _point.y > 1.0))
        {
            return getToColor(p);
        }

        if (yc > 0.0) return getFromColor(p);

        vec4 color = getFromColor(_point.xy);
        vec4 tcolor = vec4(0.0f, 0.0f, 0.0f, 0.0f);

        return antiAlias(color, tcolor, distanceToEdge(_point));
}

vec4 seeThroughWithShadow(float yc, vec2 p, vec3 _point, mat3 rotation, mat3 rrotation)
{
        float shadow = distanceToEdge(_point) * 30.0;
        shadow = (1.0 - shadow) / 3.0;

        if (shadow < 0.0) shadow = 0.0; else shadow *= amount;

        vec4 shadowColor = seeThrough(yc, p, rotation, rrotation);
        shadowColor.r -= shadow;
        shadowColor.g -= shadow;
        shadowColor.b -= shadow;

        return shadowColor;
}

vec4 backside(float yc, vec3 _point)
{
    vec4 color = getFromColor(_point.xy);
    float gray = (color.r + color.b + color.g) / 15.0;
    gray += (8.0 / 10.0) * (pow(abs(1.0 - abs(yc / cylinderRadius)), 2.0 / 10.0) / 2.0 + (5.0 / 10.0));
    color.rgb = vec3(gray, gray, gray);
    return color;
}

vec4 behindSurface(vec2 p, float yc, vec3 _point, mat3 rrotation)
{
        float shado = (1.0 - ((-cylinderRadius - yc) / amount * 7.0)) / 6.0;
        shado *= 1.0 - abs(_point.x - 0.5);

        yc = (-cylinderRadius - cylinderRadius - yc);

        float hitAngle = (acos(yc / cylinderRadius) + cylinderAngle) - PI;
        _point = hitPoint(hitAngle, yc, _point, rrotation);

        if (yc < 0.0 && _point.x >= 0.0 && _point.y >= 0.0 && _point.x <= 1.0 && _point.y <= 1.0 && (hitAngle < PI || amount > 0.5))
        {
                shado = 1.0 - (sqrt(pow(_point.x - 0.5, 2.0) + pow(_point.y - 0.5, 2.0)) / (71.0 / 100.0));
                shado *= pow(-yc / cylinderRadius, 3.0);
                shado *= 0.5;
        }
        else
        {
                shado = 0.0;
        }
        return vec4(getToColor(p).rgb - shado, 1.0);
}

vec4 transition(vec2 p) {

        float angle = 100.0 * PI / 180.0;
        float c = cos(-angle);
        float s = sin(-angle);

        mat3 rotation = mat3( c, s, 0,
                                                                -s, c, 0,
                                                                -0.801, 0.8900, 1
                                                                );
        c = cos(angle);
        s = sin(angle);

        mat3 rrotation = mat3(	c, s, 0,
                                                                        -s, c, 0,
                                                                        0.98500, 0.985, 1
                                                                );

        vec3 _point = mul(rotation, vec3(p, 1.0));

        float yc = _point.y - cylinderCenter;

        if (yc < -cylinderRadius)
        {
                // Behind surface
                return behindSurface(p,yc, _point, rrotation);
        }

        if (yc > cylinderRadius)
        {
                // Flat surface
                return getFromColor(p);
        }

        float hitAngle = (acos(yc / cylinderRadius) + cylinderAngle) - PI;

        float hitAngleMod = mod(hitAngle, 2.0 * PI);
        if ((hitAngleMod > PI && amount < 0.5) || (hitAngleMod > PI/2.0 && amount < 0.0))
        {
                return seeThrough(yc, p, rotation, rrotation);
        }

        _point = hitPoint(hitAngle, yc, _point, rrotation);

        if (_point.x < 0.0 || _point.y < 0.0 || _point.x > 1.0 || _point.y > 1.0)
        {
                return seeThroughWithShadow(yc, p, _point, rotation, rrotation);
        }

        vec4 color = backside(yc, _point);

        vec4 otherColor;
        if (yc < 0.0)
        {
                float shado = 1.0 - (sqrt(pow(_point.x - 0.5, 2.0) + pow(_point.y - 0.5, 2.0)) / 0.71);
                shado *= pow(-yc / cylinderRadius, 3.0);
                shado *= 0.5;
                otherColor = vec4(0.0, 0.0, 0.0, shado);
        }
        else
        {
                otherColor = getFromColor(p);
        }

        color = antiAlias(color, otherColor, cylinderRadius - abs(yc));

        vec4 cl = seeThroughWithShadow(yc, p, _point, rotation, rrotation);
        float dist = distanceToEdge(_point);

        return antiAlias(color, cl, dist);
}
)";
}

} // namespace Gpu 
} // namespace Mmp