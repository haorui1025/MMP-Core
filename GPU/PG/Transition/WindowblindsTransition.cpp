#include "WindowblindsTransition.h"

#include "TransitionUtility.h"

namespace Mmp
{
namespace Gpu 
{

WindowblindsTransition::WindowblindsTransition()
{
    _data = std::make_shared<RawData>(sizeof(AbstractTransitionParamsPOD));
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> WindowblindsTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"}
    };
}

std::string WindowblindsTransition::GetDescription()
{
    return "WindowblindsTransition";
}

RawData::ptr WindowblindsTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    AbstractTransitionParamsPOD* uniforms = reinterpret_cast<AbstractTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    return _data;
}

std::string WindowblindsTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition (vec2 uv) {
  float t = progress;
  
  if (mod(floor(uv.y*100.*progress),2.)==0.)
    t*=2.-.5;
  
  return mix(
    getFromColor(uv),
    getToColor(uv),
    mix(t, progress, smoothstep(0.8, 1.0, progress))
  );
}
)";
}

} // namespace Gpu 
} // namespace Mmp