//
// TransitionUtility.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 

#pragma once

#include <vector>
#include <string>

#include "GPU/GL/GLDrawContex.h"

namespace Mmp
{
namespace Gpu
{

class AbstractTransitionParamsPOD
{
public:
    float progress = 0.0f;
};

struct GLTransitionVertex
{
public:
    GLTransitionVertex(float x, float y, float u, float v);
public:
    float x, y;  // attribute vec2 Position
    float u, v;  // attribute vec2 iUv
};

std::vector<AttributeDesc> GetTransitionAttributeDescs(ShaderLanguage langauge);

std::string GetTransitionVertexShader(ShaderLanguage langauge, const std::vector<std::pair<UniformType /* type */, std::string /* name */>>& uniforms);

std::string GetTransitionFragmentShader(ShaderLanguage langauge, const std::vector<std::pair<UniformType /* type */, std::string /* name */>>& uniforms, const std::string& transitonCode);

} // namespace Gpu
} // namespace Mmp