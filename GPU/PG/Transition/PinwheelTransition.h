//
//  PinwheelTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: Mr Speaker
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class PinwheelTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<PinwheelTransitionParams>;
public:
    float speed = 2.0f;
};

class PinwheelTransition : public TemplateTransition
{
public:
    PinwheelTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp