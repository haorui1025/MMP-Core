#include "ButterflyWaveScrawlerTransition.h"

namespace Mmp
{
namespace Gpu 
{

class ButterflyWaveScrawlerTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float amplitude = 1.0f;
    float waves = 30.0f;
    float colorSeparation = 0.3f;
};

ButterflyWaveScrawlerTransition::ButterflyWaveScrawlerTransition()
{
    _data = std::make_shared<RawData>(sizeof(ButterflyWaveScrawlerTransitionParamsPOD));
    ButterflyWaveScrawlerTransitionParamsPOD* uniforms = reinterpret_cast<ButterflyWaveScrawlerTransitionParamsPOD*>(_data->GetData());
    *uniforms = ButterflyWaveScrawlerTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> ButterflyWaveScrawlerTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "amplitude"},
        {UniformType::FLOAT1, "waves"},
        {UniformType::FLOAT1, "colorSeparation"}
    };
}

std::string ButterflyWaveScrawlerTransition::GetDescription()
{
    return "ButterflyWaveScrawlerTransition";
}

RawData::ptr ButterflyWaveScrawlerTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    ButterflyWaveScrawlerTransitionParamsPOD* uniforms = reinterpret_cast<ButterflyWaveScrawlerTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        ButterflyWaveScrawlerTransitionParams::ptr _params = std::dynamic_pointer_cast<ButterflyWaveScrawlerTransitionParams>(params);
        if (_params)
        {
            uniforms->amplitude = _params->amplitude;
            uniforms->waves = _params->waves;
            uniforms->colorSeparation = _params->colorSeparation;
        }
    }
    return _data;
}

std::string ButterflyWaveScrawlerTransition::GetTransitionCode()
{
    return 
R"(
#define PI  3.14159265358979323846264
float compute(vec2 p, float progress, vec2 center) {
vec2 o = p*sin(progress * amplitude)-center;
// horizontal vector
vec2 h = vec2(1., 0.);
// butterfly polar function (don't ask me why this one :))
float theta = acos(dot(o, h)) * waves;
return (exp(cos(theta)) - 2.*cos(4.*theta) + pow(sin((2.*theta - PI) / 24.), 3.)) / 10.;
}
vec4 transition(vec2 uv) {
  vec2 p = uv.xy / vec2(1.0f, 1.0f).xy;
  float inv = 1. - progress;
  vec2 dir = p - vec2(0.5f, 0.5f);
  float dist = length(dir);
  float disp = compute(p, progress, vec2(0.5, 0.5));
  vec4 texTo = getToColor(p + inv*disp);
  vec4 texFrom = vec4(
  getFromColor(p + progress*disp*(1.0 - colorSeparation)).r,
  getFromColor(p + progress*disp).g,
  getFromColor(p + progress*disp*(1.0 + colorSeparation)).b,
  1.0);
  return texTo*progress + texFrom*inv;
}
)";
}
} // namespace Gpu 
} // namespace Mmp