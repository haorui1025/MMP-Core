#include "PinwheelTransition.h"

namespace Mmp
{
namespace Gpu 
{

class PinwheelTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float speed = 2.0f;
};

PinwheelTransition::PinwheelTransition()
{
    _data = std::make_shared<RawData>(sizeof(PinwheelTransitionParamsPOD));
    PinwheelTransitionParamsPOD* uniforms = reinterpret_cast<PinwheelTransitionParamsPOD*>(_data->GetData());
    *uniforms = PinwheelTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> PinwheelTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "speed"}
    };
}

std::string PinwheelTransition::GetDescription()
{
    return "PinwheelTransition";
}

RawData::ptr PinwheelTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    PinwheelTransitionParamsPOD* uniforms = reinterpret_cast<PinwheelTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        PinwheelTransitionParams::ptr _params = std::dynamic_pointer_cast<PinwheelTransitionParams>(params);
        if (_params)
        {
            uniforms->speed = _params->speed;
        }
    }
    return _data;
}

std::string PinwheelTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition(vec2 uv) {
  
  vec2 p = uv.xy / vec2(1.0f, 1.0f).xy;
  
  float circPos = _atan(p.y - 0.5, p.x - 0.5) + progress * speed;
  float modPos = mod(circPos, 3.1415 / 4.);
  float _signed = sign(progress - modPos);
  
  return mix(getToColor(p), getFromColor(p), step(_signed, 0.5));
  
}
)";
}

} // namespace Gpu 
} // namespace Mmp