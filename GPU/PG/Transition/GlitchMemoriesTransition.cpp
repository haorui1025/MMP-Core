#include "GlitchMemoriesTransition.h"

#include "TransitionUtility.h"

namespace Mmp
{
namespace Gpu 
{

GlitchMemoriesTransition::GlitchMemoriesTransition()
{
    _data = std::make_shared<RawData>(sizeof(AbstractTransitionParamsPOD));
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> GlitchMemoriesTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"}
    };
}

std::string GlitchMemoriesTransition::GetDescription()
{
    return "GlitchMemoriesTransition";
}

RawData::ptr GlitchMemoriesTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    AbstractTransitionParamsPOD* uniforms = reinterpret_cast<AbstractTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    return _data;
}

std::string GlitchMemoriesTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition(vec2 p) {
  vec2 block = floor(p.xy / vec2(16.0f, 16.0f));
  vec2 uv_noise = block / vec2(64.0f, 64.0f);
  uv_noise += floor(vec2(progress, progress) * vec2(1200.0f, 3500.0f)) / vec2(64.0f, 64.0f);
  vec2 dist = progress > 0.0f ? (fract(uv_noise) - 0.5f) * 0.3f *(1.0f -progress) : vec2(0.0f, 0.0f);
  vec2 red = p + dist * 0.2f;
  vec2 green = p + dist * 0.3f;
  vec2 blue = p + dist * 0.5f;

  return vec4(mix(getFromColor(red), getToColor(red), progress).r,mix(getFromColor(green), getToColor(green), progress).g,mix(getFromColor(blue), getToColor(blue), progress).b,1.0f);
}
)";
}

} // namespace Gpu 
} // namespace Mmp