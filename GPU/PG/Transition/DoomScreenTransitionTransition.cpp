#include "DoomScreenTransitionTransition.h"

namespace Mmp
{
namespace Gpu 
{

class DoomScreenTransitionTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float bars = 30.0f;
    float amplitude = 2.0f;
    float noise = 0.1f;
    float frequency = 0.5f;
    float dripScale = 0.5f;
};

DoomScreenTransitionTransition::DoomScreenTransitionTransition()
{
    _data = std::make_shared<RawData>(sizeof(DoomScreenTransitionTransitionParamsPOD));
    DoomScreenTransitionTransitionParamsPOD* uniforms = reinterpret_cast<DoomScreenTransitionTransitionParamsPOD*>(_data->GetData());
    *uniforms = DoomScreenTransitionTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> DoomScreenTransitionTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "bars"},
        {UniformType::FLOAT1, "amplitude"},
        {UniformType::FLOAT1, "noise"},
        {UniformType::FLOAT1, "frequency"},
        {UniformType::FLOAT1, "dripScale"}
    };
}

std::string DoomScreenTransitionTransition::GetDescription()
{
    return "DoomScreenTransitionTransition";
}

RawData::ptr DoomScreenTransitionTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    DoomScreenTransitionTransitionParamsPOD* uniforms = reinterpret_cast<DoomScreenTransitionTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        DoomScreenTransitionTransitionParams::ptr _params = std::dynamic_pointer_cast<DoomScreenTransitionTransitionParams>(params);
        if (_params)
        {
            uniforms->bars = _params->bars;
            uniforms->amplitude = _params->amplitude;
            uniforms->noise = _params->noise;
            uniforms->frequency = _params->frequency;
            uniforms->dripScale = _params->dripScale;
        }
    }
    return _data;
}

std::string DoomScreenTransitionTransition::GetTransitionCode()
{
    return 
R"(
// The code proper --------

float rand(int num) {
  return fract(mod(float(num) * 67123.313, 12.0) * sin(float(num) * 10.3) * cos(float(num)));
}

float wave(int num) {
  float fn = float(num) * frequency * 0.1 * float(bars);
  return cos(fn * 0.5) * cos(fn * 0.13) * sin((fn+10.0) * 0.3) / 2.0 + 0.5;
}

float drip(int num) {
  return sin(float(num) / float(bars - 1) * 3.141592) * dripScale;
}

float pos(int num) {
  return (noise == 0.0 ? wave(num) : mix(wave(num), rand(num), noise)) + (dripScale == 0.0 ? 0.0 : drip(num));
}

vec4 transition(vec2 uv) {
  int bar = int(uv.x * (float(bars)));
  float scale = 1.0 + pos(bar) * amplitude;
  float phase = progress * scale;
  float posY = uv.y / vec2(1.0f, 1.0f).y;
  vec2 p;
  vec4 c;
  if (phase + posY < 1.0) {
    p = vec2(uv.x, uv.y + mix(0.0, vec2(1.0f, 1.0f).y, phase)) / vec2(1.0f, 1.0f).xy;
    c = getFromColor(p);
  } else {
    p = uv.xy / vec2(1.0f, 1.0f).xy;
    c = getToColor(p);
  }

  // Finally, apply the color
  return c;
}
)";
}

} // namespace Gpu 
} // namespace Mmp