//
//  CrosshatchTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// author: Paweł Płóciennik
// license: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class CrosshatchTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<CrosshatchTransitionParams>;
public:
    float center[2] = {0.5f, 0.5f};
    float threshold = 3.0f;
    float fadeEdge = 0.1f;
};

class CrosshatchTransition : public TemplateTransition
{
public:
    CrosshatchTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp