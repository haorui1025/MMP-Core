#include "LinearBlurTransition.h"

namespace Mmp
{
namespace Gpu 
{

class LinearBlurTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float intensity = 0.1f;
};

LinearBlurTransition::LinearBlurTransition()
{
    _data = std::make_shared<RawData>(sizeof(LinearBlurTransitionParamsPOD));
    LinearBlurTransitionParamsPOD* uniforms = reinterpret_cast<LinearBlurTransitionParamsPOD*>(_data->GetData());
    *uniforms = LinearBlurTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> LinearBlurTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "intensity"}
    };
}

std::string LinearBlurTransition::GetDescription()
{
    return "LinearBlurTransition";
}

RawData::ptr LinearBlurTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    LinearBlurTransitionParamsPOD* uniforms = reinterpret_cast<LinearBlurTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        LinearBlurTransitionParams::ptr _params = std::dynamic_pointer_cast<LinearBlurTransitionParams>(params);
        if (_params)
        {
            uniforms->intensity = _params->intensity;
        }
    }
    return _data;
}

std::string LinearBlurTransition::GetTransitionCode()
{
    return 
R"(
#define passes 6

vec4 transition(vec2 uv) {
    vec4 c1 = vec4(0.0f, 0.0f, 0.0f, 0.0f);
    vec4 c2 = vec4(0.0f, 0.0f, 0.0f, 0.0f);

    float disp = intensity*(0.5-distance(0.5, progress));
    for (int xi=0; xi<passes; xi++)
    {
        float x = float(xi) / float(passes) - 0.5;
        for (int yi=0; yi<passes; yi++)
        {
            float y = float(yi) / float(passes) - 0.5;
            vec2 v = vec2(x,y);
            float d = disp;
            c1 += getFromColor( uv + d*v);
            c2 += getToColor( uv + d*v);
        }
    }
    c1 /= float(passes*passes);
    c2 /= float(passes*passes);
    return mix(c1, c2, progress);
}

)";
}

} // namespace Gpu 
} // namespace Mmp