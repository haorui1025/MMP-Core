#include "CrosshatchTransition.h"

namespace Mmp
{
namespace Gpu 
{

class CrosshatchTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float center[2] = {0.5f, 0.5f};
    float threshold = 3.0f;
    float fadeEdge = 0.1f;
};

CrosshatchTransition::CrosshatchTransition()
{
    _data = std::make_shared<RawData>(sizeof(CrosshatchTransitionParamsPOD));
    CrosshatchTransitionParamsPOD* uniforms = reinterpret_cast<CrosshatchTransitionParamsPOD*>(_data->GetData());
    *uniforms = CrosshatchTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> CrosshatchTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT2, "center"},
        {UniformType::FLOAT1, "threshold"},
        {UniformType::FLOAT1, "fadeEdge"}
    };
}

std::string CrosshatchTransition::GetDescription()
{
    return "CrosshatchTransition";
}

RawData::ptr CrosshatchTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    CrosshatchTransitionParamsPOD* uniforms = reinterpret_cast<CrosshatchTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        CrosshatchTransitionParams::ptr _params = std::dynamic_pointer_cast<CrosshatchTransitionParams>(params);
        if (_params)
        {
            uniforms->center[0] = _params->center[0];
            uniforms->center[1] = _params->center[1];
            uniforms->threshold = _params->threshold;
            uniforms->fadeEdge= _params->fadeEdge;
        }
    }
    return _data;
}

std::string CrosshatchTransition::GetTransitionCode()
{
    return 
R"(
float rand(vec2 co) {
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}
vec4 transition(vec2 p) {
  float dist = distance(center, p) / threshold;
  float r = progress - min(rand(vec2(p.y, 0.0)), rand(vec2(0.0, p.x)));
  return mix(getFromColor(p), getToColor(p), mix(0.0, mix(step(dist, r), 1.0, smoothstep(1.0-fadeEdge, 1.0, progress)), smoothstep(0.0, fadeEdge, progress)));    
}
)";
}

} // namespace Gpu 
} // namespace Mmp