#include "GridFlipTransition.h"

namespace Mmp
{
namespace Gpu 
{

class GridFlipTransitionParamsPOD
{
public:
    float bgcolor[4] = {0.0f, 0.0f, 0.0f, 1.0f}; // push first to make sure align
    float progress = 0.0f;
    float size[2] = {4.0f, 4.0f};
    float pause = 0.1f;
    float dividerWidth = 0.05f;
    float randomness = 0.1f;
};

GridFlipTransition::GridFlipTransition()
{
    _data = std::make_shared<RawData>(sizeof(GridFlipTransitionParamsPOD));
    GridFlipTransitionParamsPOD* uniforms = reinterpret_cast<GridFlipTransitionParamsPOD*>(_data->GetData());
    *uniforms = GridFlipTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> GridFlipTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT4, "bgcolor"},
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT2, "size"},
        {UniformType::FLOAT1, "pause"},
        {UniformType::FLOAT1, "dividerWidth"},
        {UniformType::FLOAT1, "randomness"}
    };
}

std::string GridFlipTransition::GetDescription()
{
    return "GridFlipTransition";
}

RawData::ptr GridFlipTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    GridFlipTransitionParamsPOD* uniforms = reinterpret_cast<GridFlipTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        GridFlipTransitionParams::ptr _params = std::dynamic_pointer_cast<GridFlipTransitionParams>(params);
        if (_params)
        {
            uniforms->size[0] = _params->size[0];
            uniforms->size[1] = _params->size[1];
            uniforms->pause = _params->pause;
            uniforms->dividerWidth = _params->dividerWidth;
            uniforms->bgcolor[0] = _params->bgcolor[0];
            uniforms->bgcolor[1] = _params->bgcolor[1];
            uniforms->bgcolor[2] = _params->bgcolor[2];
            uniforms->bgcolor[3] = _params->bgcolor[3];
            uniforms->randomness = _params->randomness;
        }
    }
    return _data;
}

std::string GridFlipTransition::GetTransitionCode()
{
    return 
R"(
float rand (vec2 co) {
  return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

float getDelta(vec2 p) {
  vec2 rectanglePos = floor(vec2(size) * p);
  vec2 rectangleSize = vec2(1.0 / vec2(size).x, 1.0 / vec2(size).y);
  float top = rectangleSize.y * (rectanglePos.y + 1.0);
  float bottom = rectangleSize.y * rectanglePos.y;
  float left = rectangleSize.x * rectanglePos.x;
  float right = rectangleSize.x * (rectanglePos.x + 1.0);
  float minX = min(abs(p.x - left), abs(p.x - right));
  float minY = min(abs(p.y - top), abs(p.y - bottom));
  return min(minX, minY);
}

float getDividerSize() {
  vec2 rectangleSize = vec2(1.0 / vec2(size).x, 1.0 / vec2(size).y);
  return min(rectangleSize.x, rectangleSize.y) * dividerWidth;
}

vec4 transition(vec2 p) {
  if(progress < pause) {
    float currentProg = progress / pause;
    float a = 1.0;
    if(getDelta(p) < getDividerSize()) {
      a = 1.0 - currentProg;
    }
    return mix(bgcolor, getFromColor(p), a);
  }
  else if(progress < 1.0 - pause){
    if(getDelta(p) < getDividerSize()) {
      return bgcolor;
    } else {
      float currentProg = (progress - pause) / (1.0 - pause * 2.0);
      vec2 q = p;
      vec2 rectanglePos = floor(vec2(size) * q);
      
      float r = rand(rectanglePos) - randomness;
      float cp = smoothstep(0.0, 1.0 - r, currentProg);
    
      float rectangleSize = 1.0 / vec2(size).x;
      float delta = rectanglePos.x * rectangleSize;
      float offset = rectangleSize / 2.0 + delta;
      
      p.x = (p.x - offset)/abs(cp - 0.5)*0.5 + offset;
      vec4 a = getFromColor(p);
      vec4 b = getToColor(p);
      
      float s = step(abs(vec2(size).x * (q.x - delta) - 0.5), abs(cp - 0.5));
      return mix(bgcolor, mix(b, a, step(cp, 0.5)), s);
    }
  }
  else {
    float currentProg = (progress - 1.0 + pause) / pause;
    float a = 1.0;
    if(getDelta(p) < getDividerSize()) {
      a = currentProg;
    }
    return mix(bgcolor, getToColor(p), a);
  }
}
)";
}

} // namespace Gpu 
} // namespace Mmp