//
//  GridFlipTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// License: MIT
// Author: TimDonselaar
// ported by gre from https://gist.github.com/TimDonselaar/9bcd1c4b5934ba60087bdb55c2ea92e5
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class GridFlipTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<GridFlipTransitionParams>;
public:
    float amplitude = 30.0f;
    float size[2] = {4.0f, 4.0f};
    float pause = 0.1f;
    float dividerWidth = 0.05f;
    float bgcolor[4] = {0.0f, 0.0f, 0.0f, 1.0f};
    float randomness = 0.1f;
};

class GridFlipTransition : public TemplateTransition
{
public:
    GridFlipTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp