#include "FlyeyeTransition.h"

namespace Mmp
{
namespace Gpu 
{

class FlyeyeTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float size = 0.04f;
    float zoom = 50.0f;
    float colorSeparation = 0.3f;
};

FlyeyeTransition::FlyeyeTransition()
{
    _data = std::make_shared<RawData>(sizeof(FlyeyeTransitionParamsPOD));
    FlyeyeTransitionParamsPOD* uniforms = reinterpret_cast<FlyeyeTransitionParamsPOD*>(_data->GetData());
    *uniforms = FlyeyeTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> FlyeyeTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "size"},
        {UniformType::FLOAT1, "zoom"},
        {UniformType::FLOAT1, "colorSeparation"}
    };
}

std::string FlyeyeTransition::GetDescription()
{
    return "FlyeyeTransition";
}

RawData::ptr FlyeyeTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    FlyeyeTransitionParamsPOD* uniforms = reinterpret_cast<FlyeyeTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        FlyeyeTransitionParams::ptr _params = std::dynamic_pointer_cast<FlyeyeTransitionParams>(params);
        if (_params)
        {
            uniforms->zoom = _params->zoom;
            uniforms->size = _params->size;
            uniforms->colorSeparation = _params->colorSeparation;
        }
    }
    return _data;
}

std::string FlyeyeTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition(vec2 p) {
  float inv = 1. - progress;
  vec2 disp = size*vec2(cos(zoom*p.x), sin(zoom*p.y));
  vec4 texTo = getToColor(p + inv*disp);
  vec4 texFrom = vec4(
    getFromColor(p + progress*disp*(1.0 - colorSeparation)).r,
    getFromColor(p + progress*disp).g,
    getFromColor(p + progress*disp*(1.0 + colorSeparation)).b,
    1.0);
  return texTo*progress + texFrom*inv;
}
)";
}

} // namespace Gpu 
} // namespace Mmp