//
//  DoorwayTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// author: gre
// License: MIT 
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class DoorwayTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<DoorwayTransitionParams>;
public:
    float reflection = 0.4f;
    float perspective = 0.4f;
    float depth = 3.0f;
};

class DoorwayTransition : public TemplateTransition
{
public:
    DoorwayTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp