//
// SwirlTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// License: MIT
// Author: Sergey Kosarevsky
// ( http://www.linderdaum.com )
// ported by gre from https://gist.github.com/corporateshark/cacfedb8cca0f5ce3f7c
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class SwirlTransition : public TemplateTransition
{
public:
    SwirlTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp