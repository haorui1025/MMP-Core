#include "SquareswireTransition.h"

namespace Mmp
{
namespace Gpu 
{

class SquareswireTransitionParamsPOD
{
public:
    float squares[2] = {10.0f, 10.0f};
    float direction[2] = {1.0f, -0.5f}; // make align
    float progress = 0.0f;
    float smoothness = 1.6f;
};

SquareswireTransition::SquareswireTransition()
{
    _data = std::make_shared<RawData>(sizeof(SquareswireTransitionParamsPOD));
    SquareswireTransitionParamsPOD* uniforms = reinterpret_cast<SquareswireTransitionParamsPOD*>(_data->GetData());
    *uniforms = SquareswireTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> SquareswireTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT2, "squares"},
        {UniformType::FLOAT2, "direction"},
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "smoothness"}
    };
}

std::string SquareswireTransition::GetDescription()
{
    return "SquareswireTransition";
}

RawData::ptr SquareswireTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    SquareswireTransitionParamsPOD* uniforms = reinterpret_cast<SquareswireTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        SquareswireTransitionParams::ptr _params = std::dynamic_pointer_cast<SquareswireTransitionParams>(params);
        if (_params)
        {
            uniforms->squares[0] = _params->squares[0];
            uniforms->squares[1] = _params->squares[1];
            uniforms->progress = _params->progress;
            uniforms->smoothness = _params->smoothness;
        }
    }
    return _data;
}

std::string SquareswireTransition::GetTransitionCode()
{
    return 
R"(
vec2 center = vec2(0.5, 0.5);
vec4 transition (vec2 p) {
  vec2 v = normalize(direction);
  v /= abs(v.x)+abs(v.y);
  float d = v.x * center.x + v.y * center.y;
  float offset = smoothness;
  float pr = smoothstep(-offset, 0.0, v.x * p.x + v.y * p.y - (d-0.5+progress*(1.+offset)));
  vec2 squarep = fract(p*vec2(squares));
  vec2 squaremin = vec2(pr/2.0, pr/2.0);
  vec2 squaremax = vec2(1.0 - pr/2.0, 1.0 - pr/2.0);
  float a = (1.0 - step(progress, 0.0)) * step(squaremin.x, squarep.x) * step(squaremin.y, squarep.y) * step(squarep.x, squaremax.x) * step(squarep.y, squaremax.y);
  return mix(getFromColor(p), getToColor(p), a);
}
)";
}

} // namespace Gpu 
} // namespace Mmp