//
//  CubeTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: gre
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class CubeTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<CubeTransitionParams>;
public:
    float persp = 0.7f;
    float unzoom = 0.3f;
    float reflection = 0.4f;
    float floating = 3.0f;
};

class CubeTransition : public TemplateTransition
{
public:
    CubeTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp