#include "SwapTransition.h"

namespace Mmp
{
namespace Gpu 
{

class SwapTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float reflection = 0.4f;
    float perspective = 0.2f;
    float depth = 3.0f;
};

SwapTransition::SwapTransition()
{
    _data = std::make_shared<RawData>(sizeof(SwapTransitionParamsPOD));
    SwapTransitionParamsPOD* uniforms = reinterpret_cast<SwapTransitionParamsPOD*>(_data->GetData());
    *uniforms = SwapTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> SwapTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "reflection"},
        {UniformType::FLOAT1, "perspective"},
        {UniformType::FLOAT1, "depth"}
    };
}

std::string SwapTransition::GetDescription()
{
    return "SwapTransition";
}

RawData::ptr SwapTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    SwapTransitionParamsPOD* uniforms = reinterpret_cast<SwapTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        SwapTransitionParams::ptr _params = std::dynamic_pointer_cast<SwapTransitionParams>(params);
        if (_params)
        {
            uniforms->reflection = _params->reflection;
            uniforms->perspective = _params->perspective;
            uniforms->depth = _params->depth;
        }
    }
    return _data;
}

std::string SwapTransition::GetTransitionCode()
{
    return 
R"(
#define black  vec4(0.0, 0.0, 0.0, 1.0)
#define boundMin  vec2(0.0, 0.0)
#define boundMax  vec2(1.0, 1.0)
 
bool inBounds (vec2 p) {
  return all(lessThan(boundMin, p)) && all(lessThan(p, boundMax));
}
 
vec2 project (vec2 p) {
  return p * vec2(1.0, -1.2) + vec2(0.0, -0.02);
}
 
vec4 bgColor (vec2 p, vec2 pfr, vec2 pto) {
  vec4 c = black;
  pfr = project(pfr);
  if (inBounds(pfr)) {
    c += mix(black, getFromColor(pfr), reflection * mix(1.0, 0.0, pfr.y));
  }
  pto = project(pto);
  if (inBounds(pto)) {
    c += mix(black, getToColor(pto), reflection * mix(1.0, 0.0, pto.y));
  }
  return c;
}
 
vec4 transition(vec2 p) {
    vec2 pfr = vec2(-1.0f, -1.0f);
    vec2 pto = vec2(-1.0f, -1.0f);

    float size = mix(1.0, depth, progress);
    float persp = perspective * progress;
    pfr = (p + vec2(-0.0, -0.5)) * vec2(size/(1.0-perspective*progress), size/(1.0-size*persp*p.x)) + vec2(0.0, 0.5);

    size = mix(1.0, depth, 1.-progress);
    persp = perspective * (1.-progress);
    pto = (p + vec2(-1.0, -0.5)) * vec2(size/(1.0-perspective*(1.0-progress)), size/(1.0-size*persp*(0.5-p.x))) + vec2(1.0, 0.5);

    if (progress < 0.5) 
    {
        if (inBounds(pfr)) 
        {
            return getFromColor(pfr);
        }
        if (inBounds(pto)) 
        {
            return getToColor(pto);
        }  
        } else {
        if (inBounds(pto)) 
        {
            return getToColor(pto);
        }
        if (inBounds(pfr)) 
        {
            return getFromColor(pfr);
        }
    }
    return vec4(0.0f, 0.0f, 0.0f, 1.0f);
}
)";
}

} // namespace Gpu 
} // namespace Mmp