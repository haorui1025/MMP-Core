//
//  HexagonalizeTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: Fernando Kuteken
// License: MIT
// Hexagonal math from: http://www.redblobgames.com/grids/hexagons/
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class HexagonalizeTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<HexagonalizeTransitionParams>;
public:
    float steps = 50.0f;
    float horizontalHexagons = 20.0f;
};

class HexagonalizeTransition : public TemplateTransition
{
public:
    HexagonalizeTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp