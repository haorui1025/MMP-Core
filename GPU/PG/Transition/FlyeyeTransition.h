//
//  FlyeyeTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: gre
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{
class FlyeyeTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<FlyeyeTransitionParams>;
public:
    float size = 0.04f;
    float zoom = 50.0f;
    float colorSeparation = 0.3f;
};

class FlyeyeTransition : public TemplateTransition
{
public:
    FlyeyeTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp