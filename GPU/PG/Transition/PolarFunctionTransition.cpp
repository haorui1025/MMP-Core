 #include "PolarFunctionTransition.h"

namespace Mmp
{
namespace Gpu 
{

class PolarFunctionTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float segments = 5.0f;
};

PolarFunctionTransition::PolarFunctionTransition()
{
    _data = std::make_shared<RawData>(sizeof(PolarFunctionTransitionParamsPOD));
    PolarFunctionTransitionParamsPOD* uniforms = reinterpret_cast<PolarFunctionTransitionParamsPOD*>(_data->GetData());
    *uniforms = PolarFunctionTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> PolarFunctionTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "segments"}
    };
}

std::string PolarFunctionTransition::GetDescription()
{
    return "PolarFunctionTransition";
}

RawData::ptr PolarFunctionTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    PolarFunctionTransitionParamsPOD* uniforms = reinterpret_cast<PolarFunctionTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        PolarFunctionTransitionParams::ptr _params = std::dynamic_pointer_cast<PolarFunctionTransitionParams>(params);
        if (_params)
        {
            uniforms->segments = _params->segments;
        }
    }
    return _data;
}

std::string PolarFunctionTransition::GetTransitionCode()
{
    return 
R"(
#define PI 3.14159265359
vec4 transition (vec2 uv) {
  
  float angle = _atan(uv.y - 0.5, uv.x - 0.5) - 0.5 * PI;
  float normalized = (angle + 1.5 * PI) * (2.0 * PI);
  
  float radius = (cos(float(segments) * angle) + 4.0) / 4.0;
  float difference = length(uv - vec2(0.5, 0.5));
  
  if (difference > radius * progress)
    return getFromColor(uv);
  else
    return getToColor(uv);
}
)";
}

} // namespace Gpu 
} // namespace Mmp