//
//  SquareswireTransition.h
//
// Library: GPU
// Package: Program
// Module:  Transition
// 
// Modify from [gl-transitions](https://github.com/gl-transitions/gl-transitions/tree/master)
// Author: gre
// License: MIT
//

#pragma once

#include <memory>
#include <string>
#include <mutex>

#include "TemplateTransition.h"

namespace Mmp
{
namespace Gpu 
{

class SquareswireTransitionParams : public AbstractTransitionParams
{
public:
    using ptr = std::shared_ptr<SquareswireTransitionParams>;
public:
    float squares[2] = {10.0f, 10.0f};
    float direction[2] = {1.0f, -0.5f};
    float smoothness = 1.6f;
};

class SquareswireTransition : public TemplateTransition
{
public:
    SquareswireTransition();
public:
    std::vector<std::pair<UniformType /* type */, std::string /* name */>> GetUniformsDescription();
    std::string GetTransitionCode();
    std::string GetDescription();
    RawData::ptr GetUniformsData(AbstractTransitionParams::ptr params);
};

} // namespace Gpu 
} // namespace Mmp