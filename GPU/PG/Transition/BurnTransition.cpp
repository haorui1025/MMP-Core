#include "BurnTransition.h"

namespace Mmp
{
namespace Gpu 
{

class BurnTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float color[3] = {0.9f, 0.4f, 0.2f};
};

BurnTransition::BurnTransition()
{
    _data = std::make_shared<RawData>(sizeof(BurnTransitionParamsPOD));
    BurnTransitionParamsPOD* uniforms = reinterpret_cast<BurnTransitionParamsPOD*>(_data->GetData());
    *uniforms = BurnTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> BurnTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT3, "color"}
    };
}

std::string BurnTransition::GetDescription()
{
    return "BurnTransition";
}

RawData::ptr BurnTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    BurnTransitionParamsPOD* uniforms = reinterpret_cast<BurnTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        BurnTransitionParams::ptr _params = std::dynamic_pointer_cast<BurnTransitionParams>(params);
        if (_params)
        {
            uniforms->color[0] = _params->color[0];
            uniforms->color[1] = _params->color[1];
            uniforms->color[2] = _params->color[2];
        }
    }
    return _data;
}

std::string BurnTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition (vec2 uv) {
  return mix(
    getFromColor(uv) + vec4(progress*color, 1.0),
    getToColor(uv) + vec4((1.0-progress)*color, 1.0),
    progress
  );
}
)";
}

} // namespace Gpu 
} // namespace Mmp