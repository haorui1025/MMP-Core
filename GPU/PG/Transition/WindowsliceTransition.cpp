#include "WindowsliceTransition.h"

namespace Mmp
{
namespace Gpu 
{

class WindowsliceTransitionParamsPOD
{
public:
    float progress = 0.0f;
    float count = 10.0f;
    float smoothness = 0.5f;
};

WindowsliceTransition::WindowsliceTransition()
{
    _data = std::make_shared<RawData>(sizeof(WindowsliceTransitionParamsPOD));
    WindowsliceTransitionParamsPOD* uniforms = reinterpret_cast<WindowsliceTransitionParamsPOD*>(_data->GetData());
    *uniforms = WindowsliceTransitionParamsPOD();
}

std::vector<std::pair<UniformType /* type */, std::string /* name */>> WindowsliceTransition::GetUniformsDescription()
{
    return {
        {UniformType::FLOAT1, "progress"},
        {UniformType::FLOAT1, "count"},
        {UniformType::FLOAT1, "smoothness"}
    };
}

std::string WindowsliceTransition::GetDescription()
{
    return "WindowsliceTransition";
}

RawData::ptr WindowsliceTransition::GetUniformsData(AbstractTransitionParams::ptr params)
{
    WindowsliceTransitionParamsPOD* uniforms = reinterpret_cast<WindowsliceTransitionParamsPOD*>(_data->GetData());
    uniforms->progress = params->progress;
    {
        WindowsliceTransitionParams::ptr _params = std::dynamic_pointer_cast<WindowsliceTransitionParams>(params);
        if (_params)
        {
            uniforms->count = _params->count;
            uniforms->smoothness = _params->smoothness;
        }
    }
    return _data;
}

std::string WindowsliceTransition::GetTransitionCode()
{
    return 
R"(
vec4 transition (vec2 p) {
  float pr = smoothstep(-smoothness, 0.0, p.x - progress * (1.0 + smoothness));
  float s = step(pr, fract(count * p.x));
  return mix(getFromColor(p), getToColor(p), s);
}
)";
}

} // namespace Gpu 
} // namespace Mmp