#include "SceneLayerImpl.h"

#include <cassert>
#include <mutex>
#include <memory>
#include <string>
#include <vector>
#include <algorithm>

#include "Common/AllocateMethodFactory.h"
#include "Common/ImmutableVectorAllocateMethod.h"
#include "GPU/GL/GLContex.h"
#include "GPU/GL/GLDrawContex.h"

#include "SceneItemImpl.h"
#include "SceneFrameBufferCache.h"

namespace Mmp 
{
namespace Gpu 
{

#define MPP_SCENE_LAYER_BASIC_PROGRAM          0
#define MPP_SCENE_LAYER_IMPORT_PROGRAM         1
#define MPP_SCENE_LAYER_EXPORT_PROGRAM         2
#define MPP_SCENE_LAYER_IMPORT_EXPORT_PROGRAM  3  // Hint : 其实还不支持, 也不算支持
#define MPP_SCENE_LAYER_PROGRAM_NUM            4

class SceneLayerVertex
{
public:
    SceneLayerVertex(float x, float y, float u, float v);
public:
    float x, y;  // attribute vec2 Position
    float u, v;  // attribute vec2 iUv
};

class SceneLayerUniforms
{
public:
    SceneLayerUniforms();
public:
    float worldViewProj[16] = {0.0f};
    float transparency;
};

SceneLayerVertex::SceneLayerVertex(float x, float y, float u, float v)
{
    this->x = x;
    this->y = y;
    this->u = u;
    this->v = v;
}

SceneLayerUniforms::SceneLayerUniforms()
{
    transparency = 1.0f;
}

class SceneLayerProgram
{
public:
    using ptr = std::shared_ptr<SceneLayerProgram>;
public:
    static SceneLayerProgram::ptr Create();
public:
    SceneLayerProgram();
    ~SceneLayerProgram();
public:
    void Begin();
    void Commit();
    void End();
    void ClearFrameBuffer(FrameBuffer::ptr fbo);
    void UpdateContext(float transparency);
    void Draw(Texture::ptr canvas, FrameBuffer::ptr fbo, Texture::ptr fboTex);
private:
    static const std::string GetVertexShader();
    static const std::string GetFragmentShader(uint8_t slot);
    static std::vector<AttributeDesc> GetAttributeDescs();
    Pipeline::ptr CreatePipeline(uint8_t slot);
    void InitUniforms();
    void InitVertexData();
private:
    std::mutex                      _mtx;
    GLDrawContex::ptr               _draw;
    GLBuffer::ptr                   _vbo;
    Pipeline::ptr                   _pipeLines[MPP_SCENE_LAYER_PROGRAM_NUM];
    std::vector<SamplerState::ptr>  _samples;
    RawData::ptr                    _uniformsRawData;
    SceneLayerUniforms*              _uniforms;
    RawData::ptr                    _vertexsRawData;
};

SceneLayerProgram::ptr SceneLayerProgram::Create()
{
    static std::mutex gMtx;
    static std::weak_ptr<SceneLayerProgram> gCacheInstance;
    SceneLayerProgram::ptr instace = nullptr;
    {
        std::lock_guard<std::mutex> lock(gMtx);
        instace = gCacheInstance.lock();
        if (!instace)
        {
            instace = std::make_shared<SceneLayerProgram>();
            gCacheInstance = instace;
        }
    }
    return instace;
}

static bool IsNotBaiscProgram(uint32_t slot)
{
    return slot != 0;
}

SceneLayerProgram::SceneLayerProgram()
{
    _draw = GLDrawContex::Instance();
    InitUniforms();
    InitVertexData();
    _vbo = _draw->CreateBuffer(sizeof(SceneLayerVertex)*6, BufferUsageFlag::DYNAMIC | BufferUsageFlag::VERTEXDATA);
    for (uint8_t i=0; i<MPP_SCENE_LAYER_PROGRAM_NUM; i++)
    {
        if (IsNotBaiscProgram(i) && !_draw->IsFeatureSupport(GlDrawFeature::TEXTURE_IMPORT))
        {
            _pipeLines[i] = nullptr;
        }
        else
        {
            _pipeLines[i] = CreatePipeline(i);
        }
        if (!_pipeLines[i])
        {
            if (i != 0)
            {
                _pipeLines[i] = _pipeLines[i-1];
            }
        }
    }
    {
        SamplerStateDesc desc;
        desc.magFilter             = TextureFilter::LINEAR;
        desc.minFilter             = TextureFilter::LINEAR;
        desc.mipFilter             = TextureFilter::LINEAR;
        desc.maxAniso              = 0.0f;
        desc.wrapU                 = TextureAddressMode::CLAMP_TO_EDGE;
        desc.wrapV                 = TextureAddressMode::CLAMP_TO_EDGE;
        desc.wrapW                 = TextureAddressMode::CLAMP_TO_EDGE;
        desc.shadowCompareEnabled  = false;
        desc.shadowCompareFunc     = Comparison::ALWAYS;
        desc.borderColor           = BorderColor::DONT_CARE;
        SamplerState::ptr sample   = _draw->CreateSamplerState(desc);
        _samples.push_back(sample);
    }
}

SceneLayerProgram::~SceneLayerProgram()
{

}

void SceneLayerProgram::Begin()
{
    _mtx.lock();
    _draw->FenceBegin();
}

void SceneLayerProgram::End()
{
    _draw->FenceEnd();
    _mtx.unlock();
}

void SceneLayerProgram::Commit()
{
    _draw->FenceCommit();
}

void SceneLayerProgram::ClearFrameBuffer(FrameBuffer::ptr fbo)
{
    RenderPassInfo info;
    info.color      = RPAction::CLEAR;
    info.depth      = RPAction::DONT_CARE;
    info.stencil    = RPAction::DONT_CARE; 
    info.clearColor = 0x00000000;
    _draw->BindFramebufferAsRenderTarget(fbo, info);
}

void SceneLayerProgram::UpdateContext(float transparency)
{
    _uniforms->transparency = transparency;
}

void SceneLayerProgram::Draw(Texture::ptr canvas, FrameBuffer::ptr fbo, Texture::ptr fboTex)
{
    {
        RenderPassInfo info;
        info.color      = RPAction::KEEP;
        info.depth      = RPAction::KEEP;
        info.stencil    = RPAction::KEEP; 
        info.clearColor = 0x00000000;
        _draw->BindFramebufferAsRenderTarget(fbo, info);
    }
    _draw->SetScissorRect(0, 0, fbo->Width(), fbo->Height());
    {
        Viewport vp;
        vp.TopLeftX  = 0;
        vp.TopLeftY  = 0;
        vp.Width     = (float)fbo->Width();
        vp.Height    = (float)fbo->Height();
        vp.MaxDepth  = 1.0;
        vp.MinDepth  = 0.0;
        _draw->SetViewport(vp);
    }
    _draw->BindSamplerStates(0, _samples);
    {
        std::vector<Texture::ptr> textures;
        textures.push_back(canvas);
        _draw->BindTextures(0, textures);
    }
    {
        Pipeline::ptr pipeLine;
        if (canvas->Flags() & GlTextureFlags::TEXTURE_EXTERNAL || (canvas->Flags() & GlTextureFlags::TEXTURE_EXTERNAL) && (canvas->Flags() & GlTextureFlags::TEXTURE_YUV))
        {
            if (fboTex->Flags() & GlTextureFlags::TEXTURE_YUV)
            {
                pipeLine = _pipeLines[MPP_SCENE_LAYER_IMPORT_EXPORT_PROGRAM];
            }
            else
            {
                pipeLine = _pipeLines[MPP_SCENE_LAYER_IMPORT_PROGRAM];
            }
        }
        else
        {
            if (fboTex->Flags() & GlTextureFlags::TEXTURE_YUV)
            {
                pipeLine = _pipeLines[MPP_SCENE_LAYER_EXPORT_PROGRAM];
            }
            else
            {
                pipeLine = _pipeLines[MPP_SCENE_LAYER_BASIC_PROGRAM];
            }
        }
        _draw->BindPipeline(pipeLine);
        _draw->UpdataUniformBuffer(_uniformsRawData, 2);
        // Update Vertex Buffer Object
        {
            _draw->UpdateBuffer(_vbo, _vertexsRawData, 0, _vertexsRawData->GetSize()); 
        }
        // Bind Vertex Buffer Object
        {
            std::vector<GLBuffer::ptr> vbos;
            vbos.push_back(_vbo);
            std::vector<int> offsets;
            offsets.push_back(0);
            _draw->BindVertexBuffers(0, vbos, offsets);
        }
        _draw->Draw(6, 0);
    }
}

const std::string SceneLayerProgram::GetVertexShader()
{
    std::string source;

    switch (GLDrawContex::Instance()->GetShaderLanguage())
    {
        case ShaderLanguage::ELSL_3xx:
        case ShaderLanguage::GLSL_4xx:
        {
            source = 
R"(
attribute vec2 Position;
attribute vec2 iUV;
varying   vec2 oUV;
uniform mat4 WorldViewProj;

void main(void)
{
    oUV         =  iUV;
    gl_Position =  WorldViewProj * vec4(Position, 1.0, 1.0);
}
)";
            break;
        }
        case ShaderLanguage::HLSL_D3D11:
        {
            source = 
R"(
struct VS_INPUT 
{
    vec2 Position : POSITION;
    vec2 iUV      : TEXCOORD0;
};

struct VS_OUTPUT
{
    vec2 oUV      : TEXCOORD0;
    vec4 Position : SV_Position;
};

cbuffer ConstantBuffer : register(b0) 
{
    matrix WorldViewProj;
    float Transparency;
};

VS_OUTPUT main(VS_INPUT input)
{
    VS_OUTPUT output;
    output.Position = mul(WorldViewProj, vec4(input.Position, 1.0, 1.0));
    output.oUV      = input.iUV;
    return output;
}
)";
            break;
        }
        default:
            assert(false);
            break;
    }

    return source;
}

const std::string SceneLayerProgram::GetFragmentShader(uint8_t slot)
{
    std::string source;
    switch (GLDrawContex::Instance()->GetShaderLanguage())
    {
        case ShaderLanguage::ELSL_3xx:
        case ShaderLanguage::GLSL_4xx:
        {
            switch (slot) 
            {
                case MPP_SCENE_LAYER_BASIC_PROGRAM:
                case MPP_SCENE_LAYER_EXPORT_PROGRAM:
                {
                    source += "#define samplerLayer sampler2D\n";
                    break;
                }
                case MPP_SCENE_LAYER_IMPORT_PROGRAM:
                {
                    source += "#define samplerLayer samplerExternalOES\n";
                    break;
                }
                case MPP_SCENE_LAYER_IMPORT_EXPORT_PROGRAM:
                {
                    source += "#define samplerLayer __samplerExternal2DY2YEXT\n"; // without any color conversion
                    break;
                }
                default:
                {
                    assert(false);
                    source += "#define samplerLayer sampler2D\n";
                    break;
                }
            }
            switch (slot)
            {
                case MPP_SCENE_LAYER_EXPORT_PROGRAM:
                case MPP_SCENE_LAYER_IMPORT_EXPORT_PROGRAM:
                {
                    source += "layout(yuv) out vec4 fragColor0;\n";
                    break;
                }
                default:
                {
                    break;
                }
            }
            source += 
R"(
varying vec2 oUV;

uniform samplerLayer SceneLayer;

uniform float Transparency;

vec4 getSceneLayerColor(vec2 uv)
{
    vec4 color = texture(SceneLayer, oUV);
    color.a = color.a * Transparency;
    return color;
}

void main(void)
{
    gl_FragColor = getSceneLayerColor(oUV);
}
)";
            break;
        }
        case ShaderLanguage::HLSL_D3D11:
        {
            source = 
R"(
struct PS_INPUT 
{ 
    float2 oUV : TEXCOORD0;
    float4 Position : SV_Position;
};

SamplerState SampLayer : register(s0);
Texture2D<float4> SceneLayer : register(t0);

cbuffer ConstantBuffer : register(b0) 
{
    matrix WorldViewProj;
    float Transparency;
};

vec4 getSceneLayerColor(vec2 uv)
{
    vec4 color = SceneLayer.Sample(SampLayer, uv);
    color.a = color.a * Transparency;
    return color;
}

vec4 main(PS_INPUT input) : SV_Target 
{
    return getSceneLayerColor(input.oUV);
}

)";
            break;
        }
        default:
            assert(false);
    }
    return source;
}

std::vector<AttributeDesc> SceneLayerProgram::GetAttributeDescs()
{
    std::vector<AttributeDesc> attributes;
    switch (GLDrawContex::Instance()->GetShaderLanguage())
    {
        case ShaderLanguage::ELSL_3xx:
        case ShaderLanguage::GLSL_4xx:
        {
            attributes.push_back(AttributeDesc("Position", 0, 0, DataFormat::R32G32_FLOAT, 0 /* ~ 7 */));
            attributes.push_back(AttributeDesc("iUV", 0, 1, DataFormat::R32G32_FLOAT, 8 /* ~ 15 */));
            break;
        }
        case ShaderLanguage::HLSL_D3D11:
        {
            attributes.push_back(AttributeDesc("POSITION", 0, 0, DataFormat::R32G32_FLOAT, 0 /* ~ 7 */));
            attributes.push_back(AttributeDesc("TEXCOORD", 0, 1, DataFormat::R32G32_FLOAT, 8 /* ~ 15 */));
            break;
        }
        default:
            assert(false);
            break;
    }
    return attributes;
}

Pipeline::ptr SceneLayerProgram::CreatePipeline(uint8_t slot)
{
    ShaderModule::ptr vertexShader   = _draw->CreateShaderModule(ShaderStage::VERTEX, _draw->GetShaderLanguage(), GetVertexShader());
    ShaderModule::ptr fragmentShader = _draw->CreateShaderModule(ShaderStage::FRAGMENT, _draw->GetShaderLanguage(), GetFragmentShader(slot));
    InputLayout::ptr  inputLayout;
    BlendState::ptr   blendState;
    DepthStencilState::ptr depthStencilState;
    RasterState::ptr rasterState;
    Pipeline::ptr pipeLine;
    {
        InputLayoutDesc desc;
        {
            BindingDesc binding;
            binding.stride       = sizeof(SceneLayerVertex);
            binding.instanceRate = false;
            desc.bindings.push_back(binding);
        }
        desc.attributes = GetAttributeDescs();
        inputLayout = _draw->CreateInputLayout(desc);
    }
    {
        BlendStateDesc desc;
        desc.enabled = true;
        desc.srcCol = BlendFactor::SRC_ALPHA;
        desc.dstCol = BlendFactor::ONE_MINUS_SRC_ALPHA;
        desc.colorMask = 0x0F;
        desc.srcAlpha  = BlendFactor::ONE;
        desc.dstAlpha  = BlendFactor::ONE_MINUS_SRC_ALPHA;
        blendState = _draw->CreateBlendState(desc);
    }
    {
        DepthStencilStateDesc desc;
        desc.depthTestEnabled = false;
        desc.depthWriteEnabled = false;
        desc.depthCompare = Comparison::LESS;
        desc.stencilEnabled = false;
        depthStencilState = _draw->CreateDepthStencilState(desc);
    }
    {
        RasterStateDesc desc;
        desc.cull = CullMode::NONE;
        rasterState = _draw->CreateRasterState(desc);
    }
    {
        PipelineDesc desc;
        desc.prim = Primitive::TRIANGLE_LIST;
        desc.shaders.push_back(vertexShader);
        desc.shaders.push_back(fragmentShader);
        desc.inputLayout = inputLayout;
        desc.depthStencil = depthStencilState;
        desc.blend = blendState;
        desc.raster = rasterState;
        {
            UniformBufferDesc uniform;
            uniform.uniformBufferSize = 2;
            uniform.uniforms.push_back(UniformDesc("WorldViewProj", UniformType::MATRIX4X4, 0));
            uniform.uniforms.push_back(UniformDesc("Transparency", UniformType::FLOAT1, 64));
            desc.uniformBufferDesc = uniform;
        }
        {
            desc.samplers.push_back(SamplerDef("SceneLayer"));
        }
        pipeLine = _draw->CreateGraphicsPipeline(desc, "SceneLayer");
    }
    return pipeLine;
}

void SceneLayerProgram::InitUniforms()
{
    _uniformsRawData = std::make_shared<RawData>(sizeof(SceneLayerUniforms));
    _uniforms = reinterpret_cast<SceneLayerUniforms*>(_uniformsRawData->GetData());
    {
        _draw->GetDefaultVorldViewProj(1, 1, _uniforms->worldViewProj, false);
        _uniforms->transparency = 1.0f;
    }
}

void SceneLayerProgram::InitVertexData()
{
    std::vector<SceneLayerVertex> verts;
    {
        // one rectangle
        verts.push_back(SceneLayerVertex({0.0, 0.0, 0.0, 0.0}));
        verts.push_back(SceneLayerVertex({1.0, 0.0, 1.0, 0.0}));
        verts.push_back(SceneLayerVertex({1.0, 1.0, 1.0, 1.0}));
        // another rectangle
        verts.push_back(SceneLayerVertex({0.0, 0.0, 0.0, 0.0}));
        verts.push_back(SceneLayerVertex({1.0, 1.0, 1.0, 1.0}));
        verts.push_back(SceneLayerVertex({0.0, 1.0, 0.0, 1.0}));
    }
    std::shared_ptr<ImmutableVectorAllocateMethod<SceneLayerVertex>> allocate = std::make_shared<ImmutableVectorAllocateMethod<SceneLayerVertex>>();
    allocate->container.swap(verts);
    _vertexsRawData = std::make_shared<RawData>(allocate->container.size() * sizeof(SceneLayerVertex), allocate);
}

} // namespace Gpu 
} // namespace Mmp

namespace Mmp 
{
namespace Gpu 
{

SceneLayerImpl::SceneLayerImpl()
{
    _lastcanvas = nullptr;
    _program = SceneLayerProgram::Create();
}

bool SceneLayerImpl::SetParam(const SceneLayerParam& param)
{
    std::lock_guard<std::mutex> lock(_mtx);
    _param = param;
    return true;
}

SceneLayerParam SceneLayerImpl::GetParam()
{
    std::lock_guard<std::mutex> lock(_mtx);
    return _param;
}

bool SceneLayerImpl::AddSceneItem(const std::string& tag, AbstractSceneItem::ptr item)
{
    std::lock_guard<std::mutex> itemLock(_itemMtx);
    _items[tag] = item;
    return true;
}

void SceneLayerImpl::DelSceneItem(const std::string& tag)
{
    std::lock_guard<std::mutex> lock(_itemMtx);
    _items.erase(tag);
}

AbstractSceneItem::ptr SceneLayerImpl::GetSceneItem(const std::string& tag)
{
    std::lock_guard<std::mutex> lock(_itemMtx);
    if (_items.count(tag))
    {
        return _items[tag];
    }
    else
    {
        return nullptr;
    }
}

std::vector<std::string /* tag */> SceneLayerImpl::GetSceneItemsTag()
{
    std::vector<std::string> tags;
    {
        std::lock_guard<std::mutex> lock(_itemMtx);
        for (auto& val : _items)
        {
            tags.push_back(val.first /* tag */);
        }
    }
    return tags;
}

void SceneLayerImpl::UpdateCanvas(Texture::ptr canvas)
{
    std::lock_guard<std::mutex> lock(_mtx);
    _canvas = canvas;
}

Texture::ptr SceneLayerImpl::QueryCanvans()
{
    std::lock_guard<std::mutex> lock(_mtx);
    return _canvas;
}

void SceneLayerImpl::Draw(Texture::ptr framebuffer)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (!framebuffer || !_canvas || !_param.show)
    {
        return;
    }
    _program->Begin();
    // clear layer
    {
        if (_canvas != _lastcanvas)
        {
            _lastcanvas = _canvas;
            _canvasFbo =  SceneFrameBufferCache::Singleton()->CreateFrameBufferByTexture(_canvas);
        }
        if (_param.strategy == SceneRenderStrategy::Clear)
        {
            _program->ClearFrameBuffer(_canvasFbo);
        }
    }
    // draw items to layer
    {
        std::lock_guard<std::mutex> itemLock(_itemMtx);
        std::vector<AbstractSceneItem::ptr> items;
        for (const auto& val : _items)
        {
            AbstractSceneItem::ptr item = val.second;
            items.push_back(item);
        }
        // Hint : 从下层到上层, 从左上到右下
        std::sort(items.begin(), items.end(), [](const AbstractSceneItem::ptr& left, const AbstractSceneItem::ptr& right) -> bool
        {
            SceneItemParam leftParam = left->GetParam();
            SceneItemParam rightParam = right->GetParam();
            if ((uint32_t)leftParam.level < (uint32_t)rightParam.level)
            {
                return true;
            }
            else if (leftParam.location.x < rightParam.location.x && leftParam.location.y < rightParam.location.y)
            {
                return true;
            }
            else
            {
                return false;
            }
        });
        for (const auto& item : items)
        {
            item->Draw(_canvas);
        }
    }
    _program->Commit();
    // draw layer to framebuffer
    if (framebuffer.get() != _canvas.get())
    {
        FrameBuffer::ptr fbo = SceneFrameBufferCache::Singleton()->CreateFrameBufferByTexture(framebuffer);
        _program->UpdateContext(_param.transparency);
        _program->Draw(_canvas, fbo, framebuffer);
    }
    _program->Commit();
    _program->End();
}

} // namespace Gpu 
} // namespace Mmp