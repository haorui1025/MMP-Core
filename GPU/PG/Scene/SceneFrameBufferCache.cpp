#include "SceneFrameBufferCache.h"

namespace Mmp 
{
namespace Gpu 
{

FrameBuffer::ptr SceneFrameBufferCache::CreateFrameBufferByTexture(Texture::ptr texture)
{
    std::lock_guard<std::mutex> lock(_mtx);
    FrameBuffer::ptr fbo;
    if (_caches.count(texture.get()))
    {
        fbo = _caches[texture.get()].lock();
    }
    if (!fbo)
    {
        FrameBufferDesc desc;
        desc.width  = texture->Width();
        desc.height = texture->Height();
        desc.depth  = 0;
        desc.multiSampleLevel = 0;
        desc.numLayers = 1;
        desc.zStencil = 0;
        desc.colorTexs.push_back(texture);
        fbo = GLDrawContex::Instance()->CreateFrameBuffer(desc);
        _caches[texture.get()] = fbo;
    }
    return fbo;
}

SceneFrameBufferCache* SceneFrameBufferCache::Singleton()
{
    static SceneFrameBufferCache instace;
    return &instace;
}

} // namespace Gpu 
} // namespace Mmp