//
// SceneFrameBufferCache.h
//
// Library: GPU
// Package: Program
// Module:  Scene Cache
// 

#pragma once

#include <map>
#include <mutex>

#include "GPU/GL/GLDrawContex.h"

namespace Mmp 
{
namespace Gpu 
{

class SceneFrameBufferCache final
{
public:
    SceneFrameBufferCache() = default;
    ~SceneFrameBufferCache() = default;
public:
    static SceneFrameBufferCache* Singleton();
public:
    FrameBuffer::ptr CreateFrameBufferByTexture(Texture::ptr texture);
private:
    std::mutex _mtx;
    std::map<void* /* tag */, std::weak_ptr<FrameBuffer>> _caches;
};

} // namespace Gpu 
} // namespace Mmp