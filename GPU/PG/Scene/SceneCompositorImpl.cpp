#include "SceneCompositorImpl.h"

#include <cstdint>
#include <mutex>
#include <vector>
#include <algorithm>

#include "Common/PixelFormat.h"
#include "Common/PixelFormat.h"
#include "Utility/CommonUtility.h"

#include "SceneFrameBufferCache.h"

namespace Mmp
{
namespace Gpu 
{

SceneCompositorImpl::SceneCompositorImpl()
{
    _draw = GLDrawContex::Instance();
    _isInit = false;
    _curFbIndex = 0;
}

SceneCompositorImpl::~SceneCompositorImpl()
{

}

bool SceneCompositorImpl::SetParam(const SceneCompositorParam& param)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (_isInit)
    {
        return false;
    }
    else
    {
        _param = param;
        return true;
    }
}

SceneCompositorParam SceneCompositorImpl::GetParam()
{
    std::lock_guard<std::mutex> paramLock(_mtx);
    return _param;
}

bool SceneCompositorImpl::AddSceneLayer(const std::string& tag, AbstractSceneLayer::ptr layer)
{
    std::lock_guard<std::mutex> lock(_layerMtx);
    bool foundLevelExit = false;
    {
        SceneLayerParam layerParam = layer->GetParam();
        for (const auto& val : _layers)
        {
            AbstractSceneLayer::ptr __layer = val.second;
            if (__layer->GetParam().level == layerParam.level)
            {
                foundLevelExit = true;
                break;
            }
        }
    }
    if (foundLevelExit)
    {
        return false;
    }
    else
    {
        SceneCompositorParam param = _param;
        _layers[tag] = layer;
        if ((_param.flags & GlTextureFlags::TEXTURE_EXTERNAL) && (_param.flags & GlTextureFlags::TEXTURE_YUV))
        {
            PixelsInfo info = PixelsInfo(param.width, param.height, 8, PixelFormat::NV12);
            info.colorGamut = ColorGamut::ColorGamut_BT709;
            info.dynamicRange = DynamicRange::DynamicRange_SDR;
            info.quantRange = QuantRange::FULL;
            layer->UpdateCanvas(Gpu::Create2DTextures(_draw, info, "Framebuffer", _param.flags | GlTextureFlags::TEXTURE_USE_FOR_RENDER)[0]);
        }
        else
        {
            layer->UpdateCanvas(Gpu::Create2DTextures(_draw, PixelsInfo(param.width, param.height, 8, PixelFormat::RGBA8888), "Framebuffer", _param.flags | GlTextureFlags::TEXTURE_USE_FOR_RENDER)[0]);
        }
        return true;
    }
}

void SceneCompositorImpl::DelSceneLayer(const std::string& tag)
{
    std::lock_guard<std::mutex> lock(_layerMtx);
    if (_layers.count(tag))
    {
        _layers[tag]->UpdateCanvas(nullptr);
    }
    _layers.erase(tag);
}

AbstractSceneLayer::ptr SceneCompositorImpl::GetSceneLayer(const std::string& tag)
{
    std::lock_guard<std::mutex> lock(_layerMtx);
    if (_layers.count(tag))
    {
        return _layers[tag];
    }
    else 
    {
        return nullptr;
    }
}

std::vector<std::string /* tag */> SceneCompositorImpl::GetSceneLayersTag()
{
    std::lock_guard<std::mutex> lock(_layerMtx);
    std::vector<std::string /* tag */> tags;
    for (const auto& val : _layers)
    {
        tags.push_back(val.first /* tag : string */);
    }
    return tags;
}

void SceneCompositorImpl::Draw()
{
    // 创建 FrameBuffer 缓冲区
    if (!_isInit)
    {
        std::lock_guard<std::mutex> lock(_mtx);
        SceneCompositorParam param = _param;
        for (uint32_t i=0; i<_param.bufSize; i++)
        {
            if ((_param.flags & GlTextureFlags::TEXTURE_EXTERNAL) && (_param.flags & GlTextureFlags::TEXTURE_YUV))
            {
                PixelsInfo info = PixelsInfo(param.width, param.height, 8, PixelFormat::NV12);
                info.colorGamut = ColorGamut::ColorGamut_BT709;
                info.dynamicRange = DynamicRange::DynamicRange_SDR;
                info.quantRange = QuantRange::FULL;
                _frameBuffers.push_back(Gpu::Create2DTextures(_draw, info, "Framebuffer", _param.flags | GlTextureFlags::TEXTURE_USE_FOR_RENDER)[0]);
            }
            else
            {
                _frameBuffers.push_back(Gpu::Create2DTextures(_draw, PixelsInfo(param.width, param.height, 8, PixelFormat::RGBA8888), "Framebuffer", _param.flags | GlTextureFlags::TEXTURE_USE_FOR_RENDER)[0]);
            }
        }
        for (auto& frameBuffer : _frameBuffers)
        {
            _fbos.push_back(SceneFrameBufferCache::Singleton()->CreateFrameBufferByTexture(frameBuffer));
            // Hint : Keep 模式创建时清空 FrameBuffer, 此后不再清空
            if (_param.strategy == SceneRenderStrategy::Keep)
            {
                RenderPassInfo info;
                info.color      = RPAction::CLEAR;
                info.depth      = RPAction::DONT_CARE;
                info.stencil    = RPAction::DONT_CARE; 
                info.clearColor = 0x00000000;
                _draw->BindFramebufferAsRenderTarget(_fbos.back(), info);
            }
        }
        _isInit = true;
    }
    Texture::ptr frameBuffer = nullptr;
    size_t curFbIndex;
    // 获取 FrameBuffer 缓冲区
    {
        std::lock_guard<std::mutex> fbLock(_fbMtx);
        curFbIndex = (_curFbIndex + 1) % _frameBuffers.size();
        frameBuffer = _frameBuffers[curFbIndex];
    }

    std::lock_guard<std::mutex> lock(_mtx);
    std::vector<AbstractSceneLayer::ptr> laysers;
    {
        std::lock_guard<std::mutex> layerLock(_layerMtx);
        for (const auto& val : _layers)
        {
            laysers.push_back(val.second /* layer : AbstractSceneLayer::ptr */);
        }
    }
    std::sort(laysers.begin(), laysers.end(), [](AbstractSceneLayer::ptr left, AbstractSceneLayer::ptr right) -> bool
    {
        return left->GetParam().level < right->GetParam().level;
    });
    _draw->FenceBegin();
    SceneCompositorMode mode = _param.mode;
    // Clear FrameBuffer if needed
    {
        FrameBuffer::ptr fbo = SceneFrameBufferCache::Singleton()->CreateFrameBufferByTexture(frameBuffer);
        {
            RenderPassInfo info;
            info.color      = _param.strategy == SceneRenderStrategy::Clear ? RPAction::CLEAR : RPAction::KEEP;
            info.depth      = RPAction::DONT_CARE;
            info.stencil    = RPAction::DONT_CARE; 
            info.clearColor = 0x00000000;
            _draw->BindFramebufferAsRenderTarget(fbo, info);
        }
        _draw->FenceCommit();
    }
    // * 强制合并
    {
        // compositor 只包含一个 layer 时, 减少 renderpass
        if (laysers.size() == 1)
        {
            mode = SceneCompositorMode::Merge;
        }
        // YUV 模式下必须使用 Merge, YUV Texture 无法做 Blend 操作, 会导致效果异常
        if (frameBuffer->Flags() & GlTextureFlags::TEXTURE_YUV)
        {
            mode = SceneCompositorMode::Merge;
        }
    }
    if (mode == SceneCompositorMode::Merge)
    {
        for (auto layer : laysers)
        {
            Texture::ptr canvans = layer->QueryCanvans();
            SceneLayerParam layerParam = layer->GetParam();
            SceneLayerParam tmpLayerParam = layerParam;
            tmpLayerParam.strategy = SceneRenderStrategy::Keep;
            layer->UpdateCanvas(frameBuffer);
            layer->SetParam(tmpLayerParam);

            layer->Draw(frameBuffer);
            
            layer->UpdateCanvas(canvans);
            layer->SetParam(layerParam);
        }
    }
    else
    {
        for (auto layer : laysers)
        {
            layer->Draw(frameBuffer);
        }
    }
    _draw->FenceEnd()->Sync();
    // Update index
    {
        std::lock_guard<std::mutex> fbLock(_fbMtx);
        _curFbIndex = curFbIndex;
    }
}

Texture::ptr SceneCompositorImpl::GetFrameBuffer()
{
    std::lock_guard<std::mutex> fbLock(_fbMtx);
    return _frameBuffers[_curFbIndex];
}

} // namespace Gpu 
} // namespace Mmp