//
// DXGIWindow.h
//
// Library: GPU
// Package: Windows
// Module:  DXGI
// 

#pragma once

#include <vector>

#include "Common/D3D11Device.h"
#include "AbstractWindows.h"

namespace Mmp
{

/**
 * @note 基于 DXGI 实现 (runtime loading)
 */
class DXGIWindow : public AbstractWindows
{
public:
    DXGIWindow();
    ~DXGIWindow();
public:
    bool Open() override;
    bool Close() override;
    bool BindRenderThread(bool bind) override;
    bool SetAPIType(APIType type) override;
    bool SetRenderMode(bool onScreen) override;
    WindowInfo GetInfo() override;
    void  Swap() override;
public:
    /**
     * @note
     *       key                    :    value
     *       ID3D11DeviceContext    :    ID3D11DeviceContext*
     *       D3DCompile             :    _D3DCompile
     *       Width                  :    _width
     *       Height                 :    _height
     *       BackBufferTexture      :    _backBufferTexture
     *       BackRenderTargetView   :    _backRenderTargetView 
     *       BackDepthTexture       :    _backDepthTexture
     *       BackDepthStencilView   :    _backDepthStencilView
     *       HWND                   :    _hWnd
     *       HINSTANCE              :    _hInstance
     */
    Any Get(const std::string& key) override;
private:
    bool LoadD3D11();
    void UnLoadD3D11();
    bool CreateSwapChain();
    void DestroySwapChain();
    void UpdateScreenInfo();
    void CreateWinWindow();
    void GetBackBuffer();
private:
    bool _isOpened;
    bool _isOnScreen;
private:
    bool                                _isLoaded;
    pD3DCompile                         _D3DCompile;
private:
    HINSTANCE                           _hInstance;
    HWND                                _hWnd;       // TODO : 支持外部导入
private:
    int                                 _displayScreenWidth;
    int                                 _displayScreenHeight;
    int                                 _virtualScreenWidth;
    int                                 _virtualScreenHeight;
private:
    APIType                             _type;
    int                                 _width;
    int                                 _height;
private:
    IDXGISwapChain*                     _swapChain;
    ID3D11Texture2D*                    _backBufferTexture;
    ID3D11RenderTargetView*             _backRenderTargetView;
    ID3D11Texture2D*                    _backDepthTexture;
    ID3D11DepthStencilView*             _backDepthStencilView;
};

} // namespace Mmp