//
// VulkanWindowsCommon.h
//
// Library: GPU
// Package: Windows
// Module:  Vulkan
// 

#pragma once

#include <string>

#include "Common/VulkanDevice.h"

namespace Mmp
{

std::string VkPresentModeKHRToStr(VkPresentModeKHR mode);

} // namespace Mmp