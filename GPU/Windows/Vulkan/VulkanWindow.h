//
// VulkanWindow.h
//
// Library: GPU
// Package: Windows
// Module:  Vulkan
// 

#pragma once

#include <vector>
#include <string>
#include <memory>

#include "AbstractWindows.h"

#include "VulkanWindowsCommon.h"

namespace Mmp
{

class VulkanWindow : public AbstractWindows
{
public:
    VulkanWindow();
    ~VulkanWindow();
public:
    bool Open() override;
    bool Close() override;
    bool BindRenderThread(bool bind) override;
    bool SetAPIType(APIType type) override;
    bool SetRenderMode(bool onScreen) override;
    WindowInfo GetInfo() override;
    void  Swap() override;
private:
    void ChooseSwapChainFormat();
private:
    bool CreateSurface();
    void DestroySurface();
private:
    bool CreateSwapchain();
    void DestroySwapchain();
private:
    APIType                                 _apiType;
    uint32_t                                _apiVersion;
private:
    AbstractWindows::ptr                    _windowProxy;
    VkSurfaceKHR                            _surface;
    VkSurfaceCapabilitiesKHR                _surfaceCapabilities;
    std::vector<VkSurfaceFormatKHR>         _surfaceFormats;
private:
    VkSwapchainKHR                          _swapChain;
    VkFormat                                _swapChainFormat;
    VkPresentModeKHR                        _prsentMode;
};

} // namespace Mmp