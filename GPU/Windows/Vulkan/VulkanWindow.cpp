#include "VulkanWindow.h"
#include "SDL/SDLWindow.h"
#include "Any.h"

#if defined (USE_X11)
#include <X11/Xlib.h>
#include "vulkan/vulkan_xlib.h"
#include "X11/X11Window.h"
#endif /* USE_X11 */

#if defined (USE_SDL)
#include "SDL2/SDL_syswm.h"
#include "SDL/SDLWindow.h"
#endif

#if MMP_PLATFORM(WINDOWS)
#include "vulkan/vulkan_win32.h"
#include "DXGI/DXGIWindow.h"
#endif


#include "Common/Common.h"

#include "WindowUtils.h"

namespace Mmp
{

VulkanWindow::VulkanWindow()
{
#if MMP_PLATFORM(WINDOWS)
    _windowProxy = std::make_shared<DXGIWindow>();
    _windowProxy->SetAPIType(APIType::VULKAN);
#endif
#if defined (USE_SDL) && defined(USE_X11)
    _windowProxy = std::make_shared<SDLWindow>();
    _windowProxy->SetAPIType(APIType::VULKAN);
#endif
    _apiType = APIType::VULKAN;
    _apiVersion = VK_API_VERSION_1_0;
    _surface = VK_NULL_HANDLE;
    _swapChain = VK_NULL_HANDLE;
    _surfaceCapabilities = {};
    _prsentMode = VK_PRESENT_MODE_MAX_ENUM_KHR;
}

VulkanWindow::~VulkanWindow()
{

}

bool VulkanWindow::Open()
{
    WIN_LOG_INFO << "Open Window";
    if (!CreateSurface())
    {
        WIN_LOG_ERROR << "-- CreateSurface fail";
        assert(false);
        goto END;
    }
    else
    {
        WIN_LOG_INFO << "-- CreateSurface successfully";
    }
    ChooseSwapChainFormat();
    if (!CreateSwapchain())
    {
        WIN_LOG_ERROR << "-- CreateSwapchain fail";
        assert(false);
        goto END1;
    }
    else
    {
        WIN_LOG_INFO << "-- CreateSwapchain successfully";
    }
    return true;
/* END2: */
    DestroySwapchain();
END1:
    DestroySurface();
END:
    return false;
}

bool VulkanWindow::Close()
{
    WIN_LOG_INFO << "Close Window";
    DestroySwapchain();
    WIN_LOG_INFO << "-- DestroySwapchain";
    DestroySurface();
    WIN_LOG_INFO << "-- DestroySurface";
    return true;
}

bool VulkanWindow::BindRenderThread(bool bind)
{
    return false;
}

bool VulkanWindow::SetAPIType(APIType type)
{
    assert(type == APIType::VULKAN);
    return true;
}


bool VulkanWindow::SetRenderMode(bool onScreen)
{
    if (_windowProxy)
    {
        return _windowProxy->SetRenderMode(onScreen);
    }
    else
    {
        return false;
    }
} 

WindowInfo VulkanWindow::GetInfo()
{
    WindowInfo info;
    info.apiType = _apiType;
    info.vMajor = VK_API_VERSION_MAJOR(_apiVersion);
    info.vMinor = VK_API_VERSION_MINOR(_apiVersion);
    return info;
}

void VulkanWindow::Swap() 
{
    // TODO
    assert(false);
}

void VulkanWindow::ChooseSwapChainFormat()
{
    VkResult vkRes = VK_SUCCESS;
    uint32_t formatCount = 0;
    vkRes = VulkanDevice::Singleton()->vkGetPhysicalDeviceSurfaceFormatsKHR(_surface, &formatCount, nullptr);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_WARN << "vkGetPhysicalDeviceSurfaceFormatsKHR, error is: "<< VkResultToStr(vkRes);
        goto END;
    }
    _surfaceFormats.resize(formatCount);
    vkRes = VulkanDevice::Singleton()->vkGetPhysicalDeviceSurfaceFormatsKHR(_surface, &formatCount, _surfaceFormats.data());
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_WARN << "vkGetPhysicalDeviceSurfaceFormatsKHR, error is: "<< VkResultToStr(vkRes);
        goto END;
    }
    if (formatCount == 0 || (formatCount == 1 && _surfaceFormats[0].format == VK_FORMAT_UNDEFINED))
    {
        _swapChainFormat = VK_FORMAT_B8G8R8A8_UNORM;
    }
    else
    {
        _swapChainFormat = VK_FORMAT_UNDEFINED;
        for (auto& surfaceFormat : _surfaceFormats)
        {
            if (surfaceFormat.colorSpace != VK_COLORSPACE_SRGB_NONLINEAR_KHR)
            {
                continue;
            }
            if (surfaceFormat.format == VK_FORMAT_B8G8R8A8_UNORM || surfaceFormat.format == VK_FORMAT_R8G8B8A8_UNORM)
            {
                _swapChainFormat = surfaceFormat.format;
                break;
            }
        }
    }
END:
    _swapChainFormat = VK_FORMAT_B8G8R8A8_UNORM;
}

bool VulkanWindow::CreateSurface()
{
#if MMP_PLATFORM(WINDOWS)
    VkWin32SurfaceCreateInfoKHR win32 = {};
    PFN_vkCreateWin32SurfaceKHR _vkCreateWin32SurfaceKHR = nullptr;
    _vkCreateWin32SurfaceKHR = (PFN_vkCreateWin32SurfaceKHR)VulkanDevice::Singleton()->GetInstanceFunc("vkCreateWin32SurfaceKHR");
    if (!_vkCreateWin32SurfaceKHR)
    {
        WIN_LOG_ERROR << "Get vkCreateWin32SurfaceKHR fail";
        assert(false);
        goto END;
    }
    _windowProxy->SetRenderMode(true);
    if (!_windowProxy->Open())
    {
        WIN_LOG_ERROR << "Open window fail";
        goto END;
    }
    {
        win32.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
        win32.flags = 0;
        win32.hwnd = RefAnyCast<HWND>(_windowProxy->Get("HWND"));
        win32.hinstance = RefAnyCast<HINSTANCE>(_windowProxy->Get("HINSTANCE"));
    }
    if (VK_SUCCESS != _vkCreateWin32SurfaceKHR(VulkanDevice::Singleton()->GetInstance(), &win32, nullptr, &_surface))
    {
        WIN_LOG_ERROR << "vkCreateWin32SurfaceKHR fail";
        goto END;
    }
#endif 
#if defined (USE_SDL) && defined (USE_X11)
    PFN_vkCreateXlibSurfaceKHR _vkCreateXlibSurfaceKHR = nullptr;
    SDL_SysWMinfo sysInfo = {};
    SDL_VERSION(&sysInfo.version); //Set SDL version
    VkXlibSurfaceCreateInfoKHR x11 = {};
    _vkCreateXlibSurfaceKHR = (PFN_vkCreateXlibSurfaceKHR)VulkanDevice::Singleton()->GetInstanceFunc("vkCreateXlibSurfaceKHR");
    if (!_vkCreateXlibSurfaceKHR)
    {
        WIN_LOG_ERROR << "Get vkCreateXlibSurfaceKHR fail";
        assert(false);
        goto END;
    }
    if (!_windowProxy)
    {
        assert(false);
        goto END;
    }
    if (!_windowProxy->Open())
    {
        WIN_LOG_ERROR << "Open window fail";
        goto END;
    }
    if (!SDL_GetWindowWMInfo(RefAnyCast<SDL_Window*>(_windowProxy->Get("SDL_Window")), &sysInfo))
    {
        WIN_LOG_ERROR << "SDL_GetWindowWMInfo fail";
        assert(false);
        goto END;
    }
    {
        x11.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;
        x11.flags = 0;
        x11.dpy = sysInfo.info.x11.display;
        x11.window = sysInfo.info.x11.window;
    }
    if (VK_SUCCESS != _vkCreateXlibSurfaceKHR(VulkanDevice::Singleton()->GetInstance(), &x11, nullptr, &_surface))
    {
        WIN_LOG_ERROR << "vkCreateXlibSurfaceKHR fail";
        goto END;
    }
#endif

    assert(_surface);
    // TODO : maybe other something need do
    return true;
END:
    _surface = VK_NULL_HANDLE;
    return false;
}

void VulkanWindow::DestroySurface()
{
    if (_surface != VK_NULL_HANDLE)
    {
        VulkanDevice::Singleton()->vkDestroySurfaceKHR(_surface, nullptr);
        _surface = VK_NULL_HANDLE;
        if (_windowProxy)
        {
            _windowProxy->Close();
        }
    }
}

bool VulkanWindow::CreateSwapchain()
{
    VkResult res = VK_SUCCESS;
    uint32_t presentModeCount = 0;
    std::vector<VkPresentModeKHR> presentModes;
    VkSwapchainCreateInfoKHR swapChainInfo = {};

    res = VulkanDevice::Singleton()->vkGetPhysicalDeviceSurfaceCapabilitiesKHR(_surface, &_surfaceCapabilities);
    if (res != VK_SUCCESS)
    {
        WIN_LOG_ERROR << "vkGetPhysicalDeviceSurfaceCapabilitiesKHR fail";
        goto END;
    }

    {
        res = VulkanDevice::Singleton()->vkGetPhysicalDeviceSurfacePresentModesKHR(_surface, &presentModeCount, nullptr);
        if (res != VK_SUCCESS || presentModeCount == 0)
        {
            WIN_LOG_ERROR << "vkGetPhysicalDeviceSurfacePresentModesKHR fail";
            goto END;
        }
        presentModes.resize(presentModeCount);
        res = VulkanDevice::Singleton()->vkGetPhysicalDeviceSurfacePresentModesKHR(_surface, &presentModeCount, presentModes.data());
        if (res != VK_SUCCESS)
        {
            WIN_LOG_ERROR << "vkGetPhysicalDeviceSurfacePresentModesKHR fail";
            goto END;
        }
        std::string log = "-- Support present modes : ";
        for (const auto& presentMode : presentModes)
        {
            log += VkPresentModeKHRToStr(presentMode) + ", ";
        }
        // Hint : for last ", " two space
        log.pop_back();
        log.pop_back();
        WIN_LOG_INFO << log;
    }
    _prsentMode = presentModes[0];
    {
        swapChainInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        swapChainInfo.surface = _surface;
        swapChainInfo.minImageCount = _surfaceCapabilities.maxImageCount;
        swapChainInfo.imageFormat = _swapChainFormat;
        swapChainInfo.imageColorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
#if MMP_PLATFORM(WINDOWS)
        swapChainInfo.imageExtent.width = DXGI_SCREEN_WIDTH;
        swapChainInfo.imageExtent.height = DXGI_SCREEN_HEIGHT;
#endif
        swapChainInfo.preTransform = VkSurfaceTransformFlagBitsKHR::VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
        swapChainInfo.imageArrayLayers = 1;
        swapChainInfo.presentMode = _prsentMode;
        swapChainInfo.oldSwapchain = VK_NULL_HANDLE;
        swapChainInfo.clipped = true;
        swapChainInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
        swapChainInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        swapChainInfo.queueFamilyIndexCount = 0;
        swapChainInfo.pQueueFamilyIndices = NULL;
        if (_surfaceCapabilities.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR) 
        {
            swapChainInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
        }
        else 
        {
            swapChainInfo.compositeAlpha = VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR;
        }
    }
    res = VulkanDevice::Singleton()->vkCreateSwapchainKHR( &swapChainInfo, nullptr, &_swapChain);
    if (res != VK_SUCCESS)
    {
        WIN_LOG_ERROR << "vkCreateSwapchainKHR fail";
        assert(false);
        goto END;
    }
    return true;
END:
    _swapChain = VK_NULL_HANDLE;
    _prsentMode = VK_PRESENT_MODE_MAX_ENUM_KHR;
    return false;
}

void VulkanWindow::DestroySwapchain()
{
    if (_swapChain != VK_NULL_HANDLE)
    {
        VulkanDevice::Singleton()->vkDestroySwapchainKHR( _swapChain, nullptr);
        _swapChain = VK_NULL_HANDLE;
    }
}

} // namespace Mmp