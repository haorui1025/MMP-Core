#include "GLContex.h"

#include "Common/PixelsInfo.h"

#include <cassert>

namespace Mmp
{

int FrameBuffer::Width()
{
    return _width;
}

int FrameBuffer::Height()
{
    return _height;
}

int FrameBuffer::Layers()
{
    return _layers;
}

int FrameBuffer::MultiSampleLevel()
{
    return _layers;
}

int Texture::Width()
{
    return _width;
}

int Texture::Height()
{
    return _height;
}

int Texture::HorStride()
{
    if (_horStride > 0)
    {
        return _horStride;
    }
    else
    {
        return _width;
    }
}

int Texture::VirStride()
{
    if (_virStride > 0)
    {
        return _virStride;
    }
    else
    {
        return _height;
    }
}

int Texture::Depth()
{
    return _depth;
}

uint64_t Texture::Flags()
{
    return _flags;
}

PixelsInfo Texture::GetPixelsInfo()
{
    return PixelsInfo();
}

AbstractAllocateMethod::ptr Texture::GetAllocateMethod()
{
    return nullptr;
}

void GLFence::Sync()
{

}

} // namespace Mmp