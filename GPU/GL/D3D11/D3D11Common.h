//
// D3D11Common.h
//
// Library: GPU
// Package: GL
// Module:  D3D11
//

#pragma once

#include <cstdint>
#include <string>
#include <iostream>
#include <vector>

#include "Common/Common.h"
#include "Common/D3D11Device.h"

#include "Common/Any.h"
#include "GLCommon.h"
#include "GLContexDesc.h"
#include "GLContex.h"

namespace Mmp
{

#define D3D11_MAX_BOUND_TEXTURES 8
#define D3D11_MAX_VERTEX_BUFFER_SIZE 4

DXGI_FORMAT DataFormatToD3D11(DataFormat format);
size_t DxgiFormatSize(DXGI_FORMAT format);

} // namespace Mmp