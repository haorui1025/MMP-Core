#include "VulkanDrawContex.h"

#include <memory.h>
#include <memory>

#include "SPIRV/GlslangToSpv.h"
#include "glslang/Public/ShaderLang.h"

#include "VulkanImpl.h"
#include "VulkanRenderData.h"
#include "VulkanGLTexture.h"

#define XXX_VULKAN_RENDER_CHECK_VOID()  if (!_curRenderStep || _curRenderStep->stepType != VulkanRenderStepType::RENDER)\
                                        {\
                                          assert(false);\
                                          return;\
                                        }                      

#define XXX_VULKAN_MAX_TEXTURE_SLOTS   4

namespace Mmp
{

VulkanDrawContex::VulkanDrawContex()
{
    glslang::InitializeProcess();
    _boundSamplers.resize(XXX_VULKAN_MAX_TEXTURE_SLOTS);
    _boundImageView.resize(XXX_VULKAN_MAX_TEXTURE_SLOTS);

    _vulkan = std::make_shared<Vulkan>();
}

VulkanDrawContex::~VulkanDrawContex()
{
    glslang::FinalizeProcess();
}

void VulkanDrawContex::ThreadStart()
{
    _vulkan->ThreadStart();
}

GpuTaskStatus VulkanDrawContex::ThreadFrame()
{
    return _vulkan->ThreadFrame();
}

void VulkanDrawContex::ThreadEnd()
{
    _vulkan->ThreadEnd();
}

void VulkanDrawContex::ThreadStop()
{
    _vulkan->ThreadStop();
}

void VulkanDrawContex::FenceBegin()
{
    _vulkan->FenceBegin();
}

void VulkanDrawContex::FenceCommit()
{
    _vulkan->FenceCommit();
}

GLFence::ptr VulkanDrawContex::FenceEnd()
{
    return _vulkan->FenceEnd();
}

DepthStencilState::ptr VulkanDrawContex::CreateDepthStencilState(const DepthStencilStateDesc& desc)
{
    VulkanDepthStencilState::ptr ds = std::make_shared<VulkanDepthStencilState>();
    ds->info.depthCompareOp = GLComparisonTypeToVulkanType(desc.depthCompare);
    ds->info.depthTestEnable = desc.depthTestEnabled;
    ds->info.depthWriteEnable = desc.depthWriteEnabled;
    ds->info.stencilTestEnable = desc.stencilEnabled;
    ds->info.depthBoundsTestEnable = false;
    if (ds->info.stencilTestEnable)
    {
        ds->info.front.compareOp = GLComparisonTypeToVulkanType(desc.stencil.compareOp);
        ds->info.front.failOp = GLStencilOpToVulkanType(desc.stencil.failOp);
        ds->info.front.passOp = GLStencilOpToVulkanType(desc.stencil.passOp);
        ds->info.front.depthFailOp = GLStencilOpToVulkanType(desc.stencil.depthFailOp);
        ds->info.back.compareOp = GLComparisonTypeToVulkanType(desc.stencil.compareOp);
        ds->info.back.failOp = GLStencilOpToVulkanType(desc.stencil.failOp);
        ds->info.back.passOp = GLStencilOpToVulkanType(desc.stencil.passOp);
        ds->info.back.depthFailOp = GLStencilOpToVulkanType(desc.stencil.depthFailOp);
    }
    return ds;
}

BlendState::ptr VulkanDrawContex::CreateBlendState(const BlendStateDesc& desc)
{
    VulkanBlendState::ptr bs = std::make_shared<VulkanBlendState>();
    bs->info.attachmentCount = 1;
    bs->info.logicOp = GLLogicOpToOpenVulkanType(desc.logicOp);
    bs->info.logicOpEnable = desc.enabled;
    bs->attachments.resize(1);
    bs->attachments[0].blendEnable = desc.enabled;
    bs->attachments[0].colorBlendOp = GLBlendOpToVulkanType(desc.eqCol);
    bs->attachments[0].alphaBlendOp = GLBlendOpToVulkanType(desc.eqAlpha);
    bs->attachments[0].colorWriteMask = desc.colorMask;
    bs->attachments[0].dstAlphaBlendFactor = GLBlendFactorToVulkanType(desc.dstAlpha);
    bs->attachments[0].dstColorBlendFactor = GLBlendFactorToVulkanType(desc.dstCol);
    bs->attachments[0].srcAlphaBlendFactor = GLBlendFactorToVulkanType(desc.srcAlpha);
    bs->attachments[0].srcColorBlendFactor = GLBlendFactorToVulkanType(desc.srcCol);
    bs->info.pAttachments = bs->attachments.data();
    return bs;
}

SamplerState::ptr VulkanDrawContex::CreateSamplerState(const SamplerStateDesc& desc)
{
    VulkanSamplerState::ptr ss = std::make_shared<VulkanSamplerState>();
    VkSamplerCreateInfo info = {};
    info.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    info.addressModeU = GLTextureAddressModeToVulkanType(desc.wrapU);
    info.addressModeV = GLTextureAddressModeToVulkanType(desc.wrapV);
    info.addressModeW = GLTextureAddressModeToVulkanType(desc.wrapW);
    info.anisotropyEnable = desc.maxAniso > 1.0f;
    info.maxAnisotropy = desc.maxAniso;
    info.magFilter = desc.magFilter == TextureFilter::LINEAR ? VK_FILTER_LINEAR : VK_FILTER_NEAREST;
    info.minFilter = desc.minFilter == TextureFilter::LINEAR ? VK_FILTER_LINEAR : VK_FILTER_NEAREST;
    info.mipmapMode = desc.mipFilter == TextureFilter::LINEAR ? VK_SAMPLER_MIPMAP_MODE_LINEAR : VK_SAMPLER_MIPMAP_MODE_NEAREST;
    info.maxLod = VK_LOD_CLAMP_NONE;
    VkResult res = VulkanDevice::Singleton()->vkCreateSampler( &info, nullptr, &(ss->sampler));
    if (res == VK_SUCCESS)
    {
        return ss;
    }
    else
    {
        assert(false);
        return nullptr;
    }
}

RasterState::ptr VulkanDrawContex::CreateRasterState(const RasterStateDesc& desc)
{
    VulkanRasterState::ptr rs = std::make_shared<VulkanRasterState>();
    rs->cullFace = desc.cull;
    rs->frontFace = desc.frontFace;
    return rs;
}

InputLayout::ptr VulkanDrawContex::CreateInputLayout(const InputLayoutDesc& desc)
{
    VulkanInputLayout::ptr il = std::make_shared<VulkanInputLayout>();
    // VkVertexInputAttributeDescription
    {
        il->visc.vertexAttributeDescriptionCount = desc.attributes.size();
        il->attributes.resize(il->visc.vertexAttributeDescriptionCount);
        for (size_t i=0; i<desc.attributes.size(); i++)
        {
            il->attributes[i].binding = (uint32_t)desc.attributes[i].binding;
            il->attributes[i].format = DataFormatToVulkan(desc.attributes[i].format);
            il->attributes[i].location = desc.attributes[i].location;
            il->attributes[i].offset = desc.attributes[i].offset;
        }
    }
    // VkVertexInputBindingDescription
    if (!desc.bindings.empty())
    {
        assert(desc.bindings.size() == 1);
        il->binding.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
        il->binding.binding = 0;
        il->binding.stride = desc.bindings[0].stride;
    }
    // VkPipelineVertexInputStateCreateInfo
    {
        il->visc.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
        il->visc.flags = 0;
        il->visc.vertexAttributeDescriptionCount = (uint32_t)desc.attributes.size();
        il->visc.vertexBindingDescriptionCount = 1;
        il->visc.pVertexBindingDescriptions = &il->binding;
        il->visc.pVertexAttributeDescriptions = il->attributes.data(); 
    }
    return il;
}

ShaderModule::ptr VulkanDrawContex::CreateShaderModule(ShaderStage stage, ShaderLanguage lang, const std::string& code)
{
    VulkanShaderModule::ptr shader = std::make_shared<VulkanShaderModule>(stage, std::string());

    std::string _code;

    if (stage == ShaderStage::VERTEX)
    {
        _code += 
R"(
#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

)";
    }
    else if (stage == ShaderStage::FRAGMENT)
    {
        _code += 
R"(
#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

)";
    }
    _code +=
R"(
/*********************************** Generate By MMP (Begin) **********************************/
#define mul(a, b)    (a * b)
#define _atan        atan
/*********************************** Generate By MMP (END) **********************************/
)";
    _code += code;
    bool res = shader->Compile(_code);
    if (res)
    {
        return shader;
    }
    else
    {
        assert(false);
        return nullptr;
    }
}

Pipeline::ptr VulkanDrawContex::CreateGraphicsPipeline(const PipelineDesc& desc, const std::string& tag)
{
    VulkanInputLayout::ptr input = std::dynamic_pointer_cast<VulkanInputLayout>(desc.inputLayout);
    VulkanBlendState::ptr blend = std::dynamic_pointer_cast<VulkanBlendState>(desc.blend);
    VulkanDepthStencilState::ptr depth = std::dynamic_pointer_cast<VulkanDepthStencilState>(desc.depthStencil);
    VulkanRasterState::ptr raster = std::dynamic_pointer_cast<VulkanRasterState>(desc.raster);
    VulkanGraphicsPileline::ptr vkPipeline = std::make_shared<VulkanGraphicsPileline>();

    uint32_t pilelineFlags = VulkanPipelineFlags::VK_NONE;
    if (depth->info.depthBoundsTestEnable || depth->info.stencilTestEnable)
    {
        pilelineFlags |= VulkanPipelineFlags::VK_USES_DEPTH_STENCIL;
    }
    for (auto& shader : desc.shaders)
    {
        if (shader->GetStage() == ShaderStage::VERTEX)
        {
            vkPipeline->vertexShader = std::dynamic_pointer_cast<VulkanShaderModule>(shader);
        }
        else if (shader->GetStage() == ShaderStage::FRAGMENT)
        {
            vkPipeline->fragementShader = std::dynamic_pointer_cast<VulkanShaderModule>(shader);
        }
    }
	// input
    {
        vkPipeline->ibd = input->binding;
        vkPipeline->attributes = input->attributes;
        vkPipeline->vis = input->visc;
        {
            vkPipeline->vis.pVertexBindingDescriptions = &vkPipeline->ibd;
            vkPipeline->vis.pVertexAttributeDescriptions = vkPipeline->attributes.data();
        }
    }
    // blend
    {
        vkPipeline->blend0 = blend->attachments[0];
        vkPipeline->cbs = blend->info;
        {
            vkPipeline->cbs.pAttachments = &vkPipeline->blend0;
        }
    }
    // depth
    {
        vkPipeline->dss = depth->info;
    }
    // raster
    {
        vkPipeline->rs.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;

        vkPipeline->rs.cullMode = GLCullModeToVulkanType(raster->cullFace);
        vkPipeline->rs.frontFace = GLFacingToVulkanFrontFace(raster->frontFace);
        vkPipeline->rs.polygonMode = VK_POLYGON_MODE_FILL;
        vkPipeline->rs.lineWidth = 1.0f;
    }
    vkPipeline->topology = GLPrimitiveToOpenVulkanype(desc.prim);
    return vkPipeline;
}

GLBuffer::ptr VulkanDrawContex::CreateBuffer(size_t size, uint32_t usageFlags)
{
    return std::make_shared<VulkanBuffer>(size, usageFlags);
}

Texture::ptr VulkanDrawContex::CreateTexture(const TextureDesc& desc)
{
    VkCommandBuffer initCommandBuffer = GetBundle()->GetInitCommandBuffer();
    VulkanTexture::ptr vkTex = std::make_shared<VulkanTexture>();
    if (vkTex->Create(initCommandBuffer, desc))
    {
        return vkTex;
    }
    else
    {
        assert(false);
        return nullptr;
    }
}

FrameBuffer::ptr VulkanDrawContex::CreateFrameBuffer(const FrameBufferDesc& desc)
{
    assert(false);
    return nullptr;
}

void VulkanDrawContex::UpdateTexture(Texture::ptr tex, TextureDesc desc)
{
    VkCommandBuffer initCommandBuffer = GetBundle()->GetInitCommandBuffer();
    VulkanTexture::ptr vkTex = std::dynamic_pointer_cast<VulkanTexture>(tex);
    if (vkTex->_vkTex)
    {
        vkTex->_vkTex->Update(initCommandBuffer, desc.initData);
    }
}

void VulkanDrawContex::UpdateBuffer(GLBuffer::ptr buffer, RawData::ptr data, size_t offset, size_t size)
{
    VulkanBuffer::ptr vkBuf = std::dynamic_pointer_cast<VulkanBuffer>(buffer);
    if (offset + size > vkBuf->size)
    {
        assert(false);
    }
    memcpy(vkBuf->data + offset, data->GetData(), size);
}

void VulkanDrawContex::CopyFrameBufferImage(FrameBuffer::ptr src, int level, int x, int y, int z,
                                    FrameBuffer::ptr dst, int dstLevel, int dstX, int dstY, int dstZ,
                                    int width, int height, int depth, int channelBits, const std::string& tag
                                    )
{
    assert(false);
}

bool VulkanDrawContex::BlitFrameBuffer(FrameBuffer::ptr src, int srcX1, int srcY1, int srcX2, int srcY2,
                                FrameBuffer::ptr dst, int dstX1, int dstY1, int dstX2, int dstY2,
                                int channelBits, FBBlitFilter filter, const std::string& tag
                            )
{
    assert(false);
    return false;
}

bool VulkanDrawContex::CopyFramebufferToMemory(FrameBuffer::ptr src, int channelBits, int x, int y, int w, int h,  GLPixelData::ptr pixel)
{
    assert(false);
    return false;
}

bool VulkanDrawContex::CopyTextureToMemory(const std::vector<Texture::ptr>& srcs, const std::vector<GLRect2D>& rects, AbstractSharedData::ptr picture) 
{
    assert(false);
    return false;
}

void VulkanDrawContex::SetFrameBufferSize(int width, int height)
{
    assert(false);
}

void VulkanDrawContex::SetScissorRect(int left, int top, int width, int height)
{
    XXX_VULKAN_RENDER_CHECK_VOID();
    VulkanRender render;
    VulkanScissorRenderData data;
    render.cmd = VulkanRenderCommond::SCISSOR;
    data.scissor.x = left;
    data.scissor.y = top;
    data.scissor.w = width;
    data.scissor.h = height;
    render.data = data;
    _curRenderStep->commands.push_back(render);
}

void VulkanDrawContex::SetViewport(const Viewport& viewport)
{
    XXX_VULKAN_RENDER_CHECK_VOID();
    VulkanRender render;
    VulkanViewportRenderData data;
    render.cmd = VulkanRenderCommond::VIEWPORT;
    data.vp.x = viewport.TopLeftX;
    data.vp.y = viewport.TopLeftY;
    data.vp.width = viewport.Width;
    data.vp.height = viewport.Height;
    data.vp.minDepth = viewport.MinDepth;
    data.vp.maxDepth = viewport.MaxDepth;
    render.data = data;
    _curRenderStep->commands.push_back(render);
}

void VulkanDrawContex::SetBlendFactor(float color[4])
{
    XXX_VULKAN_RENDER_CHECK_VOID();
    VulkanRender render;
    VulkanBlendColorRenderData data;
    render.cmd = VulkanRenderCommond::STENCIL;
    data.blendColor |= (uint8_t)(color[0] * 255);
    data.blendColor |= (uint8_t)(color[1] * 255) << 8;
    data.blendColor |= (uint8_t)(color[2] * 255) << 16;
    data.blendColor |= (uint8_t)(color[3] * 255) << 24;
    render.data = data;
    _curRenderStep->commands.push_back(render);
}

void VulkanDrawContex::SetStencilParams(uint8_t refValue, uint8_t writeMask, uint8_t compareMask)
{
    XXX_VULKAN_RENDER_CHECK_VOID();
    VulkanRender render;
    VulkanStencilRenderData data;
    render.cmd = VulkanRenderCommond::STENCIL;
    data.stencilWriteMask = writeMask;
    data.stencilCompareMask = compareMask;
    data.stencilRef = refValue;
    render.data = data;
    _curRenderStep->commands.push_back(render);
}

void VulkanDrawContex::BindSamplerStates(int start, std::vector<SamplerState::ptr> states)
{
    size_t count = states.size();
    assert(start + count <= XXX_VULKAN_MAX_TEXTURE_SLOTS);
    for (size_t i=0; i<count; i++)
    {
        _boundSamplers[i+start] = std::dynamic_pointer_cast<VulkanSamplerState>(states[i]);
    }
}

void VulkanDrawContex::BindTextures(int start, std::vector<Texture::ptr> textures)
{
    size_t count = textures.size();
    assert(start + count <= XXX_VULKAN_MAX_TEXTURE_SLOTS);
    for (size_t i=0; i<count; i++)
    {
        if (textures[i])
        {
            _boundTextures[i+start] = std::dynamic_pointer_cast<VulkanTexture>(textures[i]);
            _boundImageView[i+start] = _boundTextures[i+start]->_vkTex->view;
        }
        else
        {
            _boundTextures[i+start] = VK_NULL_HANDLE;
            _boundImageView[i+start] = VK_NULL_HANDLE;
        }
    }
}

void VulkanDrawContex::BindVertexBuffers(int start, const std::vector<GLBuffer::ptr>& buffers, const std::vector<int>& offsets)
{
    assert(false);
}

void VulkanDrawContex::BindIndexBuffer(GLBuffer::ptr indexBuffer, int offset)
{
    assert(false);
}

void VulkanDrawContex::BindFramebufferAsRenderTarget(FrameBuffer::ptr fbo, const RenderPassInfo& rp)
{
    assert(false);
}

void VulkanDrawContex::BindFramebufferAsTexture(FrameBuffer::ptr fbo, int binding, FBChannel channelBits, int layer)
{
    assert(false);
}

void VulkanDrawContex::BindPipeline(Pipeline::ptr pipeline)
{
    assert(false);
}

void VulkanDrawContex::UpdataUniformBuffer(RawData::ptr uniformBuffer, size_t size)
{
    assert(false);
}

ShaderLanguage VulkanDrawContex::GetShaderLanguage()
{
    return ShaderLanguage::GLSL_VULKAN;
}

DataFormat VulkanDrawContex::FrameBufferReadBackFormat(FBChannel channel)
{
    assert(false);
    return DataFormat::UNDEFINED;
}

void VulkanDrawContex::GetDefaultVorldViewProj(int width, int height, float worldViewProj[16], bool onScreen)
{
    if (onScreen)
    {
        // frist column
        worldViewProj[0] = 2.0f / (width - 0.0f);
        worldViewProj[1] = 0.0f;
        worldViewProj[2] = 0.0f;
        worldViewProj[3] = 0.0f;
        // second column
        worldViewProj[4] = 0.0f;
        worldViewProj[5] = (2.0f / (0.0f - height));
        worldViewProj[6] = 0.0;
        worldViewProj[7] = 0.0;
        // third column
        worldViewProj[8] = 0.0f;
        worldViewProj[9] = 0.0f;
        worldViewProj[10] = 1.0f;
        worldViewProj[11] = 0.0f;
        // last column
        worldViewProj[12] = -(width + 0.0f) / (width - 0.0f);
        worldViewProj[13] = -(height + 0.0f) / (0.0f - height);
        worldViewProj[14] = 0.0f;
        worldViewProj[15] = 1.0f;
    }
    else
    {
        // frist column
        worldViewProj[0] = 2.0f / (width - 0.0f);
        worldViewProj[1] = 0.0f;
        worldViewProj[2] = 0.0f;
        worldViewProj[3] = 0.0f;
        // second column
        worldViewProj[4] = 0.0f;
        worldViewProj[5] = -(2.0f / (0.0f - height));
        worldViewProj[6] = 0.0;
        worldViewProj[7] = 0.0;
        // third column
        worldViewProj[8] = 0.0f;
        worldViewProj[9] = 0.0f;
        worldViewProj[10] = 1.0f;
        worldViewProj[11] = 0.0f;
        // last column
        worldViewProj[12] = -(width + 0.0f) / (width - 0.0f);
        worldViewProj[13] = (height + 0.0f) / (0.0f - height);
        worldViewProj[14] = 0.0f;
        worldViewProj[15] = 1.0f;
    }
}

bool VulkanDrawContex::IsFeatureSupport(const GlDrawFeature& feature)
{
    assert(false);
    return false;
}

void VulkanDrawContex::Draw(int vertexCount, int offset)
{
    assert(false);
}

void VulkanDrawContex::DrawIndexed(int vertexCount, int offset)
{
    assert(false);
}

VulkanRenderPass::ptr VulkanDrawContex::GetRenderPass(VulkanRenderPassKey key)
{
    if (!_renderPasses.count(key))
    {
        assert(false); // TODO
        _renderPasses[key] = std::make_shared<VulkanRenderPass>(key, nullptr);
    }
    return _renderPasses[key];
}

VulkanGraphicsBundle::ptr VulkanDrawContex::GetBundle()
{
    if (!_bundle)
    {
        _bundle = std::make_shared<VulkanGraphicsBundle>();
        if (!_bundle->Init())
        {
            _bundle.reset();
        }
    }
    return _bundle;
}

void VulkanDrawContex::CreateDeviceObjects()
{
    VulkanRenderPassKey key = {};
    {
        key.colorLoadAction = RPAction::CLEAR;
        key.colorLoadAction = RPAction::CLEAR;
        key.colorLoadAction = RPAction::CLEAR;
        key.colorStoreAction = RPAction::STORE;
        key.colorStoreAction = RPAction::DONT_CARE;
        key.colorStoreAction = RPAction::DONT_CARE;
    }
    _compatibleRenderPass = GetRenderPass(key);
}

void VulkanDrawContex::DestoryDeviceObjects()
{
    _compatibleRenderPass.reset();
    _renderPasses.clear();
}

} // namespace Mmp

#undef XXX_VULKAN_RENDER_CHECK_VOID