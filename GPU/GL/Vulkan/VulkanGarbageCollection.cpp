#include "VulkanGarbageCollection.h"

namespace Mmp
{

VulkanGarbageCollection::VulkanGarbageCollection(VmaAllocator allocator)
{
    _allocator = allocator;
}

VulkanGarbageCollection::~VulkanGarbageCollection()
{
    Flush();
}

void VulkanGarbageCollection::DeleteVkCommandPool(const VkCommandPool& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _commandPools.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkDescriptorPool(const VkDescriptorPool& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _descriptorPools.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkShaderModul(const VkShaderModule& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _shaderModules.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkBuffer(const VkBuffer& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _buffers.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkBufferView(const VkBufferView& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _bufferViews.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkImageView(const VkImageView& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _imageViews.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkDeviceMemory(const VkDeviceMemory& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _deviceMemories.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkSampler(const VkSampler& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _samplers.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkPipeline(const VkPipeline& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _pipelines.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkPipelineCache(const VkPipelineCache& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _pipelineCaches.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkRenderPass(const VkRenderPass& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _renderPasses.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkFramebuffer(const VkFramebuffer& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _framebuffers.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkPipelineLayout(const VkPipelineLayout& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _pipelineLayouts.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkDescriptorSetLayout(const VkDescriptorSetLayout& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _descriptorSetLayouts.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkQueryPool(const VkQueryPool& data)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (data != VK_NULL_HANDLE)
    {
        _queryPools.push_back(data);
    }
}

void VulkanGarbageCollection::DeleteVkImageVmaAllocation(const VkImage& image, const VmaAllocation& allocation)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (image != VK_NULL_HANDLE || allocation != VK_NULL_HANDLE)
    {
        _imageAllocs.push_back({image, allocation});
    }
}

void VulkanGarbageCollection::Flush()
{
    std::lock_guard<std::mutex> lock(_mtx);
    for (auto& commandPool : _commandPools)
    {
        VulkanDevice::Singleton()->vkDestroyCommandPool(commandPool, nullptr);
    }
    _commandPools.clear();
    for (auto& descriptorPool : _descriptorPools)
    {
        VulkanDevice::Singleton()->vkDestroyDescriptorPool(descriptorPool, nullptr);
    }
    _descriptorPools.clear();
    for (auto& shaderModule : _shaderModules)
    {
        VulkanDevice::Singleton()->vkDestroyShaderModule(shaderModule, nullptr);
    }
    _shaderModules.clear();
    for (auto& buffer : _buffers)
    {
        VulkanDevice::Singleton()->vkDestroyBuffer(buffer, nullptr);
    }
    _buffers.clear();
    for (auto& bufferView : _bufferViews)
    {
        VulkanDevice::Singleton()->vkDestroyBufferView(bufferView, nullptr);
    }
    _bufferViews.clear();
    for (auto& imageView : _imageViews)
    {
        VulkanDevice::Singleton()->vkDestroyImageView(imageView, nullptr);
    }
    _imageViews.clear();
    for (auto& deviceMemorie : _deviceMemories)
    {
        VulkanDevice::Singleton()->vkFreeMemory(deviceMemorie, nullptr);
    }
    _deviceMemories.clear();
    for (auto& sampler : _samplers)
    {
        VulkanDevice::Singleton()->vkDestroySampler(sampler, nullptr);
    }
    _samplers.clear();
    for (auto& pipeline : _pipelines)
    {
        VulkanDevice::Singleton()->vkDestroyPipeline(pipeline, nullptr);
    }
    _pipelines.clear();
    for (auto& pipelineCache : _pipelineCaches)
    {
        VulkanDevice::Singleton()->vkDestroyPipelineCache(pipelineCache, nullptr);
    }
    _pipelineCaches.clear();
    for (auto& renderPass : _renderPasses)
    {
        VulkanDevice::Singleton()->vkDestroyRenderPass(renderPass, nullptr);
    }
    _renderPasses.clear();
    for (auto& framebuffer : _framebuffers)
    {
        VulkanDevice::Singleton()->vkDestroyFramebuffer(framebuffer, nullptr);
    }
    _framebuffers.clear();
    for (auto& pipelineLayout : _pipelineLayouts)
    {
        VulkanDevice::Singleton()->vkDestroyPipelineLayout(pipelineLayout, nullptr);
    }
    _pipelineLayouts.clear();
    for (auto& descriptorSetLayout : _descriptorSetLayouts)
    {
        VulkanDevice::Singleton()->vkDestroyDescriptorSetLayout(descriptorSetLayout, nullptr);
    }
    _descriptorSetLayouts.clear();
    for (auto& queryPool : _queryPools)
    {
        VulkanDevice::Singleton()->vkDestroyQueryPool(queryPool, nullptr);
    }
    _queryPools.clear();
    for (auto& imageAlloc : _imageAllocs)
    {
        vmaDestroyImage(_allocator, imageAlloc.first, imageAlloc.second);
    }
    _imageAllocs.clear();
}

} // namespace Mmp