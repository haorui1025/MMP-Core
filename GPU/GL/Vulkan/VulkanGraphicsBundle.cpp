#include "VulkanGraphicsBundle.h"

#include <cassert>

#include "VulkanDevice.h"
#include "vulkan/vulkan_core.h"

namespace Mmp
{

VulkanGraphicsBundle::VulkanGraphicsBundle()
{
    _initCommandPool = VK_NULL_HANDLE;
    _initCommandBuffer =  VK_NULL_HANDLE;
    _hasInitCommands = false;
}

VulkanGraphicsBundle::~VulkanGraphicsBundle()
{   
    if (_initCommandPool != VK_NULL_HANDLE)
    {
        VulkanDevice::Singleton()->vkDestroyCommandPool(_initCommandPool, nullptr);
    }
}


bool VulkanGraphicsBundle::Init()
{
    VkResult vkRes = VK_SUCCESS;
    VkCommandPoolCreateInfo commandPoolCreateInfo = {};
    VkCommandBufferAllocateInfo commandBufferAllocateInfo = {};
    {
        commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        commandPoolCreateInfo.queueFamilyIndex = VulkanDevice::Singleton()->GetQueueFamilyIndex(VkQueueFlagBits::VK_QUEUE_GRAPHICS_BIT);
        commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
    }
    vkRes = VulkanDevice::Singleton()->vkCreateCommandPool(&commandPoolCreateInfo, nullptr, &_initCommandPool);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vkCreateCommandPool fail, error is: " << VkResultToStr(vkRes);
        assert(false);
        goto END;
    }
    {
        commandBufferAllocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
        commandBufferAllocateInfo.commandPool = _initCommandPool;
        commandBufferAllocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
        commandBufferAllocateInfo.commandBufferCount = 1;
    }
    vkRes = VulkanDevice::Singleton()->vkAllocateCommandBuffers(&commandBufferAllocateInfo, &_initCommandBuffer);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vkAllocateCommandBuffers fail, error is: " << VkResultToStr(vkRes);
        assert(false);
        goto END;
    }
END:
    return VULKAN_SUCCEEDED(vkRes);
}

VkCommandBuffer VulkanGraphicsBundle::GetInitCommandBuffer()
{
    if (!_hasInitCommands)
    {
        VkResult vkRes = VK_SUCCESS;
        VkCommandBufferBeginInfo commandBufferBeginInfo = {};
        {
            commandBufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
            commandBufferBeginInfo.pInheritanceInfo = nullptr;
            commandBufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
        }
        vkRes = VulkanDevice::Singleton()->vkResetCommandPool(_initCommandPool, 0);
        if (VULKAN_FAILED(vkRes))
        {
            VULKAN_LOG_ERROR << "vkResetCommandPool fail, error is: " << VkResultToStr(vkRes);
            assert(false);
        }
        vkRes = VulkanDevice::Singleton()->vkBeginCommandBuffer(_initCommandBuffer, &commandBufferBeginInfo);
        if (VULKAN_FAILED(vkRes))
        {
            VULKAN_LOG_ERROR << "vkBeginCommandBuffer fail, error is: " << VkResultToStr(vkRes);
            assert(false);
        }
        _hasInitCommands = true;
    }
    return _initCommandBuffer;
}

} // namespace Mmp