//
// VulkanContex.h
//
// Library: GPU
// Package: GL
// Module:  Vulkan
// 

#pragma once

#include <memory>
#include <vector>

#include "VulkanCommon.h"
#include "VulkanTranslator.h"

namespace Mmp
{

class VulkanGLTexture;

class VulkanBlendState : public BlendState
{
public:
    using ptr = std::shared_ptr<VulkanBlendState>;
public:
    VulkanBlendState();
    ~VulkanBlendState();
public:
    VkPipelineColorBlendStateCreateInfo                 info;
    std::vector<VkPipelineColorBlendAttachmentState>    attachments;
};

class VulkanDepthStencilState : public DepthStencilState
{
public:
    using ptr = std::shared_ptr<VulkanDepthStencilState>;
public:
    VulkanDepthStencilState();
    ~VulkanDepthStencilState();
public:
    VkPipelineDepthStencilStateCreateInfo info;
};

/**
 * @todo 
 */
class VulkanRasterState : public RasterState
{
public:
    using ptr = std::shared_ptr<VulkanRasterState>;
public:
    VulkanRasterState();
    ~VulkanRasterState();
public:
    Facing       frontFace;
    CullMode     cullFace;
};

class VulkanShaderModule : public ShaderModule
{
public:
    using ptr = std::shared_ptr<VulkanShaderModule>;
public:
    VulkanShaderModule(ShaderStage stage, const std::string& tag);
    ~VulkanShaderModule();
public:
    ShaderStage GetStage() override;
public:
    bool Compile(const std::string& code);
public:
    VkShaderModule            smodule;
    VkShaderStageFlagBits     flag;
    ShaderStage               stage;
    std::string               source;
    bool                      isOK;
    std::string               tag;
    std::vector<uint32_t>     spirv;
};

class VulkanInputLayout : public InputLayout
{
public:
    using ptr = std::shared_ptr<VulkanInputLayout>;
public:
    VulkanInputLayout();
    ~VulkanInputLayout();
public:
    VkVertexInputBindingDescription                  binding = {};
    std::vector<VkVertexInputAttributeDescription>   attributes;
    VkPipelineVertexInputStateCreateInfo             visc = {};
};

class VulkanSamplerState : public SamplerState
{
public:
    using ptr = std::shared_ptr<VulkanSamplerState>;
public:
    VulkanSamplerState();
    ~VulkanSamplerState();
public:
    VkSampler sampler;
};

class VulkanBuffer : public GLBuffer
{
public:
    using ptr = std::shared_ptr<VulkanBuffer>;
public:
    explicit VulkanBuffer(size_t size, uint32_t usageFlags);
    VulkanBuffer();
    ~VulkanBuffer();
public:
    uint8_t* data;
    size_t   size;
    uint32_t usageFlags;
};

class VulkanTexture : public Texture
{
public:
    using ptr = std::shared_ptr<VulkanTexture>;
public:
    VulkanTexture();
    ~VulkanTexture();
public:
    bool Create(VkCommandBuffer commandBuffer, const TextureDesc& desc);
public:
    std::shared_ptr<VulkanGLTexture> _vkTex;
};

class VulkanGraphicsPileline : public Pipeline
{
public:
    using ptr = std::shared_ptr<VulkanGraphicsPileline>;
public:
    VulkanGraphicsPileline();
    ~VulkanGraphicsPileline();
public:
    VkPipelineCache                         pipelineCache = {};
    VkPipelineColorBlendStateCreateInfo     cbs = {};
    VkPipelineColorBlendAttachmentState     blend0 = {};
    VkPipelineDepthStencilStateCreateInfo   dss = {};
    VkDynamicState                          dynamicStates[6] = {};
    VkPipelineDynamicStateCreateInfo        ds = {};
    VkPipelineRasterizationStateCreateInfo  rs = {};
public:
    VulkanShaderModule::ptr                 vertexShader;
    VulkanShaderModule::ptr                 fragementShader;
    VulkanShaderModule::ptr                 geometryShader;
public:
    VkPrimitiveTopology                              topology;
    std::vector<VkVertexInputAttributeDescription>   attributes;
    VkVertexInputBindingDescription                  ibd = {};
    VkPipelineVertexInputStateCreateInfo             vis = {};
    VkPipelineViewportStateCreateInfo                views = {};
public:
    VkPipelineLayout                        pipelineLayout = {};
    VulkanRenderPassKey                     rpKey;
};

} // namespace Mmp

namespace Mmp
{

class DescriptorSetKey
{
public:
    DescriptorSetKey();
public:
    std::vector<VkImageView>               imageViews;
    std::vector<VulkanSamplerState::ptr>   samplers;
    VulkanBuffer::ptr                      buffer; 
public:
    bool operator<(const DescriptorSetKey &other) const;
};

} // namespace Mmp