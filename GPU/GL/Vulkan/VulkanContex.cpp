#include "VulkanContex.h"

#include <cassert>
#include <memory>

#include "VulkanUtil.h"
#include "VulkanGLTexture.h"
#include "VulkanTranslator.h"

#include "glslang/Public/ShaderLang.h"
#include "SPIRV/SpvTools.h"

namespace Mmp
{

static EShLanguage ShaderStageToEShLanguage(ShaderStage stage)
{
    switch (stage)
    {
        case ShaderStage::VERTEX: return EShLangVertex;
        case ShaderStage::FRAGMENT: return EShLangFragment;
        case ShaderStage::COMPUTE: return EShLangCompute;
        case ShaderStage::GEOMETRY: return EShLangGeometry;
        default:
            assert(false);
            return EShLangVertex;
    }
}

VulkanBlendState::VulkanBlendState()
{
    info = {};
    info.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
}

VulkanBlendState::~VulkanBlendState()
{

}

VulkanDepthStencilState::VulkanDepthStencilState()
{
    info = {};
    info.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
}

VulkanDepthStencilState::~VulkanDepthStencilState()
{

}

VulkanRasterState::VulkanRasterState()
{

}

VulkanRasterState::~VulkanRasterState()
{

}

VulkanShaderModule::VulkanShaderModule(ShaderStage stage, const std::string& tag)
{
    this->stage = stage;
    this->tag = tag;
    this->flag = SLShaderStageToVulkanType(stage);
    isOK = false;
    smodule = VK_NULL_HANDLE;
}

VulkanShaderModule::~VulkanShaderModule()
{
    if (smodule != VK_NULL_HANDLE)
    {
        VulkanDevice::Singleton()->vkDestroyShaderModule(smodule, nullptr);
    }
}

ShaderStage VulkanShaderModule::GetStage()
{
    return stage;
}

bool VulkanShaderModule::Compile(const std::string& code)
{
    source = code;
    {
        glslang::TProgram program;
        glslang::SpvOptions options;
        glslang::TShader shader(ShaderStageToEShLanguage(stage));
        const char *shaderStrings[1] = { code.data() };
        TBuiltInResource resource = {};
        shader.setStrings(shaderStrings, 1);
    	if (!shader.parse(&resource, 450, ECoreProfile, false, true, (EShMessages)(EShMsgSpvRules | EShMsgVulkanRules))) 
        {
            VULKAN_LOG_ERROR << code;
            VULKAN_LOG_ERROR << shader.getInfoLog();
            VULKAN_LOG_ERROR << shader.getInfoDebugLog();
            assert(false);
            return false;
        }
        program.addShader(&shader);
        if (!program.link((EShMessages)(EShMsgSpvRules | EShMsgVulkanRules))) 
        {
            VULKAN_LOG_ERROR << code;
            VULKAN_LOG_ERROR << shader.getInfoLog();
            VULKAN_LOG_ERROR << shader.getInfoDebugLog();
            assert(false);
            return false;
        }
        {
            options.disableOptimizer = false;
            options.optimizeSize = false;
            options.generateDebugInfo = false;
        }
        glslang::GlslangToSpv(*program.getIntermediate(ShaderStageToEShLanguage(stage)), spirv, &options);
    }
    return true;
}

VulkanInputLayout::VulkanInputLayout()
{
    visc = {};
    visc.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
}

VulkanInputLayout::~VulkanInputLayout()
{

}

VulkanSamplerState::VulkanSamplerState()
{
    sampler = VK_NULL_HANDLE;
}

VulkanSamplerState::~VulkanSamplerState()
{
    if (sampler != VK_NULL_HANDLE)
    {
        // TODO
        assert(false);
    }
}

VulkanBuffer::VulkanBuffer(size_t size, uint32_t usageFlags)
{
    this->usageFlags = usageFlags;
    this->size = size;
    if (size == 0)
    {
        data = nullptr;
    }
    else
    {
        data = new uint8_t[size];
    }
}

VulkanBuffer::VulkanBuffer()
{
    usageFlags = 0;
    size = 0;
    data = nullptr;
}

VulkanBuffer::~VulkanBuffer()
{
    if (data)
    {
        delete[] data;
    }
}

VulkanTexture::VulkanTexture()
{
    _vkTex = std::make_shared<VulkanGLTexture>();
}

VulkanTexture::~VulkanTexture()
{
    
}

bool VulkanTexture::Create(VkCommandBuffer commandBuffer, const TextureDesc& desc)
{
    VkImageUsageFlags usgae = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    VkImageLayout initialLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    VulkanBarrier::ptr barrier = std::make_shared<VulkanBarrier>();
    if (desc.mipLevels > (int)desc.initData.size())
    {
        usgae |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
    }
    if (!_vkTex->Create(VulkanDevice::Singleton()->GetVmaAllocator(), desc.width, desc.height, desc.depth, desc.mipLevels, 
        DataFormatToVkFormat(desc.format), initialLayout, usgae, barrier)
    )
    {
        VULKAN_LOG_ERROR << "VulkanGLTexture::Create fail";
        assert(false);
        goto FAIL;
    }
    barrier->Flush(commandBuffer);
    initialLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    if (!desc.initData.empty())
    {
        // TODO : update init data to texture
    }
    TransitionImageLayout2(commandBuffer, _vkTex->image, 0, _vkTex->numMips, 1,
        VK_IMAGE_ASPECT_COLOR_BIT, initialLayout, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_TRANSFER_WRITE_BIT, VK_ACCESS_SHADER_READ_BIT
    );
    return true;
FAIL:
    return false;
}

VulkanGraphicsPileline::VulkanGraphicsPileline()
{
    pipelineCache = VK_NULL_HANDLE;
    cbs = {};
    cbs.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    blend0 = {};
    dss = {};
    dss.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    ds = {};
    ds.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    rs = {};
    rs.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    ibd = {};
    vis = {};
    vis.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    views = {};
    views.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    pipelineLayout = VK_NULL_HANDLE;
    rpKey = {};
}

VulkanGraphicsPileline::~VulkanGraphicsPileline()
{

}

} // namespace Mmp

namespace Mmp
{

DescriptorSetKey::DescriptorSetKey()
{
    imageViews.resize(VULKAN_MAX_BOUND_TEXTURES);
    samplers.resize(VULKAN_MAX_BOUND_TEXTURES);
    for (int i=0; i<VULKAN_MAX_BOUND_TEXTURES; i++)
    {
        imageViews[i] = VK_NULL_HANDLE;
        samplers[i] = std::make_shared<VulkanSamplerState>();
    }
    buffer = std::make_shared<VulkanBuffer>();
}

bool DescriptorSetKey::operator<(const DescriptorSetKey &other) const
{
    for (int i=0; i<VULKAN_MAX_BOUND_TEXTURES; i++)
    {
        if (imageViews[i] > other.imageViews[i])
        {
            return true;
        }
        else if (imageViews[i] < other.imageViews[i])
        {
            return false;
        }
        if (samplers[i]->sampler > other.samplers[i]->sampler)
        {
            return true;
        }
        else if (samplers[i]->sampler < other.samplers[i]->sampler)
        {
            return false;
        }
    }
    if (buffer->data < other.buffer->data)
    {
        return true;
    }
    else if (buffer->data > other.buffer->data)
    {
        return false;
    }
    return false;
}

} // namespace Mmp