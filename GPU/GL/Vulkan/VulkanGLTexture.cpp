#include "VulkanGLTexture.h"

#include <memory.h>

#include "VulkanDevice.h"
#include "VulkanImpl.h"
#include "VulkanUtil.h"

namespace Mmp
{

TextureCopyBatch::TextureCopyBatch()
{
    buffer = VK_NULL_HANDLE; 
}

TextureCopyBatch::~TextureCopyBatch()
{

}

VulkanGLTexture::VulkanGLTexture()
{
    width = 0;
    height = 0;
    depth = 0;
    numMips = 0;
    format = VK_FORMAT_UNDEFINED;
    image  = VK_NULL_HANDLE;
    allocation = VK_NULL_HANDLE;
    view = VK_NULL_HANDLE;
}

VulkanGLTexture::~VulkanGLTexture()
{
    if (view != VK_NULL_HANDLE)
    {
        VulkanDevice::Singleton()->vkDestroyImageView(view, nullptr);
    }
    if (image != VK_NULL_HANDLE)
    {
        vmaDestroyImage(VulkanDevice::Singleton()->GetVmaAllocator(), image, allocation);
    }
}

bool VulkanGLTexture::Create(VmaAllocator allocator, uint32_t width, uint32_t height, uint32_t depth, uint32_t numMips, VkFormat format, VkImageLayout initialLayout, VkImageUsageFlags usage, VulkanBarrier::ptr barrierBatch)
{
    uint32_t maxImageDimension2D = VulkanDevice::Singleton()->GetVkPhysicalDeviceProperties2().properties.limits.maxImageDimension2D;
    VkImageAspectFlags aspectFlags = VK_IMAGE_ASPECT_NONE;
    VkImageCreateInfo imageCreateInfo = {};
    VmaAllocationCreateInfo allocationCreateInfo = {};
    VmaAllocationInfo allocationInfo = {};
    VkImageViewCreateInfo imageViewCreateInfo = {};
    VkResult vkRes = VK_SUCCESS;
    if (width > maxImageDimension2D || height > maxImageDimension2D)
    {
        VULKAN_LOG_ERROR << "width(" << width << ") height(" << height << ") maxImageDimension2D(" << maxImageDimension2D << ") Oversize";
        return false;
    }
    {
        this->width = width;
        this->height = height;
        this->depth = depth;
        this->numMips = numMips;
        this->format = format;
    }
    aspectFlags = IsDepthStencilFormat(format) ? VK_IMAGE_ASPECT_DEPTH_BIT : VK_IMAGE_ASPECT_COLOR_BIT;
    // VkImageCreateInfo
    {
        imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
        imageCreateInfo.imageType = depth > 1 ? VK_IMAGE_TYPE_3D : VK_IMAGE_TYPE_2D;
        imageCreateInfo.format = format;
        imageCreateInfo.extent.width = width;
        imageCreateInfo.extent.height = height;
        imageCreateInfo.extent.depth = depth;
        imageCreateInfo.mipLevels = numMips;
        imageCreateInfo.arrayLayers = 1;
        imageCreateInfo.samples = VK_SAMPLE_COUNT_1_BIT;
        imageCreateInfo.flags = 0;
        imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
        imageCreateInfo.usage = usage;
        if (initialLayout == VK_IMAGE_LAYOUT_PREINITIALIZED)
        {
            imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_PREINITIALIZED;
        }
        else
        {
            imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        }
    }
    // VmaAllocationCreateInfo
    {
        allocationCreateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;
    }
    vkRes = vmaCreateImage(allocator, &imageCreateInfo, &allocationCreateInfo, &image, &allocation, &allocationInfo);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vmaCreateImage, error is: " << VkResultToStr(vkRes);
        assert(false);
        return false;
    }
    if (initialLayout != VK_IMAGE_LAYOUT_UNDEFINED && initialLayout != VK_IMAGE_LAYOUT_PREINITIALIZED)
    {
        VkPipelineStageFlags dstPipelineStageFlags = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
        VkAccessFlagBits dstAccessFlagBits = VK_ACCESS_NONE;
        switch (initialLayout)
        {
            case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
            {
                dstPipelineStageFlags = VK_PIPELINE_STAGE_TRANSFER_BIT;
                dstAccessFlagBits = VK_ACCESS_TRANSFER_WRITE_BIT;
                break;
            }
            case VK_IMAGE_LAYOUT_GENERAL:
            {
                dstPipelineStageFlags = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
                dstAccessFlagBits = VK_ACCESS_SHADER_READ_BIT;
                break;
            }
            default:
            {
                assert(false);
                break;
            }
        }
        barrierBatch->TransitionImage(image, 0, numMips, 1, VK_IMAGE_ASPECT_COLOR_BIT, 
            VK_IMAGE_LAYOUT_UNDEFINED, initialLayout, 0,
            dstAccessFlagBits, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, dstPipelineStageFlags
        );
    }
    // VkImageViewCreateInfo
    {
        imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        imageViewCreateInfo.image = image;
        imageViewCreateInfo.viewType = depth > 1 ? VK_IMAGE_VIEW_TYPE_3D : VK_IMAGE_VIEW_TYPE_2D;
        imageViewCreateInfo.format = format;
        {
            imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
            imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
            imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
            imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        }
        imageViewCreateInfo.subresourceRange.aspectMask = aspectFlags;
        imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
        imageViewCreateInfo.subresourceRange.levelCount = numMips;
        imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
        imageViewCreateInfo.subresourceRange.layerCount = 1;
    }
    vkRes = VulkanDevice::Singleton()->vkCreateImageView(&imageViewCreateInfo, nullptr, &view);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vkCreateImageView fail, error is: " << VkResultToStr(vkRes);
        assert(false);
        return false;
    }
    return true;
}

void VulkanGLTexture::Update(VkCommandBuffer cmd, const std::vector<GLPixelData::ptr>& textureData)
{
    PrepareForTransferDst(cmd, (int32_t)textureData.size());
    UpdateInternal(cmd, textureData);
    RestoreAfterTransferDst(cmd, (int32_t)textureData.size());
}

void VulkanGLTexture::CopyBufferToMipLevel(VkCommandBuffer cmd, TextureCopyBatch::ptr copyBatch, int32_t mip, int32_t mipWidth, int32_t mipHeight, int32_t depthLayer, VkBuffer buffer, uint32_t offset, size_t rowLength)
{
    VkBufferImageCopy bufferImageCopy = {};
    // VkBufferImageCopy
    {
        bufferImageCopy.bufferOffset = offset;
        bufferImageCopy.bufferRowLength = (uint32_t)rowLength;
        bufferImageCopy.bufferImageHeight = 0;
        bufferImageCopy.imageOffset.z = depthLayer;
        bufferImageCopy.imageExtent.width = mipWidth;
        bufferImageCopy.imageExtent.height = mipHeight;
        bufferImageCopy.imageExtent.depth = 1;
        bufferImageCopy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        bufferImageCopy.imageSubresource.mipLevel = mip;
        bufferImageCopy.imageSubresource.baseArrayLayer = 0;
        bufferImageCopy.imageSubresource.layerCount = 1;
    }
    if (copyBatch->buffer == VK_NULL_HANDLE)
    {
        copyBatch->buffer = buffer;
    }
    else if (copyBatch->buffer != buffer)
    {
        FinishCopyBatch(cmd, copyBatch);
        copyBatch->buffer = buffer;
    }
    copyBatch->copies.push_back(bufferImageCopy);
}

void VulkanGLTexture::FinishCopyBatch(VkCommandBuffer cmd, TextureCopyBatch::ptr copyBatch)
{
    if (!copyBatch->copies.empty())
    {
        VulkanDevice::Singleton()->vkCmdCopyBufferToImage(cmd, copyBatch->buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, (uint32_t)copyBatch->copies.size(), copyBatch->copies.data());
        copyBatch->copies.clear();
    }
}

VulkanMemory::ptr VulkanGLTexture::GetMemory(size_t size)
{
    if (_memorys.count(size) == 0)
    {
        _memorys[size] = std::make_shared<VulkanMemory>();
        {
            _memorys[size]->size = size;
            _memorys[size]->sharingMode = VK_SHARING_MODE_EXCLUSIVE;
            _memorys[size]->memoryUsage = VMA_MEMORY_USAGE_CPU_TO_GPU;
            _memorys[size]->bufferUsage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        }
        if (_memorys[size]->Init())
        {
            VULKAN_LOG_ERROR << "VulkanMemory::Init fail";
            assert(false);
            _memorys.erase(size);
            return nullptr;
        }
        _memorys[size]->Map();
    }
    return _memorys[size];
}

void VulkanGLTexture::PrepareForTransferDst(VkCommandBuffer cmd, int32_t levels)
{
    VulkanBarrier::ptr batch = std::make_shared<VulkanBarrier>();
    VkImageMemoryBarrier memoryBarrier = {};
    {
        memoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        memoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        memoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        {
            memoryBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            memoryBarrier.subresourceRange.baseArrayLayer = 0;
            memoryBarrier.subresourceRange.layerCount = 1;
            memoryBarrier.subresourceRange.levelCount = levels;
        }
        memoryBarrier.image = image;
        memoryBarrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
        memoryBarrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        memoryBarrier.oldLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        memoryBarrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    }
    batch->Add(memoryBarrier, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0);
    batch->Flush(cmd);
}

void VulkanGLTexture::UpdateInternal(VkCommandBuffer cmd, const std::vector<GLPixelData::ptr>& textureData)
{
    uint32_t bpp = GetBytePerPixel(format);
    size_t numLevels = textureData.size();
    TextureCopyBatch::ptr batch = std::make_shared<TextureCopyBatch>();
    uint32_t _width = width;
    uint32_t _height = height;
    uint32_t _depth = depth;
    for (int32_t i=0; i<numLevels; i++)
    {
        size_t size = _width * _height * _depth * bpp;
        VulkanMemory::ptr memory = GetMemory(size);
        memcpy(memory->address, (uint8_t*)textureData[i]->GetData(0), size);
        CopyBufferToMipLevel(cmd, batch, i, _width, _height, _depth, memory->buffer, 0, _width);
        _width  = (_width + 1) / 2;
        _height = (_height + 1) / 2;
        _depth  = (_depth + 1) / 2; 
    }
    FinishCopyBatch(cmd, batch);
}

void VulkanGLTexture::RestoreAfterTransferDst(VkCommandBuffer cmd, int32_t levels)
{
    VulkanBarrier::ptr batch = std::make_shared<VulkanBarrier>();
    VkImageMemoryBarrier memoryBarrier = {};
    {
        memoryBarrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        memoryBarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        memoryBarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        {
            memoryBarrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
            memoryBarrier.subresourceRange.baseArrayLayer = 0;
            memoryBarrier.subresourceRange.layerCount = 1;
            memoryBarrier.subresourceRange.levelCount = levels;
        }
        memoryBarrier.image = image;
        memoryBarrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        memoryBarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        memoryBarrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        memoryBarrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    }
    batch->Add(memoryBarrier, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT, 0);
}

} // namespace Mmp