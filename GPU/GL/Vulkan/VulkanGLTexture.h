//
// VulkanGLTexture.h
//
// Library: GPU
// Package: GL
// Module:  Vulkan
// 

#pragma once

#include "VulkanCommon.h"
#include "VulkanBarrier.h"

namespace Mmp
{

class Vulkan;

class TextureCopyBatch
{
public:
    using ptr = std::shared_ptr<TextureCopyBatch>;
public:
    TextureCopyBatch();
    ~TextureCopyBatch();
public:
    std::vector<VkBufferImageCopy> copies;
    VkBuffer buffer;
};

class VulkanGLTexture
{
public:
    using ptr = std::shared_ptr<VulkanGLTexture>;
public:
    explicit VulkanGLTexture();
    ~VulkanGLTexture();
public:
    bool Create(VmaAllocator allocator, uint32_t width, uint32_t height, uint32_t depth, uint32_t numMips, VkFormat format, VkImageLayout initialLayout, VkImageUsageFlags usage, VulkanBarrier::ptr barrierBatch);
    void Update(VkCommandBuffer cmd, const std::vector<GLPixelData::ptr>& textureData);
    void FinishCopyBatch(VkCommandBuffer cmd, TextureCopyBatch::ptr copyBatch);
private:
    void PrepareForTransferDst(VkCommandBuffer cmd, int32_t levels);
    void UpdateInternal(VkCommandBuffer cmd, const std::vector<GLPixelData::ptr>& textureData);
    void RestoreAfterTransferDst(VkCommandBuffer cmd, int32_t levels);
    void CopyBufferToMipLevel(VkCommandBuffer cmd, TextureCopyBatch::ptr copyBatch, int32_t mip, int32_t mipWidth, int32_t mipHeight, int32_t depthLayer, VkBuffer buffer, uint32_t offset, size_t rowLength);
    VulkanMemory::ptr GetMemory(size_t size);
public:
    std::string tag;
    uint32_t width;
    uint32_t height;
    uint32_t depth;
    uint32_t numMips;
    VkFormat format;
public:
    VkImage image;
    VmaAllocation allocation;
    VkImageView view;
private:
    std::map<size_t, VulkanMemory::ptr> _memorys; // todo : do global cache
};

} // namespace Mmp