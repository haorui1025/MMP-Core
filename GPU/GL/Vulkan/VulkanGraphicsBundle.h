//
// VulkanGraphicsBundle.h
//
// Library: GPU
// Package: GL
// Module:  Vulkan
// 

#pragma once

#include <memory>

#include "VulkanCommon.h"

namespace Mmp
{

class VulkanGraphicsBundle
{
public:
    using ptr = std::shared_ptr<VulkanGraphicsBundle>;
public:
    VulkanGraphicsBundle();
    ~VulkanGraphicsBundle();
public:
    bool Init();
public:
    VkCommandBuffer GetInitCommandBuffer();
private:
    VkCommandPool   _initCommandPool;
    VkCommandBuffer _initCommandBuffer;
    bool _hasInitCommands;
};

} // namespace Mmp