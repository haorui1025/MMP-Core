#include "VulkanTranslator.h"

#include <cassert>

namespace Mmp
{

VkFormat DataFormatToVkFormat(DataFormat format)
{
	switch (format) 
    {
        case DataFormat::D16: return VK_FORMAT_D16_UNORM;
        case DataFormat::D16_S8: return VK_FORMAT_D16_UNORM_S8_UINT;
        case DataFormat::D24_S8: return VK_FORMAT_D24_UNORM_S8_UINT;
        case DataFormat::D32F: return VK_FORMAT_D32_SFLOAT;
        case DataFormat::D32F_S8: return VK_FORMAT_D32_SFLOAT_S8_UINT;
        case DataFormat::S8: return VK_FORMAT_S8_UINT;

        case DataFormat::R16_UNORM: return VK_FORMAT_R16_UNORM;

        case DataFormat::R16_FLOAT: return VK_FORMAT_R16_SFLOAT;
        case DataFormat::R16G16_FLOAT: return VK_FORMAT_R16G16_SFLOAT;
        case DataFormat::R16G16B16A16_FLOAT: return VK_FORMAT_R16G16B16A16_SFLOAT;
        case DataFormat::R8_UNORM: return VK_FORMAT_R8_UNORM;
        case DataFormat::R8G8_UNORM: return VK_FORMAT_R8G8_UNORM;
        case DataFormat::R8G8B8_UNORM: return VK_FORMAT_R8G8B8_UNORM;
        case DataFormat::R8G8B8A8_UNORM: return VK_FORMAT_R8G8B8A8_UNORM;
        case DataFormat::R4G4_UNORM_PACK8: return VK_FORMAT_R4G4_UNORM_PACK8;

        case DataFormat::R4G4B4A4_UNORM_PACK16: return VK_FORMAT_R4G4B4A4_UNORM_PACK16;
        case DataFormat::B4G4R4A4_UNORM_PACK16: return VK_FORMAT_B4G4R4A4_UNORM_PACK16;
        case DataFormat::R5G5B5A1_UNORM_PACK16: return VK_FORMAT_R5G5B5A1_UNORM_PACK16;
        case DataFormat::B5G5R5A1_UNORM_PACK16: return VK_FORMAT_B5G5R5A1_UNORM_PACK16;
        case DataFormat::R5G6B5_UNORM_PACK16: return VK_FORMAT_R5G6B5_UNORM_PACK16;
        case DataFormat::B5G6R5_UNORM_PACK16: return VK_FORMAT_B5G6R5_UNORM_PACK16;
        case DataFormat::A1R5G5B5_UNORM_PACK16: return VK_FORMAT_A1R5G5B5_UNORM_PACK16;

        case DataFormat::R32_FLOAT: return VK_FORMAT_R32_SFLOAT;
        case DataFormat::R32G32_FLOAT: return VK_FORMAT_R32G32_SFLOAT;
        case DataFormat::R32G32B32_FLOAT: return VK_FORMAT_R32G32B32_SFLOAT;
        case DataFormat::R32G32B32A32_FLOAT: return VK_FORMAT_R32G32B32A32_SFLOAT;

        case DataFormat::BC1_RGBA_UNORM_BLOCK: return VK_FORMAT_BC1_RGBA_UNORM_BLOCK;
        case DataFormat::BC2_UNORM_BLOCK: return VK_FORMAT_BC2_UNORM_BLOCK;
        case DataFormat::BC3_UNORM_BLOCK: return VK_FORMAT_BC3_UNORM_BLOCK;
        case DataFormat::BC4_UNORM_BLOCK: return VK_FORMAT_BC4_UNORM_BLOCK;
        case DataFormat::BC5_UNORM_BLOCK: return VK_FORMAT_BC5_UNORM_BLOCK;
        case DataFormat::BC7_UNORM_BLOCK: return VK_FORMAT_BC7_UNORM_BLOCK;
	    default:
            assert(false);
		    return VK_FORMAT_UNDEFINED;
	}
}

VkCompareOp GLComparisonTypeToVulkanType(Comparison comparison)
{
    switch (comparison)
    {
        case Comparison::NEVER: return VK_COMPARE_OP_NEVER;
        case Comparison::LESS:  return VK_COMPARE_OP_LESS;
        case Comparison::EQUAL: return VK_COMPARE_OP_EQUAL;
        case Comparison::LESS_EQUAL: return VK_COMPARE_OP_LESS_OR_EQUAL;
        case Comparison::GREATER: return VK_COMPARE_OP_GREATER;
        case Comparison::NOT_EQUAL: return VK_COMPARE_OP_NOT_EQUAL;
        case Comparison::GREATER_EQUAL: return VK_COMPARE_OP_GREATER_OR_EQUAL;
        case Comparison::ALWAYS: return VK_COMPARE_OP_ALWAYS;
        default:
            assert(false);
            return VK_COMPARE_OP_NEVER;
    }
}

VkBlendOp GLBlendOpToVulkanType(BlendOp op)
{
    switch (op)
    {
        case BlendOp::ADD: return VK_BLEND_OP_ADD;
        case BlendOp::SUBTRACT: return VK_BLEND_OP_SUBTRACT;
        case BlendOp::REV_SUBTRACT: return VK_BLEND_OP_REVERSE_SUBTRACT;
        case BlendOp::MIN: return VK_BLEND_OP_MIN;
        case BlendOp::MAX: return VK_BLEND_OP_MAX;
        default:
            assert(false);
            return VK_BLEND_OP_ADD;
    }
}

VkBlendFactor GLBlendFactorToVulkanType(BlendFactor bf)
{
    switch (bf)
    {
        case BlendFactor::ZERO: return VK_BLEND_FACTOR_ZERO;
        case BlendFactor::ONE: return VK_BLEND_FACTOR_ONE;
        case BlendFactor::SRC_COLOR: return VK_BLEND_FACTOR_SRC_COLOR;
        case BlendFactor::ONE_MINUS_SRC_COLOR: return VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR;
        case BlendFactor::DST_COLOR: return VK_BLEND_FACTOR_DST_COLOR;
        case BlendFactor::ONE_MINUS_DST_COLOR: return VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
        case BlendFactor::SRC_ALPHA: return VK_BLEND_FACTOR_SRC_ALPHA;
        case BlendFactor::ONE_MINUS_SRC_ALPHA: return VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
        case BlendFactor::DST_ALPHA: return VK_BLEND_FACTOR_DST_ALPHA;
        case BlendFactor::ONE_MINUS_DST_ALPHA: return VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
        case BlendFactor::CONSTANT_COLOR: return VK_BLEND_FACTOR_CONSTANT_COLOR;
        case BlendFactor::ONE_MINUS_CONSTANT_COLOR: return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR;
        case BlendFactor::CONSTANT_ALPHA: return VK_BLEND_FACTOR_CONSTANT_ALPHA;
        case BlendFactor::ONE_MINUS_CONSTANT_ALPHA: return VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA;
        case BlendFactor::SRC1_COLOR: return VK_BLEND_FACTOR_SRC1_COLOR;
        case BlendFactor::ONE_MINUS_SRC1_COLOR: return VK_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR;
        case BlendFactor::SRC1_ALPHA: return VK_BLEND_FACTOR_SRC1_ALPHA;
        case BlendFactor::ONE_MINUS_SRC1_ALPHA: return VK_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA;
        default:
            assert(false);
            return VK_BLEND_FACTOR_ZERO;
    }
}

VkLogicOp GLLogicOpToOpenVulkanType(LogicOp op)
{
    switch (op)
    {
        case LogicOp::CLEAR: return VK_LOGIC_OP_CLEAR;
        case LogicOp::SET: return VK_LOGIC_OP_SET;
        case LogicOp::COPY: return VK_LOGIC_OP_COPY;
        case LogicOp::COPY_INVERTED: return VK_LOGIC_OP_COPY_INVERTED;
        case LogicOp::NOOP: return VK_LOGIC_OP_NO_OP;
        case LogicOp::INVERT: return VK_LOGIC_OP_INVERT;
        case LogicOp::AND: return VK_LOGIC_OP_AND;
        case LogicOp::NAND: return VK_LOGIC_OP_NAND;
        case LogicOp::OR: return VK_LOGIC_OP_OR;
        case LogicOp::NOR: return VK_LOGIC_OP_NOR;
        case LogicOp::XOR: return VK_LOGIC_OP_XOR;
        case LogicOp::EQUIV: return VK_LOGIC_OP_EQUIVALENT;
        case LogicOp::AND_REVERSE: return VK_LOGIC_OP_AND_REVERSE;
        case LogicOp::AND_INVERTED: return VK_LOGIC_OP_AND_INVERTED;
        case LogicOp::OR_REVERSE: return VK_LOGIC_OP_OR_REVERSE;
        case LogicOp::OR_INVERTED: return VK_LOGIC_OP_OR_INVERTED;
        default:
            assert(false);
            return VK_LOGIC_OP_CLEAR;
    }
}

VkPrimitiveTopology GLPrimitiveToOpenVulkanype(Primitive primitive)
{
    switch (primitive)
    {
        case Primitive::POINT_LIST: return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
        case Primitive::LINE_LIST: return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
        case Primitive::LINE_STRIP: return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
        case Primitive::TRIANGLE_LIST: return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        case Primitive::TRIANGLE_STRIP: return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
        case Primitive::TRIANGLE_FAN: return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
        case Primitive::PATCH_LIST: return VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
        case Primitive::LINE_LIST_ADJ: return VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY;
        case Primitive::LINE_STRIP_ADJ: return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY;
        case Primitive::TRIANGLE_LIST_ADJ: return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY;
        case Primitive::TRIANGLE_STRIP_ADJ: return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY;
        default:
            assert(false);
            return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
    }
}

VkStencilOp GLStencilOpToVulkanType(StencilOp op)
{
    switch (op)
    {
        case StencilOp::KEEP: return VK_STENCIL_OP_KEEP;
        case StencilOp::ZERO: return VK_STENCIL_OP_ZERO;
        case StencilOp::REPLACE: return VK_STENCIL_OP_REPLACE;
        case StencilOp::INCREMENT_AND_CLAMP: return VK_STENCIL_OP_INCREMENT_AND_CLAMP;
        case StencilOp::DECREMENT_AND_CLAMP: return VK_STENCIL_OP_DECREMENT_AND_CLAMP;
        case StencilOp::INVERT: return VK_STENCIL_OP_INVERT;
        case StencilOp::INCREMENT_AND_WRAP: return VK_STENCIL_OP_INCREMENT_AND_WRAP;
        case StencilOp::DECREMENT_AND_WRAP: return VK_STENCIL_OP_DECREMENT_AND_WRAP;
        default:
            assert(false);
            return VK_STENCIL_OP_KEEP;
    }
}

VkCullModeFlagBits GLCullModeToVulkanType(CullMode mode)
{
    switch (mode)
    {
        case CullMode::BACK: return VK_CULL_MODE_BACK_BIT;
        case CullMode::FRONT: return VK_CULL_MODE_FRONT_BIT;
        case CullMode::FRONT_AND_BACK: return VK_CULL_MODE_FRONT_AND_BACK;
        case CullMode::NONE: return VK_CULL_MODE_NONE;
        default:
            assert(false);
            return VK_CULL_MODE_BACK_BIT;
    }
}

VkShaderStageFlagBits SLShaderStageToVulkanType(ShaderStage stage)
{
    switch (stage)
    {
        case ShaderStage::VERTEX: return VK_SHADER_STAGE_VERTEX_BIT;
        case ShaderStage::GEOMETRY: return VK_SHADER_STAGE_GEOMETRY_BIT;
        case ShaderStage::COMPUTE: return VK_SHADER_STAGE_COMPUTE_BIT;
        case ShaderStage::FRAGMENT: return VK_SHADER_STAGE_FRAGMENT_BIT;
        default:
            assert(false);
            return VK_SHADER_STAGE_FRAGMENT_BIT;
    }
}

VkSamplerAddressMode GLTextureAddressModeToVulkanType(TextureAddressMode mode)
{
    switch (mode)
    {
        case TextureAddressMode::REPEAT: return VK_SAMPLER_ADDRESS_MODE_REPEAT;
        case TextureAddressMode::REPEAT_MIRROR: return VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
        case TextureAddressMode::CLAMP_TO_EDGE: return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        case TextureAddressMode::CLAMP_TO_BORDER: return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
        default:
            assert(false);
            return VK_SAMPLER_ADDRESS_MODE_REPEAT;
    }
}

VkAttachmentLoadOp GLRPActionToVulkanType(RPAction action)
{
    switch (action)
    {
        case RPAction::CLEAR: return VK_ATTACHMENT_LOAD_OP_CLEAR;
        case RPAction::KEEP: return VK_ATTACHMENT_LOAD_OP_LOAD;
        case RPAction::DONT_CARE: return VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        default:
            assert(false);
            return VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    }
}

VkAttachmentLoadOp GLRPLoadActionToVulkanType(RPAction action)
{
    switch (action)
    {
        case RPAction::STORE: return VK_ATTACHMENT_LOAD_OP_NONE_KHR;
        case RPAction::KEEP: return  VK_ATTACHMENT_LOAD_OP_LOAD;
        case RPAction::CLEAR: return VK_ATTACHMENT_LOAD_OP_CLEAR;
        case RPAction::DONT_CARE: return VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        default:
            assert(false);
            return VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    }
}

VkAttachmentStoreOp GLRPStoreActionToVulkanType(RPAction action)
{
    switch (action)
    {
        case RPAction::STORE: return VK_ATTACHMENT_STORE_OP_STORE;
        case RPAction::DONT_CARE: return VK_ATTACHMENT_STORE_OP_DONT_CARE;
        default:
            assert(false);
            return VK_ATTACHMENT_STORE_OP_DONT_CARE;
    }
}

VkFrontFace GLFacingToVulkanFrontFace(Facing type)
{
    switch (type)
    {
        case Facing::CCW: return VK_FRONT_FACE_COUNTER_CLOCKWISE;
        case Facing::CW:  return VK_FRONT_FACE_CLOCKWISE;
        default:
            assert(false);
            return VK_FRONT_FACE_CLOCKWISE;
    }
}

} // namespace Mmp