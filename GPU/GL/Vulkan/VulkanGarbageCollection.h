//
// VulkanGarbageCollection.h
//
// Library: GPU
// Package: GL
// Module:  Vulkan
// 

#pragma once

#include <vector>
#include <mutex>

#include "VulkanCommon.h"
#include "Common/VulkanDevice.h"

namespace Mmp
{

class VulkanGarbageCollection
{
public:
    using ptr = std::shared_ptr<VulkanGarbageCollection>;
public:
    explicit VulkanGarbageCollection(VmaAllocator allocator);
    ~VulkanGarbageCollection();
public:
    void DeleteVkCommandPool(const VkCommandPool& data);
    void DeleteVkDescriptorPool(const VkDescriptorPool& data);
    void DeleteVkShaderModul(const VkShaderModule& data);
    void DeleteVkBuffer(const VkBuffer& data);
    void DeleteVkBufferView(const VkBufferView& data);
    void DeleteVkImageView(const VkImageView& data);
    void DeleteVkDeviceMemory(const VkDeviceMemory& data);
    void DeleteVkSampler(const VkSampler& data);
    void DeleteVkPipeline(const VkPipeline& data);
    void DeleteVkPipelineCache(const VkPipelineCache& data);
    void DeleteVkRenderPass(const VkRenderPass& data);
    void DeleteVkFramebuffer(const VkFramebuffer& data);
    void DeleteVkPipelineLayout(const VkPipelineLayout& data);
    void DeleteVkDescriptorSetLayout(const VkDescriptorSetLayout& data);
    void DeleteVkQueryPool(const VkQueryPool& data);
    void DeleteVkImageVmaAllocation(const VkImage& image, const VmaAllocation& allocation);
public:
    void Flush();
private:
    std::mutex _mtx;
    VmaAllocator _allocator;
    std::vector<VkCommandPool> _commandPools;
    std::vector<VkDescriptorPool> _descriptorPools;
    std::vector<VkShaderModule> _shaderModules;
    std::vector<VkBuffer> _buffers;
    std::vector<VkBufferView> _bufferViews;
    std::vector<VkImageView> _imageViews;
    std::vector<VkDeviceMemory> _deviceMemories;
    std::vector<VkSampler> _samplers;
    std::vector<VkPipeline> _pipelines;
    std::vector<VkPipelineCache> _pipelineCaches;
    std::vector<VkRenderPass> _renderPasses;
    std::vector<VkFramebuffer> _framebuffers;
    std::vector<VkPipelineLayout> _pipelineLayouts;
    std::vector<VkDescriptorSetLayout> _descriptorSetLayouts;
    std::vector<VkQueryPool> _queryPools;
    std::vector<std::pair<VkImage, VmaAllocation>> _imageAllocs;
};

} // namespace Mmp