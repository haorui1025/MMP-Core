//
// OpenGLUtil.h
//
// Library: GPU
// Package: GL
// Module:  OpenGL
// 

#pragma once

#include <cstdint>
#include <string>

#include "OpenGLCommon.h"

namespace Mmp
{

/**
 * @brief 查询 OpenGL Extension 相关信息
 * @note  1 - 只能调用一次
 *        2 - 需在 context  线程调用
 * @sa    openGLFeature
 */
bool CheckOpenGLExtensions();

/**
 * @brief 获取 OpenGL Extension 相关信息
 * @note  需在调用 CheckOpenGLExtensions 成功后才可调用
 */
OpenGLFeature& GetOpenGLFeature();

/**
 * @brief 比较 OpenGL 版本号
 */
bool OpenGLVersionGreater(const OpenGLFeature& openGLFeature, int major, int minor);

/**
 * @note for debug
 */
void InfoOpenGLFeature(const OpenGLFeature& openGLFeature);

/**
 * @brief  清空所有 OpenGL 错误
 * @note   在渲染线程进入时调用
 */
void ClearError();

/**
 * @brief      检查 OpenGL 是否执行成功
 * @param[in]  file : 文件名
 * @param[in]  line : 所在的代码行
 */
bool CheckGLError(const std::string& file, uint64_t line);

/**
 * @brief     忽略 OpenGL 报错 
 */
void IgnoreGLError();

/**
 * @param[out]   f
 * @param[in]    u
 */
void Uint8x4ToFloat(float f[4], uint32_t u);

/**
 * @param[in]     dataFormat
 * @param[in]     glFeature
 * @param[out]    internalFormat
 * @param[out]    format
 * @param[out]    type
 * @param[out]    alignment
 * @sa    1 - es_spec_3.2.pdf - 8.4.2 Transfer of Pixel Rectangles
 *               Table 8.2: Valid combinations of format, type, and sized internalformat.
 *               Table 8.3: Valid combinations of format, type, and unsized internalformat.
 */
bool DataFormatToGLFormatAndType(DataFormat dataFormat, const OpenGLFeature& glFeature, GLuint& internalFormat, GLuint& format, GLuint& type, int& alignment);

#ifdef ENABLE_OPENGL_DEBUG
#define CHECK_GL_ERROR_IF_DEBUG() assert(CheckGLError(std::string(__FILE__), __LINE__)) 
#define IGNORE_GL_ERROR_IF_DEBUG() IgnoreGLError()
#else
#define CHECK_GL_ERROR_IF_DEBUG()
#define IGNORE_GL_ERROR_IF_DEBUG()
#endif

//
// Hint : 仅调试使用, 某些情况下即使 GL Command Buffer 是按序的, 仍可能出现数据竞争的问题
//        比如由于驱动针对性优化导致管线运行的顺序发生些许变更
//        出现这种问题时需使用 OpenGLFence 处理解决, glFinish 作为早期快速排查的手段
//
#define FORCE_GL_FENCE_SYNC()
// #define FORCE_GL_FENCE_SYNC() glFinish()

} // namespace Mmp