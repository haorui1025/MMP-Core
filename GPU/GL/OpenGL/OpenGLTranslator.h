//
// OpenGLTranslator.h
//
// Library: GPU
// Package: GL
// Module:  OpenGL
// 

#pragma once

#include "OpenGLCommon.h"
#include <string>

namespace Mmp
{

GLenum GLShaderStageToOpenGLType(ShaderStage stage);

GLenum GLTextureTypeToOpenGLType(TextureType type);

GLenum GLComparisonTypeToOpenGLType(Comparison comparison);
std::string OpenGLComparisonToStr(GLenum comparison);

GLenum GLLogicOpToOpenGLType(LogicOp op);

GLuint GLStencilOpToOpenGLType(StencilOp op);
std::string OpenGLtencilOpToStr(GLuint op);

GLuint GLBlendFactorToOpenGLType(BlendFactor bf);
std::string OpenGLBlendFactorToStr(GLuint bf);

GLuint GLBlendOpToOpenGLType(BlendOp op);
std::string OpenGLBlendOpToStr(GLuint op);

GLuint GLTextureAddressModeToOpenGLType(TextureAddressMode mode);
std::string OpenGLTextureAddressModeToStr(GLuint mode);

GLuint GLTextureFilterToOpenGLType(TextureFilter tf);
std::string OpenGLTextureFilterToStr(GLuint tf);

GLuint GLFacingToOpenGLType(Facing face);

GLuint GLCullModeToOpenGLType(CullMode mode);

GLuint GLPrimitiveToOpenGLType(Primitive primitive);

GLuint GenerateMipFilterOpenGLType(TextureFilter minTf, TextureFilter mipTf);

} // namespace Mmp