#include "OpenGL.h"

#include "OpenGLUtil.h"
#include "OpenGLContex.h"
#include "OpenGLStepData.h"
#include "OpenGLInitStepData.h"
#include "OpenGLRenderData.h"

namespace Mmp
{

constexpr GLuint kDeafultFBO = 0;

GLenum OpenGL::FBOGetFBTarget(bool read /* !read -> write */, GLuint** cached)
{
    bool supportsBlit = _openGLFeature.ARB_framebuffer_object;
    if (_openGLFeature.IsGLES)
    {
        supportsBlit = _openGLFeature.GLES3 || _openGLFeature.NV_framebuffer_blit;
    }
    // Note: GL_FRAMEBUFFER_EXT and GL_FRAMEBUFFER have the same value, same with _NV.
    if (supportsBlit)
    {
        if (read)
        {
            *cached = &_currentReadHandle;
            return GL_READ_FRAMEBUFFER;
        }
        else
        {
            *cached = &_currentDrawHandle;
            return GL_DRAW_FRAMEBUFFER;
        }
    }
    else
    {
        *cached = &_currentDrawHandle;
        return GL_FRAMEBUFFER;
    }
}

void OpenGL::FBOExtCreate(Any data)
{
    assert(data.type() == typeid(OpenGLCreateFramebufferInitData));
    const OpenGLCreateFramebufferInitData& createFrameBufferData = RefAnyCast<OpenGLCreateFramebufferInitData>(data);
    OpenGLRenderFrameBuffer::ptr fbo = createFrameBufferData.frameBuffer;
    
    CHECK_GL_ERROR_IF_DEBUG(); 

    // Color texture is same everywhere
    glGenFramebuffersEXT(1, &fbo->handle);
    if (_openGLFeature.ARB_direct_state_access && _openGLFeature._glCreateTextures)
    {
        _openGLFeature._glCreateTextures(fbo->colorTextures[0]->target, 1, &fbo->colorTextures[0]->texture);
    }
    else
    {
        glGenTextures(1, &fbo->colorTextures[0]->texture);
    }

    // Create the surfaces.
    fbo->colorTextures[0]->target     = GL_TEXTURE_2D;
    glBindTexture(fbo->colorTextures[0]->target, fbo->colorTextures[0]->texture);
    glTexImage2D(fbo->colorTextures[0]->target, 0, GL_RGBA, fbo->width, fbo->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);

    fbo->colorTextures[0]->wrapS      = GL_CLAMP_TO_EDGE;
    fbo->colorTextures[0]->wrapT      = GL_CLAMP_TO_EDGE;
    fbo->colorTextures[0]->magFilter  = GL_LINEAR;
    fbo->colorTextures[0]->minFilter  = GL_LINEAR;
    fbo->colorTextures[0]->maxLod     = 0.0f;
    glTexParameteri(fbo->colorTextures[0]->target, GL_TEXTURE_WRAP_S, fbo->colorTextures[0]->wrapS);
    glTexParameteri(fbo->colorTextures[0]->target, GL_TEXTURE_WRAP_T, fbo->colorTextures[0]->wrapT);
    glTexParameteri(fbo->colorTextures[0]->target, GL_TEXTURE_MAG_FILTER, fbo->colorTextures[0]->magFilter);
    glTexParameteri(fbo->colorTextures[0]->target, GL_TEXTURE_MIN_FILTER, fbo->colorTextures[0]->minFilter);

    fbo->stencilBuffer = 0;
    fbo->zBuffer       = 0;
    // 24-bit Z, 8-bit stencil
    glGenRenderbuffers(1, &fbo->zStencilBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, fbo->zStencilBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_STENCIL_EXT, fbo->width, fbo->height);

	// Bind it all together
	glBindFramebuffer(GL_FRAMEBUFFER, fbo->handle);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, fbo->colorTextures[0]->target, fbo->colorTextures[0]->texture, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, fbo->zStencilBuffer);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_RENDERBUFFER, fbo->zStencilBuffer);

    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    switch (status)
    {
        case GL_FRAMEBUFFER_COMPLETE:
            GL_LOG_TRACE << "GL_FRAMEBUFFER_COMPLETE";
            break;
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
            GL_LOG_ERROR << "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT";
            break;
        default:
            GL_LOG_ERROR << "Other framebuffer error, stauts code is: " << status;
            assert(false);
            break;
    }
	// Unbind state we don't need
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	glBindTexture(fbo->colorTextures[0]->target, 0);
    
    CHECK_GL_ERROR_IF_DEBUG();

    _currentDrawHandle = fbo->handle;
    _currentReadHandle = fbo->handle;
}

void OpenGL::FBOBindFBTarget(bool read /* !read -> write */, GLuint name, OpenGLRenderFrameBuffer::ptr framebuffer)
{
    //
    // Workaround : 由于 Readback Image 和 OpenGLFrameBuffer 析构时会破坏 GL 的状态,
    //              导致 OpenGL (指本对象) 缓存数据不可信
    //              当前情况在外部尽力合并渲染, 内部不在缓存合并 FrameBuffer
    //
    glBindFramebuffer(GL_FRAMEBUFFER, name);
    if (framebuffer && framebuffer->customColor)
    {
        OpenGLRenderTexture::ptr tex = framebuffer->colorTextures[0];
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex->target, tex->texture, 0);
        glViewport(0, 0, tex->w, tex->h);
    }
    CHECK_GL_ERROR_IF_DEBUG();
}

void OpenGL::FBOUnbind()
{
    CHECK_GL_ERROR_IF_DEBUG();
    glBindFramebuffer(GL_FRAMEBUFFER, kDeafultFBO);
    _currentDrawHandle = 0;
    _currentReadHandle  = 0;
    CHECK_GL_ERROR_IF_DEBUG();
}

void OpenGL::PerformBindFramebufferAsRenderTarget(const OpenGLRenderStep& pass)
{
    assert(pass.data.type() == typeid(OpenGLRenderData));
    const OpenGLRenderData& renderData = RefAnyCast<OpenGLRenderData>(pass.data);
    if (renderData.framebuffer)
    {
        _currentFrameBufferWidth  = renderData.framebuffer->width;
        _currentFrameBufferHeight = renderData.framebuffer->height;
    }
    else
    {
        _currentFrameBufferWidth  = _targetWidth;
        _currentFrameBufferHeight = _targeHeight;
    }

    if (renderData.framebuffer)
    {
        FBOBindFBTarget(false, renderData.framebuffer->handle, renderData.framebuffer);
    }
    else if (_currentFrameBuffer /* && !enderData.framebuffer */)
    {
        FBOUnbind();
    }

    // Hint : 延迟触发 OpenGLRenderFrameBuffer 析构, 避免导致当前 GL 状态异常
    if (_currentFrameBuffer)
    {
        std::lock_guard<std::recursive_mutex> recyclingLock(_recyclingMtx);
        _recycling.push_back(_currentFrameBuffer);
    }

    _currentFrameBuffer = renderData.framebuffer;

    CHECK_GL_ERROR_IF_DEBUG();
}

} // namespace Mmp