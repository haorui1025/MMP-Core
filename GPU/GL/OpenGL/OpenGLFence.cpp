#include "OpenGL.h"
#include "OpenGLFence.h"

#include "OpenGLDrawContex.h"

namespace Mmp
{

void OpenGLBasicFence::AsyncSync()
{

}

/**
 * @note 理论上若是不支持 Fence 的话, 需要使用 glFinish 替代,
 *       但是 glFinish 的成本着实有点高, 实际上看就算没有调用 glFinish
 *       也没有什么问题, 故再观测看看
 */
class OpenGLNativeFence : public OpenGLBasicFence
{
public:
    void Flush() override;
private:
    void AsyncSync() override;
};

void OpenGLNativeFence::AsyncSync()
{

}

void OpenGLNativeFence::Flush()
{
    
}

class OpenGLFence : public OpenGLBasicFence,
                    public std::enable_shared_from_this<OpenGLFence>
{
public:
    explicit OpenGLFence(const OpenGLFeature& feature, OpenGL::ptr openGL);
    ~OpenGLFence();
public:
    void Flush() override;
public:
    void Sync() override;
private:
    void AsyncSync() override;
private:
    std::mutex _mtx;
    std::condition_variable _cond;
    OpenGL::ptr _openGL;
private:
	PFNGLWAITSYNCPROC _glWaitSync;
	PFNGLCLIENTWAITSYNCPROC _glClientWaitSync;
	PFNGLDELETESYNCPROC _glDeleteSync;
	PFNGLFENCESYNCPROC _glFenceSync;
    GLsync _sync;
};

OpenGLFence::OpenGLFence(const OpenGLFeature& feature, OpenGL::ptr openGL)
{
    _openGL = openGL;
    _glWaitSync = feature._glWaitSync;
    _glClientWaitSync = feature._glClientWaitSync;
    _glDeleteSync = feature._glDeleteSync;
    _glFenceSync = feature._glFenceSync;
    _sync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);
}

OpenGLFence::~OpenGLFence()
{
    if (_sync)
    {
        if (_openGL)
        {
            _openGL->ReleaseResource([__glDeleteSync = _glDeleteSync, sync = _sync]
            {
                __glDeleteSync(sync);
            });
        }
        else
        {
            _glDeleteSync(_sync);
        }
        _sync = nullptr;
    }
}

void OpenGLFence::Flush()
{
    glFlush();
}

void OpenGLFence::Sync()
{
    std::unique_lock<std::mutex> lock(_mtx);
    if (_sync)
    {
        _openGL->CommitCustom([this]()
        {
            AsyncSync();
        });
    }
    _cond.wait(lock);
}

void OpenGLFence::AsyncSync()
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (_sync)
    {
        _glWaitSync(_sync, 0, GL_TIMEOUT_IGNORED);
        _glClientWaitSync(_sync, GL_SYNC_FLUSH_COMMANDS_BIT, GL_TIMEOUT_IGNORED);
        _glDeleteSync(_sync);
        _sync = nullptr;
        _cond.notify_all();
    }
}

#if MMP_PLATFORM(LINUX)

static std::string EGLErrorToStr(EGLint error)
{
    switch (error)
    {
        case EGL_SUCCESS: return "EGL_SUCCESS";
        case EGL_NOT_INITIALIZED: return "EGL_NOT_INITIALIZED";
        case EGL_BAD_ACCESS: return "EGL_BAD_ACCESS";
        case EGL_BAD_ALLOC: return "EGL_BAD_ALLOC";
        case EGL_BAD_ATTRIBUTE: return "EGL_BAD_ATTRIBUTE";
        case EGL_BAD_CONTEXT: return "EGL_BAD_CONTEXT";
        case EGL_BAD_CONFIG: return "EGL_BAD_CONFIG";
        case EGL_BAD_CURRENT_SURFACE: return "EGL_BAD_CURRENT_SURFACE";
        case EGL_BAD_DISPLAY: return "EGL_BAD_DISPLAY";
        case EGL_BAD_SURFACE: return "EGL_BAD_SURFACE";
        case EGL_BAD_MATCH: return "EGL_BAD_MATCH";
        case EGL_BAD_PARAMETER: return "EGL_BAD_PARAMETER";
        case EGL_BAD_NATIVE_PIXMAP: return "EGL_BAD_NATIVE_PIXMAP";
        case EGL_BAD_NATIVE_WINDOW: return "EGL_BAD_NATIVE_WINDOW";
        case EGL_CONTEXT_LOST: return "EGL_CONTEXT_LOST";
        case EGL_TIMEOUT_EXPIRED: return "EGL_TIMEOUT_EXPIRED";
        default:  return "EGL_UNKONWN";
    }
}

class OpenGLEGLFence : public OpenGLBasicFence,
                       public std::enable_shared_from_this<OpenGLEGLFence>
{
public:
    explicit OpenGLEGLFence(const OpenGLFeature& feature, OpenGL::ptr openGL);
    ~OpenGLEGLFence();
public:
    void Flush() override;
public:
    void Sync() override;
private:
    void AsyncSync() override;
private:
    std::mutex _mtx;
    std::condition_variable _cond;
    OpenGL::ptr _openGL;
private:
	EGLDisplay _eglDisplay;
	PFNEGLCREATESYNCKHRPROC _eglCreateSyncKHR;
	PFNEGLCLIENTWAITSYNCKHRPROC _eglClientWaitSyncKHR;
	PFNEGLDESTROYSYNCKHRPROC _eglDestroySyncKHR;
    EGLSyncKHR _sync;
};

OpenGLEGLFence::OpenGLEGLFence(const OpenGLFeature& feature, OpenGL::ptr openGL)
{
    _openGL = openGL;
    _eglDisplay = feature._eglDisplay;
    _eglCreateSyncKHR = feature._eglCreateSyncKHR;
    _eglClientWaitSyncKHR = feature._eglClientWaitSyncKHR;
    _eglDestroySyncKHR = feature._eglDestroySyncKHR;
    _sync = _eglCreateSyncKHR(_eglDisplay, EGL_SYNC_FENCE_KHR, NULL);
    assert(_sync);
}

OpenGLEGLFence::~OpenGLEGLFence()
{
    if (_sync)
    {
        if (_openGL)
        {
            _openGL->ReleaseResource([__eglDestroySyncKHR = _eglDestroySyncKHR, eglDisplay = _eglDisplay, sync = _sync]
            {
                __eglDestroySyncKHR(eglDisplay, sync);
            });
        }
        else
        {
            _eglDestroySyncKHR(_eglDisplay, _sync);
        }
        _sync = nullptr;
    }

}

void OpenGLEGLFence::Flush()
{
    glFlush();
}

void OpenGLEGLFence::Sync()
{
    std::unique_lock<std::mutex> lock(_mtx);
    if (_sync)
    {
        _openGL->CommitCustom([this]()
        {
            AsyncSync();
        });
    }
    _cond.wait(lock);
}

void OpenGLEGLFence::AsyncSync()
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (_sync != nullptr)
    {
        EGLint result = _eglClientWaitSyncKHR(_eglDisplay, _sync, 0, EGL_FOREVER_KHR);
        if (result != EGL_CONDITION_SATISFIED)
        {
            GL_LOG_ERROR << "eglClientWaitSyncKHR wait fial, sync is: " << (void*)_sync  << " , error str is: " << EGLErrorToStr(result) << ", code is: " << result;
        }
        _eglDestroySyncKHR(_eglDisplay, _sync);
        _sync = nullptr;
        _cond.notify_all();
    }
}

#endif

OpenGLBasicFence::ptr CreateOpenGLFence(const OpenGLFeature& openGLFeature, OpenGL::ptr openGL)
{
    if (0)
    {
        return nullptr;
    }
    else if (openGLFeature.ARB_sync)
    {
        return std::make_shared<OpenGLFence>(openGLFeature, openGL);
    }
#if MMP_PLATFORM(LINUX)
    else if (openGLFeature._EGL_KHR_wait_sync)
    {
        return std::make_shared<OpenGLEGLFence>(openGLFeature, openGL);
    }
#endif
    else
    {
        return std::make_shared<OpenGLNativeFence>();
    }
    assert(false);
    return nullptr;
}

} // namespace Mmp