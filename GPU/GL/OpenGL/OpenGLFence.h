//
// OpenGLFence.h
//
// Library: GPU
// Package: GL
// Module:  OpenGL
// 

#pragma once

#include <vector>
#include <memory>
#include <functional>

#include "GLContex.h"

namespace Mmp
{

class OpenGL;

class OpenGLBasicFence : public GLFence
{
public:
	using ptr = std::shared_ptr<OpenGLBasicFence>;
public:
	virtual void Flush();
private:
	/**
	 * @brief      GL 所有操作都需要在 GL 线程里操作, 所以尽管是 Sync 也不能同步调用，
	 *             需要异步到 GL 线程执行, 然后异步等待, 由异步转同步 
	 */
	virtual void AsyncSync();
	friend class OpenGL;
};

OpenGLBasicFence::ptr CreateOpenGLFence(const OpenGLFeature& openGLFeature, std::shared_ptr<OpenGL> openGL = nullptr);

} // namespace Mmp