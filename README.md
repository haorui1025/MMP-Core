# README

简体中文 | [English](./README_en.md)

## 简介

MMP-Core (Multi Media Plug-in Core) 多媒体插件核心基础库;
旨在屏蔽硬件、平台差异, 为上层提供一致性接口.

## 特点

- 可靠的对象声明周期管理 : 充分使用 `RAII` 的特性
- 丰富的拓展潜力 : 采用接口类和工厂的编写方式, 便于二次开发
- 优质的教学示例 : 从零开发, 直观的调用主流程

## 适合人员

`MMP-Core` 应当比较适合端侧音视频开发人员, 涵盖 二维GPU处理, 编解码流程 等.

## 编译

`MMP` 目前仅支持运行在 `linux` 和 `windows` 平台, *支持运行在Linux非桌面发型版本*.

### Linux 桌面发型系统 (debian为例)

```shell
# debian 环境下
# [option] USE_OPENGL
sudo apt install libgl1-mesa-dev* nasm
# [option] USE_SDL
sudo apt install libsdl2-dev
# [option] USE_X11
sudo apt install libx11-dev
# [option] USE_GBM
sudo apt install libgbm-dev
# [option] USE_GBM 、USE_VAAPI
sudo apt install libdrm-dev
# [option] USE_VAAPI
sudo apt install libva-dev
rm -rf build
mkdir build
git submodule update --init --recursive
cd build
cmake .. -DUSE_OPENGL=ON -DUSE_EGL=ON -DUSE_GBM=ON -DUSE_SDL=ON -DUSE_X11=ON -DUSE_VAAPI=ON
make -j4
```

### Linux 原生系统

在许多场景下,嵌入式 linux 操作系统通常为定制系统,而非 `debian`, 这时可以通过以下命令进行**初步**构建:

```shell
sudo apt install nasm
rm -rf build
mkdir build
git submodule update --init --recursive
cd build
cmake ..
make -j4
```

## Windows (MSVC 2022 x86)

```shell
git submodule update --init --recursive
mkdir build
cmake --no-warn-unused-cli -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -S. -B./build -G "Visual Studio 17 2022" -T host=x86 -A win32
cmake --build build --config Release -j 16
```

## Windows (MSVC 2022 x64)

```shell
git submodule update --init --recursive
mkdir build
cmake --no-warn-unused-cli -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -S. -B./build -G "Visual Studio 17 2022" -T host=x64 -A x64
cmake --build build --config Release -j 16
```

>
> `Windows` 也可以直接使用 `IDE` 进行构建, 在构建时选择对应的工具链即可.
>

### 编译选项

- USE_EGL           : Windows Backend EGL(Android)
- USE_X11           : Windows Backend X11(Desktop)
- USE_SDL           : Windows Backend SDL(Desktop)
- USE_OPENGL        : Graphics Library OpenGL(ES3)
- USE_D3D           : Graphics Libray Direct 3D
- USE_VULKAN        : Graphics Library Vulkan
- USE_VAAPI         : An implemention for VA-API(Video Acceleration API)
- USE_SPRIV_CROSS   : Use to convert SPRIV to other shader languages
- USE_GBM           :  EGL backend GBM
- USE_API_RPC       : PC(Remote Process Communication) Interface (implement through HTTP OR WEBSOCKET etc.)
- USE_OPENH264      : H264 Software Encoder/Decoder
- USE_ROCKCHIP      : Rockchip hardware accelerate (VENC, VDEC, RGA etc.)
- NOT_AUTO_EGL      : Not auto append EGL dependence

## 工程结构

- Common : 基础组件库
- Extension : 第三方依赖
  - eigen : 矩阵运算
  - poco : 公共基础组件
  - GLEW : OpenGL 拓展辅助
  - lodepng : 轻量级 png 编解码实现
  - Vulkan-Headers : Vulkan 头文件
  - VulkanMemoryAllocator : vulkan 内存分配管理
- GPU : 通用 GPU 基础库
  - Windows: DXGI, EGL, SDL, Vulkan, X11
  - GL : D3D11, OpenGL, Vulkan
  - PG : 内置常用 GPU 功能
- Codec : 编解码器
  - PNG : PNG 编解码实现
  - H264 : H264 语法解析
  - VAAPI : LIBVA 硬编解码
  - D3D11 : D3D11 硬编解码
  - D3DVA : D3D12 硬编解码
  - Vulkan : Vulkan 硬编解码
- RPC : 远端调用
- Cmake : 通用 Cmake 实现 或 `Submoudle Cmake Wrapper`
- LICENSES : 使用到依赖库的相关 LICENSE

## 使用示例

[mmp_sample](https://github.com/HR1025/mmp_sample) 以 `MMP-Core` 作为核心, 展示了 `MMP-Core` 的一些使用方式.

## 项目现状

编解码方向:

- 基于适配 D3D11 、LIBVA 、Vulkan 、D3D12 H264 解码实现 (D3D11,LIBVA和D3D12主流已跑通, Vulkan)
- 基于适配 D3D11 、LIBVA 、Vulkan 、D3D12 H264 编码实现
- 适配 HDR 场景
- 基于适配 D3D11 、LIBVA 、Vulkan 、D3D12 HEVC 编码实现
- 基于适配 D3D11 、LIBVA 、Vulkan 、D3D12 AV1 编码实现

GPU 方向:

- OpenGLES 、 D3D11、 Vulkan 、D3D12 跨平台实现 (OpenGLES、D3D11已实现, 优先支持 Vulkan, 尔后 D3D12)

视频压缩格式:

- HEVC 码流解析及硬编解码上下文相关信息计算 (See `https://github.com/HR1025/MMP-H26X`)

>
> MMP-Core 从第一行代码至今应该也快两年的时间了 (2024.08)
>
> 虽然已有着数万行不小的项目规模, 当离可商用的规模确实还差了挺大一截 ...
>
> 想法很多, 但路却是要踏实地走
>
> 目前来看确实任重而道远,如果对这个项目感兴趣,请不要吝啬你的 `PR`
>