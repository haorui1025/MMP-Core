# README

English | [简体中文](./README.md)

## Brief

MMP-Core is short for *Multi Media Plug-in Core*. It aims to abstract hardware and platform differences, providing a consistent interface for upper layers.

## Features

- Reliable object lifecycle management: Fully utilizes the `RAII` (Resource Acquisition Is Initialization) principle.
- Rich extension potential: Uses interface classes and factory patterns, making it easy for secondary development.
- High-quality teaching examples: Developed from scratch, providing an intuitive main process flow.

## Suitable Audience

`MMP-Core` is suitable for edge-side audio and video developers, covering 2D GPU processing, encoding and decoding processes, etc.

## Compilation

`MMP` currently supports running on `Linux` and `Windows` platforms, and *supports running on non-desktop Linux distributions*.

### Linux Desktop Distribution (e.g., Debian)

```shell
# On Debian environment
# [option] USE_OPENGL
sudo apt install libgl1-mesa-dev* nasm
# [option] USE_SDL
sudo apt install libsdl2-dev
# [option] USE_X11
sudo apt install libx11-dev
# [option] USE_GBM
sudo apt install libgbm-dev
# [option] USE_GBM 、USE_VAAPI
sudo apt install libdrm-dev
# [option] USE_VAAPI
sudo apt install libva-dev
rm -rf build
mkdir build
git submodule update --init --recursive
cd build
cmake .. -DUSE_OPENGL=ON -DUSE_EGL=ON -DUSE_GBM=ON -DUSE_SDL=ON -DUSE_X11=ON -DUSE_VAAPI=ON
make -j4
```

### Native Linux System

In many scenarios, embedded Linux operating systems are usually custom systems rather than Debian. In this case, you can perform an initial build with the following commands:

```shell
sudo apt install nasm
rm -rf build
mkdir build
git submodule update --init --recursive
cd build
cmake ..
make -j4
```

## Windows (MSVC 2022 x86)

```shell
git submodule update --init --recursive
mkdir build
cmake --no-warn-unused-cli -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -S. -B./build -G "Visual Studio 17 2022" -T host=x86 -A win32
cmake --build build --config Release -j 16
```

## Windows (MSVC 2022 x64)

```shell
git submodule update --init --recursive
mkdir build
cmake --no-warn-unused-cli -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -S. -B./build -G "Visual Studio 17 2022" -T host=x64 -A x64
cmake --build build --config Release -j 16
```

## Compilation Options

- USE_EGL           : Windows Backend EGL(Android)
- USE_X11           : Windows Backend X11(Desktop)
- USE_SDL           : Windows Backend SDL(Desktop)
- USE_OPENGL        : Graphics Library OpenGL(ES3)
- USE_D3D           : Graphics Libray Direct 3D
- USE_VULKAN        : Graphics Library Vulkan
- USE_VAAPI         : An implemention for VA-API(Video Acceleration API)
- USE_SPRIV_CROSS   : Use to convert SPRIV to other shader languages
- USE_GBM           :  EGL backend GBM
- USE_API_RPC       : PC(Remote Process Communication) Interface (implement through HTTP OR WEBSOCKET etc.)
- USE_OPENH264      : H264 Software Encoder/Decoder
- USE_ROCKCHIP      : Rockchip hardware accelerate (VENC, VDEC, RGA etc.)
- NOT_AUTO_EGL      : Not auto append EGL dependence

## Project Structure

- Common : Basic component library
- Extension : Third-party dependencies
  - eigen : Matrix operations
  - poco : Common basic components
  - GLEW : OpenGL extension helper
  - lodepng : Lightweight PNG encoding/decoding implementation
  - Vulkan-Headers : Vulkan headers
  - VulkanMemoryAllocator : Vulkan memory allocation management
- GPU : General GPU basic library
  - Windows: DXGI, EGL, SDL, Vulkan, X11
  - GL : D3D11, OpenGL, Vulkan
  - PG : Built-in common GPU functions
- Codec : Codec
PNG : PNG encoding/decoding implementation
  - H264 : H264 syntax parsing
  - VAAPI : LIBVA hardware encoding/decoding
  - D3D11 : D3D11 hardware encoding/decoding
  - D3DVA : D3D12 hardware encoding/decoding
  - Vulkan : Vulkan hardware encoding/decoding
  - RPC : Remote procedure call
- Cmake : General Cmake implementation or Submodule Cmake Wrapper
- LICENSES : Related LICENSE of dependencies

## Usage Example

[mmp_sample](https://github.com/HR1025/mmp_sample) uses `MMP-Core` as its core to demonstrate various usage scenarios of `MMP-Core`.

## Project Status

### Codec direction

- Adaptation of D3D11, LIBVA, Vulkan, D3D12 H264 decoding implementation (D3D11, LIBVA, and D3D12 mainstream have been run through, Vulkan)
- Adaptation of D3D11, LIBVA, Vulkan, D3D12 H264 encoding implementation
- Adaptation for HDR scenarios
- Adaptation of D3D11, LIBVA, Vulkan, D3D12 HEVC encoding implementation
- Adaptation of D3D11, LIBVA, Vulkan, D3D12 AV1 encoding implementation

### GPU direction

- Cross-platform implementation of OpenGLES, D3D11, Vulkan, D3D12 (OpenGLES, D3D11 have been implemented, Vulkan is prioritized, followed by D3D12)

### Video compression format:

- HEVC stream parsing and hardware encoding/decoding context-related information calculation (See `https://github.com/HR1025/MMP-H26X`)

>
> MMP-Core has been in development for almost two years since the first line of code (2024.08).
>
> Although it has grown to a project with tens of thousands of lines of code, it is still far from being commercially viable...
>
> There are many ideas, but the road must be walked steadily.
>
> It seems that there is still a long way to go. If you are interested in this project, please do not hesitate to contribute your PR.
