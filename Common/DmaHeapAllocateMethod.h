//
// DmaHeapAllocateMethod.h
//
// Library: Common
// Package: Memory
// Module:  Memory Allocate
// 
//

#pragma once

#include <cstdint>
#include <mutex>


#include "Common.h"
#include "AbstractDeviceAllocateMethod.h"

#if MMP_PLATFORM(LINUX)

namespace Mmp
{

class DmaHeapAllocateMethod : public AbstractDeviceAllocateMethod
{
public:
    using ptr = std::shared_ptr<DmaHeapAllocateMethod>;
public:
    DmaHeapAllocateMethod();
    virtual ~DmaHeapAllocateMethod();
public:
    void* Malloc(size_t size) override;
    void* Resize(void* data, size_t size) override;
    void* GetAddress(uint64_t offset) override;
    const std::string& Tag() override;
public:
    void Map() override;
    void UnMap() override;
public:
    int GetFd() override;
    uint64_t GetFlags() override;
public:
    static constexpr uint64_t kArmAFBC = 1 << 24u;
public:
    uint64_t _flags;
private:
    std::mutex _mtx;
    int _fd;
    void* _data;
    size_t _len;
private:
    AbstractDeviceAllocateMethod::ptr _drmAllocate;
};

} // namespace Mmp

#endif