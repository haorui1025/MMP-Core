#include "VulkanMemory.h"
#include "VulkanDevice.h"
#include "vulkan/vulkan_core.h"

#include <cassert>

namespace Mmp
{

VulkanMemory::VulkanMemory()
{
    buffer = VK_NULL_HANDLE;
    allocation = VK_NULL_HANDLE;
    address = nullptr;

    size = 0;
    sharingMode = VK_SHARING_MODE_MAX_ENUM;
    bufferUsage = VK_BUFFER_USAGE_FLAG_BITS_MAX_ENUM;
    memoryUsage = VMA_MEMORY_USAGE_UNKNOWN;
}

VulkanMemory::~VulkanMemory()
{
    UnMap();
    if (buffer != VK_NULL_HANDLE && allocation != VK_NULL_HANDLE)
    {
        vmaDestroyBuffer(VulkanDevice::Singleton()->GetVmaAllocator(), buffer, allocation);
    }
}

bool VulkanMemory::Init()
{
    VkResult vkRes = VK_SUCCESS;
    VkBufferCreateInfo bufferCreateInfo = {};
    VmaAllocationCreateInfo allocationCreateInfo = {};
    VmaAllocationInfo allocationInfo = {};
    {
        bufferCreateInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferCreateInfo.size = size;
        bufferCreateInfo.usage = bufferUsage;
        bufferCreateInfo.sharingMode = sharingMode;
    }
    {
        allocationCreateInfo.usage = memoryUsage;
    }
    vkRes = vmaCreateBuffer(VulkanDevice::Singleton()->GetVmaAllocator(), &bufferCreateInfo, &allocationCreateInfo, &buffer, &allocation, &allocationInfo);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vmaCreateBuffer fail, error is: " << VkResultToStr(vkRes);
        assert(false);
    }
    return VULKAN_FAILED(vkRes);
}

void VulkanMemory::Map()
{
    if (!address && allocation != VK_NULL_HANDLE)
    {
        VkResult vkRes = VK_SUCCESS;
        vkRes = vmaMapMemory(VulkanDevice::Singleton()->GetVmaAllocator(), allocation, (void**)&address);
        if (VULKAN_FAILED(vkRes))
        {
            VULKAN_LOG_ERROR << "vmaMapMemory fail, error is: " << VkResultToStr(vkRes);
            address = nullptr;
            assert(false);
        }
    }
}

void VulkanMemory::UnMap()
{
    if (address && allocation != VK_NULL_HANDLE)
    {
        vmaUnmapMemory(VulkanDevice::Singleton()->GetVmaAllocator(), allocation);
        address = nullptr;
    }
}

} // namespace Mmp
