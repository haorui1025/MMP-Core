//
// VulkanMemory.h
//
// Library: Common
// Package: Vulkan
// Module:  Device
// 

#pragma once

#include <memory>

#include "VulkanDevice.h"

namespace Mmp
{

class VulkanMemory
{
public:
    using ptr = std::shared_ptr<VulkanMemory>;
public:
    VulkanMemory();
    ~VulkanMemory();
public:
    bool Init();
    void Map();
    void UnMap();
public:
    uint64_t size;
    VkSharingMode      sharingMode;
    VkBufferUsageFlags bufferUsage;
    VmaMemoryUsage     memoryUsage;
public:
    VkBuffer buffer;
    VmaAllocation allocation;
    uint8_t* address;
};

} // namespace Mmp
