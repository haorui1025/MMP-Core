//
// D3DDevice.h
//
// Library: Common
// Package: D3D
// Module:  Device
// 

#pragma once

#if defined(USE_D3D)

#include "Common.h"
#include "LogMessage.h"

#include <mutex>
#include <memory>

#include <initguid.h>
#include <dxva.h>
#include <dxgi.h>
#include <d3d11.h>
#include <d3dcompiler.h>

#define D3D11_SUCCEEDED(hr) (((HRESULT)(hr)) >= 0)
#define D3D11_FAILED(hr)    (((HRESULT)(hr)) < 0)

#define D3D11_OBJECT_RELEASE(resource)                  \
                                                        if (resource)\
                                                        {\
                                                            resource->Release();\
                                                            resource = nullptr;\
                                                        }

#define D3D11_LOG_TRACE   MMP_MLOG_TRACE("D3D11")    
#define D3D11_LOG_DEBUG   MMP_MLOG_DEBUG("D3D11")    
#define D3D11_LOG_INFO    MMP_MLOG_INFO("D3D11")     
#define D3D11_LOG_WARN    MMP_MLOG_WARN("D3D11")     
#define D3D11_LOG_ERROR   MMP_MLOG_ERROR("D3D11")    
#define D3D11_LOG_FATAL   MMP_MLOG_FATAL("D3D11")   

namespace Mmp 
{

class D3D11Device
{
public:
    using ptr = std::shared_ptr<D3D11Device>;
public:
    D3D11Device() = default;
    virtual ~D3D11Device() = default;
public:
    static D3D11Device::ptr Singleton();
public: /* ID3D11Device */
    virtual ID3D11Device* GetD3D11Device() = 0;
    virtual HRESULT QueryInterface(REFIID riid, void **ppvObject) = 0;
    virtual HRESULT CreateTexture2D(const D3D11_TEXTURE2D_DESC *pDesc, const D3D11_SUBRESOURCE_DATA *pInitialData, ID3D11Texture2D **ppTexture2D) = 0;
    virtual HRESULT CreateRenderTargetView(ID3D11Resource *pResource, const D3D11_RENDER_TARGET_VIEW_DESC *pDesc, ID3D11RenderTargetView **ppRTView) = 0;
    virtual HRESULT CreateDepthStencilView(ID3D11Resource *pResource, const D3D11_DEPTH_STENCIL_VIEW_DESC *pDesc, ID3D11DepthStencilView **ppDepthStencilView) = 0;
    virtual HRESULT CreateBlendState(const D3D11_BLEND_DESC *pBlendStateDesc, ID3D11BlendState **ppBlendState) = 0;
    virtual HRESULT CreateSamplerState(const D3D11_SAMPLER_DESC *pSamplerDesc, ID3D11SamplerState **ppSamplerState) = 0;
    virtual HRESULT CreateRasterizerState(const D3D11_RASTERIZER_DESC *pRasterizerDesc, ID3D11RasterizerState **ppRasterizerState) = 0;
    virtual HRESULT CreateVertexShader(const void *pShaderBytecode, SIZE_T BytecodeLength, ID3D11ClassLinkage *pClassLinkage, ID3D11VertexShader **ppVertexShader) = 0;
    virtual HRESULT CreatePixelShader(const void *pShaderBytecode, SIZE_T BytecodeLength, ID3D11ClassLinkage *pClassLinkage, ID3D11PixelShader **ppPixelShader) = 0;
    virtual HRESULT CreateGeometryShader(const void *pShaderBytecode, SIZE_T BytecodeLength, ID3D11ClassLinkage *pClassLinkage, ID3D11GeometryShader **ppGeometryShader) = 0;
    virtual HRESULT CreateBuffer(const D3D11_BUFFER_DESC *pDesc, const D3D11_SUBRESOURCE_DATA *pInitialData, ID3D11Buffer **ppBuffer) = 0;
    virtual HRESULT CreateInputLayout(const D3D11_INPUT_ELEMENT_DESC *pInputElementDescs, UINT NumElements, const void *pShaderBytecodeWithInputSignature, SIZE_T BytecodeLength, ID3D11InputLayout **ppInputLayout) = 0;
    virtual HRESULT CheckFormatSupport(DXGI_FORMAT Format, UINT *pFormatSupport) = 0;
    virtual HRESULT CreateDepthStencilState(const D3D11_DEPTH_STENCIL_DESC *pDepthStencilDesc, ID3D11DepthStencilState **ppDepthStencilState) = 0;
    virtual HRESULT CreateShaderResourceView(ID3D11Resource *pResource, const D3D11_SHADER_RESOURCE_VIEW_DESC *pDesc, ID3D11ShaderResourceView **ppSRView) = 0;
public: /* ID3D11DeviceContext */
    virtual void CopyResource(ID3D11Resource *pDstResource, ID3D11Resource *pSrcResourc) = 0;
    virtual HRESULT Map(ID3D11Resource *pResource, UINT Subresource, D3D11_MAP MapType, UINT MapFlags, D3D11_MAPPED_SUBRESOURCE *pMappedResource) = 0;
    virtual void Unmap(ID3D11Resource *pResource, UINT Subresource) = 0;
    virtual void UpdateSubresource(ID3D11Resource *pDstResource, UINT DstSubresource, const D3D11_BOX *pDstBox, const void *pSrcData, UINT SrcRowPitch, UINT SrcDepthPitch) = 0;
    virtual void GenerateMips(ID3D11ShaderResourceView *pShaderResourceView) = 0;
    virtual void CopySubresourceRegion(ID3D11Resource *pDstResource, UINT DstSubresource, UINT DstX, UINT DstY, UINT DstZ, ID3D11Resource *pSrcResource, UINT SrcSubresource, const D3D11_BOX *pSrcBox) = 0;
    virtual void DrawIndexed(UINT IndexCount, UINT StartIndexLocation, INT BaseVertexLocation) = 0;
    virtual void Draw(UINT VertexCount, UINT StartVertexLocation) = 0;
    virtual void RSSetScissorRects(UINT NumRects, const D3D11_RECT *pRects) = 0;
    virtual void RSSetViewports(UINT NumViewports, const D3D11_VIEWPORT *pViewports) = 0;
    virtual void PSSetSamplers(UINT StartSlot, UINT NumSamplers, ID3D11SamplerState *const *ppSamplers) = 0;
    virtual void PSSetShaderResources(UINT StartSlot, UINT NumViews, ID3D11ShaderResourceView *const *ppShaderResourceViews) = 0;
    virtual void PSSetConstantBuffers(UINT StartSlot, UINT NumBuffers, ID3D11Buffer *const *ppConstantBuffers) = 0;
    virtual void OMSetRenderTargets(UINT NumViews, ID3D11RenderTargetView *const *ppRenderTargetViews, ID3D11DepthStencilView *pDepthStencilView) = 0;
    virtual void ClearRenderTargetView(ID3D11RenderTargetView *pRenderTargetView, const FLOAT ColorRGBA[4]) = 0;
    virtual void ClearDepthStencilView(ID3D11DepthStencilView *pDepthStencilView, UINT ClearFlags, FLOAT Depth, UINT8 Stencil) = 0;
    virtual void VSSetConstantBuffers(UINT StartSlot, UINT NumBuffers, ID3D11Buffer *const *ppConstantBuffers) = 0;
    virtual void OMSetBlendState(ID3D11BlendState *pBlendState, const FLOAT BlendFactor[ 4 ], UINT SampleMask) = 0;
    virtual void OMSetDepthStencilState(ID3D11DepthStencilState *pDepthStencilState, UINT StencilRef) = 0;
    virtual void RSSetState(ID3D11RasterizerState *pRasterizerState) = 0;
    virtual void IASetInputLayout(ID3D11InputLayout *pInputLayout) = 0;
    virtual void IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY pInputLayout) = 0;
    virtual void IASetVertexBuffers(UINT StartSlot, UINT NumBuffers, ID3D11Buffer *const *ppVertexBuffers, const UINT *pStrides, const UINT *pOffsets) = 0;
    virtual void IASetIndexBuffer(ID3D11Buffer *pIndexBuffer, DXGI_FORMAT Format, UINT Offset) = 0;
    virtual void VSSetShader(ID3D11VertexShader *pVertexShader, ID3D11ClassInstance *const *ppClassInstances, UINT NumClassInstances) = 0;
    virtual void PSSetShader(ID3D11PixelShader *pPixelShader, ID3D11ClassInstance *const *ppClassInstances, UINT NumClassInstances) = 0;
    virtual void GSSetShader(ID3D11GeometryShader *pShader, ID3D11ClassInstance *const *ppClassInstances, UINT NumClassInstances) = 0;
public: /* ID3D11VideoDevice */
    virtual HRESULT CheckVideoDecoderFormat(const GUID *pDecoderProfile, DXGI_FORMAT Format, BOOL *pSupported) = 0;
    virtual HRESULT GetVideoDecoderConfigCount(const D3D11_VIDEO_DECODER_DESC *pDesc, UINT *pCount) = 0;
    virtual HRESULT GetVideoDecoderConfig(const D3D11_VIDEO_DECODER_DESC *pDesc, UINT Index, D3D11_VIDEO_DECODER_CONFIG *pConfig) = 0;
    virtual UINT    GetVideoDecoderProfileCount() = 0;
    virtual HRESULT GetVideoDecoderProfile(UINT Index, GUID *pDecoderProfile) = 0;
    virtual HRESULT CreateVideoDecoderOutputView(ID3D11Resource *pResource, const D3D11_VIDEO_DECODER_OUTPUT_VIEW_DESC *pDesc, ID3D11VideoDecoderOutputView **ppVDOVView) = 0;
    virtual HRESULT CreateVideoDecoder(const D3D11_VIDEO_DECODER_DESC *pVideoDesc, const D3D11_VIDEO_DECODER_CONFIG *pConfig, ID3D11VideoDecoder **ppDecoder) = 0;
public: /* ID3D11VideoContext */
    virtual HRESULT GetDecoderBuffer(ID3D11VideoDecoder *pDecoder, D3D11_VIDEO_DECODER_BUFFER_TYPE Type, UINT *pBufferSize, void **ppBuffer) = 0;
    virtual HRESULT ReleaseDecoderBuffer(ID3D11VideoDecoder *pDecoder, D3D11_VIDEO_DECODER_BUFFER_TYPE Type) = 0;
    virtual HRESULT DecoderBeginFrame(ID3D11VideoDecoder *pDecoder, ID3D11VideoDecoderOutputView *pView, UINT ContentKeySize, const void *pContentKey) = 0;
    virtual HRESULT DecoderEndFrame(ID3D11VideoDecoder *pDecoder) = 0;
    virtual HRESULT SubmitDecoderBuffers(ID3D11VideoDecoder *pDecoder, UINT NumBuffers, const D3D11_VIDEO_DECODER_BUFFER_DESC *pBufferDesc) = 0;
};

std::string D3D11ReturnCodeToStr(HRESULT rs);

} // namespace Mmp

#endif