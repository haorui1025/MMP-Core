#include "D3D11Device.h"

#if defined(USE_D3D)

#include <set>
#include <vector>
#include <cassert>

#include <Poco/SharedLibrary.h>
#include <Poco/UnicodeConverter.h>

#include "LogMessage.h"

typedef HRESULT (WINAPI *LPCREATEDXGIFACTORY)(REFIID, void **);
typedef HRESULT (WINAPI *LPD3D11CREATEDEVICE)(IDXGIAdapter *, D3D_DRIVER_TYPE, HMODULE, UINT32, D3D_FEATURE_LEVEL *, UINT, UINT32, ID3D11Device **, D3D_FEATURE_LEVEL *, ID3D11DeviceContext **);


namespace Mmp 
{

static Poco::SharedLibrary gD3D11Loader;
static Poco::SharedLibrary gDXGILoader;

typedef HRESULT(WINAPI *PFN_CREATE_DXGI_FACTORY2)(UINT Flags, REFIID riid, void **ppFactory);

class D3D11DeviceImpl : public D3D11Device
{
public:
    using ptr = std::shared_ptr<D3D11DeviceImpl>;
public:
    D3D11DeviceImpl();
    ~D3D11DeviceImpl();
public:
    bool Init();
    bool LoadSymbol();
    bool CreateID3D11Device();
    bool CreateID3D11VideoDevice();
    bool CreateID3D11VideoContext();
public:
    ID3D11Device* GetD3D11Device() override;
    HRESULT QueryInterface(REFIID riid, void  **ppvObject) override;
    HRESULT CreateTexture2D(const D3D11_TEXTURE2D_DESC *pDesc, const D3D11_SUBRESOURCE_DATA *pInitialData, ID3D11Texture2D **ppTexture2D) override;
    HRESULT CreateRenderTargetView(ID3D11Resource *pResource, const D3D11_RENDER_TARGET_VIEW_DESC *pDesc, ID3D11RenderTargetView **ppRTView) override;
    HRESULT CreateDepthStencilView(ID3D11Resource *pResource, const D3D11_DEPTH_STENCIL_VIEW_DESC *pDesc, ID3D11DepthStencilView **ppDepthStencilView) override;
    HRESULT CreateBlendState(const D3D11_BLEND_DESC *pBlendStateDesc, ID3D11BlendState **ppBlendState) override;
    HRESULT CreateSamplerState(const D3D11_SAMPLER_DESC *pSamplerDesc, ID3D11SamplerState **ppSamplerState) override;
    HRESULT CreateRasterizerState(const D3D11_RASTERIZER_DESC *pRasterizerDesc, ID3D11RasterizerState **ppRasterizerState) override;
    HRESULT CreateVertexShader(const void *pShaderBytecode, SIZE_T BytecodeLength, ID3D11ClassLinkage *pClassLinkage, ID3D11VertexShader **ppVertexShader) override;
    HRESULT CreatePixelShader(const void *pShaderBytecode, SIZE_T BytecodeLength, ID3D11ClassLinkage *pClassLinkage, ID3D11PixelShader **ppPixelShader) override;
    HRESULT CreateGeometryShader(const void *pShaderBytecode, SIZE_T BytecodeLength, ID3D11ClassLinkage *pClassLinkage, ID3D11GeometryShader **ppGeometryShader) override;
    HRESULT CreateBuffer(const D3D11_BUFFER_DESC *pDesc, const D3D11_SUBRESOURCE_DATA *pInitialData, ID3D11Buffer **ppBuffer) override;
    HRESULT CreateInputLayout(const D3D11_INPUT_ELEMENT_DESC *pInputElementDescs, UINT NumElements, const void *pShaderBytecodeWithInputSignature, SIZE_T BytecodeLength, ID3D11InputLayout **ppInputLayout) override;
    HRESULT CheckFormatSupport(DXGI_FORMAT Format, UINT *pFormatSupport) override;
    HRESULT CreateDepthStencilState(const D3D11_DEPTH_STENCIL_DESC *pDepthStencilDesc, ID3D11DepthStencilState **ppDepthStencilState) override;
    HRESULT CreateShaderResourceView(ID3D11Resource *pResource, const D3D11_SHADER_RESOURCE_VIEW_DESC *pDesc, ID3D11ShaderResourceView **ppSRView) override;
public:
    void CopyResource(ID3D11Resource *pDstResource, ID3D11Resource *pSrcResourc) override;
    HRESULT Map(ID3D11Resource *pResource, UINT Subresource, D3D11_MAP MapType, UINT MapFlags, D3D11_MAPPED_SUBRESOURCE *pMappedResource) override;
    void Unmap(ID3D11Resource *pResource, UINT Subresource) override;
    void UpdateSubresource(ID3D11Resource *pDstResource, UINT DstSubresource, const D3D11_BOX *pDstBox, const void *pSrcData, UINT SrcRowPitch, UINT SrcDepthPitch) override;
    void GenerateMips(ID3D11ShaderResourceView *pShaderResourceView) override;
    void CopySubresourceRegion(ID3D11Resource *pDstResource, UINT DstSubresource, UINT DstX, UINT DstY, UINT DstZ, ID3D11Resource *pSrcResource, UINT SrcSubresource, const D3D11_BOX *pSrcBox) override;
    void DrawIndexed(UINT IndexCount, UINT StartIndexLocation, INT BaseVertexLocation) override;
    void Draw(UINT VertexCount, UINT StartVertexLocation) override;
    void RSSetScissorRects(UINT NumRects, const D3D11_RECT *pRects) override;
    void RSSetViewports(UINT NumViewports, const D3D11_VIEWPORT *pViewports) override;
    void PSSetSamplers(UINT StartSlot, UINT NumSamplers, ID3D11SamplerState *const *ppSamplers) override;
    void PSSetShaderResources(UINT StartSlot, UINT NumViews, ID3D11ShaderResourceView *const *ppShaderResourceViews) override;
    void PSSetConstantBuffers(UINT StartSlot, UINT NumBuffers, ID3D11Buffer *const *ppConstantBuffers) override;
    void OMSetRenderTargets(UINT NumViews, ID3D11RenderTargetView *const *ppRenderTargetViews, ID3D11DepthStencilView *pDepthStencilView) override;
    void ClearRenderTargetView(ID3D11RenderTargetView *pRenderTargetView, const FLOAT ColorRGBA[4]) override;
    void ClearDepthStencilView(ID3D11DepthStencilView *pDepthStencilView, UINT ClearFlags, FLOAT Depth, UINT8 Stencil) override;
    void VSSetConstantBuffers(UINT StartSlot, UINT NumBuffers, ID3D11Buffer *const *ppConstantBuffers) override;
    void OMSetBlendState(ID3D11BlendState *pBlendState, const FLOAT BlendFactor[ 4 ], UINT SampleMask) override;
    void OMSetDepthStencilState(ID3D11DepthStencilState *pDepthStencilState, UINT StencilRef) override;
    void RSSetState(ID3D11RasterizerState *pRasterizerState) override;
    void IASetInputLayout(ID3D11InputLayout *pInputLayout) override;
    void IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY pInputLayout) override;
    void IASetVertexBuffers(UINT StartSlot, UINT NumBuffers, ID3D11Buffer *const *ppVertexBuffers, const UINT *pStrides, const UINT *pOffsets) override;
    void IASetIndexBuffer(ID3D11Buffer *pIndexBuffer, DXGI_FORMAT Format, UINT Offset) override;
    void VSSetShader(ID3D11VertexShader *pVertexShader, ID3D11ClassInstance *const *ppClassInstances, UINT NumClassInstances) override;
    void PSSetShader(ID3D11PixelShader *pPixelShader, ID3D11ClassInstance *const *ppClassInstances, UINT NumClassInstances) override;
    void GSSetShader(ID3D11GeometryShader *pShader, ID3D11ClassInstance *const *ppClassInstances, UINT NumClassInstances) override;
public: /* ID3D11VideoDevice */
    HRESULT CheckVideoDecoderFormat(const GUID *pDecoderProfile, DXGI_FORMAT Format, BOOL *pSupported) override;
    HRESULT GetVideoDecoderConfigCount(const D3D11_VIDEO_DECODER_DESC *pDesc, UINT *pCount) override;
    HRESULT GetVideoDecoderConfig(const D3D11_VIDEO_DECODER_DESC *pDesc, UINT Index, D3D11_VIDEO_DECODER_CONFIG *pConfig) override;
    UINT    GetVideoDecoderProfileCount() override;
    HRESULT GetVideoDecoderProfile(UINT Index, GUID *pDecoderProfile) override;
    HRESULT CreateVideoDecoderOutputView(ID3D11Resource *pResource, const D3D11_VIDEO_DECODER_OUTPUT_VIEW_DESC *pDesc, ID3D11VideoDecoderOutputView **ppVDOVView) override;
    HRESULT CreateVideoDecoder(const D3D11_VIDEO_DECODER_DESC *pVideoDesc, const D3D11_VIDEO_DECODER_CONFIG *pConfig, ID3D11VideoDecoder **ppDecoder) override;
public: /* ID3D11VideoContext  */
    HRESULT GetDecoderBuffer(ID3D11VideoDecoder *pDecoder, D3D11_VIDEO_DECODER_BUFFER_TYPE Type, UINT *pBufferSize, void **ppBuffer) override;
    HRESULT ReleaseDecoderBuffer(ID3D11VideoDecoder *pDecoder, D3D11_VIDEO_DECODER_BUFFER_TYPE Type) override;
    HRESULT DecoderBeginFrame(ID3D11VideoDecoder *pDecoder, ID3D11VideoDecoderOutputView *pView, UINT ContentKeySize, const void *pContentKey) override;
    HRESULT DecoderEndFrame(ID3D11VideoDecoder *pDecoder) override;
    HRESULT SubmitDecoderBuffers(ID3D11VideoDecoder *pDecoder, UINT NumBuffers, const D3D11_VIDEO_DECODER_BUFFER_DESC *pBufferDesc) override;
private:
    std::mutex _mtx;
    bool _debugMode;
private:
    std::mutex                          _deviceMtx;
    ID3D11Device*                       _device;
    D3D_FEATURE_LEVEL                   _featureLevel;
private:
    std::mutex                          _contexMtx;
    ID3D11DeviceContext*                _context;
private:
    std::mutex                          _videoDeviceMtx;
    ID3D11VideoDevice*                  _videoDevice;
private:
    std::mutex                          _videoContextMtx;
    ID3D11VideoContext*                 _videoContext;
private:
    LPCREATEDXGIFACTORY                 _CreateDXGIFactory1;
    LPD3D11CREATEDEVICE                 _D3D11CreateDevice;
};

D3D11DeviceImpl::D3D11DeviceImpl()
{
    _debugMode = false;
    _featureLevel = D3D_FEATURE_LEVEL_11_0;
}

D3D11DeviceImpl::~D3D11DeviceImpl()
{
}

bool D3D11DeviceImpl::LoadSymbol()
{
    MMP_LOG_INFO << "LoadD3D11";
    MMP_LOG_INFO << "Load D3D11 related runtime depend";
    try 
    {
        if (!gD3D11Loader.isLoaded())
        {
            gD3D11Loader.load("d3d11.dll");
        }
        MMP_LOG_INFO << "-- Load d3d11.dll successfully";
    }
    catch (const Poco::LibraryLoadException& ex)
    {
        MMP_LOG_ERROR << "-- Load d3d11.dll fail, error msg is: " << ex.message();
        assert(false);
        return false;
    }
    try 
    {
        if (!gDXGILoader.isLoaded())
        {
            gDXGILoader.load("dxgi.dll");
        }
        MMP_LOG_INFO << "-- Load dxgi.dll successfully";
    }
    catch (const Poco::LibraryLoadException& ex)
    {
        MMP_LOG_ERROR << "-- Load dxgi.dll fail, error msg is: " << ex.message();
        assert(false);
        return false;
    }
    MMP_LOG_INFO << "Load D3D11 related runtime symbol";
    if (gD3D11Loader.hasSymbol("D3D11CreateDevice"))
    {
        _D3D11CreateDevice = (LPD3D11CREATEDEVICE)gD3D11Loader.getSymbol("D3D11CreateDevice");
        MMP_LOG_INFO << "-- Load symbol D3D11CreateDevice successfully";
    }
    else
    {
        MMP_LOG_ERROR << "-- Load symbol D3D11CreateDevice successfully";
        assert(false);
        return false;
    }
    if (gDXGILoader.hasSymbol("CreateDXGIFactory1"))
    {
        _CreateDXGIFactory1 = (LPCREATEDXGIFACTORY)gDXGILoader.getSymbol("CreateDXGIFactory1");
        MMP_LOG_INFO << "-- Load symbol CreateDXGIFactory1 successfully";
    }
    else
    {
        MMP_LOG_ERROR << "-- Load symbol CreateDXGIFactory1 successfully";
        assert(false);
        return false;
    }
    return true;
}

bool D3D11DeviceImpl::CreateID3D11Device()
{
    std::vector<IDXGIAdapter*> adapters;
    IDXGIFactory* DXGIFactory = nullptr;
    MMP_LOG_INFO << "CreateDevice";
    if (SUCCEEDED(_CreateDXGIFactory1(__uuidof(IDXGIFactory), (void**)&DXGIFactory)))
    {
        IDXGIAdapter* adapter = nullptr;
        // See also : windows-win32-direct3d11.pdf - How To: Enumerate Adapters
        for (UINT i = 0; DXGIFactory->EnumAdapters(i, &adapter) != DXGI_ERROR_NOT_FOUND; i++)
        {
            DXGI_ADAPTER_DESC desc = {};
            adapter->GetDesc(&desc);
            MMP_LOG_INFO << "DXGIAdapter (" << i << ")";
            {
                std::string description;
                Poco::UnicodeConverter::convert(desc.Description, description);
                MMP_LOG_INFO << "-- Description is: " << description;
            }
            MMP_LOG_INFO << "-- Vendor id is: " << desc.VendorId;
            MMP_LOG_INFO << "-- Device id is: " << desc.DeviceId;
            MMP_LOG_INFO << "-- Sub System id is: " << desc.SubSysId;
            adapters.push_back(adapter);
        }
        if (adapters.empty())
        {
            DXGIFactory->Release();
            assert(false);
            return false;
        }
    }
    else
    {
        MMP_LOG_ERROR << "CreateDXGIFactory1 fail";
        assert(false);
        return false;
    }
    // TODO : should choosen by user or simply choose the first ???
    IDXGIAdapter* chooseAdapter = adapters.front();
    {
        UINT createDeviceFlags = 0;
        createDeviceFlags |= D3D11_CREATE_DEVICE_VIDEO_SUPPORT;
        if (_debugMode)
        {
            createDeviceFlags = D3D11_CREATE_DEVICE_DEBUG;
        }
        static const D3D_DRIVER_TYPE driverTypes[] = {
            D3D_DRIVER_TYPE_HARDWARE,
            D3D_DRIVER_TYPE_WARP,
            D3D_DRIVER_TYPE_REFERENCE,
        };
        static const D3D_FEATURE_LEVEL featureLevels[] = {
            D3D_FEATURE_LEVEL_11_0,
        };
        if (D3D11_FAILED(_D3D11CreateDevice(chooseAdapter, D3D_DRIVER_TYPE_UNKNOWN, nullptr, createDeviceFlags,
            (D3D_FEATURE_LEVEL *)featureLevels, UINT(1), D3D11_SDK_VERSION, &_device, &_featureLevel, &_context
        )))
        {
            MMP_LOG_ERROR << "3D11CreateDevice fail";
            assert(false);
            return false;
        }
    }
    for (const auto& adapter : adapters)
    {
        adapter->Release();
    }
    DXGIFactory->Release();
    return true;
}

bool D3D11DeviceImpl::CreateID3D11VideoDevice()
{
    MMP_LOG_INFO << "-- CreateID3D11VideoDevice";
    HRESULT hr = QueryInterface(IID_ID3D11VideoDevice, (void**)&_videoDevice);
    if (FAILED(hr))
    {
        MMP_LOG_ERROR << "ID3D11Device::QueryInterface fail";
        assert(false);
        return false;
    }
    return true;
}

bool D3D11DeviceImpl::CreateID3D11VideoContext()
{
    MMP_LOG_INFO << "-- CreateID3D11VideoContext";
    HRESULT hr = _context->QueryInterface(IID_ID3D11VideoContext, (void**)&_videoContext);
    if (FAILED(hr))
    {
        MMP_LOG_ERROR << "ID3D11Device::QueryInterface fail";
        assert(false);
        return false;
    }
    return true;
}


bool D3D11DeviceImpl::Init()
{
    MMP_LOG_INFO << "Init D3DDevice";
    if (!LoadSymbol())
    {
        return false;
    }
    if (!CreateID3D11Device())
    {
        return false;
    }
    if (!CreateID3D11VideoDevice())
    {
        return false;
    }
    if (!CreateID3D11VideoContext())
    {
        return false;
    }
    return true;
}

#define XXX_D3D11_DEVICE_LOCK()          std::lock_guard<std::mutex> lock(_deviceMtx);\
                                         if (!_device) return HRESULT(-1);

#define XXX_D3D11_DEVICE_LOCK_VOID()      std::lock_guard<std::mutex> lock(_deviceMtx);\
                                          if (!_device) return;

ID3D11Device* D3D11DeviceImpl::GetD3D11Device()
{
    return _device;
}

HRESULT D3D11DeviceImpl::QueryInterface(REFIID riid, void  **ppvObject)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->QueryInterface(riid, ppvObject);
}

HRESULT D3D11DeviceImpl::CreateTexture2D(const D3D11_TEXTURE2D_DESC *pDesc, const D3D11_SUBRESOURCE_DATA *pInitialData, ID3D11Texture2D **ppTexture2D)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->CreateTexture2D(pDesc, pInitialData, ppTexture2D);
}

HRESULT D3D11DeviceImpl::CreateRenderTargetView(ID3D11Resource *pResource, const D3D11_RENDER_TARGET_VIEW_DESC *pDesc, ID3D11RenderTargetView **ppRTView)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->CreateRenderTargetView(pResource, pDesc, ppRTView);
}

HRESULT D3D11DeviceImpl::CreateDepthStencilView(ID3D11Resource *pResource, const D3D11_DEPTH_STENCIL_VIEW_DESC *pDesc, ID3D11DepthStencilView **ppDepthStencilView)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->CreateDepthStencilView(pResource, pDesc, ppDepthStencilView);
}

HRESULT D3D11DeviceImpl::CreateBlendState(const D3D11_BLEND_DESC *pBlendStateDesc, ID3D11BlendState **ppBlendState)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->CreateBlendState(pBlendStateDesc, ppBlendState);
}

HRESULT D3D11DeviceImpl::CreateSamplerState(const D3D11_SAMPLER_DESC *pSamplerDesc, ID3D11SamplerState **ppSamplerState)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->CreateSamplerState(pSamplerDesc, ppSamplerState);
}

HRESULT D3D11DeviceImpl::CreateRasterizerState(const D3D11_RASTERIZER_DESC *pRasterizerDesc, ID3D11RasterizerState **ppRasterizerState)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->CreateRasterizerState(pRasterizerDesc, ppRasterizerState);
}

HRESULT D3D11DeviceImpl::CreateVertexShader(const void *pShaderBytecode, SIZE_T BytecodeLength, ID3D11ClassLinkage *pClassLinkage, ID3D11VertexShader **ppVertexShader)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->CreateVertexShader(pShaderBytecode, BytecodeLength, pClassLinkage, ppVertexShader);
}

HRESULT D3D11DeviceImpl::CreatePixelShader(const void *pShaderBytecode, SIZE_T BytecodeLength, ID3D11ClassLinkage *pClassLinkage, ID3D11PixelShader **ppPixelShader)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->CreatePixelShader(pShaderBytecode, BytecodeLength, pClassLinkage, ppPixelShader);
}

HRESULT D3D11DeviceImpl::CreateGeometryShader(const void *pShaderBytecode, SIZE_T BytecodeLength, ID3D11ClassLinkage *pClassLinkage, ID3D11GeometryShader **ppGeometryShader)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->CreateGeometryShader(pShaderBytecode, BytecodeLength, pClassLinkage, ppGeometryShader);
}

HRESULT D3D11DeviceImpl::CreateBuffer(const D3D11_BUFFER_DESC *pDesc, const D3D11_SUBRESOURCE_DATA *pInitialData, ID3D11Buffer **ppBuffer)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->CreateBuffer(pDesc, pInitialData, ppBuffer);
}

HRESULT D3D11DeviceImpl::CreateInputLayout(const D3D11_INPUT_ELEMENT_DESC *pInputElementDescs, UINT NumElements, const void *pShaderBytecodeWithInputSignature, SIZE_T BytecodeLength, ID3D11InputLayout **ppInputLayout)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->CreateInputLayout(pInputElementDescs, NumElements, pShaderBytecodeWithInputSignature, BytecodeLength, ppInputLayout);
}

HRESULT D3D11DeviceImpl::CheckFormatSupport(DXGI_FORMAT Format, UINT *pFormatSupport)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->CheckFormatSupport(Format, pFormatSupport);
}

HRESULT D3D11DeviceImpl::CreateDepthStencilState(const D3D11_DEPTH_STENCIL_DESC *pDepthStencilDesc, ID3D11DepthStencilState **ppDepthStencilState)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->CreateDepthStencilState(pDepthStencilDesc, ppDepthStencilState);
}

HRESULT D3D11DeviceImpl::CreateShaderResourceView(ID3D11Resource *pResource, const D3D11_SHADER_RESOURCE_VIEW_DESC *pDesc, ID3D11ShaderResourceView **ppSRView)
{
    XXX_D3D11_DEVICE_LOCK();
    return _device->CreateShaderResourceView(pResource, pDesc, ppSRView);
}

#undef XXX_D3D11_DEVICE_LOCK
#undef XXX_D3D11_DEVICE_LOCK_VOID

#define XXX_D3D11_CONTEXT_LOCK()         std::lock_guard<std::mutex> lock(_contexMtx);\
                                         if (!_context) return HRESULT(-1);

#define XXX_D3D11_CONTEXT_LOCK_VOID()     std::lock_guard<std::mutex> lock(_contexMtx);\
                                          if (!_context) return;

void D3D11DeviceImpl::CopyResource(ID3D11Resource *pDstResource, ID3D11Resource *pSrcResourc)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->CopyResource(pDstResource, pSrcResourc);
}

HRESULT D3D11DeviceImpl::Map(ID3D11Resource *pResource, UINT Subresource, D3D11_MAP MapType, UINT MapFlags, D3D11_MAPPED_SUBRESOURCE *pMappedResource)
{
    XXX_D3D11_CONTEXT_LOCK();
    return _context->Map(pResource, Subresource, MapType, MapFlags, pMappedResource);
}

void D3D11DeviceImpl::Unmap(ID3D11Resource *pResource, UINT Subresource)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->Unmap(pResource, Subresource);
}

void D3D11DeviceImpl::UpdateSubresource(ID3D11Resource *pDstResource, UINT DstSubresource, const D3D11_BOX *pDstBox, const void *pSrcData, UINT SrcRowPitch, UINT SrcDepthPitch)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->UpdateSubresource(pDstResource, DstSubresource, pDstBox, pSrcData, SrcRowPitch, SrcDepthPitch);
}

void D3D11DeviceImpl::GenerateMips(ID3D11ShaderResourceView *pShaderResourceView)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->GenerateMips(pShaderResourceView);
}

void D3D11DeviceImpl::CopySubresourceRegion(ID3D11Resource *pDstResource, UINT DstSubresource, UINT DstX, UINT DstY, UINT DstZ, ID3D11Resource *pSrcResource, UINT SrcSubresource, const D3D11_BOX *pSrcBox)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->CopySubresourceRegion(pDstResource, DstSubresource, DstX, DstY, DstZ, pSrcResource, SrcSubresource, pSrcBox);
}

void D3D11DeviceImpl::DrawIndexed(UINT IndexCount, UINT StartIndexLocation, INT BaseVertexLocation)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->DrawIndexed(IndexCount, StartIndexLocation, BaseVertexLocation);
}

void D3D11DeviceImpl::Draw(UINT VertexCount, UINT StartVertexLocation)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->Draw(VertexCount, StartVertexLocation);
}

void D3D11DeviceImpl::RSSetScissorRects(UINT NumRects, const D3D11_RECT *pRects)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->RSSetScissorRects(NumRects, pRects);
}

void D3D11DeviceImpl::RSSetViewports(UINT NumViewports, const D3D11_VIEWPORT *pViewports)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->RSSetViewports(NumViewports, pViewports);
}

void D3D11DeviceImpl::PSSetSamplers(UINT StartSlot, UINT NumSamplers, ID3D11SamplerState *const *ppSamplers)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->PSSetSamplers(StartSlot, NumSamplers, ppSamplers);
}

void D3D11DeviceImpl::PSSetShaderResources(UINT StartSlot, UINT NumViews, ID3D11ShaderResourceView *const *ppShaderResourceViews)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->PSSetShaderResources(StartSlot, NumViews, ppShaderResourceViews);
}

void D3D11DeviceImpl::PSSetConstantBuffers(UINT StartSlot, UINT NumBuffers, ID3D11Buffer *const *ppConstantBuffers)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->PSSetConstantBuffers(StartSlot, NumBuffers, ppConstantBuffers);
}

void D3D11DeviceImpl::OMSetRenderTargets(UINT NumViews, ID3D11RenderTargetView *const *ppRenderTargetViews, ID3D11DepthStencilView *pDepthStencilView)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->OMSetRenderTargets(NumViews, ppRenderTargetViews, pDepthStencilView);
}

void D3D11DeviceImpl::ClearRenderTargetView(ID3D11RenderTargetView *pRenderTargetView, const FLOAT ColorRGBA[4])
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->ClearRenderTargetView(pRenderTargetView, ColorRGBA);
}

void D3D11DeviceImpl::ClearDepthStencilView(ID3D11DepthStencilView *pDepthStencilView, UINT ClearFlags, FLOAT Depth, UINT8 Stencil)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->ClearDepthStencilView(pDepthStencilView, ClearFlags, Depth, Stencil);
}

void D3D11DeviceImpl::VSSetConstantBuffers(UINT StartSlot, UINT NumBuffers, ID3D11Buffer *const *ppConstantBuffers)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->VSSetConstantBuffers(StartSlot, NumBuffers, ppConstantBuffers);
}

void D3D11DeviceImpl::OMSetBlendState(ID3D11BlendState *pBlendState, const FLOAT BlendFactor[ 4 ], UINT SampleMask)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->OMSetBlendState(pBlendState, BlendFactor, SampleMask);
}

void D3D11DeviceImpl::OMSetDepthStencilState(ID3D11DepthStencilState *pDepthStencilState, UINT StencilRef)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->OMSetDepthStencilState(pDepthStencilState, StencilRef);
}

void D3D11DeviceImpl::RSSetState(ID3D11RasterizerState *pRasterizerState)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->RSSetState(pRasterizerState);
}

void D3D11DeviceImpl::IASetInputLayout(ID3D11InputLayout *pInputLayout)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->IASetInputLayout(pInputLayout);
}

void D3D11DeviceImpl::IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY pInputLayout)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->IASetPrimitiveTopology(pInputLayout);
}

void D3D11DeviceImpl::IASetVertexBuffers(UINT StartSlot, UINT NumBuffers, ID3D11Buffer *const *ppVertexBuffers, const UINT *pStrides, const UINT *pOffsets)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->IASetVertexBuffers(StartSlot, NumBuffers, ppVertexBuffers, pStrides, pOffsets);  
}

void D3D11DeviceImpl::IASetIndexBuffer(ID3D11Buffer *pIndexBuffer, DXGI_FORMAT Format, UINT Offset)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->IASetIndexBuffer(pIndexBuffer, Format, Offset);   
}

void D3D11DeviceImpl::VSSetShader(ID3D11VertexShader *pVertexShader, ID3D11ClassInstance *const *ppClassInstances, UINT NumClassInstances)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->VSSetShader(pVertexShader, ppClassInstances, NumClassInstances);  
}

void D3D11DeviceImpl::PSSetShader(ID3D11PixelShader *pPixelShader, ID3D11ClassInstance *const *ppClassInstances, UINT NumClassInstances)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->PSSetShader(pPixelShader, ppClassInstances, NumClassInstances);   
}

void D3D11DeviceImpl::GSSetShader(ID3D11GeometryShader *pShader, ID3D11ClassInstance *const *ppClassInstances, UINT NumClassInstances)
{
    XXX_D3D11_CONTEXT_LOCK_VOID();
    _context->GSSetShader(pShader, ppClassInstances, NumClassInstances);   
}

#undef XXX_D3D11_CONTEXT_LOCK
#undef XXX_D3D11_CONTEXT_LOCK_VOID

#define XXX_D3D11_VIDEO_DEVICE_LOCK()          std::lock_guard<std::mutex> lock(_videoDeviceMtx);\
                                               if (!_videoDevice) return HRESULT(-1);

#define XXX_D3D11_VIDEO_DEVICE_LOCK_VOID()     std::lock_guard<std::mutex> lock(_videoDeviceMtx);\
                                               if (!_videoDevice) return;

#define XXX_D3D11_VIDEO_DEVICE_LOCK_UINT()     std::lock_guard<std::mutex> lock(_videoDeviceMtx);\
                                               if (!_videoDevice) return 0;

HRESULT D3D11DeviceImpl::CheckVideoDecoderFormat(const GUID *pDecoderProfile, DXGI_FORMAT Format, BOOL *pSupported)
{
    XXX_D3D11_VIDEO_DEVICE_LOCK();
    return _videoDevice->CheckVideoDecoderFormat(pDecoderProfile, Format, pSupported);
}

HRESULT D3D11DeviceImpl::GetVideoDecoderConfigCount(const D3D11_VIDEO_DECODER_DESC *pDesc, UINT *pCount)
{
    XXX_D3D11_VIDEO_DEVICE_LOCK();
    return _videoDevice->GetVideoDecoderConfigCount(pDesc, pCount);
}

HRESULT D3D11DeviceImpl::GetVideoDecoderConfig(const D3D11_VIDEO_DECODER_DESC *pDesc, UINT Index, D3D11_VIDEO_DECODER_CONFIG *pConfig)
{
    XXX_D3D11_VIDEO_DEVICE_LOCK();
    return _videoDevice->GetVideoDecoderConfig(pDesc, Index, pConfig);
}

UINT    D3D11DeviceImpl::GetVideoDecoderProfileCount()
{
    XXX_D3D11_VIDEO_DEVICE_LOCK_UINT();
    return _videoDevice->GetVideoDecoderProfileCount();
}

HRESULT D3D11DeviceImpl::GetVideoDecoderProfile(UINT Index, GUID *pDecoderProfile)
{
    XXX_D3D11_VIDEO_DEVICE_LOCK();
    return _videoDevice->GetVideoDecoderProfile(Index, pDecoderProfile);
}

HRESULT D3D11DeviceImpl::CreateVideoDecoderOutputView(ID3D11Resource *pResource, const D3D11_VIDEO_DECODER_OUTPUT_VIEW_DESC *pDesc, ID3D11VideoDecoderOutputView **ppVDOVView)
{
    XXX_D3D11_VIDEO_DEVICE_LOCK();
    return _videoDevice->CreateVideoDecoderOutputView(pResource, pDesc, ppVDOVView);
}

HRESULT D3D11DeviceImpl::CreateVideoDecoder(const D3D11_VIDEO_DECODER_DESC *pVideoDesc, const D3D11_VIDEO_DECODER_CONFIG *pConfig, ID3D11VideoDecoder **ppDecoder)
{
    XXX_D3D11_VIDEO_DEVICE_LOCK();
    return _videoDevice->CreateVideoDecoder(pVideoDesc, pConfig, ppDecoder);
}

#undef XXX_D3D11_VIDEO_DEVICE_LOCK
#undef XXX_D3D11_VIDEO_DEVICE_LOCK_VOID
#undef XXX_D3D11_VIDEO_DEVICE_LOCK_UINT

#define XXX_D3D11_VIDEO_CONTEXT_LOCK()          std::lock_guard<std::mutex> lock(_videoContextMtx);\
                                                if (!_videoContext) return HRESULT(-1);

#define XXX_D3D11_VIDEO_CONTEXT_LOCK_VOID()     std::lock_guard<std::mutex> lock(_videoContextMtx);\
                                                if (!_videoContext) return;

HRESULT D3D11DeviceImpl::GetDecoderBuffer(ID3D11VideoDecoder *pDecoder, D3D11_VIDEO_DECODER_BUFFER_TYPE Type, UINT *pBufferSize, void **ppBuffer)
{
    XXX_D3D11_VIDEO_CONTEXT_LOCK();
    return _videoContext->GetDecoderBuffer(pDecoder, Type, pBufferSize, ppBuffer);
}

HRESULT D3D11DeviceImpl::ReleaseDecoderBuffer(ID3D11VideoDecoder *pDecoder, D3D11_VIDEO_DECODER_BUFFER_TYPE Type)
{
    XXX_D3D11_VIDEO_CONTEXT_LOCK();
    return _videoContext->ReleaseDecoderBuffer(pDecoder, Type);
}

HRESULT D3D11DeviceImpl::DecoderBeginFrame(ID3D11VideoDecoder *pDecoder, ID3D11VideoDecoderOutputView *pView, UINT ContentKeySize, const void *pContentKey)
{
    XXX_D3D11_VIDEO_CONTEXT_LOCK();
    return _videoContext->DecoderBeginFrame(pDecoder, pView, ContentKeySize, pContentKey);
}

HRESULT D3D11DeviceImpl::DecoderEndFrame(ID3D11VideoDecoder *pDecoder)
{
    XXX_D3D11_VIDEO_CONTEXT_LOCK();
    return _videoContext->DecoderEndFrame(pDecoder);
}

HRESULT D3D11DeviceImpl::SubmitDecoderBuffers(ID3D11VideoDecoder *pDecoder, UINT NumBuffers, const D3D11_VIDEO_DECODER_BUFFER_DESC *pBufferDesc)
{
    XXX_D3D11_VIDEO_CONTEXT_LOCK();
    return _videoContext->SubmitDecoderBuffers(pDecoder, NumBuffers, pBufferDesc);
}

#undef XXX_D3D11_VIDEO_CONTEXT_LOCK
#undef XXX_D3D11_VIDEO_CONTEXT_LOCK_VOID

D3D11Device::ptr D3D11Device::Singleton()
{
    static D3D11Device::ptr gInstace = nullptr;
    static std::atomic<bool> isInited(false);
    if (!isInited)
    {
        D3D11DeviceImpl::ptr device = std::make_shared<D3D11DeviceImpl>();
        if (device->Init())
        {
            gInstace = device;
        }
        isInited = true;
    }
    return gInstace;
}

std::string D3D11ReturnCodeToStr(HRESULT rs)
{
    // See also : windows-win32-direct3d11.pdf - Direct3D 11 Return Codes
    switch (rs)
    {
        case D3D11_ERROR_FILE_NOT_FOUND: return "D3D11_ERROR_FILE_NOT_FOUND";
        case D3D11_ERROR_TOO_MANY_UNIQUE_STATE_OBJECTS: return "D3D11_ERROR_TOO_MANY_UNIQUE_STATE_OBJECTS";
        case D3D11_ERROR_TOO_MANY_UNIQUE_VIEW_OBJECTS: return "D3D11_ERROR_TOO_MANY_UNIQUE_VIEW_OBJECTS";
        case D3D11_ERROR_DEFERRED_CONTEXT_MAP_WITHOUT_INITIAL_DISCARD: return "D3D11_ERROR_DEFERRED_CONTEXT_MAP_WITHOUT_INITIAL_DISCARD";
        case E_FAIL: return "E_FAIL";
        case E_INVALIDARG: return "E_INVALIDARG";
        case E_OUTOFMEMORY: return "E_OUTOFMEMORY";
        case E_NOTIMPL: return "E_NOTIMPL";
        case S_FALSE: return "S_FALSE";
        case S_OK: return "S_OK";
        default:
            return "UNKNOWN(" + std::to_string((uint64_t)rs) + ")";
    }
}

} // namespace Mmp

#endif /* USE_D3D */