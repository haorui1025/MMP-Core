#include "VulkanDevice.h"

#if defined(USE_VULKAN)

// See also : https://gpuopen-librariesandsdks.github.io/VulkanMemoryAllocator/html/quick_start.html
#define VMA_IMPLEMENTATION
#include "vk_mem_alloc.h"

#if defined (USE_X11)
#include <X11/Xlib.h>
#include "vulkan/vulkan_xlib.h"
#endif /* USE_X11 */

#if MMP_PLATFORM(WINDOWS)
#include "vulkan/vulkan_win32.h"
#endif

#include <set>
#include <cassert>
#include <vector>
#include <mutex>
#include <algorithm>

#include <Poco/SharedLibrary.h>

namespace Mmp 
{

static VkBool32 VulkanDebugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT severity, VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT *data, void *priv)
{
    Mmp::AbstractLogger::Level logLevel = Mmp::AbstractLogger::Level::L_INFO;
    switch (severity)
    {
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT: logLevel = Mmp::AbstractLogger::Level::L_DEBUG; break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT: logLevel = Mmp::AbstractLogger::Level::L_INFO; break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT: logLevel = Mmp::AbstractLogger::Level::L_WARN; break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT: logLevel = Mmp::AbstractLogger::Level::L_ERROR; break;
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT: logLevel = Mmp::AbstractLogger::Level::L_FATAL; break;
        default:
            break;
    }
    Mmp::LogMessage(logLevel, __FILE__, __LINE__, "Vulkan") << data;
    return 0;
}

struct LayerProperties
{
    VkLayerProperties properties = {};
    std::vector<VkExtensionProperties> extensions;
};
struct PhysicalDeviceProps 
{
    VkPhysicalDeviceProperties properties = {};
    VkPhysicalDevicePushDescriptorPropertiesKHR pushDescriptorProperties = {};
    VkPhysicalDeviceExternalMemoryHostPropertiesEXT externalMemoryHostProperties = {};
    VkPhysicalDeviceDepthStencilResolveProperties depthStencilResolve = {};
};

struct VulkanPhysicalDeviceInfo 
{
    VkFormat preferredDepthStencilFormat;
    bool canBlitToPreferredDepthStencilFormat;
};

struct AllPhysicalDeviceFeatures 
{
    VkPhysicalDeviceFeatures standard = {};
    VkPhysicalDeviceMultiviewFeatures multiview = {};
    VkPhysicalDevicePresentWaitFeaturesKHR presentWait = {};
    VkPhysicalDevicePresentIdFeaturesKHR presentId = {};
};

struct PhysicalDeviceFeatures 
{
    AllPhysicalDeviceFeatures available = {};
    AllPhysicalDeviceFeatures enabled = {};
};

class VulkanDeviceImpl : public VulkanDevice
{
public:
    using ptr = std::shared_ptr<VulkanDeviceImpl>;
public:
    VulkanDeviceImpl();
    ~VulkanDeviceImpl();
public:
    int32_t GetQueueFamilyIndex(VkQueueFlagBits queueFlag) override;
    VkQueueFamilyProperties GetQueueFamilyProperties(VkQueueFlagBits queueFlag) override;
    const VkPhysicalDeviceExternalMemoryHostPropertiesEXT& GetVkPhysicalDeviceExternalMemoryHostProperties() override;
    const VkPhysicalDeviceCooperativeMatrixPropertiesKHR& GetVkPhysicalDeviceCooperativeMatrixProperties() override;
    const VkPhysicalDeviceSubgroupSizeControlProperties& GetVkPhysicalDeviceSubgroupSizeControlProperties() override;
    const VkPhysicalDeviceDescriptorBufferPropertiesEXT& GetVkPhysicalDeviceDescriptorBufferProperties() override;
    const VkPhysicalDeviceDriverProperties& GetVkPhysicalDeviceDriverProperties() override;
    const VkPhysicalDeviceMemoryProperties& GetVkPhysicalDeviceMemoryProperties() override;
    const VkPhysicalDeviceProperties2& GetVkPhysicalDeviceProperties2() override;
    const VkPhysicalDeviceShaderAtomicFloatFeaturesEXT& GetVkPhysicalDeviceShaderAtomicFloatFeatures() override;
    const VkPhysicalDeviceFeatures2& GetVkPhysicalDeviceFeatures2() override;
    const VulkanExtensions GetVulkanExtensions() override;
    VkInstance GetInstance() override;
    VmaAllocator GetVmaAllocator() override;
    void* GetInstanceFunc(const std::string& name) override;
    void* GetDeviceFunc(const std::string& name) override;
public: /* Initialization */
    PFN_vkVoidFunction vkGetInstanceProcAddr(
        VkInstance instance,
        const char* pName
    ) override;
    VkResult vkCreateInstance(
        const VkInstanceCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkInstance* pInstance
    ) override;
    void vkDestroyInstance(
        VkInstance instance,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkEnumerateInstanceVersion(
        uint32_t* pApiVersion
    ) override;
public: /* Fundamentals */
    PFN_vkVoidFunction vkGetDeviceProcAddr(
        const char* pName
    ) override;
public: /* Devices and Queues */
    VkResult vkEnumeratePhysicalDevices(
        VkInstance instance,
        uint32_t* pPhysicalDeviceCount,
        VkPhysicalDevice* pPhysicalDevices
    ) override;
    void vkGetPhysicalDeviceProperties(
        VkPhysicalDeviceProperties* pProperties
    ) override;
    void vkGetPhysicalDeviceProperties2(
        VkPhysicalDeviceProperties2* pProperties
    ) override;
    void vkGetPhysicalDeviceQueueFamilyProperties(
        uint32_t* pQueueFamilyPropertyCount,
        VkQueueFamilyProperties* pQueueFamilyProperties
    ) override;
    void vkGetPhysicalDeviceQueueFamilyProperties2(
        uint32_t* pQueueFamilyPropertyCount,
        VkQueueFamilyProperties2* pQueueFamilyProperties
    ) override;
    VkResult vkCreateDevice(
        VkPhysicalDevice physicalDevice,
        const VkDeviceCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDevice* pDevice
    ) override;
    void vkDestroyDevice(
        const VkAllocationCallbacks* pAllocator
    ) override;
    void vkGetDeviceQueue(
        uint32_t queueFamilyIndex,
        uint32_t queueIndex,
        VkQueue* pQueue
    ) override;
public: /* Command Buffers */
    VkResult vkQueueSubmit(
        VkQueue queue,
        uint32_t submitCount,
        const VkSubmitInfo* pSubmits,
        VkFence fence
    ) override;
    VkResult vkQueueSubmit2(
        VkQueue queue,
        uint32_t submitCount,
        const VkSubmitInfo2* pSubmits,
        VkFence fence
    ) override;
    VkResult vkCreateCommandPool(
        const VkCommandPoolCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkCommandPool* pCommandPool
    ) override;
    void vkDestroyCommandPool(
        VkCommandPool commandPool,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkResetCommandPool(
        VkCommandPool commandPool,
        VkCommandPoolResetFlags flags
    ) override;
    VkResult vkAllocateCommandBuffers(
        const VkCommandBufferAllocateInfo* pAllocateInfo,
        VkCommandBuffer* pCommandBuffers
    ) override;
    void vkFreeCommandBuffers(
        VkCommandPool commandPool,
        uint32_t commandBufferCount,
        const VkCommandBuffer* pCommandBuffers
    ) override;
    VkResult vkBeginCommandBuffer(
        VkCommandBuffer commandBuffer,
        const VkCommandBufferBeginInfo* pBeginInfo
    ) override;
    VkResult vkEndCommandBuffer(
        VkCommandBuffer commandBuffer
    ) override;
    VkResult vkResetCommandBuffer(
        VkCommandBuffer commandBuffer,
        VkCommandBufferResetFlags flags
    ) override;
    void vkCmdExecuteCommands(
        VkCommandBuffer commandBuffer,
        uint32_t commandBufferCount,
        const VkCommandBuffer* pCommandBuffers
    ) override;
public: /* Synchronization and Cache Control */
    VkResult vkQueueWaitIdle(
        VkQueue queue
    ) override;
    VkResult vkDeviceWaitIdle(
    ) override;
    VkResult vkCreateFence(
        const VkFenceCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkFence* pFence
    ) override;
    void vkDestroyFence(
        VkFence fence,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkResetFences(
        uint32_t fenceCount,
        const VkFence* pFences
    ) override;
    VkResult vkGetFenceStatus(
        VkFence fence
    ) override;
    VkResult vkWaitForFences(
        uint32_t fenceCount,
        const VkFence* pFences,
        VkBool32 waitAll,
        uint64_t timeout
    ) override;
    VkResult vkCreateSemaphore(
        const VkSemaphoreCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkSemaphore* pSemaphore
    ) override;
    void vkDestroySemaphore(
        VkSemaphore semaphore,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkWaitSemaphores(
        const VkSemaphoreWaitInfo* pWaitInfo,
        uint64_t timeout
    ) override;
    VkResult vkCreateEvent(
        const VkEventCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkEvent* pEvent
    ) override;
    void vkDestroyEvent(
        VkEvent event,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkGetEventStatus(
        VkEvent event
    ) override;
    VkResult vkSetEvent(
        VkEvent event
    ) override;
    VkResult vkResetEvent(
        VkEvent event
    ) override;
    void vkCmdSetEvent(
        VkCommandBuffer commandBuffer,
        VkEvent event,
        VkPipelineStageFlags stageMask
    ) override;
    void vkCmdResetEvent(
        VkCommandBuffer commandBuffer,
        VkEvent event,
        VkPipelineStageFlags stageMask
    ) override;
    void vkCmdWaitEvents(
        VkCommandBuffer commandBuffer,
        uint32_t eventCount,
        const VkEvent* pEvents,
        VkPipelineStageFlags srcStageMask,
        VkPipelineStageFlags dstStageMask,
        uint32_t memoryBarrierCount,
        const VkMemoryBarrier* pMemoryBarriers,
        uint32_t bufferMemoryBarrierCount,
        const VkBufferMemoryBarrier* pBufferMemoryBarriers,
        uint32_t imageMemoryBarrierCount,
        const VkImageMemoryBarrier* pImageMemoryBarriers
    ) override;
public: /* Render Pass Commands */
    void vkCmdBeginRenderPass(
        VkCommandBuffer commandBuffer,
        const VkRenderPassBeginInfo* pRenderPassBegin,
        VkSubpassContents contents
    ) override;
    void vkCmdNextSubpass(
        VkCommandBuffer commandBuffer,
        VkSubpassContents contents
    ) override;
    void vkCmdEndRenderPass(
        VkCommandBuffer commandBuffer
    ) override;
public: /* Shaders */
    VkResult vkCreateShaderModule(
        const VkShaderModuleCreateInfo* pCreateInfo, 
        const VkAllocationCallbacks* pAllocator, 
        VkShaderModule* pShaderModule
    ) override;
    void vkDestroyShaderModule(
        VkShaderModule shaderModule,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR(
        uint32_t* pPropertyCount,
        VkCooperativeMatrixPropertiesKHR* pProperties
    ) override;
public: /* Render Pass */
    VkResult vkCreateFramebuffer(
        const VkFramebufferCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkFramebuffer* pFramebuffer
    ) override;
    void vkDestroyFramebuffer(
        VkFramebuffer framebuffer,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkCreateRenderPass(
        const VkRenderPassCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkRenderPass* pRenderPass
    ) override;
    void vkDestroyRenderPass(
        VkRenderPass renderPass,
        const VkAllocationCallbacks* pAllocator
    ) override;
    void vkGetRenderAreaGranularity(
        VkRenderPass renderPass,
        VkExtent2D* pGranularity
    ) override;
    VkResult vkCreateRenderPass2KHR(
        const VkRenderPassCreateInfo2* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkRenderPass* pRenderPass
    ) override;
public: /* Pipelines */
    VkResult vkCreatePipelineCache(
        const VkPipelineCacheCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkPipelineCache* pPipelineCache
    ) override;
    void vkDestroyPipelineCache(
        VkPipelineCache pipelineCache,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkGetPipelineCacheData(
        VkPipelineCache pipelineCache,
        size_t* pDataSize,
        void* pData
    ) override;
    VkResult vkMergePipelineCaches(
        VkPipelineCache dstCache,
        uint32_t srcCacheCount,
        const VkPipelineCache* pSrcCaches
    ) override;
    VkResult vkCreateGraphicsPipelines(
        VkPipelineCache pipelineCache,
        uint32_t createInfoCount,
        const VkGraphicsPipelineCreateInfo* pCreateInfos,
        const VkAllocationCallbacks* pAllocator,
        VkPipeline* pPipelines
    ) override;
    VkResult vkCreateComputePipelines(
        VkPipelineCache pipelineCache,
        uint32_t createInfoCount,
        const VkComputePipelineCreateInfo* pCreateInfos,
        const VkAllocationCallbacks* pAllocator,
        VkPipeline* pPipelines
    ) override;
    void vkDestroyPipeline(
        VkPipeline pipeline,
        const VkAllocationCallbacks* pAllocator
    ) override;
    void vkCmdBindPipeline(
        VkCommandBuffer commandBuffer,
        VkPipelineBindPoint pipelineBindPoint,
        VkPipeline pipeline
    ) override;
    void vkCmdPipelineBarrier(
        VkCommandBuffer commandBuffer,
        VkPipelineStageFlags srcStageMask,
        VkPipelineStageFlags dstStageMask,
        VkDependencyFlags dependencyFlags,
        uint32_t memoryBarrierCount,
        const VkMemoryBarrier* pMemoryBarriers,
        uint32_t bufferMemoryBarrierCount,
        const VkBufferMemoryBarrier* pBufferMemoryBarriers,
        uint32_t imageMemoryBarrierCount,
        const VkImageMemoryBarrier* pImageMemoryBarriers
    ) override;
public: /* Memory Allocation */
    void vkGetPhysicalDeviceMemoryProperties(
        VkPhysicalDeviceMemoryProperties* pMemoryProperties
    ) override;
    VkResult vkAllocateMemory(
        const VkMemoryAllocateInfo* pAllocateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDeviceMemory* pMemory
    ) override;
    void vkFreeMemory(
        VkDeviceMemory memory,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkMapMemory(
        VkDeviceMemory memory,
        VkDeviceSize offset,
        VkDeviceSize size,
        VkMemoryMapFlags flags,
        void** ppData
    ) override;
    void vkUnmapMemory(
        VkDeviceMemory memory
    ) override;
    VkResult vkFlushMappedMemoryRanges(
        uint32_t memoryRangeCount,
        const VkMappedMemoryRange* pMemoryRanges
    ) override;
    VkResult vkInvalidateMappedMemoryRanges(
        uint32_t memoryRangeCount,
        const VkMappedMemoryRange* pMemoryRanges
    ) override;
    void vkGetDeviceMemoryCommitment(
        VkDeviceMemory memory,
        VkDeviceSize* pCommittedMemoryInBytes
    ) override;
    VkResult vkCreateBuffer(
        const VkBufferCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkBuffer* pBuffer
    ) override;
    void vkDestroyBuffer(
        VkBuffer buffer,
        const VkAllocationCallbacks* pAllocator
    ) override;
public: /* Resource Creation */
    VkResult vkBindBufferMemory(
        VkBuffer buffer,
        VkDeviceMemory memory,
        VkDeviceSize memoryOffset
    ) override;
    VkResult vkBindBufferMemory2(
        uint32_t bindInfoCount,
        const VkBindBufferMemoryInfo* pBindInfos
    ) override;
    VkResult vkBindImageMemory(
        VkImage image,
        VkDeviceMemory memory,
        VkDeviceSize memoryOffset
    ) override;
    VkResult vkBindImageMemory2(
        uint32_t bindInfoCount,
        const VkBindImageMemoryInfo* pBindInfos
    ) override;
    void vkGetBufferMemoryRequirements(
        VkBuffer buffer,
        VkMemoryRequirements* pMemoryRequirements
    ) override;
    void vkGetBufferMemoryRequirements2(
        const VkBufferMemoryRequirementsInfo2* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    ) override;
    void vkGetDeviceBufferMemoryRequirements(
        const VkDeviceBufferMemoryRequirements* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    ) override;
    void vkGetImageMemoryRequirements(
        VkImage image,
        VkMemoryRequirements* pMemoryRequirements
    ) override;
    void vkGetImageMemoryRequirements2(
        const VkImageMemoryRequirementsInfo2* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    ) override;
    void vkGetDeviceImageMemoryRequirements(
        const VkDeviceImageMemoryRequirements* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    ) override;
    VkResult vkCreateBufferView(
        const VkBufferViewCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkBufferView* pView
    ) override;
    void vkDestroyBufferView(
        VkBufferView bufferView,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkCreateImage(
        const VkImageCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkImage* pImage
    ) override;
    void vkDestroyImage(
        VkImage image,
        const VkAllocationCallbacks* pAllocator
    ) override;
    void vkGetImageSubresourceLayout(
        VkImage image,
        const VkImageSubresource* pSubresource,
        VkSubresourceLayout* pLayout
    ) override;
    VkResult vkCreateImageView(
        const VkImageViewCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkImageView* pView
    ) override;
    void vkDestroyImageView(
        VkImageView imageView,
        const VkAllocationCallbacks* pAllocator
    ) override;
    void vkGetBufferMemoryRequirements2KHR(
        const VkBufferMemoryRequirementsInfo2* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    ) override;
    void vkGetImageMemoryRequirements2KHR(
        const VkImageMemoryRequirementsInfo2* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    ) override;
public: /* Samplers */
    VkResult vkCreateSampler(
        const VkSamplerCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkSampler* pSampler
    ) override;
    void vkDestroySampler(
        VkSampler sampler,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkCreateSamplerYcbcrConversion(
        const VkSamplerYcbcrConversionCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkSamplerYcbcrConversion* pYcbcrConversion
    ) override;
    void vkDestroySamplerYcbcrConversion(
        VkSamplerYcbcrConversion ycbcrConversion,
        const VkAllocationCallbacks* pAllocator
    ) override;
public: /* Resource Descriptors */
    VkResult vkCreatePipelineLayout(
        const VkPipelineLayoutCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkPipelineLayout* pPipelineLayout
    ) override;
    void vkDestroyPipelineLayout(
        VkPipelineLayout pipelineLayout,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkCreateDescriptorSetLayout(
        const VkDescriptorSetLayoutCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDescriptorSetLayout* pSetLayout
    ) override;
    void vkDestroyDescriptorSetLayout(
        VkDescriptorSetLayout descriptorSetLayout,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkCreateDescriptorPool(
        const VkDescriptorPoolCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDescriptorPool* pDescriptorPool
    ) override;
    void vkDestroyDescriptorPool(
        VkDescriptorPool descriptorPool,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkResetDescriptorPool(
        VkDescriptorPool descriptorPool,
        VkDescriptorPoolResetFlags flags
    ) override;
    VkResult vkAllocateDescriptorSets(
        const VkDescriptorSetAllocateInfo* pAllocateInfo,
        VkDescriptorSet* pDescriptorSets
    ) override;
    VkResult vkFreeDescriptorSets(
        VkDescriptorPool descriptorPool,
        uint32_t descriptorSetCount,
        const VkDescriptorSet* pDescriptorSets
    ) override;
    void vkCmdBindDescriptorSets(
        VkCommandBuffer commandBuffer,
        VkPipelineBindPoint pipelineBindPoint,
        VkPipelineLayout layout,
        uint32_t firstSet,
        uint32_t descriptorSetCount,
        const VkDescriptorSet* pDescriptorSets,
        uint32_t dynamicOffsetCount,
        const uint32_t* pDynamicOffsets
    ) override;
    void vkCmdPushConstants(
        VkCommandBuffer commandBuffer,
        VkPipelineLayout layout,
        VkShaderStageFlags stageFlags,
        uint32_t offset,
        uint32_t size,
        const void* pValues
    ) override;
public: /* Queries */
    VkResult vkCreateQueryPool(
        const VkQueryPoolCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkQueryPool* pQueryPool
    ) override;
    void vkDestroyQueryPool(
        VkQueryPool queryPool,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkGetQueryPoolResults(
        VkQueryPool queryPool,
        uint32_t firstQuery,
        uint32_t queryCount,
        size_t dataSize,
        void* pData,
        VkDeviceSize stride,
        VkQueryResultFlags flags
    ) override;
    void vkCmdBeginQuery(
        VkCommandBuffer commandBuffer,
        VkQueryPool queryPool,
        uint32_t query,
        VkQueryControlFlags flags
    ) override;
    void vkCmdEndQuery(
        VkCommandBuffer commandBuffer,
        VkQueryPool queryPool,
        uint32_t query
    ) override;
    void vkCmdResetQueryPool(
        VkCommandBuffer commandBuffer,
        VkQueryPool queryPool,
        uint32_t firstQuery,
        uint32_t queryCount
    ) override;
    void vkCmdWriteTimestamp(
        VkCommandBuffer commandBuffer,
        VkPipelineStageFlagBits pipelineStage,
        VkQueryPool queryPool,
        uint32_t query
    ) override;
    void vkCmdCopyQueryPoolResults(
        VkCommandBuffer commandBuffer,
        VkQueryPool queryPool,
        uint32_t firstQuery,
        uint32_t queryCount,
        VkBuffer dstBuffer,
        VkDeviceSize dstOffset,
        VkDeviceSize stride,
        VkQueryResultFlags flags
    ) override;
public: /* Clear Commands */
    void vkCmdUpdateBuffer(
        VkCommandBuffer commandBuffer,
        VkBuffer dstBuffer,
        VkDeviceSize dstOffset,
        VkDeviceSize dataSize,
        const void* pData
    ) override;
    void vkCmdFillBuffer(
        VkCommandBuffer commandBuffer,
        VkBuffer dstBuffer,
        VkDeviceSize dstOffset,
        VkDeviceSize size,
        uint32_t data
    ) override;
    void vkCmdClearColorImage(
        VkCommandBuffer commandBuffer,
        VkImage image,
        VkImageLayout imageLayout,
        const VkClearColorValue* pColor,
        uint32_t rangeCount,
        const VkImageSubresourceRange* pRanges
    ) override;
    void vkCmdClearDepthStencilImage(
        VkCommandBuffer commandBuffer,
        VkImage image,
        VkImageLayout imageLayout,
        const VkClearDepthStencilValue* pDepthStencil,
        uint32_t rangeCount,
        const VkImageSubresourceRange* pRanges
    ) override;
    void vkCmdClearAttachments(
        VkCommandBuffer commandBuffer,
        uint32_t attachmentCount,
        const VkClearAttachment* pAttachments,
        uint32_t rectCount,
        const VkClearRect* pRects
    ) override;
public: /* Copy Commands */
    void vkCmdCopyBuffer(
        VkCommandBuffer commandBuffer,
        VkBuffer srcBuffer,
        VkBuffer dstBuffer,
        uint32_t regionCount,
        const VkBufferCopy* pRegions
    ) override;
    void vkCmdCopyImage(
        VkCommandBuffer commandBuffer,
        VkImage srcImage,
        VkImageLayout srcImageLayout,
        VkImage dstImage,
        VkImageLayout dstImageLayout,
        uint32_t regionCount,
        const VkImageCopy* pRegions
    ) override;
    void vkCmdBlitImage(
        VkCommandBuffer commandBuffer,
        VkImage srcImage,
        VkImageLayout srcImageLayout,
        VkImage dstImage,
        VkImageLayout dstImageLayout,
        uint32_t regionCount,
        const VkImageBlit* pRegions,
        VkFilter filter
    ) override;
    void vkCmdCopyImageToBuffer(
        VkCommandBuffer commandBuffer,
        VkImage srcImage,
        VkImageLayout srcImageLayout,
        VkBuffer dstBuffer,
        uint32_t regionCount,
        const VkBufferImageCopy* pRegions
    ) override;
    void vkCmdResolveImage(
        VkCommandBuffer commandBuffer,
        VkImage srcImage,
        VkImageLayout srcImageLayout,
        VkImage dstImage,
        VkImageLayout dstImageLayout,
        uint32_t regionCount,
        const VkImageResolve* pRegions
    ) override;
    void vkCmdCopyBufferToImage(
        VkCommandBuffer commandBuffer,
         VkBuffer srcBuffer,
         VkImage dstImage,
         VkImageLayout dstImageLayout,
         uint32_t regionCount,
         const VkBufferImageCopy* pRegions
    ) override;
public: /* Drawing Commands */
    void vkCmdBindIndexBuffer(
        VkCommandBuffer commandBuffer,
        VkBuffer buffer,
        VkDeviceSize offset,
        VkIndexType indexType
    ) override;
    void vkCmdBindVertexBuffers(
        VkCommandBuffer commandBuffer,
        uint32_t firstBinding,
        uint32_t bindingCount,
        const VkBuffer* pBuffers,
        const VkDeviceSize* pOffsets
    ) override;
    void vkCmdDraw(
        VkCommandBuffer commandBuffer,
        uint32_t vertexCount,
        uint32_t instanceCount,
        uint32_t firstVertex,
        uint32_t firstInstance
    ) override;
    void vkCmdDrawIndexed(
        VkCommandBuffer commandBuffer,
        uint32_t indexCount,
        uint32_t instanceCount,
        uint32_t firstIndex,
        int32_t vertexOffset,
        uint32_t firstInstance
    ) override;
    void vkCmdDrawIndirect(
        VkCommandBuffer commandBuffer,
        VkBuffer buffer,
        VkDeviceSize offset,
        uint32_t drawCount,
        uint32_t stride
    ) override;
    void vkCmdDrawIndexedIndirect(
        VkCommandBuffer commandBuffer,
        VkBuffer buffer,
        VkDeviceSize offset,
        uint32_t drawCount,
        uint32_t stride
    ) override;
public: /* Fixed-Function Vertex Post Processing */
    void vkCmdSetViewport(
        VkCommandBuffer commandBuffer,
        uint32_t firstViewport,
        uint32_t viewportCount,
        const VkViewport* pViewports
    ) override;
public: /* Rasterization */
    void vkCmdSetLineWidth(
        VkCommandBuffer commandBuffer,
        float lineWidth
    ) override;
    void vkCmdSetDepthBias(
        VkCommandBuffer commandBuffer,
        float depthBiasConstantFactor,
        float depthBiasClamp,
        float depthBiasSlopeFactor
    ) override;
public: /* Fragment Operations */
    void vkCmdSetScissor(
        VkCommandBuffer commandBuffer,
        uint32_t firstScissor,
        uint32_t scissorCount,
        const VkRect2D* pScissors
    ) override;
public: /* The Framebuffer */
    void vkCmdSetBlendConstants(
        VkCommandBuffer commandBuffer,
        const float blendConstants[4]
    ) override;
    void vkCmdSetDepthBounds(
        VkCommandBuffer commandBuffer,
        float minDepthBounds,
        float maxDepthBounds
    ) override;
    void vkCmdSetStencilCompareMask(
        VkCommandBuffer commandBuffer,
        VkStencilFaceFlags faceMask,
        uint32_t compareMask
    ) override;
    void vkCmdSetStencilWriteMask(
        VkCommandBuffer commandBuffer,
        VkStencilFaceFlags faceMask,
        uint32_t writeMask
    ) override;
    void vkCmdSetStencilReference(
        VkCommandBuffer commandBuffer,
        VkStencilFaceFlags faceMask,
        uint32_t reference
    ) override;
public: /* Dispatching Commands */
    void vkCmdDispatch(
        VkCommandBuffer commandBuffer,
        uint32_t groupCountX,
        uint32_t groupCountY,
        uint32_t groupCountZ
    ) override;
    void vkCmdDispatchIndirect(
        VkCommandBuffer commandBuffer,
        VkBuffer buffer,
        VkDeviceSize offset
    ) override;
public: /* Sparse Resources */
    VkResult vkQueueBindSparse(
        VkQueue queue,
        uint32_t bindInfoCount,
        const VkBindSparseInfo* pBindInfo,
        VkFence fence
    ) override;
public: /* Window System Integration (WSI) */
    VkResult vkGetPhysicalDeviceSurfaceFormatsKHR(
        VkSurfaceKHR surface,
        uint32_t* pSurfaceFormatCount,
        VkSurfaceFormatKHR* pSurfaceFormats
    ) override;
    void vkDestroySurfaceKHR(
        VkSurfaceKHR surface,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkCreateSwapchainKHR(
        const VkSwapchainCreateInfoKHR* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkSwapchainKHR* pSwapchain
    ) override;
    void vkDestroySwapchainKHR(
        VkSwapchainKHR swapchain,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkGetSwapchainImagesKHR(
        VkSwapchainKHR swapchain,
        uint32_t* pSwapchainImageCount,
        VkImage* pSwapchainImages
    ) override;
    VkResult vkAcquireNextImageKHR(
        VkSwapchainKHR swapchain,
        uint64_t timeout,
        VkSemaphore semaphore,
        VkFence fence,
        uint32_t* pImageIndex
    ) override;
    VkResult vkQueuePresentKHR(
        VkQueue queue,
        const VkPresentInfoKHR* pPresentInfo
    ) override;
    VkResult vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
        VkSurfaceKHR surface,
        VkSurfaceCapabilitiesKHR* pSurfaceCapabilities
    ) override;
    VkResult vkGetPhysicalDeviceSurfacePresentModesKHR(
        VkSurfaceKHR surface,
        uint32_t* pPresentModeCount,
        VkPresentModeKHR* pPresentModes
    ) override;
public: /* Deferred Host Operations */
    VkResult vkWaitForPresentKHR(
        VkSwapchainKHR swapchain,
        uint64_t presentId,
        uint64_t timeout
    ) override;
public: /* Features */
    void vkGetPhysicalDeviceFeatures(
        VkPhysicalDeviceFeatures* pFeatures
    ) override;
    void vkGetPhysicalDeviceFeatures2(
        VkPhysicalDeviceFeatures2* pFeatures
    ) override;
    void vkGetPhysicalDeviceFormatProperties(
        VkFormat format,
        VkFormatProperties* pFormatProperties
    ) override;
public: /* Additional Capabilities */
    VkResult vkGetPhysicalDeviceImageFormatProperties(
        VkPhysicalDevice physicalDevice,
        VkFormat format,
        VkImageType type,
        VkImageTiling tiling,
        VkImageUsageFlags usage,
        VkImageCreateFlags flags,
        VkImageFormatProperties* pImageFormatProperties
    ) override;
public: /* Extending Vulkan */
    VkResult vkEnumerateInstanceExtensionProperties(
        const char* pLayerName,
        uint32_t* pPropertyCount,
        VkExtensionProperties* pProperties
    ) override;
    VkResult vkEnumerateDeviceExtensionProperties(
        const char* pLayerName,
        uint32_t* pPropertyCount,
        VkExtensionProperties* pProperties
    ) override;
    VkResult vkEnumerateInstanceLayerProperties(
        uint32_t* pPropertyCount,
        VkLayerProperties* pProperties
    ) override;
    VkResult vkEnumerateDeviceLayerProperties(
        VkPhysicalDevice physicalDevice,
        uint32_t* pPropertyCount,
        VkLayerProperties* pProperties
    ) override;
public: /* Video Coding */
    VkResult vkCreateVideoSessionKHR(
        const VkVideoSessionCreateInfoKHR* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkVideoSessionKHR* pVideoSession
    ) override;
    void vkDestroyVideoSessionKHR(
        VkVideoSessionKHR videoSession,
        const VkAllocationCallbacks* pAllocator
    ) override;
    VkResult vkGetVideoSessionMemoryRequirementsKHR(
        VkVideoSessionKHR videoSession,
        uint32_t* pMemoryRequirementsCount,
        VkVideoSessionMemoryRequirementsKHR* pMemoryRequirements
    ) override;
    VkResult vkBindVideoSessionMemoryKHR(
        VkVideoSessionKHR videoSession,
        uint32_t bindSessionMemoryInfoCount,
        const VkBindVideoSessionMemoryInfoKHR* pBindSessionMemoryInfos
    ) override;
    void vkCmdBeginVideoCodingKHR(
        VkCommandBuffer commandBuffer,
        const VkVideoBeginCodingInfoKHR* pBeginInfo
    ) override;
    void vkCmdControlVideoCodingKHR(
        VkCommandBuffer commandBuffer,
        const VkVideoCodingControlInfoKHR* pCodingControlInfo
    ) override;
    void vkCmdDecodeVideoKHR(
        VkCommandBuffer commandBuffer,
        const VkVideoDecodeInfoKHR* pDecodeInfo
    ) override;
    void vkCmdEndVideoCodingKHR(
        VkCommandBuffer commandBuffer,
        const VkVideoEndCodingInfoKHR* pEndCodingInfo
    ) override;
    VkResult vkGetPhysicalDeviceVideoCapabilitiesKHR(
        const VkVideoProfileInfoKHR* pVideoProfile,
        VkVideoCapabilitiesKHR* pCapabilities
    ) override;
    VkResult vkGetPhysicalDeviceVideoFormatPropertiesKHR(
        const VkPhysicalDeviceVideoFormatInfoKHR* pVideoFormatInfo,
        uint32_t* pVideoFormatPropertyCount,
        VkVideoFormatPropertiesKHR* pVideoFormatProperties
    ) override;
    VkResult vkCreateVideoSessionParametersKHR(
        const VkVideoSessionParametersCreateInfoKHR* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkVideoSessionParametersKHR* pVideoSessionParameters
    ) override;
    void vkDestroyVideoSessionParametersKHR(
        VkVideoSessionParametersKHR videoSessionParameters,
        const VkAllocationCallbacks* pAllocator
    ) override;
    void vkGetPhysicalDeviceFormatProperties2(
        VkFormat format,
        VkFormatProperties2* pFormatProperties
    ) override;
    VkResult vkGetPhysicalDeviceImageFormatProperties2(
        const VkPhysicalDeviceImageFormatInfo2* pImageFormatInfo,
        VkImageFormatProperties2* pImageFormatProperties
    ) override;
private:
    PFN_vkCreateInstance _vkCreateInstance = nullptr;
    PFN_vkDestroyInstance _vkDestroyInstance = nullptr;
    PFN_vkEnumeratePhysicalDevices _vkEnumeratePhysicalDevices = nullptr;
    PFN_vkEnumerateInstanceVersion _vkEnumerateInstanceVersion = nullptr;
    PFN_vkGetPhysicalDeviceFeatures _vkGetPhysicalDeviceFeatures = nullptr;
    PFN_vkGetPhysicalDeviceFeatures2 _vkGetPhysicalDeviceFeatures2 = nullptr;
    PFN_vkGetPhysicalDeviceFormatProperties _vkGetPhysicalDeviceFormatProperties = nullptr;
    PFN_vkGetPhysicalDeviceImageFormatProperties _vkGetPhysicalDeviceImageFormatProperties = nullptr;
    PFN_vkGetPhysicalDeviceProperties _vkGetPhysicalDeviceProperties = nullptr;
    PFN_vkGetPhysicalDeviceProperties2 _vkGetPhysicalDeviceProperties2 = nullptr;
    PFN_vkGetPhysicalDeviceQueueFamilyProperties _vkGetPhysicalDeviceQueueFamilyProperties = nullptr;
    PFN_vkGetPhysicalDeviceQueueFamilyProperties2 _vkGetPhysicalDeviceQueueFamilyProperties2 = nullptr;
    PFN_vkGetPhysicalDeviceMemoryProperties _vkGetPhysicalDeviceMemoryProperties = nullptr;
    PFN_vkGetInstanceProcAddr _vkGetInstanceProcAddr = nullptr;
    PFN_vkGetDeviceProcAddr _vkGetDeviceProcAddr = nullptr;
    PFN_vkCreateDevice _vkCreateDevice = nullptr;
    PFN_vkDestroyDevice _vkDestroyDevice = nullptr;
    PFN_vkEnumerateInstanceExtensionProperties _vkEnumerateInstanceExtensionProperties = nullptr;
    PFN_vkEnumerateDeviceExtensionProperties _vkEnumerateDeviceExtensionProperties = nullptr;
    PFN_vkEnumerateInstanceLayerProperties _vkEnumerateInstanceLayerProperties = nullptr;
    PFN_vkEnumerateDeviceLayerProperties _vkEnumerateDeviceLayerProperties = nullptr;
    PFN_vkCreateVideoSessionKHR _vkCreateVideoSessionKHR = nullptr;
    PFN_vkDestroyVideoSessionKHR _vkDestroyVideoSessionKHR = nullptr;
    PFN_vkGetVideoSessionMemoryRequirementsKHR _vkGetVideoSessionMemoryRequirementsKHR = nullptr;
    PFN_vkBindVideoSessionMemoryKHR _vkBindVideoSessionMemoryKHR = nullptr;
    PFN_vkCmdBeginVideoCodingKHR _vkCmdBeginVideoCodingKHR = nullptr;
    PFN_vkCmdControlVideoCodingKHR _vkCmdControlVideoCodingKHR = nullptr;
    PFN_vkCmdDecodeVideoKHR _vkCmdDecodeVideoKHR = nullptr;
    PFN_vkCmdEndVideoCodingKHR _vkCmdEndVideoCodingKHR = nullptr;
    PFN_vkGetPhysicalDeviceVideoCapabilitiesKHR _vkGetPhysicalDeviceVideoCapabilitiesKHR = nullptr;
    PFN_vkGetPhysicalDeviceVideoFormatPropertiesKHR _vkGetPhysicalDeviceVideoFormatPropertiesKHR = nullptr;
    PFN_vkCreateVideoSessionParametersKHR _vkCreateVideoSessionParametersKHR = nullptr;
    PFN_vkDestroyVideoSessionParametersKHR _vkDestroyVideoSessionParametersKHR = nullptr;
    PFN_vkGetDeviceQueue _vkGetDeviceQueue = nullptr;
    PFN_vkQueueSubmit _vkQueueSubmit = nullptr;
    PFN_vkQueueSubmit2 _vkQueueSubmit2 = nullptr;
    PFN_vkQueueWaitIdle _vkQueueWaitIdle = nullptr;
    PFN_vkDeviceWaitIdle _vkDeviceWaitIdle = nullptr;
    PFN_vkAllocateMemory _vkAllocateMemory = nullptr;
    PFN_vkFreeMemory _vkFreeMemory = nullptr;
    PFN_vkMapMemory _vkMapMemory = nullptr;
    PFN_vkUnmapMemory _vkUnmapMemory = nullptr;
    PFN_vkFlushMappedMemoryRanges _vkFlushMappedMemoryRanges = nullptr;
    PFN_vkInvalidateMappedMemoryRanges _vkInvalidateMappedMemoryRanges = nullptr;
    PFN_vkGetDeviceMemoryCommitment _vkGetDeviceMemoryCommitment = nullptr;
    PFN_vkBindBufferMemory _vkBindBufferMemory = nullptr;
    PFN_vkBindBufferMemory2 _vkBindBufferMemory2 = nullptr;
    PFN_vkBindImageMemory _vkBindImageMemory = nullptr;
    PFN_vkBindImageMemory2 _vkBindImageMemory2 = nullptr;
    PFN_vkGetBufferMemoryRequirements _vkGetBufferMemoryRequirements = nullptr;
    PFN_vkGetBufferMemoryRequirements2 _vkGetBufferMemoryRequirements2 = nullptr;
    PFN_vkGetDeviceBufferMemoryRequirements _vkGetDeviceBufferMemoryRequirements = nullptr;
    PFN_vkGetImageMemoryRequirements _vkGetImageMemoryRequirements = nullptr;
    PFN_vkGetImageMemoryRequirements2 _vkGetImageMemoryRequirements2 = nullptr;
    PFN_vkGetDeviceImageMemoryRequirements _vkGetDeviceImageMemoryRequirements = nullptr;
    PFN_vkCreateFence _vkCreateFence = nullptr;
    PFN_vkDestroyFence _vkDestroyFence = nullptr;
    PFN_vkGetFenceStatus _vkGetFenceStatus = nullptr;
    PFN_vkCreateSemaphore _vkCreateSemaphore = nullptr;
    PFN_vkDestroySemaphore _vkDestroySemaphore = nullptr;
    PFN_vkWaitSemaphores _vkWaitSemaphores = nullptr;
    PFN_vkCreateEvent _vkCreateEvent = nullptr;
    PFN_vkDestroyEvent _vkDestroyEvent = nullptr;
    PFN_vkGetEventStatus _vkGetEventStatus = nullptr;
    PFN_vkSetEvent _vkSetEvent = nullptr;
    PFN_vkResetEvent _vkResetEvent = nullptr;
    PFN_vkCreateQueryPool _vkCreateQueryPool = nullptr;
    PFN_vkDestroyQueryPool _vkDestroyQueryPool = nullptr;
    PFN_vkGetQueryPoolResults _vkGetQueryPoolResults = nullptr;
    PFN_vkCreateBuffer _vkCreateBuffer = nullptr;
    PFN_vkDestroyBuffer _vkDestroyBuffer = nullptr;
    PFN_vkCreateBufferView _vkCreateBufferView = nullptr;
    PFN_vkDestroyBufferView _vkDestroyBufferView = nullptr;
    PFN_vkCreateImage _vkCreateImage = nullptr;
    PFN_vkDestroyImage _vkDestroyImage = nullptr;
    PFN_vkGetImageSubresourceLayout _vkGetImageSubresourceLayout = nullptr;
    PFN_vkCreateImageView _vkCreateImageView = nullptr;
    PFN_vkDestroyImageView _vkDestroyImageView = nullptr;
    PFN_vkCreateShaderModule _vkCreateShaderModule = nullptr;
    PFN_vkDestroyShaderModule _vkDestroyShaderModule = nullptr;
    PFN_vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR _vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR = nullptr;
    PFN_vkCreatePipelineCache _vkCreatePipelineCache = nullptr;
    PFN_vkDestroyPipelineCache _vkDestroyPipelineCache = nullptr;
    PFN_vkGetPipelineCacheData _vkGetPipelineCacheData = nullptr;
    PFN_vkMergePipelineCaches _vkMergePipelineCaches = nullptr;
    PFN_vkCreateGraphicsPipelines _vkCreateGraphicsPipelines = nullptr;
    PFN_vkCreateComputePipelines _vkCreateComputePipelines = nullptr;
    PFN_vkDestroyPipeline _vkDestroyPipeline = nullptr;
    PFN_vkCreatePipelineLayout _vkCreatePipelineLayout = nullptr;
    PFN_vkDestroyPipelineLayout _vkDestroyPipelineLayout = nullptr;
    PFN_vkCreateSampler _vkCreateSampler = nullptr;
    PFN_vkDestroySampler _vkDestroySampler = nullptr;
    PFN_vkCreateSamplerYcbcrConversion _vkCreateSamplerYcbcrConversion = nullptr;
    PFN_vkDestroySamplerYcbcrConversion _vkDestroySamplerYcbcrConversion = nullptr;
    PFN_vkCreateDescriptorSetLayout _vkCreateDescriptorSetLayout = nullptr;
    PFN_vkDestroyDescriptorSetLayout _vkDestroyDescriptorSetLayout = nullptr;
    PFN_vkCreateDescriptorPool _vkCreateDescriptorPool = nullptr;
    PFN_vkDestroyDescriptorPool _vkDestroyDescriptorPool = nullptr;
    PFN_vkResetDescriptorPool _vkResetDescriptorPool = nullptr;
    PFN_vkAllocateDescriptorSets _vkAllocateDescriptorSets = nullptr;
    PFN_vkFreeDescriptorSets _vkFreeDescriptorSets = nullptr;
    PFN_vkUpdateDescriptorSets _vkUpdateDescriptorSets = nullptr;
    PFN_vkCreateFramebuffer _vkCreateFramebuffer = nullptr;
    PFN_vkDestroyFramebuffer _vkDestroyFramebuffer = nullptr;
    PFN_vkCreateRenderPass _vkCreateRenderPass = nullptr;
    PFN_vkDestroyRenderPass _vkDestroyRenderPass = nullptr;
    PFN_vkGetRenderAreaGranularity _vkGetRenderAreaGranularity = nullptr;
    PFN_vkCreateCommandPool _vkCreateCommandPool = nullptr;
    PFN_vkDestroyCommandPool _vkDestroyCommandPool = nullptr;
    PFN_vkResetCommandPool _vkResetCommandPool = nullptr;
    PFN_vkAllocateCommandBuffers _vkAllocateCommandBuffers = nullptr;
    PFN_vkFreeCommandBuffers _vkFreeCommandBuffers = nullptr;
    
    // Used frequently together
    PFN_vkCmdBindPipeline _vkCmdBindPipeline = nullptr;
    PFN_vkCmdSetViewport _vkCmdSetViewport = nullptr;
    PFN_vkCmdSetScissor _vkCmdSetScissor = nullptr;
    PFN_vkCmdSetBlendConstants _vkCmdSetBlendConstants = nullptr;
    PFN_vkCmdSetStencilCompareMask _vkCmdSetStencilCompareMask = nullptr;
    PFN_vkCmdSetStencilWriteMask _vkCmdSetStencilWriteMask = nullptr;
    PFN_vkCmdSetStencilReference _vkCmdSetStencilReference = nullptr;
    PFN_vkCmdBindDescriptorSets _vkCmdBindDescriptorSets = nullptr;
    PFN_vkCmdBindIndexBuffer _vkCmdBindIndexBuffer = nullptr;
    PFN_vkCmdBindVertexBuffers _vkCmdBindVertexBuffers = nullptr;
    PFN_vkCmdDraw _vkCmdDraw = nullptr;
    PFN_vkCmdDrawIndexed _vkCmdDrawIndexed = nullptr;
    PFN_vkCmdPipelineBarrier _vkCmdPipelineBarrier = nullptr;
    PFN_vkCmdPushConstants _vkCmdPushConstants = nullptr;

    // Every frame to a few times per frame
    PFN_vkWaitForFences _vkWaitForFences = nullptr;
    PFN_vkResetFences _vkResetFences = nullptr;
    PFN_vkBeginCommandBuffer _vkBeginCommandBuffer = nullptr;
    PFN_vkEndCommandBuffer _vkEndCommandBuffer = nullptr;
    PFN_vkResetCommandBuffer _vkResetCommandBuffer = nullptr;
    PFN_vkCmdClearAttachments _vkCmdClearAttachments = nullptr;
    PFN_vkCmdSetEvent _vkCmdSetEvent = nullptr;
    PFN_vkCmdResetEvent _vkCmdResetEvent = nullptr;
    PFN_vkCmdWaitEvents _vkCmdWaitEvents = nullptr;
    PFN_vkCmdBeginRenderPass _vkCmdBeginRenderPass = nullptr;
    PFN_vkCmdEndRenderPass _vkCmdEndRenderPass = nullptr;
    PFN_vkCmdCopyBuffer _vkCmdCopyBuffer = nullptr;
    PFN_vkCmdCopyImage _vkCmdCopyImage = nullptr;
    PFN_vkCmdBlitImage _vkCmdBlitImage = nullptr;
    PFN_vkCmdCopyBufferToImage _vkCmdCopyBufferToImage = nullptr;
    PFN_vkCmdCopyImageToBuffer _vkCmdCopyImageToBuffer = nullptr;

    // Rare or not used
    PFN_vkCmdSetDepthBounds _vkCmdSetDepthBounds = nullptr;
    PFN_vkCmdSetLineWidth _vkCmdSetLineWidth = nullptr;
    PFN_vkCmdSetDepthBias _vkCmdSetDepthBias = nullptr;
    PFN_vkCmdDrawIndirect _vkCmdDrawIndirect = nullptr;
    PFN_vkCmdDrawIndexedIndirect _vkCmdDrawIndexedIndirect = nullptr;
    PFN_vkCmdDispatch _vkCmdDispatch = nullptr;
    PFN_vkCmdDispatchIndirect _vkCmdDispatchIndirect = nullptr;
    PFN_vkCmdUpdateBuffer _vkCmdUpdateBuffer = nullptr;
    PFN_vkCmdFillBuffer _vkCmdFillBuffer = nullptr;
    PFN_vkCmdClearColorImage _vkCmdClearColorImage = nullptr;
    PFN_vkCmdClearDepthStencilImage _vkCmdClearDepthStencilImage = nullptr;
    PFN_vkCmdResolveImage _vkCmdResolveImage = nullptr;
    PFN_vkCmdBeginQuery _vkCmdBeginQuery = nullptr;
    PFN_vkCmdEndQuery _vkCmdEndQuery = nullptr;
    PFN_vkCmdResetQueryPool _vkCmdResetQueryPool = nullptr;
    PFN_vkCmdWriteTimestamp _vkCmdWriteTimestamp = nullptr;
    PFN_vkCmdCopyQueryPoolResults _vkCmdCopyQueryPoolResults = nullptr;
    PFN_vkCmdNextSubpass _vkCmdNextSubpass = nullptr;
    PFN_vkCmdExecuteCommands _vkCmdExecuteCommands = nullptr;
private:
    PFN_vkDestroySurfaceKHR _vkDestroySurfaceKHR = nullptr;
    PFN_vkGetPhysicalDeviceSurfaceFormatsKHR _vkGetPhysicalDeviceSurfaceFormatsKHR = nullptr;
    PFN_vkCreateSwapchainKHR _vkCreateSwapchainKHR = nullptr;
    PFN_vkDestroySwapchainKHR _vkDestroySwapchainKHR = nullptr;
    PFN_vkGetSwapchainImagesKHR _vkGetSwapchainImagesKHR = nullptr;
    PFN_vkAcquireNextImageKHR _vkAcquireNextImageKHR = nullptr;
    PFN_vkQueuePresentKHR _vkQueuePresentKHR = nullptr;
    PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR _vkGetPhysicalDeviceSurfaceCapabilitiesKHR = nullptr;
    PFN_vkGetPhysicalDeviceSurfacePresentModesKHR _vkGetPhysicalDeviceSurfacePresentModesKHR = nullptr;
    PFN_vkCreateRenderPass2KHR _vkCreateRenderPass2KHR = nullptr;
    PFN_vkWaitForPresentKHR _vkWaitForPresentKHR = nullptr;
    PFN_vkGetBufferMemoryRequirements2KHR _vkGetBufferMemoryRequirements2KHR = nullptr;
    PFN_vkGetImageMemoryRequirements2KHR _vkGetImageMemoryRequirements2KHR = nullptr;
    PFN_vkGetPhysicalDeviceFormatProperties2 _vkGetPhysicalDeviceFormatProperties2 = nullptr;
    PFN_vkGetPhysicalDeviceImageFormatProperties2 _vkGetPhysicalDeviceImageFormatProperties2 = nullptr;
public:
    bool Init();
private:
    Poco::SharedLibrary _vulkanLoader;
private:
    uint32_t                                _apiVersion;
    uint32_t                                _vulkanApiVersion = VK_API_VERSION_1_2;
private:
    bool CreateInstance();
    void DestroyInstance();
    VkResult GetInstanceLayerProperties();
    VkResult GetInstanceLayerExtensionList(const std::string& layerName, std::vector<VkExtensionProperties> &extensions);
    bool IsInstanceExtensionAvailable(const std::string& extensionName);
private: /* debug */
    PFN_vkCreateDebugUtilsMessengerEXT      _vkCreateDebugUtilsMessengerEXT = nullptr;
    PFN_vkDestroyDebugUtilsMessengerEXT     _vkDestroyDebugUtilsMessengerEXT = nullptr;
    bool                                    _debugMode;
    VkDebugUtilsMessengerEXT                _debugUtilsMessenger;
private:
    VkInstance                              _instance;
    VmaAllocator                            _allocator;
    std::vector<std::string>                _instanceLayerNames;
    std::vector<LayerProperties>            _instanceLayerProperties;
    std::vector<VkExtensionProperties>      _instanceExtensionProperties;
    std::vector<std::string>                _instanceExtensionsEnabled;
    std::vector<std::string>                _deviceLayerNames;
private:
    void LoadBasicFunc();
    void LoadInstanceFunc(VkInstance instance);
    void LoadDeviceFunc(VkDevice device, const VulkanExtensions &enabledExtensions);
    bool LoadProps();
    void SetupQueueFamilies();
    bool CreateDevice();
    void DestroyDevice();
    void ChooseDevice(int32_t physicalDevice = 0);
    void CheckExtensions();
    void GetDeviceLayerExtensionList();
    bool EnableDeviceExtension(const std::string& extension);
    void InitPropsParams();
    void InitDeviceParams();
    void InitVkDeviceQueueCreateInfos(std::vector<VkDeviceQueueCreateInfo>& queueCreateInfos);
private:
    std::mutex                              _deviceMtx;
    VkDevice                                _device;
private:
    VkQueue                                                     _gfxQueue;
    VkSwapchainKHR                                              _swapChain;
    VkFormat                                                    _swapChainFormat;
    VkPresentModeKHR                                            _prsentMode;
    VulkanPhysicalDeviceInfo                                    _deviceInfo;
    std::vector<PhysicalDeviceProps>                            _physicalDeviceProperties;
    VkPhysicalDeviceMemoryProperties                            _memoryProperties = {};
    PhysicalDeviceFeatures                                      _deviceFeature = {};
    std::vector<std::string>                                    _deviceExtensionsEnabled;
    std::vector<VkExtensionProperties>                          _deviceExtensionProperties;
    VulkanExtensions                                            _extensionsLookup;
private:
    std::mutex                              _physicalDeviceMtx;
    int32_t                                 _physicalDevice;
    std::vector<VkPhysicalDevice>           _physicalDevices;
private: /* props */
    VkPhysicalDeviceCooperativeMatrixFeaturesKHR                 _deviceCooperativeMatrixFeatures = {};
    VkPhysicalDeviceDescriptorBufferFeaturesEXT                  _deviceDescriptorBufferFeatures = {};
    VkPhysicalDeviceVulkan13Features                             _deviceVulkan13Features = {};
    VkPhysicalDeviceVulkan12Features                             _deviceVulkan12Features = {};
    VkPhysicalDeviceVulkan11Features                             _deviceVulkan11Features = {};
    VkPhysicalDeviceExternalMemoryHostPropertiesEXT              _deviceExternalMemoryHostProperties = {};
    VkPhysicalDeviceCooperativeMatrixPropertiesKHR               _deviceCooperativeMatrixProperties = {};
    VkPhysicalDeviceSubgroupSizeControlProperties                _deviceSubgroupSizeControlProperties = {};
    VkPhysicalDeviceDescriptorBufferPropertiesEXT                _deviceDescriptorBufferProperties = {};
    VkPhysicalDeviceDriverProperties                             _deviceDriverProperties = {};
    VkPhysicalDeviceMemoryProperties                             _deviceMemoryProperties = {};
    VkPhysicalDeviceProperties2                                  _deviceProperties2 = {};
    VkPhysicalDeviceShaderAtomicFloatFeaturesEXT                 _deviceShaderAtomicFloatFeaturesEXT = {};
    VkPhysicalDeviceFeatures2                                    _deviceFeatures2 = {};
    std::vector<VkQueueFamilyProperties2>                        _queueFamilyProperties2 = {};
    std::vector<VkQueueFamilyQueryResultStatusPropertiesKHR>     _queueFamilyQueryResultStatusProperties = {};
    std::vector<VkQueueFamilyVideoPropertiesKHR>                 _queueFamilyVideoPropertiesKHR = {};
private: /* queue families */
    float                                                        _queuePriorities = 1.0f;
    uint32_t                                                     _queueCount;
    std::vector<VkQueueFamilyProperties>                         _queueFamilyProperties;
    int32_t                                                      _graphicsQueueIndex = -1;
    int32_t                                                      _computeQueueIndex = -1;
    int32_t                                                      _transferQueueIndex = -1;
    int32_t                                                      _videoEncodeQueueIndex = -1;
    int32_t                                                      _videoDecodeQueueIndex = -1;
};

#define XXX_VULKAN_DEVICE_LOCK(PFN)                    std::lock_guard<std::mutex> lock(_deviceMtx);\
                                                       assert(_device);\
                                                       if (!_device)\
                                                       {\
                                                            return VK_INCOMPLETE;\
                                                       }\
                                                       if (!PFN)\
                                                       {\
                                                            return VK_INCOMPLETE;\
                                                       }
#define XXX_VULKAN_DEVICE_LOCK_VOID(PFN)               std::lock_guard<std::mutex> lock(_deviceMtx);\
                                                       assert(_device);\
                                                       if (!_device)\
                                                       {\
                                                            return;\
                                                       }\
                                                       if (!PFN)\
                                                       {\
                                                            return;\
                                                       }

#define XXX_VULKAN_PHYSICAL_DEVICE_LOCK(PFN)           std::lock_guard<std::mutex> lock(_physicalDeviceMtx);\
                                                       if (_physicalDevice < 0)\
                                                       {\
                                                            return VK_INCOMPLETE;\
                                                       }\
                                                       if (!PFN)\
                                                       {\
                                                            return VK_INCOMPLETE;\
                                                       }
#define XXX_VULKAN_PHYSICAL_DEVICE_LOCK_VOID(PFN)      std::lock_guard<std::mutex> lock(_physicalDeviceMtx);\
                                                       if (_physicalDevice < 0)\
                                                       {\
                                                            return;\
                                                       }\
                                                       if (!PFN)\
                                                       {\
                                                            return;\
                                                       }

VulkanDeviceImpl::VulkanDeviceImpl()
{
    _debugMode = true;
    _device = VK_NULL_HANDLE;
    _instance = VK_NULL_HANDLE;
    _allocator = VK_NULL_HANDLE;
    _queueCount = 0;
    _physicalDevice = 0;
    _deviceInfo = {};
    _apiVersion = {};
    VULKAN_LOG_INFO << "Load vulkan related runtime";
#if MMP_PLATFORM(LINUX)
    _vulkanLoader.load("libvulkan.so");
    assert(_vulkanLoader.isLoaded());
#endif
#if MMP_PLATFORM(WINDOWS)
    _vulkanLoader.load("vulkan-1.dll");
    assert(_vulkanLoader.isLoaded());
#endif
    VULKAN_LOG_INFO << "Load vulkan successfully";
}

VulkanDeviceImpl::~VulkanDeviceImpl()
{
    _vulkanLoader.unload();
}

void VulkanDeviceImpl::LoadBasicFunc()
{
    #define LOAD_BAISC_VULKAN_FUNC(x) _##x = (PFN_##x)_vulkanLoader.getSymbol(#x)

    LOAD_BAISC_VULKAN_FUNC(vkCreateInstance);
    LOAD_BAISC_VULKAN_FUNC(vkGetInstanceProcAddr);
    LOAD_BAISC_VULKAN_FUNC(vkGetDeviceProcAddr);

    LOAD_BAISC_VULKAN_FUNC(vkEnumerateInstanceVersion);
    LOAD_BAISC_VULKAN_FUNC(vkEnumerateInstanceExtensionProperties);
    LOAD_BAISC_VULKAN_FUNC(vkEnumerateInstanceLayerProperties);

    assert(
        _vkCreateInstance && _vkGetInstanceProcAddr && _vkGetDeviceProcAddr 
        && _vkEnumerateInstanceVersion && _vkEnumerateInstanceExtensionProperties 
        && _vkEnumerateInstanceLayerProperties
    );

    #undef LOAD_BAISC_VULKAN_FUNC
}

void VulkanDeviceImpl::LoadInstanceFunc(VkInstance instance)
{
    #define LOAD_INSTANCE_VULKAN_FUNC(x) _##x = (PFN_##x)_vkGetInstanceProcAddr(instance, #x)

    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceProperties2);
    LOAD_INSTANCE_VULKAN_FUNC(vkCreateVideoSessionKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceVideoCapabilitiesKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceVideoFormatPropertiesKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkCreateVideoSessionParametersKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetVideoSessionMemoryRequirementsKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkBindVideoSessionMemoryKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkCmdBeginVideoCodingKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkCmdControlVideoCodingKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkCmdEndVideoCodingKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkCmdDecodeVideoKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkDestroyVideoSessionParametersKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkDestroyVideoSessionKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkWaitForPresentKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkDestroyInstance);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceProperties);
    LOAD_INSTANCE_VULKAN_FUNC(vkEnumeratePhysicalDevices);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceFeatures);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceFeatures2);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceFormatProperties);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceImageFormatProperties);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceQueueFamilyProperties);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceQueueFamilyProperties2);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceMemoryProperties);
    LOAD_INSTANCE_VULKAN_FUNC(vkCreateDevice);
    LOAD_INSTANCE_VULKAN_FUNC(vkDestroyDevice);
    LOAD_INSTANCE_VULKAN_FUNC(vkEnumerateDeviceExtensionProperties);
    LOAD_INSTANCE_VULKAN_FUNC(vkEnumerateDeviceLayerProperties);
    LOAD_INSTANCE_VULKAN_FUNC(vkDeviceWaitIdle);

    LOAD_INSTANCE_VULKAN_FUNC(vkCreateSwapchainKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkDestroySwapchainKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetSwapchainImagesKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkAcquireNextImageKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkQueuePresentKHR);

    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceSurfaceFormatsKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkDestroySurfaceKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceSurfaceCapabilitiesKHR);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceSurfacePresentModesKHR);

    LOAD_INSTANCE_VULKAN_FUNC(vkCreateDebugUtilsMessengerEXT);
    LOAD_INSTANCE_VULKAN_FUNC(vkDestroyDebugUtilsMessengerEXT);
    LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceQueueFamilyProperties2);
    if (_extensionsLookup.KHR_COOPERATIVE_MATRIX_EXTENSION_NAME)
    {
        LOAD_INSTANCE_VULKAN_FUNC(vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR);
    }
    
    #undef LOAD_INSTANCE_VULKAN_FUNC
}

void VulkanDeviceImpl::LoadDeviceFunc(VkDevice device, const VulkanExtensions &enabledExtensions)
{
    #define LOAD_DEVICE_VULKAN_FUNC(x) if ((PFN_##x)_vkGetDeviceProcAddr(device, #x)) \
                                        {\
                                            _##x = (PFN_##x)_vkGetDeviceProcAddr(device, #x);\
                                        }


    LOAD_DEVICE_VULKAN_FUNC(vkGetPhysicalDeviceVideoFormatPropertiesKHR);
    LOAD_DEVICE_VULKAN_FUNC(vkCreateVideoSessionParametersKHR);
    LOAD_DEVICE_VULKAN_FUNC(vkGetVideoSessionMemoryRequirementsKHR);
    LOAD_DEVICE_VULKAN_FUNC(vkBindVideoSessionMemoryKHR);
    LOAD_DEVICE_VULKAN_FUNC(vkCmdBeginVideoCodingKHR);
    LOAD_DEVICE_VULKAN_FUNC(vkCmdControlVideoCodingKHR);
    LOAD_DEVICE_VULKAN_FUNC(vkCmdEndVideoCodingKHR);
    LOAD_DEVICE_VULKAN_FUNC(vkCmdDecodeVideoKHR);
    LOAD_DEVICE_VULKAN_FUNC(vkDestroyVideoSessionParametersKHR);
	LOAD_DEVICE_VULKAN_FUNC(vkQueueSubmit);
    LOAD_DEVICE_VULKAN_FUNC(vkQueueSubmit2);
	LOAD_DEVICE_VULKAN_FUNC(vkQueueWaitIdle);
	LOAD_DEVICE_VULKAN_FUNC(vkAllocateMemory);
	LOAD_DEVICE_VULKAN_FUNC(vkFreeMemory);
	LOAD_DEVICE_VULKAN_FUNC(vkMapMemory);
	LOAD_DEVICE_VULKAN_FUNC(vkUnmapMemory);
	LOAD_DEVICE_VULKAN_FUNC(vkFlushMappedMemoryRanges);
	LOAD_DEVICE_VULKAN_FUNC(vkInvalidateMappedMemoryRanges);
	LOAD_DEVICE_VULKAN_FUNC(vkGetDeviceMemoryCommitment);
	LOAD_DEVICE_VULKAN_FUNC(vkBindBufferMemory);
	LOAD_DEVICE_VULKAN_FUNC(vkBindBufferMemory2);
	LOAD_DEVICE_VULKAN_FUNC(vkBindImageMemory);
	LOAD_DEVICE_VULKAN_FUNC(vkBindImageMemory2);
	LOAD_DEVICE_VULKAN_FUNC(vkGetBufferMemoryRequirements);
	LOAD_DEVICE_VULKAN_FUNC(vkGetBufferMemoryRequirements2);
	LOAD_DEVICE_VULKAN_FUNC(vkGetDeviceBufferMemoryRequirements);
	LOAD_DEVICE_VULKAN_FUNC(vkGetImageMemoryRequirements);
	LOAD_DEVICE_VULKAN_FUNC(vkGetImageMemoryRequirements2);
	LOAD_DEVICE_VULKAN_FUNC(vkGetDeviceImageMemoryRequirements);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateFence);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyFence);
	LOAD_DEVICE_VULKAN_FUNC(vkResetFences);
	LOAD_DEVICE_VULKAN_FUNC(vkGetFenceStatus);
	LOAD_DEVICE_VULKAN_FUNC(vkWaitForFences);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateSemaphore);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroySemaphore);
    LOAD_DEVICE_VULKAN_FUNC(vkWaitSemaphores);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateEvent);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyEvent);
	LOAD_DEVICE_VULKAN_FUNC(vkGetEventStatus);
	LOAD_DEVICE_VULKAN_FUNC(vkSetEvent);
	LOAD_DEVICE_VULKAN_FUNC(vkResetEvent);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateQueryPool);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyQueryPool);
	LOAD_DEVICE_VULKAN_FUNC(vkGetQueryPoolResults);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateBuffer);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyBuffer);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateBufferView);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyBufferView);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateImage);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyImage);
	LOAD_DEVICE_VULKAN_FUNC(vkGetImageSubresourceLayout);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateImageView);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyImageView);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateShaderModule);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyShaderModule);
    LOAD_DEVICE_VULKAN_FUNC(vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR);
	LOAD_DEVICE_VULKAN_FUNC(vkCreatePipelineCache);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyPipelineCache);
	LOAD_DEVICE_VULKAN_FUNC(vkGetPipelineCacheData);
	LOAD_DEVICE_VULKAN_FUNC(vkMergePipelineCaches);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateGraphicsPipelines);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateComputePipelines);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyPipeline);
	LOAD_DEVICE_VULKAN_FUNC(vkCreatePipelineLayout);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyPipelineLayout);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateSampler);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroySampler);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateDescriptorSetLayout);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyDescriptorSetLayout);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateDescriptorPool);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyDescriptorPool);
	LOAD_DEVICE_VULKAN_FUNC(vkResetDescriptorPool);
	LOAD_DEVICE_VULKAN_FUNC(vkAllocateDescriptorSets);
	LOAD_DEVICE_VULKAN_FUNC(vkFreeDescriptorSets);
	LOAD_DEVICE_VULKAN_FUNC(vkUpdateDescriptorSets);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateFramebuffer);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyFramebuffer);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateRenderPass);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyRenderPass);
	LOAD_DEVICE_VULKAN_FUNC(vkGetRenderAreaGranularity);
	LOAD_DEVICE_VULKAN_FUNC(vkCreateCommandPool);
	LOAD_DEVICE_VULKAN_FUNC(vkDestroyCommandPool);
	LOAD_DEVICE_VULKAN_FUNC(vkResetCommandPool);
	LOAD_DEVICE_VULKAN_FUNC(vkAllocateCommandBuffers);
	LOAD_DEVICE_VULKAN_FUNC(vkFreeCommandBuffers);
	LOAD_DEVICE_VULKAN_FUNC(vkBeginCommandBuffer);
	LOAD_DEVICE_VULKAN_FUNC(vkEndCommandBuffer);
	LOAD_DEVICE_VULKAN_FUNC(vkResetCommandBuffer);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdBindPipeline);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdSetViewport);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdSetScissor);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdSetLineWidth);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdSetDepthBias);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdSetBlendConstants);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdSetDepthBounds);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdSetStencilCompareMask);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdSetStencilWriteMask);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdSetStencilReference);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdBindDescriptorSets);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdBindIndexBuffer);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdBindVertexBuffers);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdDraw);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdDrawIndexed);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdDrawIndirect);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdDrawIndexedIndirect);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdDispatch);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdDispatchIndirect);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdCopyBuffer);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdCopyImage);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdBlitImage);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdCopyBufferToImage);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdCopyImageToBuffer);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdUpdateBuffer);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdFillBuffer);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdClearColorImage);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdClearDepthStencilImage);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdClearAttachments);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdResolveImage);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdSetEvent);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdResetEvent);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdWaitEvents);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdPipelineBarrier);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdBeginQuery);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdEndQuery);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdResetQueryPool);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdWriteTimestamp);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdCopyQueryPoolResults);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdPushConstants);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdBeginRenderPass);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdNextSubpass);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdEndRenderPass);
	LOAD_DEVICE_VULKAN_FUNC(vkCmdExecuteCommands);
    LOAD_DEVICE_VULKAN_FUNC(vkGetDeviceQueue);
    LOAD_DEVICE_VULKAN_FUNC(vkCreateSamplerYcbcrConversion);
    LOAD_DEVICE_VULKAN_FUNC(vkDestroySamplerYcbcrConversion);

	if (enabledExtensions.KHR_PRESENT_WAIT_EXTENSION_NAME) 
    {
		LOAD_DEVICE_VULKAN_FUNC(vkWaitForPresentKHR);
	}
    else
    {
        _vkWaitForPresentKHR = nullptr;
    }
	if (enabledExtensions.KHR_DEDICATED_ALLOCATION_EXTENSION_NAME) 
    {
		LOAD_DEVICE_VULKAN_FUNC(vkGetBufferMemoryRequirements2KHR);
		LOAD_DEVICE_VULKAN_FUNC(vkGetImageMemoryRequirements2KHR);
	}
    else
    {
        _vkGetBufferMemoryRequirements2KHR = nullptr;
        _vkGetImageMemoryRequirements2KHR = nullptr;
    }
	if (enabledExtensions.KHR_CREATE_RENDERPASS_2_EXTENSION_NAME) 
    {
		LOAD_DEVICE_VULKAN_FUNC(vkCreateRenderPass2KHR);
	}
    else
    {
        _vkCreateRenderPass2KHR = nullptr;
    }

    #undef LOAD_DEVICE_VULKAN_FUNC
}

void VulkanDeviceImpl::InitPropsParams()
{
    {
        _deviceExternalMemoryHostProperties.sType    = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_EXTERNAL_MEMORY_HOST_PROPERTIES_EXT;
        _deviceCooperativeMatrixProperties.sType     = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_PROPERTIES_KHR;
        _deviceSubgroupSizeControlProperties.sType   = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SUBGROUP_SIZE_CONTROL_PROPERTIES;
        _deviceDescriptorBufferProperties.sType      = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_BUFFER_PROPERTIES_EXT;
        _deviceDriverProperties.sType                = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DRIVER_PROPERTIES;
        _deviceProperties2.sType                     = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
        _deviceShaderAtomicFloatFeaturesEXT.sType    = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_FEATURES_EXT;
        _deviceVulkan12Features.sType                = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES;
        _deviceFeatures2.sType                       = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    }
    {
        _deviceCooperativeMatrixProperties.pNext = &_deviceExternalMemoryHostProperties;
        _deviceSubgroupSizeControlProperties.pNext = &_deviceCooperativeMatrixProperties;
        _deviceDescriptorBufferProperties.pNext = &_deviceSubgroupSizeControlProperties;
        _deviceDriverProperties.pNext = &_deviceDescriptorBufferProperties;
        _deviceProperties2.pNext = &_deviceDriverProperties;
        _deviceVulkan12Features.pNext = &_deviceShaderAtomicFloatFeaturesEXT;
        _deviceFeatures2.pNext = &_deviceVulkan12Features;
    }
}

void VulkanDeviceImpl::InitDeviceParams()
{
    _deviceFeatures2 = {}; // Reset
    {
        _deviceCooperativeMatrixFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_COOPERATIVE_MATRIX_FEATURES_KHR;
        _deviceShaderAtomicFloatFeaturesEXT.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_SHADER_ATOMIC_FLOAT_FEATURES_EXT;
        _deviceDescriptorBufferFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_BUFFER_FEATURES_EXT;
        _deviceVulkan13Features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES;
        _deviceVulkan12Features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES;
        _deviceVulkan11Features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES;
        _deviceFeatures2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    }
    {
        _deviceShaderAtomicFloatFeaturesEXT.pNext = &_deviceCooperativeMatrixFeatures;
        _deviceDescriptorBufferFeatures.pNext = &_deviceShaderAtomicFloatFeaturesEXT;
        _deviceVulkan13Features.pNext = &_deviceDescriptorBufferFeatures;
        _deviceVulkan12Features.pNext = &_deviceVulkan13Features;
        _deviceVulkan11Features.pNext = &_deviceVulkan12Features;
        _deviceFeatures2.pNext = &_deviceVulkan11Features;
    }
}

bool VulkanDeviceImpl::LoadProps()
{
    if (!(_vkGetPhysicalDeviceProperties2 && _vkGetPhysicalDeviceMemoryProperties && _vkGetPhysicalDeviceFeatures2 && _vkGetPhysicalDeviceQueueFamilyProperties2))
    {
        assert(false);
        return false;
    }
    VkResult vkRes = VK_SUCCESS;
    InitPropsParams();
    _vkGetPhysicalDeviceProperties2(_physicalDevices[_physicalDevice], &_deviceProperties2);
    _vkGetPhysicalDeviceMemoryProperties(_physicalDevices[_physicalDevice], &_deviceMemoryProperties);
    _vkGetPhysicalDeviceFeatures2(_physicalDevices[_physicalDevice], &_deviceFeatures2);
    {
        uint32_t numQueueFamilyProperties = 0;
        vkGetPhysicalDeviceQueueFamilyProperties2(&numQueueFamilyProperties, nullptr);
        if (numQueueFamilyProperties == 0)
        {
            assert(false);
            return false;
        }
        _queueFamilyProperties2.resize(numQueueFamilyProperties);
        _queueFamilyQueryResultStatusProperties.resize(numQueueFamilyProperties);
        _queueFamilyVideoPropertiesKHR.resize(numQueueFamilyProperties);
        for (uint32_t i=0; i<numQueueFamilyProperties; i++)
        {
            {
                _queueFamilyVideoPropertiesKHR[i].sType = VK_STRUCTURE_TYPE_QUEUE_FAMILY_QUERY_RESULT_STATUS_PROPERTIES_KHR;
                _queueFamilyQueryResultStatusProperties[i].sType = VK_STRUCTURE_TYPE_QUEUE_FAMILY_VIDEO_PROPERTIES_KHR;
                _queueFamilyProperties2[i].sType = VK_STRUCTURE_TYPE_QUEUE_FAMILY_PROPERTIES_2;
            }
            {
                _queueFamilyQueryResultStatusProperties[i].pNext = &_queueFamilyVideoPropertiesKHR[i];
                _queueFamilyProperties2[i].pNext = &_queueFamilyQueryResultStatusProperties[i];
            }
        }
        _vkGetPhysicalDeviceQueueFamilyProperties2(_physicalDevices[_physicalDevice], &numQueueFamilyProperties, _queueFamilyProperties2.data());
    }

    // TODO

    return true;
}


void VulkanDeviceImpl::InitVkDeviceQueueCreateInfos(std::vector<VkDeviceQueueCreateInfo>& queueCreateInfos)
{
    std::set<int32_t> indexs; // remove duplicate
    if (_graphicsQueueIndex >= 0 && indexs.count(_graphicsQueueIndex) == 0)
    {
        VkDeviceQueueCreateInfo deviceQueueCreateInfo = {};
        deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        deviceQueueCreateInfo.queueFamilyIndex = _graphicsQueueIndex;
        deviceQueueCreateInfo.queueCount = _queueFamilyProperties[_graphicsQueueIndex].queueCount;
        deviceQueueCreateInfo.pQueuePriorities = &_queuePriorities;
        queueCreateInfos.push_back(deviceQueueCreateInfo);
        indexs.insert(_graphicsQueueIndex);
    }
    if (_computeQueueIndex >= 0 && indexs.count(_computeQueueIndex) == 0)
    {
        VkDeviceQueueCreateInfo deviceQueueCreateInfo = {};
        deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        deviceQueueCreateInfo.queueFamilyIndex = _computeQueueIndex;
        deviceQueueCreateInfo.queueCount = _queueFamilyProperties[_computeQueueIndex].queueCount;
        deviceQueueCreateInfo.pQueuePriorities = &_queuePriorities;
        queueCreateInfos.push_back(deviceQueueCreateInfo);
        indexs.insert(_computeQueueIndex);
    }
    if (_transferQueueIndex >= 0 && indexs.count(_transferQueueIndex) == 0)
    {
        VkDeviceQueueCreateInfo deviceQueueCreateInfo = {};
        deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        deviceQueueCreateInfo.queueFamilyIndex = _transferQueueIndex;
        deviceQueueCreateInfo.queueCount = _queueFamilyProperties[_transferQueueIndex].queueCount;
        deviceQueueCreateInfo.pQueuePriorities = &_queuePriorities;
        queueCreateInfos.push_back(deviceQueueCreateInfo);
        indexs.insert(_transferQueueIndex);
    }
    if (_videoEncodeQueueIndex >= 0 && indexs.count(_videoEncodeQueueIndex) == 0)
    {
        VkDeviceQueueCreateInfo deviceQueueCreateInfo = {};
        deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        deviceQueueCreateInfo.queueFamilyIndex = _videoEncodeQueueIndex;
        deviceQueueCreateInfo.queueCount = _queueFamilyProperties[_videoEncodeQueueIndex].queueCount;
        deviceQueueCreateInfo.pQueuePriorities = &_queuePriorities;
        queueCreateInfos.push_back(deviceQueueCreateInfo);
        indexs.insert(_videoEncodeQueueIndex);
    }
    if (_videoDecodeQueueIndex >= 0 && indexs.count(_videoDecodeQueueIndex) == 0)
    {
        VkDeviceQueueCreateInfo deviceQueueCreateInfo = {};
        deviceQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        deviceQueueCreateInfo.queueFamilyIndex = _videoDecodeQueueIndex;
        deviceQueueCreateInfo.queueCount = _queueFamilyProperties[_videoDecodeQueueIndex].queueCount;
        deviceQueueCreateInfo.pQueuePriorities = &_queuePriorities;
        queueCreateInfos.push_back(deviceQueueCreateInfo);
        indexs.insert(_videoDecodeQueueIndex);
    }
}

} // namespace Mmp

namespace Mmp
{

PFN_vkVoidFunction VulkanDeviceImpl::vkGetInstanceProcAddr(
        VkInstance instance,
        const char* pName
    )
{
    return _vkGetInstanceProcAddr(instance, pName);
}

VkResult VulkanDeviceImpl::vkCreateInstance(
        const VkInstanceCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkInstance* pInstance
    )
{
    return _vkCreateInstance(pCreateInfo, pAllocator, pInstance);
}

void VulkanDeviceImpl::vkDestroyInstance(
        VkInstance instance,
        const VkAllocationCallbacks* pAllocator
    )
{
    _vkDestroyInstance(instance, pAllocator);
}

VkResult VulkanDeviceImpl::vkEnumerateInstanceVersion(
        uint32_t* pApiVersion
    )
{
    return _vkEnumerateInstanceVersion(pApiVersion);
}

PFN_vkVoidFunction VulkanDeviceImpl::vkGetDeviceProcAddr(
        const char* pName
    )
{
    if (_vkGetDeviceProcAddr)
    {
        std::lock_guard<std::mutex> lock(_deviceMtx);
        return _vkGetDeviceProcAddr(_device, pName);
    }
    else
    {
        return nullptr;   
    }
}

VkResult VulkanDeviceImpl::vkEnumeratePhysicalDevices(
        VkInstance instance,
        uint32_t* pPhysicalDeviceCount,
        VkPhysicalDevice* pPhysicalDevices
    )
{
    return _vkEnumeratePhysicalDevices(instance, pPhysicalDeviceCount, pPhysicalDevices);
}

void VulkanDeviceImpl::vkGetPhysicalDeviceProperties(
        VkPhysicalDeviceProperties* pProperties
    )
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK_VOID(_vkGetPhysicalDeviceProperties);
    _vkGetPhysicalDeviceProperties(_physicalDevices[_physicalDevice], pProperties);
}

void VulkanDeviceImpl::vkGetPhysicalDeviceProperties2(
        VkPhysicalDeviceProperties2* pProperties
    )
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK_VOID(_vkGetPhysicalDeviceProperties2);
    _vkGetPhysicalDeviceProperties2(_physicalDevices[_physicalDevice], pProperties);
}

void VulkanDeviceImpl::vkGetPhysicalDeviceQueueFamilyProperties(
        uint32_t* pQueueFamilyPropertyCount,
        VkQueueFamilyProperties* pQueueFamilyProperties
    )
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK_VOID(_vkGetPhysicalDeviceQueueFamilyProperties);
    _vkGetPhysicalDeviceQueueFamilyProperties(_physicalDevices[_physicalDevice], pQueueFamilyPropertyCount, pQueueFamilyProperties);
}

void VulkanDeviceImpl::vkGetPhysicalDeviceQueueFamilyProperties2(
        uint32_t* pQueueFamilyPropertyCount,
        VkQueueFamilyProperties2* pQueueFamilyProperties
    )
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK_VOID(_vkGetPhysicalDeviceQueueFamilyProperties2);
    _vkGetPhysicalDeviceQueueFamilyProperties2(_physicalDevices[_physicalDevice], pQueueFamilyPropertyCount, pQueueFamilyProperties);
}

VkResult VulkanDeviceImpl::vkCreateDevice(
        VkPhysicalDevice physicalDevice,
        const VkDeviceCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDevice* pDevice
    )
{
    return _vkCreateDevice(physicalDevice, pCreateInfo, pAllocator, pDevice);
}

void VulkanDeviceImpl::vkDestroyDevice(
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyDevice);
    _vkDestroyDevice(_device, pAllocator);
}

void VulkanDeviceImpl::vkGetDeviceQueue(
        uint32_t queueFamilyIndex,
        uint32_t queueIndex,
        VkQueue* pQueue
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkGetDeviceQueue);
    _vkGetDeviceQueue(_device, queueFamilyIndex, queueIndex, pQueue);
}

VkResult VulkanDeviceImpl::vkQueueSubmit(
        VkQueue queue,
        uint32_t submitCount,
        const VkSubmitInfo* pSubmits,
        VkFence fence
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkQueueSubmit);
    return _vkQueueSubmit(queue, submitCount, pSubmits, fence);
}

VkResult VulkanDeviceImpl::vkQueueSubmit2(
        VkQueue queue,
        uint32_t submitCount,
        const VkSubmitInfo2* pSubmits,
        VkFence fence
    )
{
    return _vkQueueSubmit2(queue, submitCount, pSubmits, fence);
}

VkResult VulkanDeviceImpl::vkCreateCommandPool(
        const VkCommandPoolCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkCommandPool* pCommandPool
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateCommandPool);
    return _vkCreateCommandPool(_device, pCreateInfo, pAllocator, pCommandPool);
}

void VulkanDeviceImpl::vkDestroyCommandPool(
        VkCommandPool commandPool,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyCommandPool);
    _vkDestroyCommandPool(_device, commandPool, pAllocator);
}

VkResult VulkanDeviceImpl::vkResetCommandPool(
        VkCommandPool commandPool,
        VkCommandPoolResetFlags flags
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkResetCommandPool);
    return _vkResetCommandPool(_device, commandPool, flags);
}

VkResult VulkanDeviceImpl::vkAllocateCommandBuffers(
        const VkCommandBufferAllocateInfo* pAllocateInfo,
        VkCommandBuffer* pCommandBuffers
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkAllocateCommandBuffers);
    return _vkAllocateCommandBuffers(_device, pAllocateInfo, pCommandBuffers);
}

void VulkanDeviceImpl::vkFreeCommandBuffers(
        VkCommandPool commandPool,
        uint32_t commandBufferCount,
        const VkCommandBuffer* pCommandBuffers
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkFreeCommandBuffers);
    _vkFreeCommandBuffers(_device, commandPool, commandBufferCount, pCommandBuffers);
}

VkResult VulkanDeviceImpl::vkBeginCommandBuffer(
        VkCommandBuffer commandBuffer,
        const VkCommandBufferBeginInfo* pBeginInfo
    )
{
    return _vkBeginCommandBuffer(commandBuffer, pBeginInfo);
}

VkResult VulkanDeviceImpl::vkEndCommandBuffer(
        VkCommandBuffer commandBuffer
    )
{
    return _vkEndCommandBuffer(commandBuffer);
}

VkResult VulkanDeviceImpl::vkResetCommandBuffer(
        VkCommandBuffer commandBuffer,
        VkCommandBufferResetFlags flags
    )
{
    return _vkResetCommandBuffer(commandBuffer, flags);
}

void VulkanDeviceImpl::vkCmdExecuteCommands(
        VkCommandBuffer commandBuffer,
        uint32_t commandBufferCount,
        const VkCommandBuffer* pCommandBuffers
    )
{
    _vkCmdExecuteCommands(commandBuffer, commandBufferCount, pCommandBuffers);
}

VkResult VulkanDeviceImpl::vkQueueWaitIdle(
        VkQueue queue
    )
{
    return _vkQueueWaitIdle(queue);
}

VkResult VulkanDeviceImpl::vkDeviceWaitIdle(
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkDeviceWaitIdle);
    return _vkDeviceWaitIdle(_device);
}

VkResult VulkanDeviceImpl::vkCreateFence(
        const VkFenceCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkFence* pFence
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateFence);
    return _vkCreateFence(_device, pCreateInfo, pAllocator, pFence);
}

void VulkanDeviceImpl::vkDestroyFence(
        VkFence fence,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyFence);
    return _vkDestroyFence(_device, fence, pAllocator);
}

VkResult VulkanDeviceImpl::vkResetFences(
        uint32_t fenceCount,
        const VkFence* pFences
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkResetFences);
    return _vkResetFences(_device, fenceCount, pFences);
}

VkResult VulkanDeviceImpl::vkGetFenceStatus(
        VkFence fence
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkGetFenceStatus);
    return _vkGetFenceStatus(_device, fence);
}

VkResult VulkanDeviceImpl::vkWaitForFences(
        uint32_t fenceCount,
        const VkFence* pFences,
        VkBool32 waitAll,
        uint64_t timeout
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkWaitForFences);
    return _vkWaitForFences(_device, fenceCount, pFences, waitAll, timeout);
}

VkResult VulkanDeviceImpl::vkCreateSemaphore(
        const VkSemaphoreCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkSemaphore* pSemaphore
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateSemaphore);
    return _vkCreateSemaphore(_device, pCreateInfo, pAllocator, pSemaphore);
}

void VulkanDeviceImpl::vkDestroySemaphore(
        VkSemaphore semaphore,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroySemaphore);
    _vkDestroySemaphore(_device, semaphore, pAllocator);
}

VkResult VulkanDeviceImpl::vkWaitSemaphores(
        const VkSemaphoreWaitInfo* pWaitInfo,
        uint64_t timeout
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkWaitSemaphores);
    return _vkWaitSemaphores(_device, pWaitInfo, timeout);
}

VkResult VulkanDeviceImpl::vkCreateEvent(
        const VkEventCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkEvent* pEvent
    )
{
    XXX_VULKAN_DEVICE_LOCK(_device);
    return _vkCreateEvent(_device, pCreateInfo, pAllocator, pEvent);
}

void VulkanDeviceImpl::vkDestroyEvent(
        VkEvent event,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyEvent);
    _vkDestroyEvent(_device, event, pAllocator);
}

VkResult VulkanDeviceImpl::vkGetEventStatus(
        VkEvent event
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkGetEventStatus);
    return _vkGetEventStatus(_device, event);
}

VkResult VulkanDeviceImpl::vkSetEvent(
        VkEvent event
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkSetEvent);
    return _vkSetEvent(_device, event);
}

VkResult VulkanDeviceImpl::vkResetEvent(
        VkEvent event
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkResetEvent);
    return _vkResetEvent(_device, event);
}

void VulkanDeviceImpl::vkCmdSetEvent(
        VkCommandBuffer commandBuffer,
        VkEvent event,
        VkPipelineStageFlags stageMask
    )
{
    _vkCmdSetEvent(commandBuffer, event, stageMask);
}

void VulkanDeviceImpl::vkCmdResetEvent(
        VkCommandBuffer commandBuffer,
        VkEvent event,
        VkPipelineStageFlags stageMask
    )
{
    _vkCmdResetEvent(commandBuffer, event, stageMask);
}

void VulkanDeviceImpl::vkCmdWaitEvents(
        VkCommandBuffer commandBuffer,
        uint32_t eventCount,
        const VkEvent* pEvents,
        VkPipelineStageFlags srcStageMask,
        VkPipelineStageFlags dstStageMask,
        uint32_t memoryBarrierCount,
        const VkMemoryBarrier* pMemoryBarriers,
        uint32_t bufferMemoryBarrierCount,
        const VkBufferMemoryBarrier* pBufferMemoryBarriers,
        uint32_t imageMemoryBarrierCount,
        const VkImageMemoryBarrier* pImageMemoryBarriers
    )
{
    _vkCmdWaitEvents(commandBuffer, eventCount, pEvents, srcStageMask, dstStageMask, memoryBarrierCount, pMemoryBarriers, bufferMemoryBarrierCount, pBufferMemoryBarriers, imageMemoryBarrierCount, pImageMemoryBarriers);
}

void VulkanDeviceImpl::vkCmdBeginRenderPass(
        VkCommandBuffer commandBuffer,
        const VkRenderPassBeginInfo* pRenderPassBegin,
        VkSubpassContents contents
    )
{
    _vkCmdBeginRenderPass(commandBuffer, pRenderPassBegin, contents);
}

void VulkanDeviceImpl::vkCmdNextSubpass(
        VkCommandBuffer commandBuffer,
        VkSubpassContents contents
    )
{
    _vkCmdNextSubpass(commandBuffer, contents);
}

void VulkanDeviceImpl::vkCmdEndRenderPass(
        VkCommandBuffer commandBuffer
    )
{
    _vkCmdEndRenderPass(commandBuffer);
}

VkResult VulkanDeviceImpl::vkCreateShaderModule(
        const VkShaderModuleCreateInfo* pCreateInfo, 
        const VkAllocationCallbacks* pAllocator, 
        VkShaderModule* pShaderModule
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateShaderModule);
    return _vkCreateShaderModule(_device, pCreateInfo, pAllocator, pShaderModule);
}

void VulkanDeviceImpl::vkDestroyShaderModule(
        VkShaderModule shaderModule,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyShaderModule);
    _vkDestroyShaderModule(_device, shaderModule, pAllocator);
}

VkResult VulkanDeviceImpl::vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR(
        uint32_t* pPropertyCount,
        VkCooperativeMatrixPropertiesKHR* pProperties
    )
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK(_vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR);
    return _vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR(_physicalDevices[_physicalDevice], pPropertyCount, pProperties);
}

VkResult VulkanDeviceImpl::vkCreateFramebuffer(
        const VkFramebufferCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkFramebuffer* pFramebuffer
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateFramebuffer);
    return _vkCreateFramebuffer(_device, pCreateInfo, pAllocator, pFramebuffer);
}

void VulkanDeviceImpl::vkDestroyFramebuffer(
        VkFramebuffer framebuffer,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyFramebuffer);
    _vkDestroyFramebuffer(_device, framebuffer, pAllocator);
}

VkResult VulkanDeviceImpl::vkCreateRenderPass(
        const VkRenderPassCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkRenderPass* pRenderPass
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateRenderPass);
    return _vkCreateRenderPass(_device, pCreateInfo, pAllocator, pRenderPass);
}

void VulkanDeviceImpl::vkDestroyRenderPass(
        VkRenderPass renderPass,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyRenderPass);
    _vkDestroyRenderPass(_device, renderPass, pAllocator);
}

void VulkanDeviceImpl::vkGetRenderAreaGranularity(
        VkRenderPass renderPass,
        VkExtent2D* pGranularity
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkGetRenderAreaGranularity);
    _vkGetRenderAreaGranularity(_device, renderPass, pGranularity);
}

VkResult VulkanDeviceImpl::vkCreateRenderPass2KHR(
        const VkRenderPassCreateInfo2* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkRenderPass* pRenderPass
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateRenderPass2KHR);
    return _vkCreateRenderPass2KHR(_device, pCreateInfo, pAllocator, pRenderPass);
}

VkResult VulkanDeviceImpl::vkCreatePipelineCache(
        const VkPipelineCacheCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkPipelineCache* pPipelineCache
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreatePipelineCache);
    return _vkCreatePipelineCache(_device, pCreateInfo, pAllocator, pPipelineCache);
}

void VulkanDeviceImpl::vkDestroyPipelineCache(
        VkPipelineCache pipelineCache,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyPipelineCache);
    _vkDestroyPipelineCache(_device, pipelineCache, pAllocator);
}

VkResult VulkanDeviceImpl::vkGetPipelineCacheData(
        VkPipelineCache pipelineCache,
        size_t* pDataSize,
        void* pData
    ) 
{
    XXX_VULKAN_DEVICE_LOCK(_vkGetPipelineCacheData);
    return _vkGetPipelineCacheData(_device, pipelineCache, pDataSize, pData);
}

VkResult VulkanDeviceImpl::vkMergePipelineCaches(
        VkPipelineCache dstCache,
        uint32_t srcCacheCount,
        const VkPipelineCache* pSrcCaches
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkMergePipelineCaches);
    return _vkMergePipelineCaches(_device, dstCache, srcCacheCount, pSrcCaches);
}

VkResult VulkanDeviceImpl::vkCreateGraphicsPipelines(
        VkPipelineCache pipelineCache,
        uint32_t createInfoCount,
        const VkGraphicsPipelineCreateInfo* pCreateInfos,
        const VkAllocationCallbacks* pAllocator,
        VkPipeline* pPipelines
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateGraphicsPipelines);
    return _vkCreateGraphicsPipelines(_device, pipelineCache, createInfoCount, pCreateInfos, pAllocator, pPipelines);
}

VkResult VulkanDeviceImpl::vkCreateComputePipelines(
        VkPipelineCache pipelineCache,
        uint32_t createInfoCount,
        const VkComputePipelineCreateInfo* pCreateInfos,
        const VkAllocationCallbacks* pAllocator,
        VkPipeline* pPipelines
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateComputePipelines);
    return _vkCreateComputePipelines(_device, pipelineCache, createInfoCount, pCreateInfos, pAllocator, pPipelines);
}

void VulkanDeviceImpl::vkDestroyPipeline(
        VkPipeline pipeline,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyPipeline);
    return _vkDestroyPipeline(_device, pipeline, pAllocator);
}

void VulkanDeviceImpl::vkCmdBindPipeline(
        VkCommandBuffer commandBuffer,
        VkPipelineBindPoint pipelineBindPoint,
        VkPipeline pipeline
    )
{
    _vkCmdBindPipeline(commandBuffer, pipelineBindPoint, pipeline);
}

void VulkanDeviceImpl::vkCmdPipelineBarrier(
        VkCommandBuffer commandBuffer,
        VkPipelineStageFlags srcStageMask,
        VkPipelineStageFlags dstStageMask,
        VkDependencyFlags dependencyFlags,
        uint32_t memoryBarrierCount,
        const VkMemoryBarrier* pMemoryBarriers,
        uint32_t bufferMemoryBarrierCount,
        const VkBufferMemoryBarrier* pBufferMemoryBarriers,
        uint32_t imageMemoryBarrierCount,
        const VkImageMemoryBarrier* pImageMemoryBarriers
    )
{
    _vkCmdPipelineBarrier(commandBuffer, srcStageMask, dstStageMask, dependencyFlags,
        memoryBarrierCount, pMemoryBarriers, bufferMemoryBarrierCount, pBufferMemoryBarriers,
        imageMemoryBarrierCount, pImageMemoryBarriers
    );
}

void VulkanDeviceImpl::vkGetPhysicalDeviceMemoryProperties(
        VkPhysicalDeviceMemoryProperties* pMemoryProperties
    )
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK_VOID(_vkGetPhysicalDeviceMemoryProperties);
    _vkGetPhysicalDeviceMemoryProperties(_physicalDevices[_physicalDevice], pMemoryProperties);
}

VkResult VulkanDeviceImpl::vkAllocateMemory(
        const VkMemoryAllocateInfo* pAllocateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDeviceMemory* pMemory
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkAllocateMemory);
    return _vkAllocateMemory(_device, pAllocateInfo, pAllocator, pMemory);
}

void VulkanDeviceImpl::vkFreeMemory(
        VkDeviceMemory memory,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkFreeMemory);
    _vkFreeMemory(_device, memory, pAllocator);
}

VkResult VulkanDeviceImpl::vkMapMemory(
        VkDeviceMemory memory,
        VkDeviceSize offset,
        VkDeviceSize size,
        VkMemoryMapFlags flags,
        void** ppData
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkMapMemory);
    return _vkMapMemory(_device, memory, offset, size, flags, ppData);
}

void VulkanDeviceImpl::vkUnmapMemory(
        VkDeviceMemory memory
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkUnmapMemory);
    _vkUnmapMemory(_device, memory);
}

VkResult VulkanDeviceImpl::vkFlushMappedMemoryRanges(
        uint32_t memoryRangeCount,
        const VkMappedMemoryRange* pMemoryRanges
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkFlushMappedMemoryRanges);
    return _vkFlushMappedMemoryRanges(_device, memoryRangeCount, pMemoryRanges);
}

VkResult VulkanDeviceImpl::vkInvalidateMappedMemoryRanges(
        uint32_t memoryRangeCount,
        const VkMappedMemoryRange* pMemoryRanges
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkInvalidateMappedMemoryRanges);
    return _vkInvalidateMappedMemoryRanges(_device, memoryRangeCount, pMemoryRanges);
}

void VulkanDeviceImpl::vkGetDeviceMemoryCommitment(
        VkDeviceMemory memory,
        VkDeviceSize* pCommittedMemoryInBytes
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkGetDeviceMemoryCommitment);
    _vkGetDeviceMemoryCommitment(_device, memory, pCommittedMemoryInBytes);
}

VkResult VulkanDeviceImpl::vkCreateBuffer(
        const VkBufferCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkBuffer* pBuffer
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateBuffer);
    return _vkCreateBuffer(_device, pCreateInfo, pAllocator, pBuffer);
}

void VulkanDeviceImpl::vkDestroyBuffer(
        VkBuffer buffer,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyBuffer);
    _vkDestroyBuffer(_device, buffer, pAllocator);
}

VkResult VulkanDeviceImpl::vkBindBufferMemory(
        VkBuffer buffer,
        VkDeviceMemory memory,
        VkDeviceSize memoryOffset
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkBindBufferMemory);
    return _vkBindBufferMemory(_device, buffer, memory, memoryOffset);
}

VkResult VulkanDeviceImpl::vkBindBufferMemory2(
        uint32_t bindInfoCount,
        const VkBindBufferMemoryInfo* pBindInfos
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkBindBufferMemory2);
    return _vkBindBufferMemory2(_device, bindInfoCount, pBindInfos);
}

VkResult VulkanDeviceImpl::vkBindImageMemory(
        VkImage image,
        VkDeviceMemory memory,
        VkDeviceSize memoryOffset
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkBindImageMemory);
    return _vkBindImageMemory(_device, image, memory, memoryOffset);
}

VkResult VulkanDeviceImpl::vkBindImageMemory2(
        uint32_t bindInfoCount,
        const VkBindImageMemoryInfo* pBindInfos
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkBindImageMemory2);
    return _vkBindImageMemory2(_device, bindInfoCount, pBindInfos);
}

void VulkanDeviceImpl::vkGetBufferMemoryRequirements(
        VkBuffer buffer,
        VkMemoryRequirements* pMemoryRequirements
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkGetBufferMemoryRequirements);
    _vkGetBufferMemoryRequirements(_device, buffer, pMemoryRequirements);
}

void VulkanDeviceImpl::vkGetBufferMemoryRequirements2(
        const VkBufferMemoryRequirementsInfo2* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkGetBufferMemoryRequirements2);
    _vkGetBufferMemoryRequirements2(_device, pInfo, pMemoryRequirements);
}

void VulkanDeviceImpl::vkGetDeviceBufferMemoryRequirements(
        const VkDeviceBufferMemoryRequirements* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkGetDeviceBufferMemoryRequirements);
    _vkGetDeviceBufferMemoryRequirements(_device, pInfo, pMemoryRequirements);
}

void VulkanDeviceImpl::vkGetImageMemoryRequirements(
        VkImage image,
        VkMemoryRequirements* pMemoryRequirements
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkGetImageMemoryRequirements);
    _vkGetImageMemoryRequirements(_device, image, pMemoryRequirements);
}

void VulkanDeviceImpl::vkGetImageMemoryRequirements2(
        const VkImageMemoryRequirementsInfo2* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkGetImageMemoryRequirements2);
    _vkGetImageMemoryRequirements2(_device, pInfo, pMemoryRequirements);
}

void VulkanDeviceImpl::vkGetDeviceImageMemoryRequirements(
        const VkDeviceImageMemoryRequirements* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkGetDeviceImageMemoryRequirements);
    _vkGetDeviceImageMemoryRequirements(_device, pInfo, pMemoryRequirements);
}

VkResult VulkanDeviceImpl::vkCreateBufferView(
        const VkBufferViewCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkBufferView* pView
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateBufferView);
    return _vkCreateBufferView(_device, pCreateInfo, pAllocator, pView);
}

void VulkanDeviceImpl::vkDestroyBufferView(
        VkBufferView bufferView,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyBufferView);
    _vkDestroyBufferView(_device, bufferView, pAllocator);
}

VkResult VulkanDeviceImpl::vkCreateImage(
        const VkImageCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkImage* pImage
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateImage);
    return _vkCreateImage(_device, pCreateInfo, pAllocator, pImage);
}

void VulkanDeviceImpl::vkDestroyImage(
        VkImage image,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkCreateImage);
    _vkDestroyImage(_device, image, pAllocator);
}

void VulkanDeviceImpl::vkGetImageSubresourceLayout(
        VkImage image,
        const VkImageSubresource* pSubresource,
        VkSubresourceLayout* pLayout
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkGetImageSubresourceLayout);
    _vkGetImageSubresourceLayout(_device, image, pSubresource, pLayout);
}

VkResult VulkanDeviceImpl::vkCreateImageView(
        const VkImageViewCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkImageView* pView
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateImageView);
    return _vkCreateImageView(_device, pCreateInfo, pAllocator, pView);
}

void VulkanDeviceImpl::vkDestroyImageView(
        VkImageView imageView,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyImageView);
    _vkDestroyImageView(_device, imageView, pAllocator);
}

void VulkanDeviceImpl::vkGetBufferMemoryRequirements2KHR(
    const VkBufferMemoryRequirementsInfo2* pInfo,
    VkMemoryRequirements2* pMemoryRequirements
)
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkGetBufferMemoryRequirements2KHR);
    _vkGetBufferMemoryRequirements2KHR(_device, pInfo, pMemoryRequirements);
}

void VulkanDeviceImpl::vkGetImageMemoryRequirements2KHR(
    const VkImageMemoryRequirementsInfo2* pInfo,
    VkMemoryRequirements2* pMemoryRequirements
)
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkGetImageMemoryRequirements2KHR);
    _vkGetImageMemoryRequirements2KHR(_device, pInfo, pMemoryRequirements);
}

VkResult VulkanDeviceImpl::vkCreateSampler(
        const VkSamplerCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkSampler* pSampler
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateSampler);
    return _vkCreateSampler(_device, pCreateInfo, pAllocator, pSampler);
}

void VulkanDeviceImpl::vkDestroySampler(
        VkSampler sampler,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroySampler);
    _vkDestroySampler(_device, sampler, pAllocator);
}

VkResult VulkanDeviceImpl::vkCreateSamplerYcbcrConversion(
        const VkSamplerYcbcrConversionCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkSamplerYcbcrConversion* pYcbcrConversion
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateSamplerYcbcrConversion);
    return _vkCreateSamplerYcbcrConversion(_device, pCreateInfo, pAllocator, pYcbcrConversion);
}

void VulkanDeviceImpl::vkDestroySamplerYcbcrConversion(
        VkSamplerYcbcrConversion ycbcrConversion,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroySamplerYcbcrConversion);
    _vkDestroySamplerYcbcrConversion(_device, ycbcrConversion, pAllocator);
}

VkResult VulkanDeviceImpl::vkCreatePipelineLayout(
        const VkPipelineLayoutCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkPipelineLayout* pPipelineLayout
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreatePipelineLayout);
    return _vkCreatePipelineLayout(_device, pCreateInfo, pAllocator, pPipelineLayout);
}

void VulkanDeviceImpl::vkDestroyPipelineLayout(
        VkPipelineLayout pipelineLayout,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyPipelineLayout);
    _vkDestroyPipelineLayout(_device, pipelineLayout, pAllocator);
}

VkResult VulkanDeviceImpl::vkCreateDescriptorSetLayout(
        const VkDescriptorSetLayoutCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDescriptorSetLayout* pSetLayout
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateDescriptorSetLayout);
    return _vkCreateDescriptorSetLayout(_device, pCreateInfo, pAllocator, pSetLayout);
}

void VulkanDeviceImpl::vkDestroyDescriptorSetLayout(
        VkDescriptorSetLayout descriptorSetLayout,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyDescriptorSetLayout);
    _vkDestroyDescriptorSetLayout(_device, descriptorSetLayout, pAllocator);
}

VkResult VulkanDeviceImpl::vkCreateDescriptorPool(
        const VkDescriptorPoolCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDescriptorPool* pDescriptorPool
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateDescriptorPool);
    return _vkCreateDescriptorPool(_device, pCreateInfo, pAllocator, pDescriptorPool);
}

void VulkanDeviceImpl::vkDestroyDescriptorPool(
        VkDescriptorPool descriptorPool,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyDescriptorPool);
    _vkDestroyDescriptorPool(_device, descriptorPool, pAllocator);
}

VkResult VulkanDeviceImpl::vkResetDescriptorPool(
        VkDescriptorPool descriptorPool,
        VkDescriptorPoolResetFlags flags
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkResetDescriptorPool);
    return _vkResetDescriptorPool(_device, descriptorPool, flags);
}

VkResult VulkanDeviceImpl::vkAllocateDescriptorSets(
        const VkDescriptorSetAllocateInfo* pAllocateInfo,
        VkDescriptorSet* pDescriptorSets
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkAllocateDescriptorSets);
    return _vkAllocateDescriptorSets(_device, pAllocateInfo, pDescriptorSets);
}

VkResult VulkanDeviceImpl::vkFreeDescriptorSets(
        VkDescriptorPool descriptorPool,
        uint32_t descriptorSetCount,
        const VkDescriptorSet* pDescriptorSets
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkFreeDescriptorSets);
    return _vkFreeDescriptorSets(_device, descriptorPool, descriptorSetCount, pDescriptorSets);
}

void VulkanDeviceImpl::vkCmdBindDescriptorSets(
        VkCommandBuffer commandBuffer,
        VkPipelineBindPoint pipelineBindPoint,
        VkPipelineLayout layout,
        uint32_t firstSet,
        uint32_t descriptorSetCount,
        const VkDescriptorSet* pDescriptorSets,
        uint32_t dynamicOffsetCount,
        const uint32_t* pDynamicOffsets
    )
{
    _vkCmdBindDescriptorSets(commandBuffer, pipelineBindPoint, layout, firstSet, descriptorSetCount,
        pDescriptorSets, dynamicOffsetCount, pDynamicOffsets
    );
}

void VulkanDeviceImpl::vkCmdPushConstants(
        VkCommandBuffer commandBuffer,
        VkPipelineLayout layout,
        VkShaderStageFlags stageFlags,
        uint32_t offset,
        uint32_t size,
        const void* pValues
    )
{
    _vkCmdPushConstants(commandBuffer, layout, stageFlags, offset, size, pValues);
}

VkResult VulkanDeviceImpl::vkCreateQueryPool(
        const VkQueryPoolCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkQueryPool* pQueryPool
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateQueryPool);
    return _vkCreateQueryPool(_device, pCreateInfo, pAllocator, pQueryPool);
}

void VulkanDeviceImpl::vkDestroyQueryPool(
        VkQueryPool queryPool,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyQueryPool);
    _vkDestroyQueryPool(_device, queryPool, pAllocator);
}

VkResult VulkanDeviceImpl::vkGetQueryPoolResults(
        VkQueryPool queryPool,
        uint32_t firstQuery,
        uint32_t queryCount,
        size_t dataSize,
        void* pData,
        VkDeviceSize stride,
        VkQueryResultFlags flags
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkGetQueryPoolResults);
    return _vkGetQueryPoolResults(_device, queryPool, firstQuery, queryCount, dataSize, pData, stride, flags);
}

void VulkanDeviceImpl::vkCmdBeginQuery(
        VkCommandBuffer commandBuffer,
        VkQueryPool queryPool,
        uint32_t query,
        VkQueryControlFlags flags
    )
{
    _vkCmdBeginQuery(commandBuffer, queryPool, query, flags);
}

void VulkanDeviceImpl::vkCmdEndQuery(
        VkCommandBuffer commandBuffer,
        VkQueryPool queryPool,
        uint32_t query
    )
{
    _vkCmdEndQuery(commandBuffer, queryPool, query);
}

void VulkanDeviceImpl::vkCmdResetQueryPool(
        VkCommandBuffer commandBuffer,
        VkQueryPool queryPool,
        uint32_t firstQuery,
        uint32_t queryCount
    )
{
    _vkCmdResetQueryPool(commandBuffer, queryPool, firstQuery, queryCount);
}

void VulkanDeviceImpl::vkCmdWriteTimestamp(
        VkCommandBuffer commandBuffer,
        VkPipelineStageFlagBits pipelineStage,
        VkQueryPool queryPool,
        uint32_t query
    )
{
    _vkCmdWriteTimestamp(commandBuffer, pipelineStage, queryPool, query);
}

void VulkanDeviceImpl::vkCmdCopyQueryPoolResults(
        VkCommandBuffer commandBuffer,
        VkQueryPool queryPool,
        uint32_t firstQuery,
        uint32_t queryCount,
        VkBuffer dstBuffer,
        VkDeviceSize dstOffset,
        VkDeviceSize stride,
        VkQueryResultFlags flags
    )
{
    _vkCmdCopyQueryPoolResults(commandBuffer, queryPool, firstQuery, queryCount, 
        dstBuffer, dstOffset, stride, flags);
}

void VulkanDeviceImpl::vkCmdUpdateBuffer(
        VkCommandBuffer commandBuffer,
        VkBuffer dstBuffer,
        VkDeviceSize dstOffset,
        VkDeviceSize dataSize,
        const void* pData
    )
{
    _vkCmdUpdateBuffer(commandBuffer, dstBuffer, dstOffset, dataSize, pData);
}

void VulkanDeviceImpl::vkCmdFillBuffer(
        VkCommandBuffer commandBuffer,
        VkBuffer dstBuffer,
        VkDeviceSize dstOffset,
        VkDeviceSize size,
        uint32_t data
    )
{
    _vkCmdFillBuffer(commandBuffer, dstBuffer, dstOffset, size, data);
}

void VulkanDeviceImpl::vkCmdClearColorImage(
        VkCommandBuffer commandBuffer,
        VkImage image,
        VkImageLayout imageLayout,
        const VkClearColorValue* pColor,
        uint32_t rangeCount,
        const VkImageSubresourceRange* pRanges
    )
{
    _vkCmdClearColorImage(commandBuffer, image, imageLayout, pColor, rangeCount, pRanges);
}

void VulkanDeviceImpl::vkCmdClearDepthStencilImage(
        VkCommandBuffer commandBuffer,
        VkImage image,
        VkImageLayout imageLayout,
        const VkClearDepthStencilValue* pDepthStencil,
        uint32_t rangeCount,
        const VkImageSubresourceRange* pRanges
    )
{
    _vkCmdClearDepthStencilImage(commandBuffer, image, imageLayout, pDepthStencil, rangeCount, pRanges);
}

void VulkanDeviceImpl::vkCmdClearAttachments(
        VkCommandBuffer commandBuffer,
        uint32_t attachmentCount,
        const VkClearAttachment* pAttachments,
        uint32_t rectCount,
        const VkClearRect* pRects
    )
{
    _vkCmdClearAttachments(commandBuffer, attachmentCount, pAttachments, rectCount, pRects);
}

void VulkanDeviceImpl::vkCmdCopyBuffer(
        VkCommandBuffer commandBuffer,
        VkBuffer srcBuffer,
        VkBuffer dstBuffer,
        uint32_t regionCount,
        const VkBufferCopy* pRegions
    )
{
    _vkCmdCopyBuffer(commandBuffer, srcBuffer, dstBuffer, regionCount, pRegions);
}

void VulkanDeviceImpl::vkCmdCopyImage(
        VkCommandBuffer commandBuffer,
        VkImage srcImage,
        VkImageLayout srcImageLayout,
        VkImage dstImage,
        VkImageLayout dstImageLayout,
        uint32_t regionCount,
        const VkImageCopy* pRegions
    )
{
    _vkCmdCopyImage(commandBuffer, srcImage, srcImageLayout, dstImage,
        dstImageLayout, regionCount, pRegions
    );
}

void VulkanDeviceImpl::vkCmdBlitImage(
        VkCommandBuffer commandBuffer,
        VkImage srcImage,
        VkImageLayout srcImageLayout,
        VkImage dstImage,
        VkImageLayout dstImageLayout,
        uint32_t regionCount,
        const VkImageBlit* pRegions,
        VkFilter filter
    )
{
    _vkCmdBlitImage(commandBuffer, srcImage, srcImageLayout,
        dstImage, dstImageLayout, regionCount, pRegions, filter
    );
}

void VulkanDeviceImpl::vkCmdCopyImageToBuffer(
        VkCommandBuffer commandBuffer,
        VkImage srcImage,
        VkImageLayout srcImageLayout,
        VkBuffer dstBuffer,
        uint32_t regionCount,
        const VkBufferImageCopy* pRegions
    )
{
    _vkCmdCopyImageToBuffer(commandBuffer, srcImage, srcImageLayout, dstBuffer, regionCount, pRegions);
}

void VulkanDeviceImpl::vkCmdResolveImage(
        VkCommandBuffer commandBuffer,
        VkImage srcImage,
        VkImageLayout srcImageLayout,
        VkImage dstImage,
        VkImageLayout dstImageLayout,
        uint32_t regionCount,
        const VkImageResolve* pRegions
    )
{
    _vkCmdResolveImage(commandBuffer, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions);
}

void VulkanDeviceImpl::vkCmdCopyBufferToImage(
        VkCommandBuffer commandBuffer,
         VkBuffer srcBuffer,
         VkImage dstImage,
         VkImageLayout dstImageLayout,
         uint32_t regionCount,
         const VkBufferImageCopy* pRegions
    )
{
    _vkCmdCopyBufferToImage(commandBuffer, srcBuffer, dstImage, dstImageLayout, regionCount, pRegions);
}

void VulkanDeviceImpl::vkCmdBindIndexBuffer(
        VkCommandBuffer commandBuffer,
        VkBuffer buffer,
        VkDeviceSize offset,
        VkIndexType indexType
    )
{
    _vkCmdBindIndexBuffer(commandBuffer, buffer, offset, indexType);
}

void VulkanDeviceImpl::vkCmdBindVertexBuffers(
        VkCommandBuffer commandBuffer,
        uint32_t firstBinding,
        uint32_t bindingCount,
        const VkBuffer* pBuffers,
        const VkDeviceSize* pOffsets
    )
{
    _vkCmdBindVertexBuffers(commandBuffer, firstBinding, bindingCount, pBuffers, pOffsets);
}

void VulkanDeviceImpl::vkCmdDraw(
        VkCommandBuffer commandBuffer,
        uint32_t vertexCount,
        uint32_t instanceCount,
        uint32_t firstVertex,
        uint32_t firstInstance
    )
{
    _vkCmdDraw(commandBuffer, vertexCount, instanceCount, firstVertex, firstInstance);
}

void VulkanDeviceImpl::vkCmdDrawIndexed(
        VkCommandBuffer commandBuffer,
        uint32_t indexCount,
        uint32_t instanceCount,
        uint32_t firstIndex,
        int32_t vertexOffset,
        uint32_t firstInstance
    )
{
    _vkCmdDrawIndexed(commandBuffer, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);
}

void VulkanDeviceImpl::vkCmdDrawIndirect(
        VkCommandBuffer commandBuffer,
        VkBuffer buffer,
        VkDeviceSize offset,
        uint32_t drawCount,
        uint32_t stride
    )
{
    _vkCmdDrawIndirect(commandBuffer, buffer, offset, drawCount, stride);
}

void VulkanDeviceImpl::vkCmdDrawIndexedIndirect(
        VkCommandBuffer commandBuffer,
        VkBuffer buffer,
        VkDeviceSize offset,
        uint32_t drawCount,
        uint32_t stride
    )
{
    _vkCmdDrawIndexedIndirect(commandBuffer, buffer, offset, drawCount, stride);
}

void VulkanDeviceImpl::vkCmdSetViewport(
        VkCommandBuffer commandBuffer,
        uint32_t firstViewport,
        uint32_t viewportCount,
        const VkViewport* pViewports
    )
{
    _vkCmdSetViewport(commandBuffer, firstViewport, viewportCount, pViewports);
}

void VulkanDeviceImpl::vkCmdSetLineWidth(
        VkCommandBuffer commandBuffer,
        float lineWidth
    )
{
    _vkCmdSetLineWidth(commandBuffer, lineWidth);
}

void VulkanDeviceImpl::vkCmdSetDepthBias(
        VkCommandBuffer commandBuffer,
        float depthBiasConstantFactor,
        float depthBiasClamp,
        float depthBiasSlopeFactor
    )
{
    _vkCmdSetDepthBias(commandBuffer, depthBiasConstantFactor, depthBiasClamp, depthBiasSlopeFactor);
}

void VulkanDeviceImpl::vkCmdSetScissor(
        VkCommandBuffer commandBuffer,
        uint32_t firstScissor,
        uint32_t scissorCount,
        const VkRect2D* pScissors
    )
{
    _vkCmdSetScissor(commandBuffer, firstScissor, scissorCount, pScissors);
}

void VulkanDeviceImpl::vkCmdSetBlendConstants(
        VkCommandBuffer commandBuffer,
        const float blendConstants[4]
    )
{
    _vkCmdSetBlendConstants(commandBuffer, blendConstants);
}

void VulkanDeviceImpl::vkCmdSetDepthBounds(
        VkCommandBuffer commandBuffer,
        float minDepthBounds,
        float maxDepthBounds
    )
{
    _vkCmdSetDepthBounds(commandBuffer, minDepthBounds, maxDepthBounds);
}

void VulkanDeviceImpl::vkCmdSetStencilCompareMask(
        VkCommandBuffer commandBuffer,
        VkStencilFaceFlags faceMask,
        uint32_t compareMask
    )
{
    _vkCmdSetStencilCompareMask(commandBuffer, faceMask, compareMask);
}

void VulkanDeviceImpl::vkCmdSetStencilWriteMask(
        VkCommandBuffer commandBuffer,
        VkStencilFaceFlags faceMask,
        uint32_t writeMask
    )
{
    _vkCmdSetStencilWriteMask(commandBuffer, faceMask, writeMask);
}

void VulkanDeviceImpl::vkCmdSetStencilReference(
        VkCommandBuffer commandBuffer,
        VkStencilFaceFlags faceMask,
        uint32_t reference
    )
{
    _vkCmdSetStencilReference(commandBuffer, faceMask, reference);
}

void VulkanDeviceImpl::vkCmdDispatch(
        VkCommandBuffer commandBuffer,
        uint32_t groupCountX,
        uint32_t groupCountY,
        uint32_t groupCountZ
    )
{
    _vkCmdDispatch(commandBuffer, groupCountX, groupCountY, groupCountZ);
}

void VulkanDeviceImpl::vkCmdDispatchIndirect(
        VkCommandBuffer commandBuffer,
        VkBuffer buffer,
        VkDeviceSize offset
    )
{
    _vkCmdDispatchIndirect(commandBuffer, buffer, offset);
}

VkResult VulkanDeviceImpl::vkQueueBindSparse(
        VkQueue queue,
        uint32_t bindInfoCount,
        const VkBindSparseInfo* pBindInfo,
        VkFence fence
    )
{
    // TODO
    assert(false);
    return VK_NOT_READY;
}

void VulkanDeviceImpl::vkDestroySurfaceKHR(
    VkSurfaceKHR surface,
    const VkAllocationCallbacks* pAllocator
)
{
    _vkDestroySurfaceKHR(_instance, surface, pAllocator);
}

VkResult VulkanDeviceImpl::vkGetPhysicalDeviceSurfaceFormatsKHR(
        VkSurfaceKHR surface,
        uint32_t* pSurfaceFormatCount,
        VkSurfaceFormatKHR* pSurfaceFormats
    )
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK(_vkGetPhysicalDeviceSurfaceFormatsKHR);
    return _vkGetPhysicalDeviceSurfaceFormatsKHR(_physicalDevices[_physicalDevice], surface, pSurfaceFormatCount, pSurfaceFormats);
}

VkResult VulkanDeviceImpl::vkCreateSwapchainKHR(
    const VkSwapchainCreateInfoKHR* pCreateInfo,
    const VkAllocationCallbacks* pAllocator,
    VkSwapchainKHR* pSwapchain
)
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateSwapchainKHR);
    return _vkCreateSwapchainKHR(_device, pCreateInfo, pAllocator, pSwapchain);
}

void VulkanDeviceImpl::vkDestroySwapchainKHR(
    VkSwapchainKHR swapchain,
    const VkAllocationCallbacks* pAllocator
)
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroySwapchainKHR);
    return _vkDestroySwapchainKHR(_device, swapchain, pAllocator);
}

VkResult VulkanDeviceImpl::vkGetSwapchainImagesKHR(
    VkSwapchainKHR swapchain,
    uint32_t* pSwapchainImageCount,
    VkImage* pSwapchainImages
)
{
    XXX_VULKAN_DEVICE_LOCK(_vkGetSwapchainImagesKHR);
    return _vkGetSwapchainImagesKHR(_device, swapchain, pSwapchainImageCount, pSwapchainImages);
}

VkResult VulkanDeviceImpl::vkAcquireNextImageKHR(
    VkSwapchainKHR swapchain,
    uint64_t timeout,
    VkSemaphore semaphore,
    VkFence fence,
    uint32_t* pImageIndex
)
{
    XXX_VULKAN_DEVICE_LOCK(_vkAcquireNextImageKHR);
    return _vkAcquireNextImageKHR(_device, swapchain, timeout, semaphore, fence, pImageIndex);
}

VkResult VulkanDeviceImpl::vkQueuePresentKHR(
    VkQueue queue,
    const VkPresentInfoKHR* pPresentInfo
)
{
    return _vkQueuePresentKHR(queue, pPresentInfo);
}

VkResult VulkanDeviceImpl::vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
    VkSurfaceKHR surface,
    VkSurfaceCapabilitiesKHR* pSurfaceCapabilities
)
{
    return _vkGetPhysicalDeviceSurfaceCapabilitiesKHR(_physicalDevices[_physicalDevice], surface, pSurfaceCapabilities);
}

VkResult VulkanDeviceImpl::vkGetPhysicalDeviceSurfacePresentModesKHR(
        VkSurfaceKHR surface,
        uint32_t* pPresentModeCount,
        VkPresentModeKHR* pPresentModes
    )
{
    return _vkGetPhysicalDeviceSurfacePresentModesKHR(_physicalDevices[_physicalDevice], surface, pPresentModeCount, pPresentModes);
}

VkResult VulkanDeviceImpl::vkWaitForPresentKHR(
        VkSwapchainKHR swapchain,
        uint64_t presentId,
        uint64_t timeout
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkWaitForPresentKHR);
    return _vkWaitForPresentKHR(_device, swapchain, presentId, timeout);
}

void VulkanDeviceImpl::vkGetPhysicalDeviceFeatures(
        VkPhysicalDeviceFeatures* pFeatures
    )
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK_VOID(_vkGetPhysicalDeviceFeatures);
    _vkGetPhysicalDeviceFeatures(_physicalDevices[_physicalDevice], pFeatures);
}

void VulkanDeviceImpl::vkGetPhysicalDeviceFeatures2(
        VkPhysicalDeviceFeatures2* pFeatures
    )
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK_VOID(_vkGetPhysicalDeviceFeatures2);
    _vkGetPhysicalDeviceFeatures2(_physicalDevices[_physicalDevice], pFeatures);
}

void VulkanDeviceImpl::vkGetPhysicalDeviceFormatProperties(
        VkFormat format,
        VkFormatProperties* pFormatProperties
    )
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK_VOID(_vkGetPhysicalDeviceFormatProperties);
    _vkGetPhysicalDeviceFormatProperties(_physicalDevices[_physicalDevice], format, pFormatProperties);
}

VkResult VulkanDeviceImpl::vkGetPhysicalDeviceImageFormatProperties(
        VkPhysicalDevice physicalDevice,
        VkFormat format,
        VkImageType type,
        VkImageTiling tiling,
        VkImageUsageFlags usage,
        VkImageCreateFlags flags,
        VkImageFormatProperties* pImageFormatProperties
    )
{
    return _vkGetPhysicalDeviceImageFormatProperties(physicalDevice, format, type, tiling, usage, flags, pImageFormatProperties);
}

VkResult VulkanDeviceImpl::vkEnumerateInstanceExtensionProperties(
        const char* pLayerName,
        uint32_t* pPropertyCount,
        VkExtensionProperties* pProperties
    )
{
    return _vkEnumerateInstanceExtensionProperties(pLayerName, pPropertyCount, pProperties);
}

VkResult VulkanDeviceImpl::vkEnumerateDeviceExtensionProperties(
        const char* pLayerName,
        uint32_t* pPropertyCount,
        VkExtensionProperties* pProperties
    )
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK(_vkEnumerateDeviceExtensionProperties);
    return _vkEnumerateDeviceExtensionProperties(_physicalDevices[_physicalDevice], pLayerName, pPropertyCount, pProperties);
}

VkResult VulkanDeviceImpl::vkEnumerateInstanceLayerProperties(
        uint32_t* pPropertyCount,
        VkLayerProperties* pProperties
    )
{
    return _vkEnumerateInstanceLayerProperties(pPropertyCount, pProperties);
}

VkResult VulkanDeviceImpl::vkEnumerateDeviceLayerProperties(
        VkPhysicalDevice physicalDevice,
        uint32_t* pPropertyCount,
        VkLayerProperties* pProperties
    )
{
    return _vkEnumerateDeviceLayerProperties(physicalDevice, pPropertyCount, pProperties);
}

void VulkanDeviceImpl::vkCmdBeginVideoCodingKHR(
    VkCommandBuffer commandBuffer,
    const VkVideoBeginCodingInfoKHR* pBeginInfo
)
{
    _vkCmdBeginVideoCodingKHR(commandBuffer, pBeginInfo);
}

VkResult VulkanDeviceImpl::vkCreateVideoSessionKHR(
        const VkVideoSessionCreateInfoKHR* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkVideoSessionKHR* pVideoSession
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateVideoSessionKHR);
    return _vkCreateVideoSessionKHR(_device, pCreateInfo, pAllocator, pVideoSession);
}

void VulkanDeviceImpl::vkDestroyVideoSessionKHR(
        VkVideoSessionKHR videoSession,
        const VkAllocationCallbacks* pAllocator
    )
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyVideoSessionKHR);
    _vkDestroyVideoSessionKHR(_device, videoSession, pAllocator);
}

VkResult VulkanDeviceImpl::vkGetVideoSessionMemoryRequirementsKHR(
        VkVideoSessionKHR videoSession,
        uint32_t* pMemoryRequirementsCount,
        VkVideoSessionMemoryRequirementsKHR* pMemoryRequirements
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkGetVideoSessionMemoryRequirementsKHR);
    return _vkGetVideoSessionMemoryRequirementsKHR(_device, videoSession, pMemoryRequirementsCount, pMemoryRequirements);
}

VkResult VulkanDeviceImpl::vkBindVideoSessionMemoryKHR(
        VkVideoSessionKHR videoSession,
        uint32_t bindSessionMemoryInfoCount,
        const VkBindVideoSessionMemoryInfoKHR* pBindSessionMemoryInfos
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkBindVideoSessionMemoryKHR);
    return _vkBindVideoSessionMemoryKHR(_device, videoSession, bindSessionMemoryInfoCount, pBindSessionMemoryInfos);
}

void VulkanDeviceImpl::vkCmdControlVideoCodingKHR(
    VkCommandBuffer commandBuffer,
    const VkVideoCodingControlInfoKHR* pCodingControlInfo
)
{
    _vkCmdControlVideoCodingKHR(commandBuffer, pCodingControlInfo);
}

void VulkanDeviceImpl::vkCmdEndVideoCodingKHR(
    VkCommandBuffer commandBuffer,
    const VkVideoEndCodingInfoKHR* pEndCodingInfo
)
{
    _vkCmdEndVideoCodingKHR(commandBuffer, pEndCodingInfo);
}

void VulkanDeviceImpl::vkCmdDecodeVideoKHR(
    VkCommandBuffer commandBuffer,
    const VkVideoDecodeInfoKHR* pDecodeInfo
)
{
    _vkCmdDecodeVideoKHR(commandBuffer, pDecodeInfo);
}

VkResult VulkanDeviceImpl::vkGetPhysicalDeviceVideoCapabilitiesKHR(
    const VkVideoProfileInfoKHR* pVideoProfile,
    VkVideoCapabilitiesKHR* pCapabilities
)
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK(_vkGetPhysicalDeviceVideoCapabilitiesKHR);
    return _vkGetPhysicalDeviceVideoCapabilitiesKHR(_physicalDevices[_physicalDevice], pVideoProfile, pCapabilities);
}

VkResult VulkanDeviceImpl::vkGetPhysicalDeviceVideoFormatPropertiesKHR(
    const VkPhysicalDeviceVideoFormatInfoKHR* pVideoFormatInfo,
    uint32_t* pVideoFormatPropertyCount,
    VkVideoFormatPropertiesKHR* pVideoFormatProperties
)
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK(_vkGetPhysicalDeviceVideoFormatPropertiesKHR);
    return _vkGetPhysicalDeviceVideoFormatPropertiesKHR(_physicalDevices[_physicalDevice], pVideoFormatInfo, pVideoFormatPropertyCount, pVideoFormatProperties);
}

VkResult VulkanDeviceImpl::vkCreateVideoSessionParametersKHR(
        const VkVideoSessionParametersCreateInfoKHR* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkVideoSessionParametersKHR* pVideoSessionParameters
    )
{
    XXX_VULKAN_DEVICE_LOCK(_vkCreateVideoSessionParametersKHR);
    return _vkCreateVideoSessionParametersKHR(_device, pCreateInfo, pAllocator, pVideoSessionParameters);
}

void VulkanDeviceImpl::vkDestroyVideoSessionParametersKHR(
    VkVideoSessionParametersKHR videoSessionParameters,
    const VkAllocationCallbacks* pAllocator
)
{
    XXX_VULKAN_DEVICE_LOCK_VOID(_vkDestroyVideoSessionParametersKHR);
    _vkDestroyVideoSessionParametersKHR(_device, videoSessionParameters, pAllocator);
}

void VulkanDeviceImpl::vkGetPhysicalDeviceFormatProperties2(
    VkFormat format,
    VkFormatProperties2* pFormatProperties
)
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK_VOID(_vkGetPhysicalDeviceFormatProperties2);
    _vkGetPhysicalDeviceFormatProperties2(_physicalDevices[_physicalDevice], format, pFormatProperties);
}

VkResult VulkanDeviceImpl::vkGetPhysicalDeviceImageFormatProperties2(
    const VkPhysicalDeviceImageFormatInfo2* pImageFormatInfo,
    VkImageFormatProperties2* pImageFormatProperties
)
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK(_vkGetPhysicalDeviceImageFormatProperties2);
    return _vkGetPhysicalDeviceImageFormatProperties2(_physicalDevices[_physicalDevice], pImageFormatInfo, pImageFormatProperties);
}

bool VulkanDeviceImpl::Init()
{
    LoadBasicFunc();
    if (!CreateInstance())
    {
        VULKAN_LOG_ERROR << "-- CreateInstance fail";
        assert(false);
        return false;
    }
    if (!CreateDevice())
    {
        VULKAN_LOG_ERROR << "-- CreateDevice fail;";
        assert(false);
        return false;
    }
    return true;
}

bool VulkanDeviceImpl::CreateInstance()
{
    uint32_t gpuCount = 1;
    VkResult res = VK_RESULT_MAX_ENUM;
    VkApplicationInfo applicationInfo = {};
    VkInstanceCreateInfo instanceCreateInfo = {};
    VkValidationFeaturesEXT validationFeatures = {};
    std::vector<VkValidationFeatureEnableEXT> validationFeatureEnables;
    std::vector<const char *> instanceLayerNames; // Hint : keep alive
    std::vector<const char *> instanceExtensionsEnabled; // Hint : keep alive

    _instanceLayerNames.clear();
    _deviceLayerNames.clear();
    
    GetInstanceLayerProperties();
    GetInstanceLayerExtensionList(std::string(), _instanceExtensionProperties);

    if (!IsInstanceExtensionAvailable(VK_KHR_SURFACE_EXTENSION_NAME))
    {
        assert(false);
    }
    _instanceExtensionsEnabled.push_back(VK_KHR_SURFACE_EXTENSION_NAME);
    _instanceExtensionsEnabled.push_back(VK_KHR_GET_PHYSICAL_DEVICE_PROPERTIES_2_EXTENSION_NAME);
#if MMP_PLATFORM(WINDOWS)
    _instanceExtensionsEnabled.push_back(VK_KHR_WIN32_SURFACE_EXTENSION_NAME);
#endif
#if defined (USE_X11)
    _instanceExtensionsEnabled.push_back(VK_KHR_XLIB_SURFACE_EXTENSION_NAME);
#endif

    vkEnumerateInstanceVersion(&_apiVersion);
    _apiVersion = VK_MAKE_API_VERSION(VK_API_VERSION_VARIANT(_apiVersion), VK_API_VERSION_MAJOR(_apiVersion), VK_API_VERSION_MINOR(_apiVersion), 0);

    // VkApplicationInfo
    {
        applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
        applicationInfo.pApplicationName = MMP_NAME;
        applicationInfo.applicationVersion = 0;
        applicationInfo.pEngineName = MMP_NAME;
        applicationInfo.engineVersion = 0;
        applicationInfo.apiVersion = _apiVersion;
    }
    // VkInstanceCreateInfo
    {
        for (auto& instanceLayerName : _instanceLayerNames)
        {
            instanceLayerNames.push_back((const char *)instanceLayerName.c_str());
        }
        for (auto& instanceExtensionEnabled : _instanceExtensionsEnabled)
        {
            instanceExtensionsEnabled.push_back((const char*)instanceExtensionEnabled.c_str());
        }
        instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
        instanceCreateInfo.pApplicationInfo = &applicationInfo;
        instanceCreateInfo.enabledLayerCount = (uint32_t)instanceLayerNames.size();
        instanceCreateInfo.ppEnabledLayerNames = instanceLayerNames.size() ? instanceLayerNames.data() : nullptr;
        instanceCreateInfo.enabledExtensionCount = (uint32_t)instanceExtensionsEnabled.size();
        instanceCreateInfo.ppEnabledExtensionNames = instanceExtensionsEnabled.size() ? instanceExtensionsEnabled.data() : nullptr;
    }
    // VkValidationFeaturesEXT
    if (_debugMode)
    {
        {
            validationFeatureEnables.push_back(VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_EXT);
            validationFeatureEnables.push_back(VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT);
            validationFeatureEnables.push_back(VK_VALIDATION_FEATURE_ENABLE_SYNCHRONIZATION_VALIDATION_EXT);
        }
        validationFeatures.sType = VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT;
        validationFeatures.pEnabledValidationFeatures = validationFeatureEnables.data();
        validationFeatures.enabledValidationFeatureCount = (uint32_t)validationFeatureEnables.size();
        instanceCreateInfo.pNext = &validationFeatures;
    }
    res = _vkCreateInstance(&instanceCreateInfo, nullptr, &_instance);
    assert(res == VK_SUCCESS);
    if (res != VK_SUCCESS)
    {
        VULKAN_LOG_ERROR << "vkCreateInstance fail";
        goto END;
    }
    LoadInstanceFunc(_instance);
    res = vkEnumeratePhysicalDevices(_instance, &gpuCount, nullptr);
    if (gpuCount <= 0)
    {
        VULKAN_LOG_ERROR << "vkEnumeratePhysicalDevices query device count fail";
        assert(false);
        goto END1;
    }
    _physicalDevices.resize(gpuCount);
    _physicalDeviceProperties.resize(gpuCount);
    res = vkEnumeratePhysicalDevices(_instance, &gpuCount, _physicalDevices.data());
    if (res != VK_SUCCESS)
    {
        VULKAN_LOG_ERROR << "vkEnumeratePhysicalDevices get device fail";
        assert(false);
        goto END1;
    }
    for (uint32_t i=0; i<gpuCount; i++)
    {
        _vkGetPhysicalDeviceProperties(_physicalDevices[i], &_physicalDeviceProperties[i].properties);
    }
    if (_debugMode && _vkCreateDebugUtilsMessengerEXT) 
    {
        VkDebugUtilsMessengerCreateInfoEXT debugUtilsMessengerCreateInfo = {};
        debugUtilsMessengerCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
        debugUtilsMessengerCreateInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
        debugUtilsMessengerCreateInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
        debugUtilsMessengerCreateInfo.pfnUserCallback = (PFN_vkDebugUtilsMessengerCallbackEXT)VulkanDebugCallback;
        debugUtilsMessengerCreateInfo.pUserData = nullptr;
        if (VULKAN_FAILED(_vkCreateDebugUtilsMessengerEXT(_instance, &debugUtilsMessengerCreateInfo, nullptr, &_debugUtilsMessenger)))
        {
            VULKAN_LOG_WARN << "vkCreateDebugUtilsMessengerEXT fail";
        }
    }
    return true;
END1:
    vkDestroyInstance(_instance, nullptr);
    _instance = VK_NULL_HANDLE;
END:
    return false;
}

void VulkanDeviceImpl::DestroyInstance()
{
    if (_instance != VK_NULL_HANDLE)
    {
        vkDestroyInstance(_instance, nullptr);
        _instance = VK_NULL_HANDLE;
    }
}

VkResult VulkanDeviceImpl::GetInstanceLayerProperties()
{
    if (!_vkEnumerateInstanceLayerProperties)
    {
        assert(false);
        return VK_INCOMPLETE;
    }
    uint32_t instanceLayerCount = 0;
    std::vector<VkLayerProperties> vkProps;
    VkResult res = VK_RESULT_MAX_ENUM;
    do
    {
        res = _vkEnumerateInstanceLayerProperties(&instanceLayerCount, nullptr);
        if (res != VK_SUCCESS)
        {
            return res;
        }
        if (!instanceLayerCount)
        {
            return VK_SUCCESS;
        }
        vkProps.resize(instanceLayerCount);
        res = _vkEnumerateInstanceLayerProperties(&instanceLayerCount, vkProps.data());
    } while (res == VK_INCOMPLETE);
    for (uint32_t i=0; i<instanceLayerCount; i++)
    {
        LayerProperties layerProps;
        layerProps.properties = vkProps[i];
        res = GetInstanceLayerExtensionList(std::string(layerProps.properties.layerName), layerProps.extensions);
        if (res != VK_SUCCESS)
        {
            return res;
        }
        _instanceLayerProperties.push_back(layerProps);
    }
    {
        VULKAN_LOG_DEBUG << "GetInstanceLayerProperties";
        for (auto& instanceLayerPropertie : _instanceLayerProperties)
        {
            VULKAN_LOG_DEBUG << "-- LayerName(" << instanceLayerPropertie.properties.layerName << ") Description(" << instanceLayerPropertie.properties.description << ")";
            for (auto& extension : instanceLayerPropertie.extensions)
            {
                VULKAN_LOG_DEBUG << "  -- " << extension.extensionName;
            }
        }
    }
    return res;
}

VkResult VulkanDeviceImpl::GetInstanceLayerExtensionList(const std::string& layerName, std::vector<VkExtensionProperties> &extensions)
{
    if (!_vkEnumerateInstanceExtensionProperties)
    {
        assert(false);
        return VK_INCOMPLETE;
    }
	VkResult res = VK_RESULT_MAX_ENUM;
	do 
    {
		uint32_t instanceExtensionCount = 0;
		res = _vkEnumerateInstanceExtensionProperties(layerName.c_str(), &instanceExtensionCount, nullptr);
		if (res != VK_SUCCESS)
			return res;
		if (instanceExtensionCount == 0)
			return VK_SUCCESS;
		extensions.resize(instanceExtensionCount);
		res = _vkEnumerateInstanceExtensionProperties(layerName.c_str(), &instanceExtensionCount, extensions.data());
	} while (res == VK_INCOMPLETE);
	return res;
}

bool VulkanDeviceImpl::IsInstanceExtensionAvailable(const std::string& extensionName)
{
    for (const auto& instanceExtensionPropertie : _instanceExtensionProperties)
    {
        if (instanceExtensionPropertie.extensionName == extensionName)
        {
            return true;
        }
    }
    for (const auto& instanceLayerPropertie : _instanceLayerProperties)
    {
        for (const auto& instanceExtensionPropertie : instanceLayerPropertie.extensions)
        {
            if (instanceExtensionPropertie.extensionName == extensionName)
            {
                return true;
            }
        }     
    }
    return false;
}

void VulkanDeviceImpl::CheckExtensions()
{
    _extensionsLookup.KHR_MAINTENANCE1_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_MAINTENANCE1_EXTENSION_NAME);
    _extensionsLookup.KHR_MAINTENANCE2_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_MAINTENANCE2_EXTENSION_NAME);
    _extensionsLookup.KHR_MAINTENANCE3_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_MAINTENANCE3_EXTENSION_NAME);
    _extensionsLookup.KHR_MULTIVIEW_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_MULTIVIEW_EXTENSION_NAME);
    _extensionsLookup.KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME);
    if (_extensionsLookup.KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME)
    {
        _extensionsLookup.KHR_DEDICATED_ALLOCATION_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_DEDICATED_ALLOCATION_EXTENSION_NAME);
    }
    _extensionsLookup.KHR_CREATE_RENDERPASS_2_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_CREATE_RENDERPASS_2_EXTENSION_NAME);
    if (_extensionsLookup.KHR_CREATE_RENDERPASS_2_EXTENSION_NAME)
    {
        _extensionsLookup.KHR_DEPTH_STENCIL_RESOLVE_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_DEPTH_STENCIL_RESOLVE_EXTENSION_NAME);
    }
    _extensionsLookup.EXT_SHADER_STENCIL_EXPORT_EXTENSION_NAME = EnableDeviceExtension(VK_EXT_SHADER_STENCIL_EXPORT_EXTENSION_NAME);
    _extensionsLookup.EXT_FRAGMENT_SHADER_INTERLOCK_EXTENSION_NAME = EnableDeviceExtension(VK_EXT_FRAGMENT_SHADER_INTERLOCK_EXTENSION_NAME);
    _extensionsLookup.ARM_RASTERIZATION_ORDER_ATTACHMENT_ACCESS_EXTENSION_NAME = EnableDeviceExtension(VK_ARM_RASTERIZATION_ORDER_ATTACHMENT_ACCESS_EXTENSION_NAME);
    _extensionsLookup.KHR_PRESENT_ID_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_PRESENT_ID_EXTENSION_NAME);
    _extensionsLookup.KHR_PRESENT_WAIT_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_PRESENT_WAIT_EXTENSION_NAME);
    _extensionsLookup.KHR_PORTABILITY_SUBSET_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_PORTABILITY_SUBSET_EXTENSION_NAME);
    _extensionsLookup.KHR_PUSH_DESCRIPTOR_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_PUSH_DESCRIPTOR_EXTENSION_NAME);
    _extensionsLookup.KHR_SAMPLER_YCBCR_CONVERSION_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_SAMPLER_YCBCR_CONVERSION_EXTENSION_NAME);
    _extensionsLookup.EXT_DESCRIPTOR_BUFFER_EXTENSION_NAME = EnableDeviceExtension(VK_EXT_DESCRIPTOR_BUFFER_EXTENSION_NAME);
    _extensionsLookup.EXT_PHYSICAL_DEVICE_DRM_EXTENSION_NAME = EnableDeviceExtension(VK_EXT_PHYSICAL_DEVICE_DRM_EXTENSION_NAME);
    _extensionsLookup.EXT_SHADER_ATOMIC_FLOAT_EXTENSION_NAME = EnableDeviceExtension(VK_EXT_SHADER_ATOMIC_FLOAT_EXTENSION_NAME);
    _extensionsLookup.KHR_VIDEO_QUEUE_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_VIDEO_QUEUE_EXTENSION_NAME);
    _extensionsLookup.KHR_VIDEO_DECODE_QUEUE_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_VIDEO_DECODE_QUEUE_EXTENSION_NAME);
    _extensionsLookup.KHR_VIDEO_DECODE_H264_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_VIDEO_DECODE_H264_EXTENSION_NAME);
    _extensionsLookup.KHR_VIDEO_DECODE_H265_EXTENSION_NAME = EnableDeviceExtension(VK_KHR_VIDEO_DECODE_H265_EXTENSION_NAME);
}

bool VulkanDeviceImpl::CreateDevice()
{
    VkResult vkRes = VK_SUCCESS;
    bool found = false;
    std::vector<VkDeviceQueueCreateInfo> queueCreateInfos;
    VkDeviceQueueCreateInfo queueInfo = {};
    VkDeviceCreateInfo      deviceCreateInfo = {};
    VmaAllocatorCreateInfo  allocatorCreateInfo = {};
    VmaVulkanFunctions vulkanFunctions = {};
    std::vector<const char*> deviceLayerNames; // Keep alive
    std::vector<const char*> deviceExtensionsEnabled; // Keep alive

    if (!_vkCreateDevice)
    {
        assert(false);
        return false;
    }

    ChooseDevice();
    if (!LoadProps())
    {
        assert(false);
        return false;
    }
    CheckExtensions();
    InitVkDeviceQueueCreateInfos(queueCreateInfos);
    InitDeviceParams();
    _vkGetPhysicalDeviceFeatures2(_physicalDevices[_physicalDevice], &_deviceFeatures2);
    {
        _deviceFeature.enabled = {};
        _deviceFeature.enabled.standard.dualSrcBlend = _deviceFeature.available.standard.dualSrcBlend;
        _deviceFeature.enabled.standard.logicOp = _deviceFeature.available.standard.logicOp;
        _deviceFeature.enabled.standard.depthClamp = _deviceFeature.available.standard.depthClamp;
        _deviceFeature.enabled.standard.depthBounds = _deviceFeature.available.standard.depthBounds;
        _deviceFeature.enabled.standard.samplerAnisotropy = _deviceFeature.available.standard.samplerAnisotropy;
        _deviceFeature.enabled.standard.shaderClipDistance = _deviceFeature.available.standard.shaderClipDistance;
        _deviceFeature.enabled.standard.shaderCullDistance = _deviceFeature.available.standard.shaderCullDistance;
        _deviceFeature.enabled.standard.geometryShader = _deviceFeature.available.standard.geometryShader;
        _deviceFeature.enabled.standard.sampleRateShading = _deviceFeature.available.standard.sampleRateShading;

        _deviceFeature.enabled.multiview = {};
        _deviceFeature.enabled.multiview.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES;
        if (_extensionsLookup.KHR_MULTIVIEW_EXTENSION_NAME)
        {
            _deviceFeature.enabled.multiview.multiview = _deviceFeature.available.multiview.multiview;
        }
        _deviceFeature.enabled.presentId = {};
        _deviceFeature.enabled.presentId.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRESENT_ID_FEATURES_KHR;
        if (_extensionsLookup.KHR_PRESENT_ID_EXTENSION_NAME) 
        {
            _deviceFeature.enabled.presentId.presentId = _deviceFeature.available.presentId.presentId;
        }
        _deviceFeature.enabled.presentWait = {};
        _deviceFeature.enabled.presentWait.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PRESENT_WAIT_FEATURES_KHR;
        if (_extensionsLookup.KHR_PRESENT_WAIT_EXTENSION_NAME) 
        {
            _deviceFeature.enabled.presentWait.presentWait = _deviceFeature.available.presentWait.presentWait;
        }
    }
    // VkDeviceCreateInfo
    {
        for (auto& deviceLayerName : _deviceLayerNames)
        {
            deviceLayerNames.push_back(deviceLayerName.c_str());
        }
        for (auto& deviceExtensionEnabled : _deviceExtensionsEnabled)
        {
            deviceExtensionsEnabled.push_back(deviceExtensionEnabled.c_str());
        }
        deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
        deviceCreateInfo.queueCreateInfoCount = (uint32_t)queueCreateInfos.size();
        deviceCreateInfo.pQueueCreateInfos = queueCreateInfos.data();
        deviceCreateInfo.enabledExtensionCount = (uint32_t)deviceExtensionsEnabled.size();
        deviceCreateInfo.ppEnabledExtensionNames = deviceCreateInfo.enabledExtensionCount ? deviceExtensionsEnabled.data() : nullptr;
        deviceCreateInfo.pNext = &_deviceFeatures2;
    }
    vkRes = _vkCreateDevice(_physicalDevices[_physicalDevice], &deviceCreateInfo, nullptr, &_device);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vkCreateDevice fail";
        assert(false);
        goto END;
    }
    LoadDeviceFunc(_device, _extensionsLookup);
    if (_vkEnumerateInstanceVersion)
    {
        _vkEnumerateInstanceVersion(&_vulkanApiVersion);
    }
    {
        vulkanFunctions.vkGetInstanceProcAddr = _vkGetInstanceProcAddr;
        vulkanFunctions.vkGetDeviceProcAddr = _vkGetDeviceProcAddr;
    }
    {
        allocatorCreateInfo.vulkanApiVersion = _vulkanApiVersion & 0xFFFFF000;
        allocatorCreateInfo.physicalDevice = _physicalDevices[_physicalDevice];
        allocatorCreateInfo.device = _device;
        allocatorCreateInfo.instance = _instance;
        allocatorCreateInfo.pVulkanFunctions = &vulkanFunctions;
    }
    vkRes = vmaCreateAllocator(&allocatorCreateInfo, &_allocator);
    if (VULKAN_FAILED(vkRes))
    {
        VULKAN_LOG_ERROR << "vmaCreateAllocator fail";
        assert(false);
        goto END;
    }
    return true;
END:
    _device = VK_NULL_HANDLE;
    return false;
}

void VulkanDeviceImpl::DestroyDevice()
{
    if (_allocator != VK_NULL_HANDLE)
    {
        vmaDestroyAllocator(_allocator);
        _allocator = VK_NULL_HANDLE;
    }
    if (_device != VK_NULL_HANDLE)
    {
        _vkDestroyDevice(_device, nullptr);
        _device = VK_NULL_HANDLE;
    }
}

void VulkanDeviceImpl::SetupQueueFamilies()
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK_VOID(_vkGetPhysicalDeviceQueueFamilyProperties);

    _vkGetPhysicalDeviceQueueFamilyProperties(_physicalDevices[_physicalDevice], &_queueCount, nullptr);
    assert(_queueCount > 0);
    _queueFamilyProperties.resize(_queueCount);
    _vkGetPhysicalDeviceQueueFamilyProperties(_physicalDevices[_physicalDevice], &_queueCount, _queueFamilyProperties.data());
    assert(_queueCount > 0);

    VULKAN_LOG_INFO << "Vulkan Queue Families Info:";
    for (uint32_t i=0; i<_queueCount; i++)
    {
        std::stringstream ss;
        ss << "-- (" << i << ")";
        if (_queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            ss << " " << "GRAPHICS";
        }
        if (_queueFamilyProperties[i].queueFlags & VK_QUEUE_COMPUTE_BIT)
        {
            ss << " " << "COMPUTE";
        }
        if (_queueFamilyProperties[i].queueFlags & VK_QUEUE_TRANSFER_BIT)
        {
            ss << " " << "TRANSFER";
        }
        if (_queueFamilyProperties[i].queueFlags & VK_QUEUE_VIDEO_ENCODE_BIT_KHR)
        {
            ss << " " << "VIDEO_ENCODE";
        }
        if (_queueFamilyProperties[i].queueFlags & VK_QUEUE_VIDEO_DECODE_BIT_KHR)
        {
            ss << " " << "VIDEO_DECODE";
        }
        ss << " Count(" << _queueFamilyProperties[i].queueCount << ")";
        VULKAN_LOG_INFO << ss.str();
    }
    // Hint : 
    //       1 - 根据支持的拓展数量从多到少进行重排, 确认优先级
    //       2 - 选择 index
    //
    {
        std::vector<std::pair<uint32_t /* index */, VkQueueFamilyProperties /* properties */>> sortQueueFamilyProperties;
        for (uint32_t i=0; i<_queueCount; i++)
        {
            sortQueueFamilyProperties.push_back({i, _queueFamilyProperties[i]});
        }
        std::sort(sortQueueFamilyProperties.begin(), sortQueueFamilyProperties.end(), [](const std::pair<uint32_t /* index */, VkQueueFamilyProperties /* properties */>& lhs, const std::pair<uint32_t /* index */, VkQueueFamilyProperties /* properties */>& rhs) -> bool
        {
            uint32_t leftScore = 0;
            uint32_t rightScore = 0;
            // leftScore
            {
                if (lhs.second.queueFlags & VK_QUEUE_GRAPHICS_BIT)
                {
                    leftScore++;
                }
                if (lhs.second.queueFlags & VK_QUEUE_COMPUTE_BIT)
                {
                    leftScore++;
                }
                if (lhs.second.queueFlags & VK_QUEUE_TRANSFER_BIT)
                {
                    leftScore++;
                }
                if (lhs.second.queueFlags & VK_QUEUE_VIDEO_ENCODE_BIT_KHR)
                {
                    leftScore++;
                }
                if (lhs.second.queueFlags & VK_QUEUE_VIDEO_DECODE_BIT_KHR)
                {
                    leftScore++;
                }
            }
            // rightScore
            {
                if (rhs.second.queueFlags & VK_QUEUE_GRAPHICS_BIT)
                {
                    rightScore++;
                }
                if (rhs.second.queueFlags & VK_QUEUE_COMPUTE_BIT)
                {
                    rightScore++;
                }
                if (rhs.second.queueFlags & VK_QUEUE_TRANSFER_BIT)
                {
                    rightScore++;
                }
                if (rhs.second.queueFlags & VK_QUEUE_VIDEO_ENCODE_BIT_KHR)
                {
                    rightScore++;
                }
                if (rhs.second.queueFlags & VK_QUEUE_VIDEO_DECODE_BIT_KHR)
                {
                    rightScore++;
                }
            }
            return leftScore < rightScore;
        });
        VULKAN_LOG_INFO << "Choose Queue Family";
        for (auto& val : sortQueueFamilyProperties)
        {
            uint32_t index = val.first;
            VkQueueFamilyProperties& queueFamilyProperties = val.second;
            if (queueFamilyProperties.queueFlags & VK_QUEUE_GRAPHICS_BIT && _graphicsQueueIndex == -1)
            {
                _graphicsQueueIndex = index;
                VULKAN_LOG_INFO << "-- GraphicsQueueIndex " << index;
            }
            if (queueFamilyProperties.queueFlags & VK_QUEUE_COMPUTE_BIT && _computeQueueIndex == -1)
            {
                _computeQueueIndex = index;
                VULKAN_LOG_INFO << "-- ComputeQueueIndex " << index;
            }
            if (queueFamilyProperties.queueFlags & VK_QUEUE_TRANSFER_BIT && _transferQueueIndex == -1)
            {
                _transferQueueIndex = index;
                VULKAN_LOG_INFO << "-- TransferQueueIndex " << index;
            }
            if (queueFamilyProperties.queueFlags & VK_QUEUE_VIDEO_ENCODE_BIT_KHR && _videoEncodeQueueIndex == -1)
            {
                _videoEncodeQueueIndex = index;
                VULKAN_LOG_INFO << "-- VideoEncodeQueueIndex " << index;
            }
            if (queueFamilyProperties.queueFlags & VK_QUEUE_VIDEO_DECODE_BIT_KHR && _videoDecodeQueueIndex == -1)
            {
                _videoDecodeQueueIndex = index;
                VULKAN_LOG_INFO << "-- VideoDecodeQueueIndex " << index;
            }
        }
    }
}

int32_t VulkanDeviceImpl::GetQueueFamilyIndex(VkQueueFlagBits queueFlag)
{
    switch (queueFlag)
    {
        case VK_QUEUE_GRAPHICS_BIT: return _graphicsQueueIndex;
        case VK_QUEUE_COMPUTE_BIT: return _computeQueueIndex;
        case VK_QUEUE_TRANSFER_BIT: return _transferQueueIndex;
        case VK_QUEUE_VIDEO_ENCODE_BIT_KHR: return _videoEncodeQueueIndex;
        case VK_QUEUE_VIDEO_DECODE_BIT_KHR: return _videoDecodeQueueIndex;
        default:
            assert(false);
            return -1;
    }
}

VkQueueFamilyProperties VulkanDeviceImpl::GetQueueFamilyProperties(VkQueueFlagBits queueFlag)
{
    if (GetQueueFamilyIndex(queueFlag) > 0 && GetQueueFamilyIndex(queueFlag) < _queueFamilyProperties.size())
    {
        return _queueFamilyProperties[GetQueueFamilyIndex(queueFlag)];
    }
    else
    {
        return VkQueueFamilyProperties();
    }
}

const VkPhysicalDeviceExternalMemoryHostPropertiesEXT& VulkanDeviceImpl::GetVkPhysicalDeviceExternalMemoryHostProperties() 
{
    return _deviceExternalMemoryHostProperties;
}

const VkPhysicalDeviceCooperativeMatrixPropertiesKHR& VulkanDeviceImpl::GetVkPhysicalDeviceCooperativeMatrixProperties() 
{
    return _deviceCooperativeMatrixProperties;
}

const VkPhysicalDeviceSubgroupSizeControlProperties& VulkanDeviceImpl::GetVkPhysicalDeviceSubgroupSizeControlProperties() 
{
    return _deviceSubgroupSizeControlProperties;
}

const VkPhysicalDeviceDescriptorBufferPropertiesEXT& VulkanDeviceImpl::GetVkPhysicalDeviceDescriptorBufferProperties() 
{
    return _deviceDescriptorBufferProperties;
}

const VkPhysicalDeviceDriverProperties& VulkanDeviceImpl::GetVkPhysicalDeviceDriverProperties() 
{
    return _deviceDriverProperties;
}

const VkPhysicalDeviceMemoryProperties& VulkanDeviceImpl::GetVkPhysicalDeviceMemoryProperties() 
{
    return _deviceMemoryProperties;
}

const VkPhysicalDeviceProperties2& VulkanDeviceImpl::GetVkPhysicalDeviceProperties2() 
{
    return _deviceProperties2;
}

const VkPhysicalDeviceShaderAtomicFloatFeaturesEXT& VulkanDeviceImpl::GetVkPhysicalDeviceShaderAtomicFloatFeatures() 
{
    return _deviceShaderAtomicFloatFeaturesEXT;
}

const VkPhysicalDeviceFeatures2& VulkanDeviceImpl::GetVkPhysicalDeviceFeatures2() 
{
    return _deviceFeatures2;
}

const VulkanExtensions VulkanDeviceImpl::GetVulkanExtensions()
{
    return _extensionsLookup;
}

VkInstance VulkanDeviceImpl::GetInstance()
{
    return _instance;
}

VmaAllocator VulkanDeviceImpl::GetVmaAllocator()
{
    return _allocator;
}

void* VulkanDeviceImpl::GetInstanceFunc(const std::string& name)
{
    return (void*)_vkGetInstanceProcAddr(_instance, name.c_str());
}

void* VulkanDeviceImpl::GetDeviceFunc(const std::string& name)
{
    return (void*)_vkGetDeviceProcAddr(_device, name.c_str());
}

void VulkanDeviceImpl::ChooseDevice(int32_t physicalDevice)
{        
    if (!_vkGetPhysicalDeviceFeatures)
    {
        assert(false);
        return;
    }

    static std::vector<VkFormat> depthStencilFormats = 
    {
        VK_FORMAT_D24_UNORM_S8_UINT,
        VK_FORMAT_D32_SFLOAT_S8_UINT,
        VK_FORMAT_D16_UNORM_S8_UINT
    };

    assert(physicalDevice < _physicalDevices.size());
    _physicalDevice = physicalDevice;
    VULKAN_LOG_INFO << "-- Choose device " << _physicalDevice << " : " << _physicalDeviceProperties[_physicalDevice].properties.deviceName;
    SetupQueueFamilies();

    _deviceInfo.preferredDepthStencilFormat = VK_FORMAT_UNDEFINED;
	for (size_t i = 0; i < depthStencilFormats.size(); i++) 
    {
		VkFormatProperties props;
		_vkGetPhysicalDeviceFormatProperties(_physicalDevices[_physicalDevice], depthStencilFormats[i], &props);
		if (props.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT) 
        {
			_deviceInfo.preferredDepthStencilFormat = depthStencilFormats[i];
			break;
		}
	}
    VkFormatProperties preferredProps;
	_vkGetPhysicalDeviceFormatProperties(_physicalDevices[_physicalDevice], _deviceInfo.preferredDepthStencilFormat, &preferredProps);
	if ((preferredProps.optimalTilingFeatures & VK_FORMAT_FEATURE_BLIT_SRC_BIT) &&
		(preferredProps.optimalTilingFeatures & VK_FORMAT_FEATURE_BLIT_DST_BIT)) 
    {
		_deviceInfo.canBlitToPreferredDepthStencilFormat = true;
	}

    _vkGetPhysicalDeviceMemoryProperties(_physicalDevices[_physicalDevice], &_memoryProperties);
    for (int i = 0; i<_memoryProperties.memoryTypeCount; i++)
    {
        if (!_memoryProperties.memoryTypes[i].propertyFlags)
            continue;
        VULKAN_LOG_INFO << "-- (" << i << ") " << "Heap : " << _memoryProperties.memoryTypes[i].heapIndex << "; "
                     << "Flags : "
                     << ((_memoryProperties.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) ? "DEVICE_LOCAL " : std::string()) << ":"
                     << ((_memoryProperties.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) ? "HOST_VISIBLE " : std::string()) << ":"
                     << ((_memoryProperties.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_HOST_CACHED_BIT) ? "HOST_CACHED " : std::string()) << ":"
                     << ((_memoryProperties.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) ? "HOST_COHERENT " : std::string());
    }

    _vkGetPhysicalDeviceFeatures(_physicalDevices[_physicalDevice], &_deviceFeature.available.standard);

    GetDeviceLayerExtensionList();

    _deviceExtensionsEnabled.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
}

void VulkanDeviceImpl::GetDeviceLayerExtensionList()
{
    XXX_VULKAN_PHYSICAL_DEVICE_LOCK_VOID(_vkEnumerateDeviceExtensionProperties);
	VkResult res;
	do 
    {
		uint32_t deviceExtensionCount;
		res = _vkEnumerateDeviceExtensionProperties(_physicalDevices[_physicalDevice], nullptr, &deviceExtensionCount, nullptr);
		if (res != VK_SUCCESS)
			return;
		if (!deviceExtensionCount)
			return;
		_deviceExtensionProperties.resize(deviceExtensionCount);
		res = _vkEnumerateDeviceExtensionProperties(_physicalDevices[_physicalDevice], nullptr, &deviceExtensionCount, _deviceExtensionProperties.data());
	} while (res == VK_INCOMPLETE);
    {
        size_t index = 0;
        VULKAN_LOG_INFO << "Vulkan Device Layer Extension";
        for (auto& deviceExtensionPropertie : _deviceExtensionProperties)
        {
            VULKAN_LOG_INFO << "(" << index++ << ") " << deviceExtensionPropertie.extensionName;
        }
    }
}

bool VulkanDeviceImpl::EnableDeviceExtension(const std::string& extension)
{
    for (auto& deviceExtensionPropertie : _deviceExtensionProperties)
    {
        if (std::string(deviceExtensionPropertie.extensionName) == extension)
        {
            _deviceExtensionsEnabled.push_back(extension);
            return true;
        }
    }
    return false;
}

#undef XXX_VULKAN_DEVICE_LOCK
#undef XXX_VULKAN_DEVICE_LOCK_VOID
#undef XXX_VULKAN_PHYSICAL_DEVICE_LOCK
#undef XXX_VULKAN_PHYSICAL_DEVICE_LOCK_VOID

VulkanDevice::ptr VulkanDevice::Singleton()
{
    static VulkanDevice::ptr gInstace = nullptr;
    static std::atomic<bool> isInited(false);
    if (!isInited)
    {
        VulkanDeviceImpl::ptr device = std::make_shared<VulkanDeviceImpl>();
        if (device->Init())
        {
            gInstace = device;
        }
        isInited = true;
    }
    return gInstace;
}

std::string VkResultToStr(VkResult res)
{
    switch (res)
    {
        case VkResult::VK_SUCCESS: return "VK_SUCCESS";
        case VkResult::VK_NOT_READY: return "VK_NOT_READY";
        case VkResult::VK_TIMEOUT: return "VK_TIMEOUT";
        case VkResult::VK_EVENT_SET: return "VK_EVENT_SET";
        case VkResult::VK_EVENT_RESET: return "VK_EVENT_RESET";
        case VkResult::VK_INCOMPLETE: return "VK_INCOMPLETE";
        case VkResult::VK_ERROR_OUT_OF_HOST_MEMORY: return "VK_ERROR_OUT_OF_HOST_MEMORY";
        case VkResult::VK_ERROR_OUT_OF_DEVICE_MEMORY: return "VK_ERROR_OUT_OF_DEVICE_MEMORY";
        case VkResult::VK_ERROR_INITIALIZATION_FAILED: return "VK_ERROR_INITIALIZATION_FAILED";
        case VkResult::VK_ERROR_DEVICE_LOST: return "VK_ERROR_DEVICE_LOST";
        case VkResult::VK_ERROR_MEMORY_MAP_FAILED: return "VK_ERROR_MEMORY_MAP_FAILED";
        case VkResult::VK_ERROR_LAYER_NOT_PRESENT: return "VK_ERROR_LAYER_NOT_PRESENT";
        case VkResult::VK_ERROR_EXTENSION_NOT_PRESENT: return "VK_ERROR_EXTENSION_NOT_PRESENT";
        case VkResult::VK_ERROR_FEATURE_NOT_PRESENT: return "VK_ERROR_FEATURE_NOT_PRESENT";
        case VkResult::VK_ERROR_INCOMPATIBLE_DRIVER: return "VK_ERROR_INCOMPATIBLE_DRIVER";
        case VkResult::VK_ERROR_TOO_MANY_OBJECTS: return "VK_ERROR_TOO_MANY_OBJECTS";
        case VkResult::VK_ERROR_FORMAT_NOT_SUPPORTED: return "VK_ERROR_FORMAT_NOT_SUPPORTED";
        case VkResult::VK_ERROR_FRAGMENTED_POOL: return "VK_ERROR_FRAGMENTED_POOL";
        case VkResult::VK_ERROR_UNKNOWN: return "VK_ERROR_UNKNOWN";
        case VkResult::VK_ERROR_OUT_OF_POOL_MEMORY: return "VK_ERROR_OUT_OF_POOL_MEMORY";
        case VkResult::VK_ERROR_INVALID_EXTERNAL_HANDLE: return "VK_ERROR_INVALID_EXTERNAL_HANDLE";
        case VkResult::VK_ERROR_FRAGMENTATION: return "VK_ERROR_FRAGMENTATION";
        case VkResult::VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS: return "VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS";
        case VkResult::VK_PIPELINE_COMPILE_REQUIRED: return "VK_PIPELINE_COMPILE_REQUIRED";
        case VkResult::VK_ERROR_SURFACE_LOST_KHR: return "VK_ERROR_SURFACE_LOST_KHR";
        case VkResult::VK_ERROR_NATIVE_WINDOW_IN_USE_KHR: return "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR";
        case VkResult::VK_SUBOPTIMAL_KHR: return "VK_SUBOPTIMAL_KHR";
        case VkResult::VK_ERROR_OUT_OF_DATE_KHR: return "VK_ERROR_OUT_OF_DATE_KHR";
        case VkResult::VK_ERROR_INCOMPATIBLE_DISPLAY_KHR: return "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR";
        case VkResult::VK_ERROR_VALIDATION_FAILED_EXT: return "VK_ERROR_VALIDATION_FAILED_EXT";
        case VkResult::VK_ERROR_INVALID_SHADER_NV: return "VK_ERROR_INVALID_SHADER_NV";
        case VkResult::VK_ERROR_IMAGE_USAGE_NOT_SUPPORTED_KHR: return "VK_ERROR_IMAGE_USAGE_NOT_SUPPORTED_KHR";
        case VkResult::VK_ERROR_VIDEO_PICTURE_LAYOUT_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PICTURE_LAYOUT_NOT_SUPPORTED_KHR";
        case VkResult::VK_ERROR_VIDEO_PROFILE_OPERATION_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PROFILE_OPERATION_NOT_SUPPORTED_KHR";
        case VkResult::VK_ERROR_VIDEO_PROFILE_FORMAT_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PROFILE_FORMAT_NOT_SUPPORTED_KHR";
        case VkResult::VK_ERROR_VIDEO_PROFILE_CODEC_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_PROFILE_CODEC_NOT_SUPPORTED_KHR";
        case VkResult::VK_ERROR_VIDEO_STD_VERSION_NOT_SUPPORTED_KHR: return "VK_ERROR_VIDEO_STD_VERSION_NOT_SUPPORTED_KHR";
        case VkResult::VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT: return "VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT";
        case VkResult::VK_ERROR_NOT_PERMITTED_KHR: return "VK_ERROR_NOT_PERMITTED_KHR";
        case VkResult::VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT: return "VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT";
        case VkResult::VK_THREAD_IDLE_KHR: return "VK_THREAD_IDLE_KHR";
        case VkResult::VK_THREAD_DONE_KHR: return "VK_THREAD_DONE_KHR";
        case VkResult::VK_OPERATION_DEFERRED_KHR: return "VK_OPERATION_DEFERRED_KHR";
        case VkResult::VK_OPERATION_NOT_DEFERRED_KHR: return "VK_OPERATION_NOT_DEFERRED_KHR";
        case VkResult::VK_ERROR_COMPRESSION_EXHAUSTED_EXT: return "VK_ERROR_COMPRESSION_EXHAUSTED_EXT";
        case VkResult::VK_ERROR_INCOMPATIBLE_SHADER_BINARY_EXT: return "VK_ERROR_INCOMPATIBLE_SHADER_BINARY_EXT";
        default:
             return "VK_ERROR_UNKNOWN";
    }
}

} // namespace Mmp

#endif