#include "DmaHeapAllocateMethod.h"

#include <cassert>
#include <memory>

#if MMP_PLATFORM(LINUX)

#include <mutex>
#include <cstdint>
#include <unordered_map>

#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include <Poco/File.h>

#if defined(USE_DRM)
#include "DrmAllocateMethod.h"
#endif /* USE_DRM */

#include "LogMessage.h"

// 
// Note : This file is modified from osal/allocator/allocator_dma_heap.c
//        Date : 2024-03-21
//        Hash : 3b2784384468f13fb7659a47058024d990048cfa
// Copyright 2015 Rockchip Electronics Co. LTD
//

//
// See also :  https://source.android.google.cn/docs/core/architecture/kernel/dma-buf-heaps
// Note     :  按照计划, linux (v4l2 or drm) dma buf 和 android ION 应当会逐渐废弃
//

namespace Mmp
{

struct mmp_dma_heap_allocation_data 
{
    uint64_t len;
    uint32_t fd;
    uint32_t fd_flags;
    uint64_t heap_flags;
};

#define MMP_DMA_HEAP_IOC_MAGIC              'H'
#define MMP_DMA_HEAP_IOCTL_ALLOC            _IOWR(MMP_DMA_HEAP_IOC_MAGIC, 0x0, struct mmp_dma_heap_allocation_data)

enum RkDmaHeapFlag
{
    DMA_FLAG_NONE       = 0,
    DMA_HEAP_CMA        = (1 << 0),
    DMA_HEAP_CACHABLE   = (1 << 1),
    DMA_HEAP_DMA32      = (1 << 2),
    DMA_HEAP_NUM        = (DMA_HEAP_CMA | DMA_HEAP_CACHABLE | DMA_HEAP_DMA32)
};

constexpr char kRkHeapPath[] = "/dev/dma_heap/";

const std::string GetRkDmaHeapName(uint8_t flags)
{
    static std::unordered_map<uint8_t /* flags */, std::string /* heap name */> kLookup = 
    {
        {DMA_FLAG_NONE,                                       "system-uncached"},
        {DMA_HEAP_CMA,                                        "cma-uncached"},
        {DMA_HEAP_CACHABLE,                                   "system"},
        {DMA_HEAP_CMA | DMA_HEAP_CACHABLE,                    "cma"},
        {DMA_HEAP_DMA32,                                      "system-uncached-dma32"},
        {DMA_HEAP_DMA32 | DMA_HEAP_CMA,                       "cma-uncached"},
        {DMA_HEAP_DMA32 | DMA_HEAP_CACHABLE,                  "system-dma32"},
        {DMA_HEAP_CMA   | DMA_HEAP_CACHABLE | DMA_HEAP_DMA32, "cma"}
    };
    if (kLookup.count(flags))
    {
        return kLookup[flags];
    }
    else
    {
        assert(false);
        return "";
    }
}

static bool DrmIoCtl(int fd, int req, void* arg)
{
    int ret = -1;
    do 
    {
        ret = ioctl(fd, req, arg);
    } while (ret == -1 && (errno == EINTR || errno == EAGAIN));

    return ret >= 0;    
}

class DmaHeapDevice
{
public:
    using ptr = std::shared_ptr<DmaHeapDevice>;
public:
    DmaHeapDevice();
    ~DmaHeapDevice();
public:
    static DmaHeapDevice::ptr DmaHeapDeviceSingleton();
public:
    bool  Allocate(size_t len, int& fd, uint8_t flags);
    void  DeAllocate(int fd);
    void* Map(int fd, size_t len);
    void  UnMap(void* ptr, size_t len);
private:
    int  Open(uint8_t flags);
    void Close(uint8_t flags);
private:
    std::mutex _mtx;
    int _dmaHeapFds[DMA_HEAP_NUM] = {0};
};

DmaHeapDevice::ptr DmaHeapDevice::DmaHeapDeviceSingleton()
{
    static DmaHeapDevice::ptr gDevice = std::make_shared<DmaHeapDevice>();
    return gDevice;
}

DmaHeapDevice::DmaHeapDevice()
{
    
}

DmaHeapDevice::~DmaHeapDevice()
{
    for (uint8_t flags = 0; flags<DMA_HEAP_NUM; flags++)
    {
        close(flags);
    }
}

bool DmaHeapDevice::Allocate(size_t len, int& fd, uint8_t flags)
{
    bool ret = false;
    
    mmp_dma_heap_allocation_data data = {};
    {
        data.len = len;
        data.fd_flags = O_RDWR | O_CLOEXEC;
        data.heap_flags = 0;
    }
    ret = DrmIoCtl(Open(flags), MMP_DMA_HEAP_IOCTL_ALLOC, &data);
    if (ret)
    {
        fd = data.fd;
    }
    return ret;
}

void DmaHeapDevice::DeAllocate(int fd)
{
    assert(fd >= 0);
    close(fd);
}

void* DmaHeapDevice::Map(int fd, size_t len)
{
    int flags = PROT_READ;
    if (fcntl(fd, F_GETFL) & O_RDWR)
    {
        flags |= PROT_WRITE;
    }
    return mmap(nullptr, len, flags, MAP_SHARED, fd, 0);
}

void DmaHeapDevice::UnMap(void* ptr, size_t len)
{
    munmap(ptr, len);
}

int DmaHeapDevice::Open(uint8_t flags)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (_dmaHeapFds[flags] <= 0)
    {
        std::string devPath = kRkHeapPath + GetRkDmaHeapName(flags);
        if (Poco::File(devPath).exists())
        {
            _dmaHeapFds[flags] = open(devPath.c_str(), O_RDONLY | O_CLOEXEC);
            if (_dmaHeapFds[flags] <= 0)
            {
                MMP_LOG_ERROR << "open " << devPath << " fail, error is: " << strerror(errno);
                return -1;
            }
        }
        else
        {
            return -1;
        }
    }
    return _dmaHeapFds[flags];
}

void DmaHeapDevice::Close(uint8_t flags)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (_dmaHeapFds[flags] > 0)
    {
        close(_dmaHeapFds[flags]);
        _dmaHeapFds[flags] = 0;
    }
}

} // namespace Mmp

namespace Mmp
{

DmaHeapAllocateMethod::DmaHeapAllocateMethod()
{
    _fd = -1;
    _data = nullptr;
    _len = 0;
    _flags = 0;
    _syncOperation = std::make_shared<Promise<void>>([]{});
}

DmaHeapAllocateMethod::~DmaHeapAllocateMethod()
{
    if (_data)
    {
        DmaHeapDevice::DmaHeapDeviceSingleton()->UnMap(_data, _len);
    }
    if (_fd >= 0)
    {
        DmaHeapDevice::DmaHeapDeviceSingleton()->DeAllocate(_fd);
    }
}

void* DmaHeapAllocateMethod::Malloc(size_t size)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (_drmAllocate)
    {
        return _drmAllocate->Malloc(size);
    }
    if (_fd == -1) // Hint : 可重入
    {
        if (!DmaHeapDevice::DmaHeapDeviceSingleton()->Allocate(size, _fd, _flags))
        {
#if defined(USE_DRM)
            // Hint : 失败情况下回滚到 DrmAllocateMethod
            _drmAllocate = std::make_shared<DrmAllocateMethod>();
            if (_drmAllocate)
            {
                return _drmAllocate->Malloc(size);
            }
#endif /* USE_DRM */
            assert(false);
            goto END;
        }
    }
    _len = size;
    return _data;
/* END1: */
    DmaHeapDevice::DmaHeapDeviceSingleton()->DeAllocate(_fd);
END:
    _fd = -1;
    _len = 0;
    return nullptr;
}

void* DmaHeapAllocateMethod::Resize(void* data, size_t size)
{
    std::lock_guard<std::mutex> lock(_mtx);
    // Hint : not support resize
    assert(false);
    return _data;
}

void* DmaHeapAllocateMethod::GetAddress(uint64_t offset)
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (_drmAllocate)
    {
        return _drmAllocate->GetAddress(0);
    }
    else if (!_data)
    {
        _data = DmaHeapDevice::DmaHeapDeviceSingleton()->Map(_fd, _len);
    }
    return _data ? (uint8_t*)_data + offset : nullptr;
}

const std::string& DmaHeapAllocateMethod::Tag()
{
    static const std::string tag = "DmaHeapAllocateMethod";
    if (_drmAllocate)
    {
        return _drmAllocate->Tag();
    }
    else
    {
        return tag;   
    }
}

void DmaHeapAllocateMethod::Map()
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (_drmAllocate)
    {
        _drmAllocate->Map();
    }
    else if (!_data)
    {
        _data = DmaHeapDevice::DmaHeapDeviceSingleton()->Map(_fd, _len);
        assert(_data);
    }
}

void DmaHeapAllocateMethod::UnMap()
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (_drmAllocate)
    {
        _drmAllocate->UnMap();
    }
    else if (_data)
    {
        DmaHeapDevice::DmaHeapDeviceSingleton()->UnMap(_data, _len);
        _data = nullptr;
    }
}

int DmaHeapAllocateMethod::GetFd()
{
    std::lock_guard<std::mutex> lock(_mtx);
    if (_drmAllocate)
    {
        return _drmAllocate->GetFd();
    }
    else
    {
        return _fd;
    }
}

uint64_t DmaHeapAllocateMethod::GetFlags()
{
    std::lock_guard<std::mutex> lock(_mtx);
    return _flags;
}

} // namespace Mmp

#endif