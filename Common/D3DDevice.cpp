#include "D3DDevice.h"

#if defined(USE_D3D)

#include <vector>
#include <cassert>

#include <Poco/SharedLibrary.h>
#include <Poco/UnicodeConverter.h>

#include "LogMessage.h"

namespace Mmp 
{

static Poco::SharedLibrary gD3D12Loader;
static Poco::SharedLibrary gDXGILoader;

typedef HRESULT(WINAPI *PFN_CREATE_DXGI_FACTORY2)(UINT Flags, REFIID riid, void **ppFactory);

class D3DDeviceImpl : public D3DDevice
{
public:
    using ptr = std::shared_ptr<D3DDeviceImpl>;
public:
    D3DDeviceImpl();
    ~D3DDeviceImpl();
public:
    bool Init();
public:
    HRESULT QueryInterface(REFIID riid, void** ppvObject) override;
    HRESULT CreateCommandQueue(const D3D12_COMMAND_QUEUE_DESC *pDesc, REFIID riid, void **ppCommandQueue) override;
    HRESULT CreateCommandList(UINT nodeMask, D3D12_COMMAND_LIST_TYPE type, ID3D12CommandAllocator *pCommandAllocator, ID3D12PipelineState *pInitialState, REFIID riid, void **ppCommandList) override;
    HRESULT CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE type, REFIID riid, void **ppCommandAllocator) override;
    HRESULT CreateCommittedResource(const D3D12_HEAP_PROPERTIES *pHeapProperties, D3D12_HEAP_FLAGS HeapFlags, const D3D12_RESOURCE_DESC *pDesc, D3D12_RESOURCE_STATES InitialResourceState, const D3D12_CLEAR_VALUE *pOptimizedClearValue, REFIID riidResource, void **ppvResource) override;
    HRESULT CreateFence(UINT64 InitialValue, D3D12_FENCE_FLAGS Flags, REFIID riid, void **ppFence) override;
    HRESULT CreateGraphicsPipelineState(const D3D12_GRAPHICS_PIPELINE_STATE_DESC *pDesc, REFIID riid, void **ppPipelineState) override;
    void CreateRenderTargetView(ID3D12Resource *pResource, const D3D12_RENDER_TARGET_VIEW_DESC *pDesc, D3D12_CPU_DESCRIPTOR_HANDLE DestDescriptor) override;
    void CreateDepthStencilView(ID3D12Resource *pResource, const D3D12_DEPTH_STENCIL_VIEW_DESC *pDesc, D3D12_CPU_DESCRIPTOR_HANDLE DestDescriptor) override;
public:
    HRESULT CheckFeatureSupport(D3D12_FEATURE_VIDEO FeatureVideo, void *pFeatureSupportData, UINT FeatureSupportDataSize) override;
    HRESULT CreateVideoDecoder(const D3D12_VIDEO_DECODER_DESC *pDesc, REFIID riid, void **ppVideoDecoder) override;
    HRESULT CreateVideoDecoderHeap(const D3D12_VIDEO_DECODER_HEAP_DESC *pVideoDecoderHeapDesc, REFIID riid, void **ppVideoDecoderHeap) override;
private:
    bool LoadSymbol();
    bool CreateID3D12Device();
    bool CreateID3D12VideoDevice();
private:
    std::mutex _mtx;
    bool _debugMode;
private:
    std::mutex _deviceMtx;
    ID3D12Device* _device;
private:
    std::mutex _videoDeviceMtx;
    ID3D12VideoDevice* _videoDevice;
private:
    PFN_D3D12_CREATE_DEVICE _D3D12CreateDevice;
    PFN_CREATE_DXGI_FACTORY2 _CreateDXGIFactory2;
    PFN_D3D12_GET_DEBUG_INTERFACE _D3D12GetDebugInterface;
};

#define XXX_D3DVA_DEVICE_LOCK()          std::lock_guard<std::mutex> lock(_deviceMtx);\
                                         if (!_device) return HRESULT(-1);

#define XXX_D3DVA_DEVICE_LOCK_VOID()      std::lock_guard<std::mutex> lock(_deviceMtx);\
                                          if (!_device) return;

HRESULT D3DDeviceImpl::QueryInterface(REFIID riid, void** ppvObject)
{
    XXX_D3DVA_DEVICE_LOCK();
    return _device->QueryInterface(riid, ppvObject);
}

HRESULT D3DDeviceImpl::CreateCommandQueue(const D3D12_COMMAND_QUEUE_DESC *pDesc, REFIID riid, void **ppCommandQueue)
{
    XXX_D3DVA_DEVICE_LOCK();
    return _device->CreateCommandQueue(pDesc, riid, ppCommandQueue);
}

HRESULT D3DDeviceImpl::CreateCommandList(UINT nodeMask, D3D12_COMMAND_LIST_TYPE type, ID3D12CommandAllocator *pCommandAllocator, ID3D12PipelineState *pInitialState, REFIID riid, void **ppCommandList)
{
    XXX_D3DVA_DEVICE_LOCK();
    return _device->CreateCommandList(nodeMask, type, pCommandAllocator, pInitialState, riid, ppCommandList);
}

HRESULT D3DDeviceImpl::CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE type, REFIID riid, void **ppCommandAllocator)
{
    XXX_D3DVA_DEVICE_LOCK();
    return _device->CreateCommandAllocator(type, riid, ppCommandAllocator);
}

HRESULT D3DDeviceImpl::CreateCommittedResource(const D3D12_HEAP_PROPERTIES *pHeapProperties, D3D12_HEAP_FLAGS HeapFlags, const D3D12_RESOURCE_DESC *pDesc, D3D12_RESOURCE_STATES InitialResourceState, const D3D12_CLEAR_VALUE *pOptimizedClearValue, REFIID riidResource, void **ppvResource)
{
    XXX_D3DVA_DEVICE_LOCK();
    return _device->CreateCommittedResource(pHeapProperties, HeapFlags, pDesc, InitialResourceState, pOptimizedClearValue, riidResource, ppvResource);
}

HRESULT D3DDeviceImpl::CreateFence(UINT64 InitialValue, D3D12_FENCE_FLAGS Flags, REFIID riid, void **ppFence)
{
    XXX_D3DVA_DEVICE_LOCK();
    return _device->CreateFence(InitialValue, Flags, riid, ppFence);
}

HRESULT D3DDeviceImpl::CreateGraphicsPipelineState(const D3D12_GRAPHICS_PIPELINE_STATE_DESC *pDesc, REFIID riid, void **ppPipelineState)
{
    XXX_D3DVA_DEVICE_LOCK();
    return _device->CreateGraphicsPipelineState(pDesc, riid, ppPipelineState);
}

void D3DDeviceImpl::CreateRenderTargetView(ID3D12Resource *pResource, const D3D12_RENDER_TARGET_VIEW_DESC *pDesc, D3D12_CPU_DESCRIPTOR_HANDLE DestDescriptor)
{
    XXX_D3DVA_DEVICE_LOCK_VOID();
    _device->CreateRenderTargetView(pResource, pDesc, DestDescriptor);
}

void D3DDeviceImpl::CreateDepthStencilView(ID3D12Resource *pResource, const D3D12_DEPTH_STENCIL_VIEW_DESC *pDesc, D3D12_CPU_DESCRIPTOR_HANDLE DestDescriptor)
{
    XXX_D3DVA_DEVICE_LOCK_VOID();
    _device->CreateDepthStencilView(pResource, pDesc, DestDescriptor);
}

#undef XXX_D3DVA_DEVICE_LOCK
#undef XXX_D3DVA_DEVICE_LOCK_VOID

#define XXX_D3DVA_VIDEO_DEVICE_LOCK()   std::lock_guard<std::mutex> lock(_videoDeviceMtx);\
                                        if (!_videoDevice) return HRESULT(-1);

HRESULT D3DDeviceImpl::CheckFeatureSupport(D3D12_FEATURE_VIDEO FeatureVideo, void *pFeatureSupportData, UINT FeatureSupportDataSize)
{
    XXX_D3DVA_VIDEO_DEVICE_LOCK();
    return _videoDevice->CheckFeatureSupport(FeatureVideo, pFeatureSupportData, FeatureSupportDataSize);
}

HRESULT D3DDeviceImpl::CreateVideoDecoder(const D3D12_VIDEO_DECODER_DESC *pDesc, REFIID riid, void **ppVideoDecoder)
{
    XXX_D3DVA_VIDEO_DEVICE_LOCK();
    return _videoDevice->CreateVideoDecoder(pDesc, riid, ppVideoDecoder);
}

HRESULT D3DDeviceImpl::CreateVideoDecoderHeap(const D3D12_VIDEO_DECODER_HEAP_DESC *pVideoDecoderHeapDesc, REFIID riid, void **ppVideoDecoderHeap)
{
    XXX_D3DVA_VIDEO_DEVICE_LOCK();
    return _videoDevice->CreateVideoDecoderHeap(pVideoDecoderHeapDesc, riid, ppVideoDecoderHeap);
}

#undef XXX_D3DVA_VIDEO_DEVICE_LOCK

D3DDeviceImpl::D3DDeviceImpl()
{
    _debugMode = true;
}

D3DDeviceImpl::~D3DDeviceImpl()
{

}

bool D3DDeviceImpl::Init()
{
    MMP_LOG_INFO << "Init D3DDevice";
    if (!LoadSymbol())
    {
        return false;
    }
    if (!CreateID3D12Device())
    {
        return false;
    }
    if (!CreateID3D12VideoDevice())
    {
        return false;
    }
    return true;
}

bool D3DDeviceImpl::LoadSymbol()
{
    MMP_LOG_INFO << "-- LoadSymbol";
    {
        MMP_LOG_INFO << "Load D3D12";
        MMP_LOG_INFO << "Load D3D12 related runtime depend";
        try 
        {
            if (!gD3D12Loader.isLoaded())
            {
                gD3D12Loader.load("d3d12.dll");
            }
            MMP_LOG_INFO << "-- Load d3d12.dll successfully";
        }
        catch (const Poco::LibraryLoadException& ex)
        {
            MMP_LOG_ERROR << "-- Load d3d12.dll fail, error msg is: " << ex.message();
            return false;
        }
        if (gD3D12Loader.hasSymbol("D3D12CreateDevice"))
        {
            _D3D12CreateDevice = (PFN_D3D12_CREATE_DEVICE)gD3D12Loader.getSymbol("D3D12CreateDevice");
            MMP_LOG_INFO << "-- Load symbol D3D12CreateDevice successfully";
        }
        else
        {
            MMP_LOG_ERROR << "-- Load symbol D3D12CreateDevice fail";
            assert(false);
            return false;
        }
        if (gD3D12Loader.hasSymbol("D3D12GetDebugInterface"))
        {
            _D3D12GetDebugInterface = (PFN_D3D12_GET_DEBUG_INTERFACE)gD3D12Loader.getSymbol("D3D12GetDebugInterface");
            MMP_LOG_INFO << "-- Load symbol D3D12GetDebugInterface successfully";
        }
        else
        {
            MMP_LOG_WARN << "-- Load symbol D3D12GetDebugInterface fail";
        }
    }
    {
        MMP_LOG_INFO << "Load DXGI";
        MMP_LOG_INFO << "Load DXGI related runtime depend";
        try 
        {
            if (!gDXGILoader.isLoaded())
            {
                gDXGILoader.load("dxgi.dll");
            }
            MMP_LOG_INFO << "-- Load dxgi.dll successfully";
        }
        catch (const Poco::LibraryLoadException& ex)
        {
            MMP_LOG_ERROR << "-- Load dxgi.dll fail, error msg is: " << ex.message();
            return false;
        }
        if (gDXGILoader.hasSymbol("CreateDXGIFactory2"))
        {
            _CreateDXGIFactory2 = (PFN_CREATE_DXGI_FACTORY2)gDXGILoader.getSymbol("CreateDXGIFactory2");
            MMP_LOG_INFO << "-- Load symbol CreateDXGIFactory2 successfully";
        }
        else
        {
            MMP_LOG_WARN << "-- Load symbol CreateDXGIFactory2 fail";
            assert(false);
            return false;
        }
    }
    return true;
}

bool D3DDeviceImpl::CreateID3D12Device()
{
    MMP_LOG_INFO << "-- CreateID3D12Device";
    UINT createFlags = 0;
    HRESULT hr = 0;
    if (_debugMode)
    {
        ID3D12Debug* debug = nullptr;
        if (D3DVA_SUCCEEDED(_D3D12GetDebugInterface(IID_ID3D12Debug, (void**)&debug)))
        {
            createFlags |= DXGI_CREATE_FACTORY_DEBUG;
            debug->EnableDebugLayer();
            D3DVA_OBJECT_RELEASE(debug);
        }
    }
    std::vector<IDXGIAdapter*> adapters;
    {
        IDXGIFactory2* DXGIFactory = nullptr;
        hr = _CreateDXGIFactory2(createFlags, IID_IDXGIFactory2, (void**)&DXGIFactory);
        if (D3DVA_SUCCEEDED(hr))
        {
            IDXGIAdapter* adapter = nullptr;
            for (UINT i = 0; DXGIFactory->EnumAdapters(i, &adapter) != DXGI_ERROR_NOT_FOUND; i++)
            {
                DXGI_ADAPTER_DESC desc = {};
                adapter->GetDesc(&desc);
                MMP_LOG_INFO << "DXGIAdapter (" << i << ")";
                {
                    std::string description;
                    Poco::UnicodeConverter::convert(desc.Description, description);
                    MMP_LOG_INFO << "-- Description is: " << description;
                }
                MMP_LOG_INFO << "-- Vendor id is: " << desc.VendorId;
                MMP_LOG_INFO << "-- Device id is: " << desc.DeviceId;
                MMP_LOG_INFO << "-- Sub System id is: " << desc.SubSysId;
                adapters.push_back(adapter);
            }
            if (adapters.empty())
            {
                DXGIFactory->Release();
                assert(false);
                return false;
            }
        }
        else
        {
            MMP_LOG_ERROR << "CreateDXGIFactory2 fail";
            assert(false);
            return false;
        }
    }
    if (adapters.empty())
    {
        assert(false);
        return false;
    }
    {
        IDXGIAdapter* chooseAdapter = adapters.front();
        hr =  _D3D12CreateDevice((IUnknown*)chooseAdapter, D3D_FEATURE_LEVEL_12_0, IID_ID3D12Device, (void **)&_device);
        if (D3DVA_FAILED(hr))
        {
            MMP_LOG_ERROR << "D3D12CreateDevice fail";
            assert(false);
            return false;
        }
    }
    return true;
}

bool D3DDeviceImpl::CreateID3D12VideoDevice()
{
    MMP_LOG_INFO << "-- CreateID3D12VideoDevice";
    HRESULT hr = QueryInterface(IID_ID3D12VideoDevice, (void**)&_videoDevice);
    if (D3DVA_FAILED(hr))
    {
        MMP_LOG_ERROR << "ID3D12Device::QueryInterface fail";
        assert(false);
        return false;
    }
    return true;
}

D3DDevice::ptr D3DDevice::Singleton()
{
    static D3DDevice::ptr gInstace = nullptr;
    static std::atomic<bool> isInited(false);
    if (!isInited)
    {
        D3DDeviceImpl::ptr device = std::make_shared<D3DDeviceImpl>();
        if (device->Init())
        {
            gInstace = device;
        }
        isInited = true;
    }
    return gInstace;
}

} // namespace Mmp

#endif /* USE_D3D */