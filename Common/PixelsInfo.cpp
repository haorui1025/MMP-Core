#include "PixelsInfo.h"

namespace Mmp
{

PixelsInfo::PixelsInfo()
{
    width    = -1;
    height   = -1;
    horStride = -1;
    virStride = -1;
    bitdepth = 0;
    format   = PixelFormat::RGB888;
    quantRange = QuantRange::UNSPECIFIED;
    colorGamut = ColorGamut::ColorGamut_UNSPECIFIED;
    dynamicRange = DynamicRange::DynamicRange_UNSPECIFIED;
}

PixelsInfo::PixelsInfo(int32_t width, int32_t height, int32_t bitdepth, PixelFormat format)
{
    this->width    = width;
    this->height   = height;
    this->bitdepth = bitdepth;;
    this->format   = format;
    this->horStride = width;
    this->virStride = height;
    if (MMP_PIXFORMAT_YUV(format))
    {
        quantRange = QuantRange::FULL;
        colorGamut = ColorGamut::ColorGamut_BT601;
        dynamicRange = DynamicRange::DynamicRange_SDR;
    }
    else
    {
        quantRange = QuantRange::UNSPECIFIED;
        colorGamut = ColorGamut::ColorGamut_UNSPECIFIED;
        dynamicRange = DynamicRange::DynamicRange_UNSPECIFIED;
    }
}

bool operator==(const PixelsInfo& left, const PixelsInfo& right)
{
    if (left.width == right.width &&
        left.height == right.height &&
        left.bitdepth == right.bitdepth &&
        left.format == right.format &&
        left.horStride == right.horStride && 
        left.virStride == right.virStride &&
        left.quantRange == right.quantRange &&
        left.colorGamut == right.colorGamut &&
        left.dynamicRange == right.dynamicRange
    )
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool operator!=(const PixelsInfo& left, const PixelsInfo& right)
{
    return left == right ? false : true;
}

} // namespace Mmp