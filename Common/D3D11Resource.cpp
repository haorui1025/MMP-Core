#include "D3D11Resource.h"

#if defined(USE_D3D)

namespace Mmp 
{

ID3D11Texture::ID3D11Texture()
{
    texture = nullptr;
}

ID3D11Texture::~ID3D11Texture()
{
    D3D11_OBJECT_RELEASE(texture);
}

} // namespace Mmp


#endif