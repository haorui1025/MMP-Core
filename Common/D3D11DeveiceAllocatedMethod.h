//
// D3D11DeveiceAllocatedMethod.h
//
// Library: Common
// Package: Codec
// Module:  D3D11
// 
//

#pragma once

#if defined(USE_D3D)

#include "AbstractAllocateMethod.h"
#include "D3D11Resource.h"

namespace Mmp
{

class D3D11DeviceAllocateMethod : public AbstractAllocateMethod
{
public:
    using ptr = std::shared_ptr<D3D11DeviceAllocateMethod>;
public:
    D3D11DeviceAllocateMethod() = default;
    ~D3D11DeviceAllocateMethod() = default;
public:
    void* Malloc(size_t size) override;
    void* Resize(void* data, size_t size) override;
    const std::string& Tag() override; 
public:
    ID3D11Texture::ptr srcFrame;
};

} // namespace Mmp

#endif