//
// D3D11Resource.h
//
// Library: Common
// Package: D3D
// Module:  Resource
// 

#pragma once

#if defined(USE_D3D)

#include <memory>

#include "D3D11Device.h"

namespace Mmp 
{

class ID3D11Texture
{
public:
    using ptr = std::shared_ptr<ID3D11Texture>;
public:
    ID3D11Texture();
    virtual ~ID3D11Texture();
public:
    ID3D11Texture2D* texture;
};

} // namespace Mmp

#endif