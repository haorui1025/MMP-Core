//
// D3DDevice.h
//
// Library: Common
// Package: D3D
// Module:  Device
// 

#pragma once

#if defined(USE_D3D)

#include "Common.h"

#include <mutex>
#include <memory>

#include <initguid.h>
#include <dxgi1_5.h>
#include <dxva.h>
#include <d3d12.h>
#include <d3d12video.h>

#define D3DVA_SUCCEEDED(hr) (((HRESULT)(hr)) >= 0)
#define D3DVA_FAILED(hr)    (((HRESULT)(hr)) < 0)

#define D3DVA_OBJECT_RELEASE(object)                    \
    do {                                                \
        if (object)                                     \
        {                                               \
            object->Release();                          \
        }                                               \
    } while (0)

namespace Mmp 
{

class D3DDevice
{
public:
    using ptr = std::shared_ptr<D3DDevice>;
public:
    D3DDevice() = default;
    virtual ~D3DDevice() = default;
public:
    static D3DDevice::ptr Singleton();
public: /* ID3D12Device */
    virtual HRESULT QueryInterface(REFIID riid, void** ppvObject) = 0;
    virtual HRESULT CreateCommandQueue(const D3D12_COMMAND_QUEUE_DESC *pDesc, REFIID riid, void **ppCommandQueue) = 0;
    virtual HRESULT CreateCommandList(UINT nodeMask, D3D12_COMMAND_LIST_TYPE type, ID3D12CommandAllocator *pCommandAllocator, ID3D12PipelineState *pInitialState, REFIID riid, void **ppCommandList) = 0;
    virtual HRESULT CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE type, REFIID riid, void **ppCommandAllocator) = 0;
    virtual HRESULT CreateCommittedResource(const D3D12_HEAP_PROPERTIES *pHeapProperties, D3D12_HEAP_FLAGS HeapFlags, const D3D12_RESOURCE_DESC *pDesc, D3D12_RESOURCE_STATES InitialResourceState, const D3D12_CLEAR_VALUE *pOptimizedClearValue, REFIID riidResource, void **ppvResource) = 0;
    virtual HRESULT CreateFence(UINT64 InitialValue, D3D12_FENCE_FLAGS Flags, REFIID riid, void **ppFence) = 0;
    virtual HRESULT CreateGraphicsPipelineState(const D3D12_GRAPHICS_PIPELINE_STATE_DESC *pDesc, REFIID riid, void **ppPipelineState) = 0;
    virtual void CreateRenderTargetView(ID3D12Resource *pResource, const D3D12_RENDER_TARGET_VIEW_DESC *pDesc, D3D12_CPU_DESCRIPTOR_HANDLE DestDescriptor) = 0;
    virtual void CreateDepthStencilView(ID3D12Resource *pResource, const D3D12_DEPTH_STENCIL_VIEW_DESC *pDesc, D3D12_CPU_DESCRIPTOR_HANDLE DestDescriptor) = 0;
public: /* ID3D12VideoDevice */
    virtual HRESULT CheckFeatureSupport(D3D12_FEATURE_VIDEO FeatureVideo, void *pFeatureSupportData, UINT FeatureSupportDataSize) = 0;
    virtual HRESULT CreateVideoDecoder(const D3D12_VIDEO_DECODER_DESC *pDesc, REFIID riid, void **ppVideoDecoder) = 0;
    virtual HRESULT CreateVideoDecoderHeap(const D3D12_VIDEO_DECODER_HEAP_DESC *pVideoDecoderHeapDesc, REFIID riid, void **ppVideoDecoderHeap) = 0;
};

} // namespace Mmp

#endif