//
// PixelFormat.h
//
// Library: Common
// Package: Media
// Module:  Format
// 

#pragma once

#include <iostream>
#include <string>

namespace Mmp
{

/**
 * @brief  像素格式
 */
enum class PixelFormat
{
    YUV420P = 0,     /* YYYY... UUUU... VVVV*/
    NV12,            /* YYYY... UV...*/ 
    RGB565 = 1 << 5u,
    ARGB1555,
    RGBA5551,
    RGB888,
    ARGB8888,
    RGBA8888,
    BGR565,
    ABGR1555,
    BRGA5551,
    BGR888,
    BGRA8888,
    ABGR8888,
    HSV888 = 1 << 10u,
};
float BytePerPixel(PixelFormat format);
#define MMP_PIXFORMAT_YUV(foramt) ((uint64_t)format < (1 << 5u))

/**
 * @brief 量化范围
 */
enum class QuantRange
{
    UNSPECIFIED = 0,
    LIMIT,             // (16~235)
    FULL,              // (0~255)
};

/**
 * @brief   色域空间
 * @sa      色域 : https://blog.csdn.net/cc289123557/article/details/137405717
 */
enum ColorGamut : uint64_t
{
    ColorGamut_UNSPECIFIED = 0,
    ColorGamut_BT601       = 1 << 4u,
    ColorGamut_BT709       = 1 << 8u,
    ColorGamut_BT2020      = 1 << 12u
};
#define MMP_COLOR_GAMUT(gamut, mask)     ((gamut & ColorGamut::mask) == mask)
#define MMP_COLOR_GAMUT_BT601(gamut)     MMP_COLOR_GAMUT(gamut, ColorGamut_BT601)
#define MMP_COLOR_GAMUT_BT709(gamut)     MMP_COLOR_GAMUT(gamut, ColorGamut_BT709)
#define MMP_COLOR_GAMUT_BT2020(gamut)    MMP_COLOR_GAMUT(gamut, ColorGamut_BT2020)

/**
 * @brief 动态对比范围
 * @sa    HDR 技术简介 : https://blog.csdn.net/chenwang1824/article/details/131353235
 *        影视飓风为什么不常做HDR : https://www.bilibili.com/video/BV1hZ421g7xC
 */
enum DynamicRange : uint64_t
{
    DynamicRange_UNSPECIFIED = 0,
    DynamicRange_SDR         = 1 << 8u,                        // Standard Dynamic Range
    DynamicRange_SDR10       = DynamicRange_SDR + 1u,
    DynamicRange_HDR         = 1 << 16u,                       // High Dynamic Range
    DynamicRange_HLG         = DynamicRange_HDR + 1u           // Hybrid Log-Gamma
};

#define MMP_DYNAMIC_RANGE(range, mask)  ((range & DynamicRange::mask) == mask)
#define MMP_DYNAMIC_RANGE_SDR(range)    MMP_DYNAMIC_RANGE(range, DynamicRange_SDR)
#define MMP_DYNAMIC_RANGE_HDR(range)    MMP_DYNAMIC_RANGE(range, DynamicRange_HDR)

const std::string PixelFormatToStr(PixelFormat format);
extern std::ostream& operator<<(std::ostream& os, PixelFormat format);

} // namespace Mmp