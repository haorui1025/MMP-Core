#include "D3D11DeveiceAllocatedMethod.h"

#if defined(USE_D3D)

namespace Mmp
{

void* D3D11DeviceAllocateMethod::Malloc(size_t size)
{
    // Hint : 延迟获取, 等到 GetAddress 再进行尝试
    return nullptr;
}

void* D3D11DeviceAllocateMethod::Resize(void* data, size_t size)
{
    assert(false);
    return nullptr;
}

const std::string& D3D11DeviceAllocateMethod::Tag()
{
    static std::string tag = "D3D11DeviceAllocateMethod";
    return tag;
}

} // namespace Mmp

#endif