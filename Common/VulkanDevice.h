//
// VulkanDevice.h
//
// Library: Common
// Package: Vulkan
// Module:  Device
// 

#pragma once

#if defined(USE_VULKAN)

#include "Common.h"
#include "LogMessage.h"

#define VK_ENABLE_BETA_EXTENSIONS 1

#include "vulkan/vulkan_core.h"

#define VMA_STATIC_VULKAN_FUNCTIONS 0
#define VMA_DYNAMIC_VULKAN_FUNCTIONS 1
#define VMA_DEBUG_GLOBAL_MUTEX 1

#include "vk_mem_alloc.h"

#define VULKAN_LOG_TRACE   MMP_MLOG_TRACE("VULKAN")    
#define VULKAN_LOG_DEBUG   MMP_MLOG_DEBUG("VULKAN")    
#define VULKAN_LOG_INFO    MMP_MLOG_INFO("VULKAN")     
#define VULKAN_LOG_WARN    MMP_MLOG_WARN("VULKAN")     
#define VULKAN_LOG_ERROR   MMP_MLOG_ERROR("VULKAN")    
#define VULKAN_LOG_FATAL   MMP_MLOG_FATAL("VULKAN")   

#define VULKAN_SUCCEEDED(vkRes) (vkRes == VK_SUCCESS)
#define VULKAN_FAILED(vkRes)    (vkRes != VK_SUCCESS)

namespace Mmp 
{

struct VulkanExtensions 
{
    bool EXT_debug_utils = false;
    bool KHR_MAINTENANCE1_EXTENSION_NAME = false;
    bool KHR_MAINTENANCE2_EXTENSION_NAME = false;
    bool KHR_MAINTENANCE3_EXTENSION_NAME = false;
    bool KHR_MULTIVIEW_EXTENSION_NAME = false;
    bool KHR_GET_MEMORY_REQUIREMENTS_2_EXTENSION_NAME = false;
    bool KHR_DEDICATED_ALLOCATION_EXTENSION_NAME = false;
    bool KHR_CREATE_RENDERPASS_2_EXTENSION_NAME = false;
    bool KHR_get_physical_device_properties2 = false;
    bool KHR_DEPTH_STENCIL_RESOLVE_EXTENSION_NAME = false;
    bool EXT_SHADER_STENCIL_EXPORT_EXTENSION_NAME = false;
    bool EXT_swapchain_colorspace = false;
    bool ARM_RASTERIZATION_ORDER_ATTACHMENT_ACCESS_EXTENSION_NAME = false;
    bool EXT_FRAGMENT_SHADER_INTERLOCK_EXTENSION_NAME = false;
    bool KHR_PRESENT_ID_EXTENSION_NAME = false;
    bool KHR_PRESENT_WAIT_EXTENSION_NAME = false;
    bool GOOGLE_display_timing = false;
    bool KHR_PORTABILITY_SUBSET_EXTENSION_NAME = false;
    bool KHR_PUSH_DESCRIPTOR_EXTENSION_NAME = false;
    bool KHR_SAMPLER_YCBCR_CONVERSION_EXTENSION_NAME = false;
    bool EXT_DESCRIPTOR_BUFFER_EXTENSION_NAME = false;
    bool EXT_PHYSICAL_DEVICE_DRM_EXTENSION_NAME = false;
    bool EXT_SHADER_ATOMIC_FLOAT_EXTENSION_NAME = false;
    bool KHR_COOPERATIVE_MATRIX_EXTENSION_NAME = false;
    bool KHR_VIDEO_QUEUE_EXTENSION_NAME = false;
    bool KHR_VIDEO_DECODE_QUEUE_EXTENSION_NAME = false;
    bool KHR_VIDEO_DECODE_H264_EXTENSION_NAME = false;
    bool KHR_VIDEO_DECODE_H265_EXTENSION_NAME = false;
    bool KHR_VIDEO_DECODE_AV1_EXTENSION_NAME = false;
};

class VulkanDevice
{
public:
    using ptr = std::shared_ptr<VulkanDevice>;
public:
    VulkanDevice() = default;
    virtual ~VulkanDevice() = default;
public:
    static VulkanDevice::ptr Singleton();
public:
    virtual int32_t GetQueueFamilyIndex(VkQueueFlagBits queueFlag) = 0;
    virtual VkQueueFamilyProperties GetQueueFamilyProperties(VkQueueFlagBits queueFlag) = 0;
    virtual const VkPhysicalDeviceExternalMemoryHostPropertiesEXT& GetVkPhysicalDeviceExternalMemoryHostProperties() = 0;
    virtual const VkPhysicalDeviceCooperativeMatrixPropertiesKHR& GetVkPhysicalDeviceCooperativeMatrixProperties() = 0;
    virtual const VkPhysicalDeviceSubgroupSizeControlProperties& GetVkPhysicalDeviceSubgroupSizeControlProperties() = 0;
    virtual const VkPhysicalDeviceDescriptorBufferPropertiesEXT& GetVkPhysicalDeviceDescriptorBufferProperties() = 0;
    virtual const VkPhysicalDeviceDriverProperties& GetVkPhysicalDeviceDriverProperties() = 0;
    virtual const VkPhysicalDeviceMemoryProperties& GetVkPhysicalDeviceMemoryProperties() = 0;
    virtual const VkPhysicalDeviceProperties2& GetVkPhysicalDeviceProperties2() = 0;
    virtual const VkPhysicalDeviceShaderAtomicFloatFeaturesEXT& GetVkPhysicalDeviceShaderAtomicFloatFeatures() = 0;
    virtual const VkPhysicalDeviceFeatures2& GetVkPhysicalDeviceFeatures2() = 0;
    virtual const VulkanExtensions GetVulkanExtensions() = 0;
    virtual VkInstance GetInstance() = 0;
    virtual VmaAllocator GetVmaAllocator() = 0;
    virtual void* GetInstanceFunc(const std::string& name) = 0;
    virtual void* GetDeviceFunc(const std::string& name) = 0;
public: /* Initialization */
    virtual PFN_vkVoidFunction vkGetInstanceProcAddr(
        VkInstance instance,
        const char* pName
    ) = 0;
    virtual VkResult vkCreateInstance(
        const VkInstanceCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkInstance* pInstance
    ) = 0;
    virtual void vkDestroyInstance(
        VkInstance instance,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkEnumerateInstanceVersion(
        uint32_t* pApiVersion
    ) = 0;
public: /* Fundamentals */
    virtual PFN_vkVoidFunction vkGetDeviceProcAddr(
        const char* pName
    ) = 0;
public: /* Devices and Queues */
    virtual VkResult vkEnumeratePhysicalDevices(
        VkInstance instance,
        uint32_t* pPhysicalDeviceCount,
        VkPhysicalDevice* pPhysicalDevices
    ) = 0;
    virtual void vkGetPhysicalDeviceProperties(
        VkPhysicalDeviceProperties* pProperties
    ) = 0;
    virtual void vkGetPhysicalDeviceProperties2(
        VkPhysicalDeviceProperties2* pProperties
    ) = 0;
    virtual void vkGetPhysicalDeviceQueueFamilyProperties(
        uint32_t* pQueueFamilyPropertyCount,
        VkQueueFamilyProperties* pQueueFamilyProperties
    ) = 0;
    virtual void vkGetPhysicalDeviceQueueFamilyProperties2(
        uint32_t* pQueueFamilyPropertyCount,
        VkQueueFamilyProperties2* pQueueFamilyProperties
    ) = 0;
    virtual VkResult vkCreateDevice(
        VkPhysicalDevice physicalDevice,
        const VkDeviceCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDevice* pDevice
    ) = 0;
    virtual void vkDestroyDevice(
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual void vkGetDeviceQueue(
        uint32_t queueFamilyIndex,
        uint32_t queueIndex,
        VkQueue* pQueue
    ) = 0;
public: /* Command Buffers */
    virtual VkResult vkQueueSubmit(
        VkQueue queue,
        uint32_t submitCount,
        const VkSubmitInfo* pSubmits,
        VkFence fence
    ) = 0;
    virtual VkResult vkQueueSubmit2(
        VkQueue queue,
        uint32_t submitCount,
        const VkSubmitInfo2* pSubmits,
        VkFence fence
    ) = 0;
    virtual VkResult vkCreateCommandPool(
        const VkCommandPoolCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkCommandPool* pCommandPool
    ) = 0;
    virtual void vkDestroyCommandPool(
        VkCommandPool commandPool,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkResetCommandPool(
        VkCommandPool commandPool,
        VkCommandPoolResetFlags flags
    ) = 0;
    virtual VkResult vkAllocateCommandBuffers(
        const VkCommandBufferAllocateInfo* pAllocateInfo,
        VkCommandBuffer* pCommandBuffers
    ) = 0;
    virtual void vkFreeCommandBuffers(
        VkCommandPool commandPool,
        uint32_t commandBufferCount,
        const VkCommandBuffer* pCommandBuffers
    ) = 0;
    virtual VkResult vkBeginCommandBuffer(
        VkCommandBuffer commandBuffer,
        const VkCommandBufferBeginInfo* pBeginInfo
    ) = 0;
    virtual VkResult vkEndCommandBuffer(
        VkCommandBuffer commandBuffer
    ) = 0;
    virtual VkResult vkResetCommandBuffer(
        VkCommandBuffer commandBuffer,
        VkCommandBufferResetFlags flags
    ) = 0;
    virtual void vkCmdExecuteCommands(
        VkCommandBuffer commandBuffer,
        uint32_t commandBufferCount,
        const VkCommandBuffer* pCommandBuffers
    ) = 0;
public: /* Synchronization and Cache Control */
    virtual VkResult vkQueueWaitIdle(
        VkQueue queue
    ) = 0;
    virtual VkResult vkDeviceWaitIdle(
    ) = 0;
    virtual VkResult vkCreateFence(
        const VkFenceCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkFence* pFence
    ) = 0;
    virtual void vkDestroyFence(
        VkFence fence,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkResetFences(
        uint32_t fenceCount,
        const VkFence* pFences
    ) = 0;
    virtual VkResult vkGetFenceStatus(
        VkFence fence
    ) = 0;
    virtual VkResult vkWaitForFences(
        uint32_t fenceCount,
        const VkFence* pFences,
        VkBool32 waitAll,
        uint64_t timeout
    ) = 0;
    virtual VkResult vkCreateSemaphore(
        const VkSemaphoreCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkSemaphore* pSemaphore
    ) = 0;
    virtual void vkDestroySemaphore(
        VkSemaphore semaphore,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkWaitSemaphores(
        const VkSemaphoreWaitInfo* pWaitInfo,
        uint64_t timeout
    ) = 0;
    virtual VkResult vkCreateEvent(
        const VkEventCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkEvent* pEvent
    ) = 0;
    virtual void vkDestroyEvent(
        VkEvent event,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkGetEventStatus(
        VkEvent event
    ) = 0;
    virtual VkResult vkSetEvent(
        VkEvent event
    ) = 0;
    virtual VkResult vkResetEvent(
        VkEvent event
    ) = 0;
    virtual void vkCmdSetEvent(
        VkCommandBuffer commandBuffer,
        VkEvent event,
        VkPipelineStageFlags stageMask
    ) = 0;
    virtual void vkCmdResetEvent(
        VkCommandBuffer commandBuffer,
        VkEvent event,
        VkPipelineStageFlags stageMask
    ) = 0;
    virtual void vkCmdWaitEvents(
        VkCommandBuffer commandBuffer,
        uint32_t eventCount,
        const VkEvent* pEvents,
        VkPipelineStageFlags srcStageMask,
        VkPipelineStageFlags dstStageMask,
        uint32_t memoryBarrierCount,
        const VkMemoryBarrier* pMemoryBarriers,
        uint32_t bufferMemoryBarrierCount,
        const VkBufferMemoryBarrier* pBufferMemoryBarriers,
        uint32_t imageMemoryBarrierCount,
        const VkImageMemoryBarrier* pImageMemoryBarriers
    ) = 0;
public: /* Render Pass Commands */
    virtual void vkCmdBeginRenderPass(
        VkCommandBuffer commandBuffer,
        const VkRenderPassBeginInfo* pRenderPassBegin,
        VkSubpassContents contents
    ) = 0;
    virtual void vkCmdNextSubpass(
        VkCommandBuffer commandBuffer,
        VkSubpassContents contents
    ) = 0;
    virtual void vkCmdEndRenderPass(
        VkCommandBuffer commandBuffer
    ) = 0;
public: /* Shaders */
    virtual VkResult vkCreateShaderModule(
        const VkShaderModuleCreateInfo* pCreateInfo, 
        const VkAllocationCallbacks* pAllocator, 
        VkShaderModule* pShaderModule
    ) = 0;
    virtual void vkDestroyShaderModule(
        VkShaderModule shaderModule,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkGetPhysicalDeviceCooperativeMatrixPropertiesKHR(
        uint32_t* pPropertyCount,
        VkCooperativeMatrixPropertiesKHR* pProperties
    ) = 0;
public: /* Render Pass */
    virtual VkResult vkCreateFramebuffer(
        const VkFramebufferCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkFramebuffer* pFramebuffer
    ) = 0;
    virtual void vkDestroyFramebuffer(
        VkFramebuffer framebuffer,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkCreateRenderPass(
        const VkRenderPassCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkRenderPass* pRenderPass
    ) = 0;
    virtual void vkDestroyRenderPass(
        VkRenderPass renderPass,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual void vkGetRenderAreaGranularity(
        VkRenderPass renderPass,
        VkExtent2D* pGranularity
    ) = 0;
    virtual VkResult vkCreateRenderPass2KHR(
        const VkRenderPassCreateInfo2* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkRenderPass* pRenderPass
    ) = 0;
public: /* Pipelines */
    virtual VkResult vkCreatePipelineCache(
        const VkPipelineCacheCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkPipelineCache* pPipelineCache
    ) = 0;
    virtual void vkDestroyPipelineCache(
        VkPipelineCache pipelineCache,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkGetPipelineCacheData(
        VkPipelineCache pipelineCache,
        size_t* pDataSize,
        void* pData
    ) = 0;
    virtual VkResult vkMergePipelineCaches(
        VkPipelineCache dstCache,
        uint32_t srcCacheCount,
        const VkPipelineCache* pSrcCaches
    ) = 0;
    virtual VkResult vkCreateGraphicsPipelines(
        VkPipelineCache pipelineCache,
        uint32_t createInfoCount,
        const VkGraphicsPipelineCreateInfo* pCreateInfos,
        const VkAllocationCallbacks* pAllocator,
        VkPipeline* pPipelines
    ) = 0;
    virtual VkResult vkCreateComputePipelines(
        VkPipelineCache pipelineCache,
        uint32_t createInfoCount,
        const VkComputePipelineCreateInfo* pCreateInfos,
        const VkAllocationCallbacks* pAllocator,
        VkPipeline* pPipelines
    ) = 0;
    virtual void vkDestroyPipeline(
        VkPipeline pipeline,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual void vkCmdBindPipeline(
        VkCommandBuffer commandBuffer,
        VkPipelineBindPoint pipelineBindPoint,
        VkPipeline pipeline
    ) = 0;
    virtual void vkCmdPipelineBarrier(
        VkCommandBuffer commandBuffer,
        VkPipelineStageFlags srcStageMask,
        VkPipelineStageFlags dstStageMask,
        VkDependencyFlags dependencyFlags,
        uint32_t memoryBarrierCount,
        const VkMemoryBarrier* pMemoryBarriers,
        uint32_t bufferMemoryBarrierCount,
        const VkBufferMemoryBarrier* pBufferMemoryBarriers,
        uint32_t imageMemoryBarrierCount,
        const VkImageMemoryBarrier* pImageMemoryBarriers
    ) = 0;
public: /* Memory Allocation */
    virtual void vkGetPhysicalDeviceMemoryProperties(
        VkPhysicalDeviceMemoryProperties* pMemoryProperties
    ) = 0;
    virtual VkResult vkAllocateMemory(
        const VkMemoryAllocateInfo* pAllocateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDeviceMemory* pMemory
    ) = 0;
    virtual void vkFreeMemory(
        VkDeviceMemory memory,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkMapMemory(
        VkDeviceMemory memory,
        VkDeviceSize offset,
        VkDeviceSize size,
        VkMemoryMapFlags flags,
        void** ppData
    ) = 0;
    virtual void vkUnmapMemory(
        VkDeviceMemory memory
    ) = 0;
    virtual VkResult vkFlushMappedMemoryRanges(
        uint32_t memoryRangeCount,
        const VkMappedMemoryRange* pMemoryRanges
    ) = 0;
    virtual VkResult vkInvalidateMappedMemoryRanges(
        uint32_t memoryRangeCount,
        const VkMappedMemoryRange* pMemoryRanges
    ) = 0;
    virtual void vkGetDeviceMemoryCommitment(
        VkDeviceMemory memory,
        VkDeviceSize* pCommittedMemoryInBytes
    ) = 0;
    virtual VkResult vkCreateBuffer(
        const VkBufferCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkBuffer* pBuffer
    ) = 0;
    virtual void vkDestroyBuffer(
        VkBuffer buffer,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
public: /* Resource Creation */
    virtual VkResult vkBindBufferMemory(
        VkBuffer buffer,
        VkDeviceMemory memory,
        VkDeviceSize memoryOffset
    ) = 0;
    virtual VkResult vkBindBufferMemory2(
        uint32_t bindInfoCount,
        const VkBindBufferMemoryInfo* pBindInfos
    ) = 0;
    virtual VkResult vkBindImageMemory(
        VkImage image,
        VkDeviceMemory memory,
        VkDeviceSize memoryOffset
    ) = 0;
    virtual VkResult vkBindImageMemory2(
        uint32_t bindInfoCount,
        const VkBindImageMemoryInfo* pBindInfos
    ) = 0;
    virtual void vkGetBufferMemoryRequirements(
        VkBuffer buffer,
        VkMemoryRequirements* pMemoryRequirements
    ) = 0;
    virtual void vkGetBufferMemoryRequirements2(
        const VkBufferMemoryRequirementsInfo2* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    ) = 0;
    virtual void vkGetDeviceBufferMemoryRequirements(
        const VkDeviceBufferMemoryRequirements* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    ) = 0;
    virtual void vkGetImageMemoryRequirements(
        VkImage image,
        VkMemoryRequirements* pMemoryRequirements
    ) = 0;
    virtual void vkGetImageMemoryRequirements2(
        const VkImageMemoryRequirementsInfo2* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    ) = 0;
    virtual void vkGetDeviceImageMemoryRequirements(
        const VkDeviceImageMemoryRequirements* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    ) = 0;
    virtual VkResult vkCreateBufferView(
        const VkBufferViewCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkBufferView* pView
    ) = 0;
    virtual void vkDestroyBufferView(
        VkBufferView bufferView,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkCreateImage(
        const VkImageCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkImage* pImage
    ) = 0;
    virtual void vkDestroyImage(
        VkImage image,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual void vkGetImageSubresourceLayout(
        VkImage image,
        const VkImageSubresource* pSubresource,
        VkSubresourceLayout* pLayout
    ) = 0;
    virtual VkResult vkCreateImageView(
        const VkImageViewCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkImageView* pView
    ) = 0;
    virtual void vkDestroyImageView(
        VkImageView imageView,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual void vkGetBufferMemoryRequirements2KHR(
        const VkBufferMemoryRequirementsInfo2* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    ) = 0;
    virtual void vkGetImageMemoryRequirements2KHR(
        const VkImageMemoryRequirementsInfo2* pInfo,
        VkMemoryRequirements2* pMemoryRequirements
    ) = 0;
public: /* Samplers */
    virtual VkResult vkCreateSampler(
        const VkSamplerCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkSampler* pSampler
    ) = 0;
    virtual void vkDestroySampler(
        VkSampler sampler,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkCreateSamplerYcbcrConversion(
        const VkSamplerYcbcrConversionCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkSamplerYcbcrConversion* pYcbcrConversion
    ) = 0;
    virtual void vkDestroySamplerYcbcrConversion(
        VkSamplerYcbcrConversion ycbcrConversion,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
public: /* Resource Descriptors */
    virtual VkResult vkCreatePipelineLayout(
        const VkPipelineLayoutCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkPipelineLayout* pPipelineLayout
    ) = 0;
    virtual void vkDestroyPipelineLayout(
        VkPipelineLayout pipelineLayout,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkCreateDescriptorSetLayout(
        const VkDescriptorSetLayoutCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDescriptorSetLayout* pSetLayout
    ) = 0;
    virtual void vkDestroyDescriptorSetLayout(
        VkDescriptorSetLayout descriptorSetLayout,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkCreateDescriptorPool(
        const VkDescriptorPoolCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkDescriptorPool* pDescriptorPool
    ) = 0;
    virtual void vkDestroyDescriptorPool(
        VkDescriptorPool descriptorPool,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkResetDescriptorPool(
        VkDescriptorPool descriptorPool,
        VkDescriptorPoolResetFlags flags
    ) = 0;
    virtual VkResult vkAllocateDescriptorSets(
        const VkDescriptorSetAllocateInfo* pAllocateInfo,
        VkDescriptorSet* pDescriptorSets
    ) = 0;
    virtual VkResult vkFreeDescriptorSets(
        VkDescriptorPool descriptorPool,
        uint32_t descriptorSetCount,
        const VkDescriptorSet* pDescriptorSets
    ) = 0;
    virtual void vkCmdBindDescriptorSets(
        VkCommandBuffer commandBuffer,
        VkPipelineBindPoint pipelineBindPoint,
        VkPipelineLayout layout,
        uint32_t firstSet,
        uint32_t descriptorSetCount,
        const VkDescriptorSet* pDescriptorSets,
        uint32_t dynamicOffsetCount,
        const uint32_t* pDynamicOffsets
    ) = 0;
    virtual void vkCmdPushConstants(
        VkCommandBuffer commandBuffer,
        VkPipelineLayout layout,
        VkShaderStageFlags stageFlags,
        uint32_t offset,
        uint32_t size,
        const void* pValues
    ) = 0;
public: /* Queries */
    virtual VkResult vkCreateQueryPool(
        const VkQueryPoolCreateInfo* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkQueryPool* pQueryPool
    ) = 0;
    virtual void vkDestroyQueryPool(
        VkQueryPool queryPool,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkGetQueryPoolResults(
        VkQueryPool queryPool,
        uint32_t firstQuery,
        uint32_t queryCount,
        size_t dataSize,
        void* pData,
        VkDeviceSize stride,
        VkQueryResultFlags flags
    ) = 0;
    virtual void vkCmdBeginQuery(
        VkCommandBuffer commandBuffer,
        VkQueryPool queryPool,
        uint32_t query,
        VkQueryControlFlags flags
    ) = 0;
    virtual void vkCmdEndQuery(
        VkCommandBuffer commandBuffer,
        VkQueryPool queryPool,
        uint32_t query
    ) = 0;
    virtual void vkCmdResetQueryPool(
        VkCommandBuffer commandBuffer,
        VkQueryPool queryPool,
        uint32_t firstQuery,
        uint32_t queryCount
    ) = 0;
    virtual void vkCmdWriteTimestamp(
        VkCommandBuffer commandBuffer,
        VkPipelineStageFlagBits pipelineStage,
        VkQueryPool queryPool,
        uint32_t query
    ) = 0;
    virtual void vkCmdCopyQueryPoolResults(
        VkCommandBuffer commandBuffer,
        VkQueryPool queryPool,
        uint32_t firstQuery,
        uint32_t queryCount,
        VkBuffer dstBuffer,
        VkDeviceSize dstOffset,
        VkDeviceSize stride,
        VkQueryResultFlags flags
    ) = 0;
public: /* Clear Commands */
    virtual void vkCmdUpdateBuffer(
        VkCommandBuffer commandBuffer,
        VkBuffer dstBuffer,
        VkDeviceSize dstOffset,
        VkDeviceSize dataSize,
        const void* pData
    ) = 0;
    virtual void vkCmdFillBuffer(
        VkCommandBuffer commandBuffer,
        VkBuffer dstBuffer,
        VkDeviceSize dstOffset,
        VkDeviceSize size,
        uint32_t data
    ) = 0;
    virtual void vkCmdClearColorImage(
        VkCommandBuffer commandBuffer,
        VkImage image,
        VkImageLayout imageLayout,
        const VkClearColorValue* pColor,
        uint32_t rangeCount,
        const VkImageSubresourceRange* pRanges
    ) = 0;
    virtual void vkCmdClearDepthStencilImage(
        VkCommandBuffer commandBuffer,
        VkImage image,
        VkImageLayout imageLayout,
        const VkClearDepthStencilValue* pDepthStencil,
        uint32_t rangeCount,
        const VkImageSubresourceRange* pRanges
    ) = 0;
    virtual void vkCmdClearAttachments(
        VkCommandBuffer commandBuffer,
        uint32_t attachmentCount,
        const VkClearAttachment* pAttachments,
        uint32_t rectCount,
        const VkClearRect* pRects
    ) = 0;
public: /* Copy Commands */
    virtual void vkCmdCopyBuffer(
        VkCommandBuffer commandBuffer,
        VkBuffer srcBuffer,
        VkBuffer dstBuffer,
        uint32_t regionCount,
        const VkBufferCopy* pRegions
    ) = 0;
    virtual void vkCmdCopyImage(
        VkCommandBuffer commandBuffer,
        VkImage srcImage,
        VkImageLayout srcImageLayout,
        VkImage dstImage,
        VkImageLayout dstImageLayout,
        uint32_t regionCount,
        const VkImageCopy* pRegions
    ) = 0;
    virtual void vkCmdBlitImage(
        VkCommandBuffer commandBuffer,
        VkImage srcImage,
        VkImageLayout srcImageLayout,
        VkImage dstImage,
        VkImageLayout dstImageLayout,
        uint32_t regionCount,
        const VkImageBlit* pRegions,
        VkFilter filter
    ) = 0;
    virtual void vkCmdCopyImageToBuffer(
        VkCommandBuffer commandBuffer,
        VkImage srcImage,
        VkImageLayout srcImageLayout,
        VkBuffer dstBuffer,
        uint32_t regionCount,
        const VkBufferImageCopy* pRegions
    ) = 0;
    virtual void vkCmdResolveImage(
        VkCommandBuffer commandBuffer,
        VkImage srcImage,
        VkImageLayout srcImageLayout,
        VkImage dstImage,
        VkImageLayout dstImageLayout,
        uint32_t regionCount,
        const VkImageResolve* pRegions
    ) = 0;
    virtual void vkCmdCopyBufferToImage(
        VkCommandBuffer commandBuffer,
         VkBuffer srcBuffer,
         VkImage dstImage,
         VkImageLayout dstImageLayout,
         uint32_t regionCount,
         const VkBufferImageCopy* pRegions
    ) = 0;
public: /* Drawing Commands */
    virtual void vkCmdBindIndexBuffer(
        VkCommandBuffer commandBuffer,
        VkBuffer buffer,
        VkDeviceSize offset,
        VkIndexType indexType
    ) = 0;
    virtual void vkCmdBindVertexBuffers(
        VkCommandBuffer commandBuffer,
        uint32_t firstBinding,
        uint32_t bindingCount,
        const VkBuffer* pBuffers,
        const VkDeviceSize* pOffsets
    ) = 0;
    virtual void vkCmdDraw(
        VkCommandBuffer commandBuffer,
        uint32_t vertexCount,
        uint32_t instanceCount,
        uint32_t firstVertex,
        uint32_t firstInstance
    ) = 0;
    virtual void vkCmdDrawIndexed(
        VkCommandBuffer commandBuffer,
        uint32_t indexCount,
        uint32_t instanceCount,
        uint32_t firstIndex,
        int32_t vertexOffset,
        uint32_t firstInstance
    ) = 0;
    virtual void vkCmdDrawIndirect(
        VkCommandBuffer commandBuffer,
        VkBuffer buffer,
        VkDeviceSize offset,
        uint32_t drawCount,
        uint32_t stride
    ) = 0;
    virtual void vkCmdDrawIndexedIndirect(
        VkCommandBuffer commandBuffer,
        VkBuffer buffer,
        VkDeviceSize offset,
        uint32_t drawCount,
        uint32_t stride
    ) = 0;
public: /* Fixed-Function Vertex Post Processing */
    virtual void vkCmdSetViewport(
        VkCommandBuffer commandBuffer,
        uint32_t firstViewport,
        uint32_t viewportCount,
        const VkViewport* pViewports
    ) = 0;
public: /* Rasterization */
    virtual void vkCmdSetLineWidth(
        VkCommandBuffer commandBuffer,
        float lineWidth
    ) = 0;
    virtual void vkCmdSetDepthBias(
        VkCommandBuffer commandBuffer,
        float depthBiasConstantFactor,
        float depthBiasClamp,
        float depthBiasSlopeFactor
    ) = 0;
public: /* Fragment Operations */
    virtual void vkCmdSetScissor(
        VkCommandBuffer commandBuffer,
        uint32_t firstScissor,
        uint32_t scissorCount,
        const VkRect2D* pScissors
    ) = 0;
public: /* The Framebuffer */
    virtual void vkCmdSetBlendConstants(
        VkCommandBuffer commandBuffer,
        const float blendConstants[4]
    ) = 0;
    virtual void vkCmdSetDepthBounds(
        VkCommandBuffer commandBuffer,
        float minDepthBounds,
        float maxDepthBounds
    ) = 0;
    virtual void vkCmdSetStencilCompareMask(
        VkCommandBuffer commandBuffer,
        VkStencilFaceFlags faceMask,
        uint32_t compareMask
    ) = 0;
    virtual void vkCmdSetStencilWriteMask(
        VkCommandBuffer commandBuffer,
        VkStencilFaceFlags faceMask,
        uint32_t writeMask
    ) = 0;
    virtual void vkCmdSetStencilReference(
        VkCommandBuffer commandBuffer,
        VkStencilFaceFlags faceMask,
        uint32_t reference
    ) = 0;
public: /* Dispatching Commands */
    virtual void vkCmdDispatch(
        VkCommandBuffer commandBuffer,
        uint32_t groupCountX,
        uint32_t groupCountY,
        uint32_t groupCountZ
    ) = 0;
    virtual void vkCmdDispatchIndirect(
        VkCommandBuffer commandBuffer,
        VkBuffer buffer,
        VkDeviceSize offset
    ) = 0;
public: /* Sparse Resources */
    virtual VkResult vkQueueBindSparse(
        VkQueue queue,
        uint32_t bindInfoCount,
        const VkBindSparseInfo* pBindInfo,
        VkFence fence
    ) = 0;
public: /* Window System Integration (WSI) */
    virtual VkResult vkGetPhysicalDeviceSurfaceFormatsKHR(
        VkSurfaceKHR surface,
        uint32_t* pSurfaceFormatCount,
        VkSurfaceFormatKHR* pSurfaceFormats
    ) = 0;
    virtual void vkDestroySurfaceKHR(
        VkSurfaceKHR surface,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkCreateSwapchainKHR(
        const VkSwapchainCreateInfoKHR* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkSwapchainKHR* pSwapchain
    ) = 0;
    virtual void vkDestroySwapchainKHR(
        VkSwapchainKHR swapchain,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkGetSwapchainImagesKHR(
        VkSwapchainKHR swapchain,
        uint32_t* pSwapchainImageCount,
        VkImage* pSwapchainImages
    ) = 0;
    virtual VkResult vkAcquireNextImageKHR(
        VkSwapchainKHR swapchain,
        uint64_t timeout,
        VkSemaphore semaphore,
        VkFence fence,
        uint32_t* pImageIndex
    ) = 0;
    virtual VkResult vkQueuePresentKHR(
        VkQueue queue,
        const VkPresentInfoKHR* pPresentInfo
    ) = 0;
    virtual VkResult vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
        VkSurfaceKHR surface,
        VkSurfaceCapabilitiesKHR* pSurfaceCapabilities
    ) = 0;
    virtual VkResult vkGetPhysicalDeviceSurfacePresentModesKHR(
        VkSurfaceKHR surface,
        uint32_t* pPresentModeCount,
        VkPresentModeKHR* pPresentModes
    ) = 0;
public: /* Deferred Host Operations */
    virtual VkResult vkWaitForPresentKHR(
        VkSwapchainKHR swapchain,
        uint64_t presentId,
        uint64_t timeout
    ) = 0;
public: /* Features */
    virtual void vkGetPhysicalDeviceFeatures(
        VkPhysicalDeviceFeatures* pFeatures
    ) = 0;
    virtual void vkGetPhysicalDeviceFeatures2(
        VkPhysicalDeviceFeatures2* pFeatures
    ) = 0;
    virtual void vkGetPhysicalDeviceFormatProperties(
        VkFormat format,
        VkFormatProperties* pFormatProperties
    ) = 0;
public: /* Additional Capabilities */
    virtual VkResult vkGetPhysicalDeviceImageFormatProperties(
        VkPhysicalDevice physicalDevice,
        VkFormat format,
        VkImageType type,
        VkImageTiling tiling,
        VkImageUsageFlags usage,
        VkImageCreateFlags flags,
        VkImageFormatProperties* pImageFormatProperties
    ) = 0;
public: /* Extending Vulkan */
    virtual VkResult vkEnumerateInstanceExtensionProperties(
        const char* pLayerName,
        uint32_t* pPropertyCount,
        VkExtensionProperties* pProperties
    ) = 0;
    virtual VkResult vkEnumerateDeviceExtensionProperties(
        const char* pLayerName,
        uint32_t* pPropertyCount,
        VkExtensionProperties* pProperties
    ) = 0;
    virtual VkResult vkEnumerateInstanceLayerProperties(
        uint32_t* pPropertyCount,
        VkLayerProperties* pProperties
    ) = 0;
    virtual VkResult vkEnumerateDeviceLayerProperties(
        VkPhysicalDevice physicalDevice,
        uint32_t* pPropertyCount,
        VkLayerProperties* pProperties
    ) = 0;
public: /* Video Coding */
    virtual VkResult vkCreateVideoSessionKHR(
        const VkVideoSessionCreateInfoKHR* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkVideoSessionKHR* pVideoSession
    ) = 0;
    virtual void vkDestroyVideoSessionKHR(
        VkVideoSessionKHR videoSession,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
    virtual VkResult vkGetVideoSessionMemoryRequirementsKHR(
        VkVideoSessionKHR videoSession,
        uint32_t* pMemoryRequirementsCount,
        VkVideoSessionMemoryRequirementsKHR* pMemoryRequirements
    ) = 0;
    virtual VkResult vkBindVideoSessionMemoryKHR(
        VkVideoSessionKHR videoSession,
        uint32_t bindSessionMemoryInfoCount,
        const VkBindVideoSessionMemoryInfoKHR* pBindSessionMemoryInfos
    ) = 0;
    virtual void vkCmdBeginVideoCodingKHR(
        VkCommandBuffer commandBuffer,
        const VkVideoBeginCodingInfoKHR* pBeginInfo
    ) = 0;
    virtual void vkCmdControlVideoCodingKHR(
        VkCommandBuffer commandBuffer,
        const VkVideoCodingControlInfoKHR* pCodingControlInfo
    ) = 0;
    virtual void vkCmdDecodeVideoKHR(
        VkCommandBuffer commandBuffer,
        const VkVideoDecodeInfoKHR* pDecodeInfo
    ) = 0;
    virtual void vkCmdEndVideoCodingKHR(
        VkCommandBuffer commandBuffer,
        const VkVideoEndCodingInfoKHR* pEndCodingInfo
    ) = 0;
    virtual VkResult vkGetPhysicalDeviceVideoCapabilitiesKHR(
        const VkVideoProfileInfoKHR* pVideoProfile,
        VkVideoCapabilitiesKHR* pCapabilities
    ) = 0;
    virtual VkResult vkGetPhysicalDeviceVideoFormatPropertiesKHR(
        const VkPhysicalDeviceVideoFormatInfoKHR* pVideoFormatInfo,
        uint32_t* pVideoFormatPropertyCount,
        VkVideoFormatPropertiesKHR* pVideoFormatProperties
    ) = 0;
    virtual VkResult vkCreateVideoSessionParametersKHR(
        const VkVideoSessionParametersCreateInfoKHR* pCreateInfo,
        const VkAllocationCallbacks* pAllocator,
        VkVideoSessionParametersKHR* pVideoSessionParameters
    ) = 0;
    virtual void vkDestroyVideoSessionParametersKHR(
        VkVideoSessionParametersKHR videoSessionParameters,
        const VkAllocationCallbacks* pAllocator
    ) = 0;
public: /* Formats */
    virtual void vkGetPhysicalDeviceFormatProperties2(
        VkFormat format,
        VkFormatProperties2* pFormatProperties
    ) = 0;
    virtual VkResult vkGetPhysicalDeviceImageFormatProperties2(
        const VkPhysicalDeviceImageFormatInfo2* pImageFormatInfo,
        VkImageFormatProperties2* pImageFormatProperties
    ) = 0;
};

std::string VkResultToStr(VkResult res);

} // namespace Mmp

#endif